<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Format Helpers
 *
 *
 * @package	    Helpers
 * @subpackage	
 * @category	Helpers
 * 
 * @version     1.0 Build 22.08.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------

/**
 * Currency
 * @access	public
 * @return	string
 */
if ( ! function_exists('currency'))
{
	function currency($number, $symbol = TRUE)
	{
        $money = number_format($number,2,".",",");
        $number = number_format($number,0,",",".");
        $result = (($symbol) ? $money : $number );
        return $result;
	}
}

/**
 * Sapaan
 * @access	public
 * @return	string
 */
if ( ! function_exists('sapaan'))
{
	function sapaan($gender, $age, $status)
	{
        if($gender == PEREMPUAN && $age == 30 && $status == 176){
            $result = "Ny.";
        }elseif($gender == LAKI_LAKI && $age == 30 && $status == 176){
            $result = "Tn.";
        }
        print_out(array($gender,$age,$status,$result));
	}
}

/**
 * Age Range
 * @access	public
 * @return	integer
 */
if ( ! function_exists('age_range'))
{
	function age_range($date)
	{
        $time = strtotime($date);
        $age = get_age($time);
        $year = substr($age,'0','2');
        if($year <= 5){
            $result = 29;   
        }elseif($year <= 15){
            $result = 317;   
        }elseif($year <= 60){
            $result = 30;   
        }else{
            $result = 31;   
        }
        return $result;
	}
}

/* End of file formate_helper.php */
/* Location: ./site/helpers/formate_helper.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcomes extends BC_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$info = new stdClass();
		$info->msg = "This is Webservice, API v1.0";
		$info->errorcode = 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	function getModule()
	{
		$output = modules_mapping();
		$this->output->set_content_type('application/json')->set_output(json_encode($output)); 
	}

	function getController()
	{
		$id = $_POST['id'];
		$module = $_POST['module'];
		$controller = $_POST['controller'];
	    controllers_mapping($id,$module,$controller);
	}

	function getDocNO()
	{
		$no = $_GET['type'];
		$res = DocNo($no);
		$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	function useredit()
	{
		$data = array(
			'id' => 2,
			'password' => '12345',
			'email' => 'arief',
			'user_role_id' => 1,
			'access' => 'a:1:{i:0;s:1:"1";}'
		);
		$this->ezrbac->updateUser($data);

		// $data = array(
		// 	'password' => '123456',
		// 	'email' => 'arief',
		// 	'user_role_id' => 1
		// );
		// $this->ezrbac->createUser($data);

	}

	function readDLL()
	{
		phpinfo();
		die;
// 		$word = new COM("word.application") or die("Unable to instantiate Word");
// echo "Loaded Word, version {$word->Version}\n";
// die;
		try {
			$var = new COM('test.dll');
			print_out($var);
		} catch (com_exception $e) {
		//couldn't load, do something else like register
			exec('regsvr32 /s test.dll');
			print_out('fail');
		}
		//$my_dll = new COM('Class1.Functions');
		print_out($var);

		$hasil = $var->tambah(1, 2);
		print_out($hasil);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

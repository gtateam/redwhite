<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends BC_Controller {

	public function index()
	{
        $this->load->model('test_model');
        
        $group = new Entity\UserGroup;
        $group->setName('Users');

        $user = new Entity\User;
        $user->setUsername('wildlyinaccurate');
        $user->setPassword('Passw0rd');
        $user->setEmail('wildlyinaccurate@gmail.com');
        $user->setGroup($group);
        
        // When you have set up your database, you can persist these entities:
//        $em = $this->doctrine->em;
//        $em->persist($group);
//        $em->persist($user);
//        $em->flush();
        
		print_out($user);
	}
    
    public function getdata()
    {
        $id=1;
        $em = $this->doctrine->em;
        $users = $em->getRepository('Entity\User')->findOneBy(array('username' => 'wildlyinaccurate'));
        
        echo "<pre>"; print_r($this->accessProtected($users,'email')); die;
    }
    
    function accessProtected($obj, $prop) {
      $reflection = new ReflectionClass($obj);
      $property = $reflection->getProperty($prop);
      $property->setAccessible(true);
      return $property->getValue($obj);
    }
}

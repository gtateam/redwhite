<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Account Component
 *
 *
 * @package	    Apps
 * @subpackage	Component
 * @category	Component
 * 
 * @version     1.1 Build 15.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Speedwayconnect extends BC_Controller {

	function __construct()
    {
    	parent::__construct();
	}

	public function index()
	{
		print_out("This web speedwayconnect.");
	}

	public function scpost(){
		# tester without Database #
	   	// $fn = "D://log.txt";
	   	// $fp = fopen($fn, "a");
	   	// $rawPostData = file_get_contents('php://input');
	   	// fwrite($fp, date("l F d, Y, h:i A") . "," . $rawPostData . "\n"); 
	   	// fclose($fp);
	
	//    // // Store the POST variables.
	   $readerName = $_POST["reader_name"];
	   $macAddress = $_POST["mac_address"];
	   $lineEnding = $_POST["line_ending"];
	   $fieldDelim = $_POST["field_delim"];
	   $fieldNames = $_POST["field_names"];
	   $fieldValues = $_POST["field_values"];

	 // Replace the field delimiter with a comma.
	   str_replace($fieldDelim, ",", $fieldNames);
	      
     // Break the field values up into rows.
	   $rows = explode("\n", $fieldValues);

	 // Remove the last row. It's always blank
	   if (sizeof($rows)) array_pop($rows);
	   
	   $fieldNames = "reader_name,mac_address," . $fieldNames;
		   // $totalEPC = count($rows);
		   // fwrite($fp, $totalEPC . "\n"); 
	   	//    fclose($fp);

		   foreach ($rows as $row)
		   {
		   		$row = $readerName . "," . $macAddress . "," . $row;
		   	  	$val = explode(",", $row);
	   			$EPC = str_replace('"', '', $val['3']);			
	   			$ReaderID = str_replace('"', '', $val['0']);
	   			$check = substr($EPC, 3,1);
	   			if ($check == 4) {
	   				$NoEPC = substr($EPC, 0, 12);
	   				// fwrite($fp, $EPC . "\n"); 
		   	  // 	    fclose($fp);
	   				ScInsertSS($NoEPC, $ReaderID, $EPC);
	   			}else{
	   				// fwrite($fp, $NoEPC . "\n"); 
		   	  // 	    fclose($fp);
	   				ScInsert($EPC, $ReaderID);
	   			}		   
			}                       
		}
}

/* End of file speedwayconnect.php */
/* Location: ./apps/component/speedwayconnect.php */
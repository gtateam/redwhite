<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('T_ITEM')   OR define('T_ITEM', 't8010');
defined('T_ITEM_RecordID')   OR define('T_ITEM_RecordID', 't8010r001');
defined('T_ITEM_RecordTimeStamp')   OR define('T_ITEM_RecordTimeStamp', 't8010r002');
defined('T_ITEM_RecordStatus')   OR define('T_ITEM_RecordStatus', 't8010r003');
defined('T_ITEM_UpdateAt')   OR define('T_ITEM_UpdateAt', 't8010r004');
defined('T_ITEM_UpdateBy')   OR define('T_ITEM_UpdateBy', 't8010r005');
defined('T_ITEM_UpdateOn')   OR define('T_ITEM_UpdateOn', 't8010r006');
defined('T_ITEM_ID')   OR define('T_ITEM_ID', 't8010f001');
defined('T_ITEM_Name')   OR define('T_ITEM_Name', 't8010f002');
defined('T_ITEM_Type')   OR define('T_ITEM_Type', 't8010f003');
defined('T_ITEM_UOM')   OR define('T_ITEM_UOM', 't8010f004');
defined('T_ITEM_HSCODE')   OR define('T_ITEM_HSCODE', 't8010f005');


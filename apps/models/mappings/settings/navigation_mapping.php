<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('T_NAVIGATION')   OR define('T_NAVIGATION', 't1000');
defined('T_NAVIGATION_RecordID')   OR define('T_NAVIGATION_RecordID', 't1000r001');
defined('T_NAVIGATION_RecordTimeStamp')   OR define('T_NAVIGATION_RecordTimeStamp', 't1000r002');
defined('T_NAVIGATION_RecordStatus')   OR define('T_NAVIGATION_RecordStatus', 't1000r003');
defined('T_NAVIGATION_UpdateAt')   OR define('T_NAVIGATION_UpdateAt', 't1000r004');
defined('T_NAVIGATION_UpdateBy')   OR define('T_NAVIGATION_UpdateBy', 't1000r005');
defined('T_NAVIGATION_UpdateOn')   OR define('T_NAVIGATION_UpdateOn', 't1000r006');
defined('T_NAVIGATION_ID')   OR define('T_NAVIGATION_ID', 't1000f001');
defined('T_NAVIGATION_Alias')   OR define('T_NAVIGATION_Alias', 't1000f002');
defined('T_NAVIGATION_Title')   OR define('T_NAVIGATION_Title', 't1000f003');
defined('T_NAVIGATION_Description')   OR define('T_NAVIGATION_Description', 't1000f004');
defined('T_NAVIGATION_Icon')   OR define('T_NAVIGATION_Icon', 't1000f005');
defined('T_NAVIGATION_Access')   OR define('T_NAVIGATION_Access', 't1000f006');
defined('T_NAVIGATION_CreatedUser')   OR define('T_NAVIGATION_CreatedUser', 't1000f007');
defined('T_NAVIGATION_CreatedDate')   OR define('T_NAVIGATION_CreatedDate', 't1000f008');
defined('T_NAVIGATION_ModifiedUser')   OR define('T_NAVIGATION_ModifiedUser', 't1000f009');
defined('T_NAVIGATION_ModifiedDate')   OR define('T_NAVIGATION_ModifiedDate', 't1000f010');
defined('T_NAVIGATION_Js')   OR define('T_NAVIGATION_Js', 't1000f011');
defined('T_NAVIGATION_Depth')   OR define('T_NAVIGATION_Depth', 't1000f012');
defined('T_NAVIGATION_Ordering')   OR define('T_NAVIGATION_Ordering', 't1000f013');
defined('T_NAVIGATION_Publish')   OR define('T_NAVIGATION_Publish', 't1000f014');
defined('T_NAVIGATION_Type')   OR define('T_NAVIGATION_Type', 't1000f015');


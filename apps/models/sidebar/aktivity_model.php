<?php
class Aktivity_Model extends CI_Model
{

    public function __construct(){
        parent::__construct();
	}
    public function get_list()
    {
        $ret = array();
        $this->db->order_by('create_datetime','DESC');
		$query= $this->db->get('activity_user');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $data[$item->id] = array(
                    'message' => $item->message,
                    'kategori' => $item->activity_kategori_id,
                    'jenis' => $item->activity_jenis_id,
                    'object_id' => $item->object_id,
                    'created' => $this->user($item->create_user),
                    'created_date' => '<abbr class="timeago" title="'.date("m d Y g:i:s",$item->create_datetime).'">'.date("d M Y g:i:s",$item->create_datetime).'</abbr>'
                );
            endforeach;
            return $data;
        endif;
		return $ret;
	}

    private function user($id)
    {
        $this->db->where('user_id',$id);
        $query = $this->db->get('user_meta');
        $result = $query->result();
        $data = $result[0]->first_name." ".$result[0]->last_name;
        return $data;
    }
}

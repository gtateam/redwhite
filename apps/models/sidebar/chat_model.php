<?php
class Chat_Model extends CI_Model
{

    public function __construct(){
        parent::__construct();
	}
    public function data_user($id)
    {
        $this->db->where('id',$id);
		$query= $this->db->get('system_users');
        $result = $query->result();
		return $result;
	}
	public function get_users($id)
    {
        $this->db->where_not_in('id',$id);
		$query = $this->db->get('system_users');
        $result = $query->result();
		return $result;
	}
}

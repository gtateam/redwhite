<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Form Label Lang
 *
 *
 * @package	    Site
 * @subpackage	Language
 * @category	english language
 * 
 * @version     1.1 Build 05.10.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------

// form label
$lang['form_label_alias']				= 'Alias';
$lang['form_label_title']				= 'Title';
$lang['form_label_description']			= 'Description';
$lang['form_label_icon']				= 'Icon';
$lang['form_label_addmenu']				= 'Form add new menu';
$lang['form_label_type']				= 'Type';
$lang['form_label_ordering']			= 'Ordering';
$lang['form_label_location']			= 'Location';
$lang['form_label_access']				= 'Access';
$lang['register']				        = 'Registration';
$lang['my_profile']				        = 'My Profile';
$lang['logout']				            = 'Log Out';
$lang['emergency_patient']				= 'Emergency Patient';
$lang['outpatient']				        = 'Outpatient';
$lang['hospitalization_patient']		= 'Hospitalization Patient';
$lang['medical_support']		        = 'Medical Support';
$lang['view_more']		                = 'View More';
$lang['all_monthly_visit']		        = 'Monthly Patient visit';
$lang['underconstruction']		        = 'Underconstruction';
$lang['doctor_schedule']		        = 'Doctor Schedule';
$lang['label_date']		                = 'Date';
$lang['label_norm']		                = 'No.RM';
$lang['label_name']		                = 'Name';
$lang['label_first_name']		        = 'First Name';
$lang['label_last_name']		        = 'Last Name';
$lang['label_identity']		            = 'Identity';
$lang['label_identity_number']		    = 'Identity Number';
$lang['label_gender']		            = 'Gender';
$lang['label_male']		                = 'Male';
$lang['label_female']		            = 'Female';
$lang['label_born']		                = 'Born';
$lang['label_birthcity']		        = 'Birth City';
$lang['label_birthdate']		        = 'Birth Date';
$lang['label_marital_status']		    = 'Marital Status';
$lang['label_religion']		            = 'Religion';
$lang['label_employ']		            = 'Employ';
$lang['label_address']		            = 'Address';
$lang['label_province']		            = 'Province';
$lang['label_district_city']            = 'District / City';
$lang['label_subdistric']               = 'Subdistric';
$lang['label_blood_type']               = 'Blood Type';
$lang['label_save']                     = 'Save';
$lang['label_cancel']                   = 'Cancel';
$lang['label_new_patient']              = 'New Patient';
$lang['label_old_patient']              = 'Old Patient';
$lang['label_patient_data']             = 'Patient Data';
$lang['label_responsible_person']       = 'Responsible Person';
$lang['label_visit']                    = 'Visit';
$lang['label_php_error']                = 'A PHP Error was encountered';
$lang['label_add_new']                  = 'Add New';
$lang['label_user_name']				= 'User Name';
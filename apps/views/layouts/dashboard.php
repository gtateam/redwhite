<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling header-dark sidebar-left-sm no-mobile-device custom-scroll js flexbox flexboxlegacy no-touch csstransforms csstransforms3d no-overflowscrolling k-ff k-ff44">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $template['title'] ?></title>
		<base href="<?php echo base_url(); ?>" />
		<meta name="keywords" content="" />
		<meta name="description" content="BluesCode">
		<meta name="author" content="Muhammad Arief">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<link rel="icon" href="assets/backend/images/fav-ico.png" />
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/backend/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/backend/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="assets/backend/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/backend/vendor/modernizr/modernizr.js"></script>

        <!-- Vendor -->
		<script src="assets/backend/vendor/jquery/jquery.js"></script>
		<script src="assets/backend/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/backend/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/backend/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/backend/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/backend/vendor/jquery-placeholder/jquery.placeholder.js"></script>
	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<?php $this->navigations->header(); ?>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<?php $this->navigations->generate(); ?>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<?php $this->navigations->breadcrumb(); ?>

					<!-- start: page -->
					<?php echo $template['content']; ?>
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="assets/backend/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>


		
		<!-- Specific Page Vendor -->
		<script src="assets/backend/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="assets/backend/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="assets/backend/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="assets/backend/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="assets/backend/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="assets/backend/vendor/flot/jquery.flot.js"></script>
		<script src="assets/backend/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="assets/backend/vendor/flot/jquery.flot.pie.js"></script>
		<script src="assets/backend/vendor/flot/jquery.flot.categories.js"></script>
		<script src="assets/backend/vendor/flot/jquery.flot.resize.js"></script>
		<script src="assets/backend/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="assets/backend/vendor/raphael/raphael.js"></script>
		<script src="assets/backend/vendor/morris/morris.js"></script>
		<script src="assets/backend/vendor/gauge/gauge.js"></script>
		<script src="assets/backend/vendor/snap-svg/snap.svg.js"></script>
		<script src="assets/backend/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="assets/backend/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="assets/backend/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="assets/backend/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/backend/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/backend/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/backend/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<!--<script src="assets/backend/javascripts/dashboard/examples.dashboard.js"></script>-->
	</body>
</html>

<!doctype html>
<html class="fixed js flexbox flexboxlegacy csstransforms csstransforms3d no-overflowscrolling header-dark sidebar-left-sm no-mobile-device custom-scroll js flexbox flexboxlegacy no-touch csstransforms csstransforms3d no-overflowscrolling k-ff k-ff44">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $template['title'] ?></title>
		<base href="<?php echo base_url(); ?>" />
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="BluesCode">
		<meta name="author" content="">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<link rel="icon" href="assets/backend/images/fav-ico.png" />
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/backend/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/backend/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/backend/vendor/bootstrap-datepicker/css/datepicker3.css" />
		<link rel="stylesheet" href="assets/backend/vendor/jquery-datatables-bs3/assets/css/datatables.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/backend/vendor/pnotify/pnotify.custom.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/backend/stylesheets/theme-custom.css">

		<link href="assets/Content/kendo/2015.2.720/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/Content/kendo/2015.2.720/kendo.rtl.min.css" rel="stylesheet" type="text/css" />
	    <link href="assets/Content/kendo/2015.2.720/kendo.material.mobile.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/Content/kendo/2015.2.720/kendo.material.min.css" rel="stylesheet" type="text/css" />

		<!-- Head Libs -->
		<script src="assets/backend/vendor/modernizr/modernizr.js"></script>
		<script src="assets/backend/vendor/jquery/jquery.js"></script>
		<script type="text/javascript">
			function site_url(url){
				var site_url = "<?php echo site_url('"+url+"'); ?>";
				return site_url;
			}
			$(document).ready(function() {
				$( document ).ajaxError(function(event, jqxhr, settings, thrownError) {
					new PNotify({ title: "Failed", text: jqxhr.responseText, type: 'error', shadow: true });
				});
			});
		</script>
	</head>
	<body>
		<section class="body">
			<?php echo $template['content']; ?>
		</section>

		<!-- Vendor -->
		
		<script src="assets/backend/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/backend/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/backend/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/backend/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/backend/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		<script src="assets/backend/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
		<script src="assets/backend/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
		<script src="assets/backend/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/backend/javascripts/tables/examples.datatables.default.js"></script>
		<script src="assets/backend/javascripts/tables/examples.datatables.row.with.details.js"></script>
		<script src="assets/backend/javascripts/tables/examples.datatables.tabletools.js"></script>
		<script src="assets/backend/vendor/pnotify/pnotify.custom.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/backend/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/backend/javascripts/theme.custom.js"></script>
		<script src="assets/backend/javascripts/ui-elements/examples.modals.js"></script>
		<script src="assets/Scripts/kendo/2015.2.720/jszip.min.js"></script>

		<!-- Theme Initialization Files -->
		<script src="assets/backend/javascripts/theme.init.js"></script>
		<script src="assets/Scripts/kendo/2015.2.720/kendo.all.min.js"></script>

		<!-- Examples -->
		<script src="assets/backend/javascripts/ui-elements/examples.notifications.js"></script>
		<script src="assets/backend/javascripts/forms/gridview.lib.js"></script>
        <script type="text/javascript">
			function goBack() {
			    window.history.back();
			}
		</script>
	</body>
</html>

<aside id="sidebar-left" class="sidebar-left">

	<div class="sidebar-header">
		<div class="sidebar-title">
			Navigation
		</div>
		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
				<ul class="nav nav-main">
					<?php 
					if(!empty($nav)):
					$access = unserialize($access);
				    foreach($nav as $items): 
				        $current = explode("/",uri_string());
				    	$c = isset($current[1]) ? $current[1] : "dashboard";
				        $title  = unserialize($items->t1000f003);
				        $active = (strtolower($c) === strtolower($items->t1000f002)) ? 'nav-expanded nav-active' : '';
				        $child  = $this->navigation_model->sub_nav_backend($items->t1000r001);
				        $url    = (!empty($child)) ? 'javascript:void();' : site_url($items->t1000f002);
				        $toggle = (!empty($child)) ? 'sidebar-discover' : '';
				        $arrow  = (!empty($child)) ? '<span class="arrow "></span>' : '';

						$valid = 0;
						if(!empty($child)) {
							foreach($child as $sub){
								if (in_array($sub->t1000r001, $access)) { $valid = $sub->t1000r001; }
							}
						}else{ if (in_array($items->t1000r001, $access)) { $valid = 1; } }

					if($valid > 0){
				    ?>
				    <?php if(empty($child)) { ?>
					<li class="<?php echo $active; ?>">
						<a href="<?php echo $url; ?>">
							<i class="<?php echo $items->t1000f005; ?>" aria-hidden="true"></i>
							<span><?php echo $title[$this->lang->lang()]; ?></span>
						</a>
					</li>
					<?php }else{ ?>
					<li class="nav-parent <?php echo $active; ?>">
						<a>
							<i class="<?php echo $items->t1000f005; ?>" aria-hidden="true"></i>
							<span><?php echo $title[$this->lang->lang()]; ?></span>
						</a>
						<ul class="nav nav-children">
							<?php
						      foreach($child as $sub):
								// if (in_array($sub->t1000r001, $access)) {
						          $child2  = $this->navigation_model->sub_nav_backend($sub->t1000r001);
						          $sub_title = unserialize($sub->t1000f003);
						          $sub_url   = site_url($items->t1000f002.'/'.$sub->t1000f002);
						          $sub_url 	 = (!empty($child2)) ? "javascript:void(0);" : $sub_url;
						          $subactive = (strtolower($this->uri->segment(3)) === strtolower($sub->t1000f002) && strtolower($c) === strtolower($items->t1000f002)) ? 'nav-expanded nav-active' : '';
						          $subParent = (!empty($child2)) ? "nav-parent" : "";
						          $valid2 = 0;
						          if(!empty($child2)) {
						          	foreach($child2 as $sub2){
						          		if (in_array($sub2->t1000r001, $access)) { $valid2 = $sub2->t1000r001; }
						          	}
						          }else{ if (in_array($sub->t1000r001, $access)) { $valid2 = 1; } }

					          if($valid2 > 0){
							          echo '<li class="'.$subactive.' '.$subParent.'">
							          <a href="'.$sub_url.'">'.$sub_title[$this->lang->lang()].'</a>';
									
								        if(!empty($child2)) {
								          	foreach($child2 as $sub2){
								          		if (in_array($sub2->t1000r001, $access)) {
									          	$segment = ($this->uri->segment(5) === $sub2->t1000f002) ? strtolower($this->uri->segment(5)) : strtolower($this->uri->segment(4));
									          	$sub2_title = unserialize($sub2->t1000f003);
									            $sub2_url   = site_url($items->t1000f002.'/'.$sub->t1000f002.'/'.$sub2->t1000f002);
									            $subactive2 = ($segment === strtolower($sub2->t1000f002) && strtolower($this->uri->segment(3)) === strtolower($sub->t1000f002)) ? 'nav-active' : '';
									          	echo'<ul class="nav nav-children">
														<li class="'.$subactive2.'">
															<a href="'.$sub2_url.'">'.$sub2_title[$this->lang->lang()].'</a>
														</li>
													</ul>';
												}
											}
										}
								         echo '</li>';
						     	}
						      endforeach;
					      	?>
						</ul>
					</li>
					<?php } ?>
				  <?php 
				  }
				  endforeach; endif;?>
				</ul>
			</nav>

			<hr class="separator" />

		</div>

	</div>

</aside>
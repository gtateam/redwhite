<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Insert Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Insert extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Insert_model');
	}

//Master Item
	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_MasterDataItem_ItemID   => $this->input->post("ItemID"),
					T_MasterDataItem_ItemName   => $this->input->post("ItemName"),
					T_MasterDataItem_UOMID   => $this->input->post("UOMID"),
					T_MasterDataItem_AutoIDType   => $this->input->post("AutoIDType"),
					T_MasterDataItem_EPC   => $this->input->post("EPC"),
					T_MasterDataItem_Barcode   => $this->input->post("Barcode"),
					T_MasterDataItem_UnitPrice   => $this->input->post("UnitPrice"),
					T_MasterDataItem_Picture   => "assets/upload/photo/no-photo.png",
					T_MasterDataItem_GroupID   => $this->input->post("GroupID"),
					T_MasterDataItem_Color   => $this->input->post("Color"),
					T_MasterDataItem_IMEI   => $this->input->post("IMEI"),
					T_MasterDataItem_Brand   => $this->input->post("Brand"),
					T_MasterDataItem_Model   => $this->input->post("Model"),
					T_MasterDataItem_Status   => $this->input->post("Status"),
					T_MasterDataItem_SellingPrice   => $this->input->post("SellingPrice"),
					T_MasterDataItem_Remarks   => $this->input->post("Remarks"),
				);
				$this->Insert_model->Insert(T_MasterDataItem, $data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Balance

}

/* End of file insert.php */
/* Location: ./app/modules/master/controllers/insert.php */

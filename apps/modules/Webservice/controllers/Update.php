<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Update Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 10.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Update extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Update_model');
	}

//Master Item
	public function UpdateItem(){
		try{
			if(check_column(T_MasterDataItem_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_MasterDataItem_RecordID   => $this->input->post("RecordID"),
					T_MasterDataItem_RecordTimestamp   => $this->input->post("TimeStamp"),
                    T_MasterDataItem_RecordStatus => 0,
					T_MasterDataItem_ItemID   => $this->input->post("ItemID"),
					T_MasterDataItem_ItemName   => $this->input->post("ItemName"),
					T_MasterDataItem_UOMID   => $this->input->post("UOMID"),
					T_MasterDataItem_AutoIDType   => $this->input->post("AutoIDType"),
					T_MasterDataItem_EPC   => $this->input->post("EPC"),
					T_MasterDataItem_Barcode   => $this->input->post("Barcode"),
					T_MasterDataItem_UnitPrice   => $this->input->post("UnitPrice"),
					//T_MasterDataItem_Picture   => !empty($this->input->post("Picture")) ? $this->input->post("Picture") : $this->input->post("PictureExist"),
					T_MasterDataItem_GroupID   => $this->input->post("GroupID"),
					T_MasterDataItem_Model   => $this->input->post("Model"),
					T_MasterDataItem_Color   => $this->input->post("Color"),
					T_MasterDataItem_IMEI   => $this->input->post("IMEI"),
					T_MasterDataItem_Brand   => $this->input->post("Brand"),
					T_MasterDataItem_Status   => $this->input->post("Status"),
					T_MasterDataItem_SellingPrice   => $this->input->post("SellingPrice"),
					T_MasterDataItem_Remarks   => $this->input->post("Remarks"),
				);

				$this->Update_model->UpdateItem($data[T_MasterDataItem_RecordID],$data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Balance
	public function UpdateStockBalance(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
					);

				$this->Update_model->UpdateStockBalance($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UpdateStockBalanceJson(){
		try{			
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $obj->DocStatus;
				$t1010f005 = $obj->Remark;

				$data = array(
					'detail' => $obj->detail, 
					);

				$this->Update_model->UpdateStockBalanceJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Insert Stock Transfer
	public function UpdateStockTransfer(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
					);

				$this->Update_model->UpdateStockTransfer($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);
				activity_log($activity_log);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

public function UpdateStockTransferJson(){
		try{
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $obj->DocStatus;
				$t1010f005 = $obj->Remark;

				$data = array(
					'detail' => $obj->detail, 
					);

				$this->Update_model->UpdateStockTransferJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);
				activity_log($activity_log);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Take
	public function UpdateStockTake(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
				);

				$this->Update_model->UpdateStockTake($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Update new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

public function UpdateStockTakeJson(){
		try{
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $obj->DocStatus;
				$t1010f005 = $obj->Remark;

				$data = array(
					'detail' => $obj->detail, 
				);

				$this->Update_model->UpdateStockTakeJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Update new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Adjust
	public function UpdateStockAdjust(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail'			=> $this->input->post("detail"), 
				);

				$this->Update_model->UpdateStockAdjust($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Update new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UpdateStockAdjustJson(){
		try{
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $obj->DocStatus;
				$t1010f005 = $obj->Remark;

				$data = array(
					'detail'	=> $obj->detail, 
				);

				$this->Update_model->UpdateStockAdjustJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Update new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Sales
	public function UpdateSales(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
					);

				$this->Update_model->UpdateSales($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UpdateSalesJson(){
		try{
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
					);

				$this->Update_model->UpdateSalesJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Purchase
	public function UpdatePurchase(){
		try{
			if(check_column('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $this->input->post("ID");
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($this->input->post("DocDate"));
				$t1010f004 = $this->input->post("DocStatus");
				$t1010f005 = $this->input->post("Remark");

				$data = array(
					'detail' => $this->input->post("detail"), 
					);

				$this->Update_model->UpdatePurchase($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UpdatePurchaseJson(){
		try{
			$json = file_get_contents('php://input');
			$obj = json_decode($json);
			if(check_column_json('t1010r002', 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID  = $obj->ID;
				$t1010r002 = date("Y-m-d g:i:s",now());
				$t1010r003 = 0;
				$t1010f003 = convert_datetime($obj->DocDate);
				$t1010f004 = $obj->DocStatus;
				$t1010f005 = $obj->Remark;

				$data = array(
					'detail' => $obj->detail, 
					);

				$this->Update_model->UpdatePurchaseJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data);
				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file update.php */
/* Location: ./app/modules/webservice/controllers/update.php */

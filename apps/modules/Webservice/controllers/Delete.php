<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Delete Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Delete extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Delete_model');
	}

//Master Item
	public function DeleteItem()
	{
		try{
			$this->Delete_model->DeleteItem($this->input->get("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->get("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Master Location
	public function DeleteLocation()	
	{
		try{
			$this->Delete_model->DeleteLocation($this->input->get("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->get("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Master General List
	public function DeleteGeneralList()
	{
		try{
			$this->Delete_model->DeleteGeneralList($this->input->get("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->get("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Balance
	public function DeleteStockBalance()
	{
		try{
			$this->Delete_model->DeleteStockBalance($this->input->get("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->get("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Transfer
	public function DeleteStockTransfer()
	{
		try{
			$this->stocktransfer_model->DeleteStockTransfer($this->input->get("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->get("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Take
	public function DeleteStockTake()
	{
		try{
			$this->stocktake_model->DeleteStockTake($this->input->get("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->get("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Adjust
	public function DeleteStockAdjust()
	{
		try{
			$this->stockadjust_model->DeleteStockAdjust($this->input->get("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->get("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Sales
	public function DeleteSales()
	{
		try{
			$this->Delete_model->DeleteSales($this->input->get("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Purchase
	public function DeletePurchase()
	{
		try{
			$this->Delete_model->DeletePurchase($this->input->post("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file delete.php */
/* Location: ./app/modules/webservice/controllers/delete.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Read Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Read extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
		
    	$this->load->model(array('Read_model','../../Dashboard/models/Dashboard_model','Options_model'));
	}

	public function index(){
		print_out("This is Webservice Read");
	}

//Get List Data
	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		$TransDate = $this->input->get('TransDate');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter, $TransDate);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Read_model->getListCount($table,$Customfilter,$TransDate);
			}
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
		
	}

	//Lookup Data Item 
	public function GetListItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getListItem($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Read_model->getListItemCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getExchangeRate($currency,$type)
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$this->db->where(T_MasterDataKurs_Currency,$currency);
		$this->db->where(T_MasterDataKurs_KursType,$type);
		$query = $this->db->get(T_MasterDataKurs);
		if($query->num_rows() > 0){
			$info->data =  $query->last_row();
			$info->msg = "Success";
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetListItemForCOA(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Read_model->getListItemForCOA($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $getList->num_rows();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetStockWithoutCOA(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->post("ItemID");
 		$data['Detail'] = $this->Read_model->GetStockWithoutCOA($id);

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function alertData(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_VIEWAlert;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAlert($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListAlertCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($getList->result()));
	}

	public function resetAlert()
	{
		$this->db->update(T_Alert,array(T_Alert_FLAG => 1));
	}

	public function DLBrand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$result = $this->Options_model->get_lookupDL('9',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key);
		if(!empty($result))
		{
			$info->data = $result;
		}else{
			$info->msg = "Empty data";
			$info->errorcode = 100;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function DLModel()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$result = $this->Options_model->get_lookupDL('6',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key);
		
		if(!empty($result))
		{
			$info->data = $result;
		}else{
			$info->msg = "Empty data";
			$info->errorcode = 100;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function DLColor()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$result = $this->Options_model->get_lookupDL('5',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key);
		
		if(!empty($result))
		{
			$info->data = $result;
		}else{
			$info->msg = "Empty data";
			$info->errorcode = 100;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function LoadDetail()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
		$this->db->from('t6010');
		$this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
		$this->db->where("t6011f005", 1);
		if($this->input->get('Brand')) {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model')) {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color')) {
            $this->db->where("t6010f006", $this->input->get('Color'));
		}
		if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
		}
		if($this->input->get('IMEI')) {
            $this->db->where("t6011f001", $this->input->get('IMEI'));
		}
		if($this->input->get('RFID')) {
            $this->db->where("t6011f002", $this->input->get('RFID'));
        }
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$info->data = $query->result();
			$info->count = $query->num_rows();
		}else{
			$info->msg = "Empty data";
			$info->errorcode = 100;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function LoadDetailStockTake()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
		$this->db->from('t6010');
		$this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
		$this->db->where("t6011f005", 1);
		if($this->input->get('Brand')) {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model')) {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color')) {
            $this->db->where("t6010f006", $this->input->get('Color'));
		}
		if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
		}
		if($this->input->get('IMEI')) {
            $this->db->where("t6011f001", $this->input->get('IMEI'));
		}
		if($this->input->get('RFID')) {
            $this->db->where("t6011f002", $this->input->get('RFID'));
        }
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$info->data = $query->result();
			$info->qtyBalance = $query->num_rows();
			$info->qtyScanned = 0;
			$info->qtyDifference = 0;
			$info->qtyMiss = 0;
		}else{
			$info->msg = "Empty data";
			$info->errorcode = 100;
			$info->qtyBalance = 0;
			$info->qtyScanned = 0;
			$info->qtyDifference = 0;
			$info->qtyMiss = 0;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file read.php */
/* Location: ./app/modules/webservice/controllers/read.php */

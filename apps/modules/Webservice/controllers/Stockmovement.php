<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2014, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Inventory Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 07.04.2017	
 * @author	    Hikmat Fauzy Erdiyana
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2014, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Stockmovement extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model(array('Stockmovement_model','StockList_model'));
    	$this->load->model('../../Stockmovement/models/Stocktake_model');
	}

	//StockMovement
	public function createheader(){
		$this->db->trans_begin();
		try{
			$doctype = $this->input->post("DocTypeID");
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => substr(DocNo($doctype), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					T_TransactionStockMovementHeader_Brand => empty($this->input->post("Brand"))? ' ':$this->input->post("Brand"),
					T_TransactionStockMovementHeader_Model => empty($this->input->post("Model"))? ' ':$this->input->post("Model"),
					T_TransactionStockMovementHeader_Color => empty($this->input->post("Color"))? ' ':$this->input->post("Color"),
					T_TransactionStockMovementHeader_Status => empty($this->input->post("Status"))? ' ':$this->input->post("Status")
					//T_TransactionStockMovementHeader_ReffDocNo => empty($this->input->post("RefDocNo")) ? ' ' : $this->input->post("RefDocNo")
				);

				$this->db->insert(T_TransactionStockMovementHeader, $data);
				$RecordID = $this->db->insert_id();
				UpdateDocNo($doctype);
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success', 'RecordID' => $RecordID );
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function readheader()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionStockMovementHeader_RecordID." as RecordID, ".
            T_TransactionStockMovementHeader_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementHeader_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementHeader_DocTypeID." as DocTypeID, ".
            T_TransactionStockMovementHeader_DocNo." as DocNo, ".
            T_TransactionStockMovementHeader_DocDate." as DocDate, ".
            T_TransactionStockMovementHeader_DocStatus." as DocStatus, ".
            T_TransactionStockMovementHeader_Brand." as Brand, ".
            T_TransactionStockMovementHeader_Model." as Model, ".
            T_TransactionStockMovementHeader_Color." as Color, ".
            T_TransactionStockMovementHeader_Status." as Status, ".
            T_TransactionStockMovementHeader_Remarks." as Remarks ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function updateheader(){
		$this->db->trans_begin();
		try{
			$doctype = $this->input->post("DocTypeID");
			if(check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => $this->input->post("DocNo"),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					T_TransactionStockMovementHeader_ReffDocNo => empty($this->input->post("RefDocNo")) ? ' ' : $this->input->post("RefDocNo")
				);

				$this->db->where(T_TransactionStockMovementHeader_RecordID, $data[T_TransactionStockMovementHeader_RecordID]);
        		$this->db->update(T_TransactionStockMovementHeader,$data);
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function deleteheader()
	{
		$this->db->trans_begin();
		try{
			$this->Stockmovement_model->Delete($this->input->post("RecordID"));
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function createdetail(){
		$this->db->trans_begin();
		try{
			$parentID = $this->input->post("ParentRecordID");
			$val = array(
				T_TransactionStockMovementDetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_TransactionStockMovementDetail_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
				T_TransactionStockMovementDetail_PRI => empty($parentID)? 0:$parentID,
				T_TransactionStockMovementDetail_RowIndex => empty($this->input->post("RowIndex"))? 0:$this->input->post("RowIndex"),
				T_TransactionStockMovementDetail_ItemID => empty($this->input->post("ItemID"))? ' ':$this->input->post("ItemID"),
				T_TransactionStockMovementDetail_LocationID1 => empty($this->input->post("LocationID1"))? ' ':$this->input->post("LocationID1"),
				T_TransactionStockMovementDetail_LocationID2 => empty($this->input->post("LocationID2"))? ' ':$this->input->post("LocationID2"),
				T_TransactionStockMovementDetail_Quantity1 => empty($this->input->post("Quantity1"))? 0:$this->input->post("Quantity1"),
				T_TransactionStockMovementDetail_Quantity2 => empty($this->input->post("Quantity2"))? 0:$this->input->post("Quantity2"),
				T_TransactionStockMovementDetail_EPC => empty($this->input->post("EPC"))? "":$this->input->post("EPC"),
				T_TransactionStockMovementDetail_Barcode => empty($this->input->post("Barcode"))? " ":$this->input->post("Barcode"),
				T_TransactionStockMovementDetail_Remarks => empty($this->input->post("Remarks")) ? " ":$this->input->post("Remarks")
			);
			$this->db->insert(T_TransactionStockMovementDetail, $val);
			
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
			
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function readdetailStocktakeOld()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementDetail;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  		$select = T_TransactionStockMovementDetail_RecordID." as RecordID, ".
            T_TransactionStockMovementDetail_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementDetail_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementDetail_PRI." as PRI, ".
            T_TransactionStockMovementDetail_RowIndex." as RowIndex, ".
            T_TransactionStockMovementDetail_ItemID." as ItemID, ".
            T_MasterDataItem_ItemName." as ItemName, ".
            T_MasterDataItem_AutoIDType." as ItemTypeID, ".
			T_MasterDataGeneralTableValue_Key." as ItemType, ".
            T_MasterDataItem_UOMID." as ItemUOM, ".
            T_MasterDataItem_EPC." as EPC, ".
            T_MasterDataItem_Barcode." as Barcode, ".
            T_TransactionStockMovementDetail_LocationID1." as LocationID1, ".
            T_TransactionStockMovementDetail_LocationID2." as LocationID2, ".
            T_TransactionStockMovementDetail_Quantity1." as Quantity1, ".
            T_TransactionStockMovementDetail_Quantity2." as Quantity2, ".
            T_TransactionStockMovementDetail_Remarks." as Remarks ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
        $this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
        $sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
        $allSN = $sqlAllSN->result("array");
        $allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
		$allSNCount = count($allSN);

		$DocSNCount = 0;
		$diffSNCount = 0;
		$FoundCount =0;
		$MissCount =0;
		$PRI = 0;
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$dataDetail = $getList->result("array");
			$PRISNDoc = array_column($dataDetail,"RecordID");
            $PRISNDoc = array_unique($PRISNDoc);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);
			$PRI = $dataDetail[0]["PRI"];
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Empty Data!";
		}
		$diffItem = array();
		$lastItemID = "";
		$this->db->where(T_TransactionStockMovementHeader_RecordID, $PRI);
		$SQLheaderData = $this->db->get(T_TransactionStockMovementHeader);
		$headerStatus = isset($SQLheaderData->first_row()->{T_TransactionStockMovementHeader_RecordStatus}) ? $SQLheaderData->first_row()->{T_TransactionStockMovementHeader_RecordStatus} : 0 ;
		if($headerStatus){
			$this->db->where(T_LogDiffEPC_PRI,$PRI);
			$sqlHMiss = $this->db->get(T_LogDiffEPC);
			$dataHMiss = $sqlHMiss->first_row();
			$allSNCount = $dataHMiss->{T_LogDiffEPC_Balance};
			$DocSNCount = $dataHMiss->{T_LogDiffEPC_Scanned};
			$diffSNCount = $dataHMiss->{T_LogDiffEPC_Diff};
			$diffSNCount2 = explode("|",$dataHMiss->{T_LogDiffEPC_Diff});
			$FoundCount =  $diffSNCount2[0];
			$MissCount = $diffSNCount2[1];

			$this->db->where(T_LogDiffEPDetail_PRI,$PRI);
			$sqlMiss = $this->db->get(T_LogDiffEPDetail);
			$diffSN = $sqlMiss->result("array");
			foreach($diffSN as $dataSN)
			{
				$label = ($dataSN[T_LogDiffEPDetail_RecordStatus]) ? " (+)" : " (-)";
				$snTag = substr($dataSN[T_LogDiffEPDetail_EPC],SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$query = $this->db->get(T_MasterDataItem);
				$itemData = $query->first_row();
				if($lastItemID != $snTag){
					$diffItem[$snTag] = array(
						'ItemID' => $itemData->{T_MasterDataItem_ItemID},
						'ItemName' => $itemData->{T_MasterDataItem_ItemName}.$label,
						'SN' => array($dataSN[T_LogDiffEPDetail_EPC]),
					);
				}else{
					array_push($diffItem[$lastItemID]['SN'],$dataSN[T_LogDiffEPDetail_EPC]); 
				}
				$lastItemID = $snTag;
			}
		}else{
			if($DocSNCount != 0){
				$diffSN2 = array_diff($DocSN,$allSN);
				$diffSNCount2 = count($diffSN2);

				sort($diffSN2);
				foreach($diffSN2 as $dataSN)
				{
					// $snTag = substr($dataSN,SNINDEX,SNLENGTH);
					// $this->db->where(T_MasterDataItem_EPC,$snTag);
					// $query = $this->db->get(T_MasterDataItem);
					// $itemData = $query->first_row();

					$this->db->where(T_TransactionStockBalanceHeader_EPC,$dataSN);
                    //$this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $query = $this->db->get(T_TransactionStockBalanceHeader);
                    if($query->num_rows() == 0){
                        $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$dataSN);
                        //$this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                        $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                        $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                        $query = $this->db->get(T_TransactionStockBalanceDetail);
					}
					$itemData = $query->first_row();
					if($lastItemID != $itemData->{T_MasterDataItem_ItemID}){
						$diffItem[$itemData->{T_MasterDataItem_ItemID}] = array(
							'ItemID' => $itemData->{T_MasterDataItem_ItemID},
							'ItemName' => $itemData->{T_MasterDataItem_ItemName}." (+)",
							'SN' => array($dataSN),
							'IMEI' => array($itemData->{T_TransactionStockBalanceDetail_IMEI})
						);
					}else{
						array_push($diffItem[$lastItemID]['SN'],$dataSN); 
					}
					$lastItemID = $itemData->{T_MasterDataItem_ItemID};
				}

				$diffSN1 = array_diff($allSN,$DocSN);
				$diffSNCount1 = count($diffSN1)*-1;

				sort($diffSN1);
				foreach($diffSN1 as $dataSN)
				{
					// $snTag = substr($dataSN,SNINDEX,SNLENGTH);
					// $this->db->where(T_MasterDataItem_EPC,$snTag);
					// $query = $this->db->get(T_MasterDataItem);
					// $itemData = $query->first_row();
					$this->db->where(T_TransactionStockBalanceHeader_EPC,$dataSN);
                    //$this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $query = $this->db->get(T_TransactionStockBalanceHeader);
                    if($query->num_rows() == 0){
                        $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$dataSN);
                        //$this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                        $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                        $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                        $query = $this->db->get(T_TransactionStockBalanceDetail);
					}
					$itemData = $query->first_row();
					if($lastItemID != $itemData->{T_MasterDataItem_ItemID}){
						$diffItem[$itemData->{T_MasterDataItem_ItemID}] = array(
							'ItemID' => $itemData->{T_MasterDataItem_ItemID},
							'ItemName' => $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}." (-)",
							'SN' => array(
								"RFID" => array($dataSN),
								"IMEI" => array($itemData->{T_TransactionStockBalanceDetail_IMEI})
							)
						);
					}else{
						array_push($diffItem[$lastItemID]['SN']['RFID'],$dataSN); 
						array_push($diffItem[$lastItemID]['SN']['IMEI'],$itemData->{T_TransactionStockBalanceDetail_IMEI}); 
					}
					$lastItemID = $itemData->{T_MasterDataItem_ItemID};
				}

				$diffSN = $diffItem;
				$diffSNCount = $diffSNCount2." | ".$diffSNCount1;
				$FoundCount =  $diffSNCount2;
				$MissCount = $diffSNCount1;
			}
		}
		$info->qtyBalance = $allSNCount;
		$info->qtyScanned = $DocSNCount;
		$info->qtyDifference = $diffSNCount;
		$info->qtyFound = $FoundCount;
		$info->qtyMiss = $MissCount;
		$info->dataMiss = $diffItem;

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function readdetailStocktake()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$DocSNCount = 0;
		$diffCount = 0;
		$FoundCount = 0;
		$MissCount = 0;
		$diffItem = array();
		$lastItemID = "";
		$Customfilter = $this->input->get('customfilter');

		// Begin Balance
		$this->db->where(T_TRANSINOUTDETAIL_Flag,1);
		if($this->input->get('Brand') && $this->input->get('Brand') != "All") {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model') && $this->input->get('Model') != "All") {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color') && $this->input->get('Color') != "All" ) {
            $this->db->where("t6010f006", $this->input->get('Color'));
		}
		if($this->input->get('Status') && $this->input->get('Status') != "All") {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
		}
		$this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TRANSINOUTDETAIL.".".T_TRANSINOUTDETAIL_ItemID);
		$this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
		$this->db->select("*, t6011.t6011f002 as rfid");
		$sqlBalance = $this->db->get(T_TRANSINOUTDETAIL);
		// End Balance

		// Begin Scanned item
		$select = T_TransactionStockMovementDetail_RecordID." as RecordID, ".
            T_TransactionStockMovementDetail_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementDetail_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementDetail_PRI." as PRI, ".
            T_TransactionStockMovementDetail_RowIndex." as RowIndex, ".
            T_TransactionStockMovementDetail_ItemID." as ItemID, ".
            T_MasterDataItem_ItemName." as ItemName, ".
            T_MasterDataItem_AutoIDType." as ItemTypeID, ".
			T_MasterDataGeneralTableValue_Key." as ItemType, ".
			T_MasterDataItem_UOMID." as ItemUOM, ".
			T_MasterDataItem_Brand." as Brand, ".
			T_MasterDataItem_Model." as Model, ".
			T_MasterDataItem_Color." as Color, ".
			T_TransactionStockMovementDetail_EPC." as EPC, ".
			T_TransactionStockMovementDetail_IMEI." as IMEI,".
			T_TransactionStockMovementDetail_Barcode." as Barcode, ".
            T_TransactionStockMovementDetail_LocationID1." as LocationID1, ".
            T_TransactionStockMovementDetail_LocationID2." as LocationID2, ".
            T_TransactionStockMovementDetail_Quantity1." as Quantity1, ".
            T_TransactionStockMovementDetail_Quantity2." as Quantity2, ".
            T_TransactionStockMovementDetail_Remarks." as Remarks ";
		if($Customfilter != ''){
            foreach ($Customfilter as $col => $val) {
                $col = constant("T_".$col);
                $this->db->where($col,$val);
            }
		}
		$this->db->where(T_TransactionStockMovementDetail_Quantity2,1);
		$this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TransactionStockMovementDetail.".".T_TransactionStockMovementDetail_ItemID);  
		$this->db->join(T_MasterDataGeneralTableValue, T_MasterDataGeneralTableValue.".".T_MasterDataGeneralTableValue_RecordID." = ".T_MasterDataItem.".".T_MasterDataItem_AutoIDType);
		$this->db->select($select);
		$sqlScanned = $this->db->get(T_TransactionStockMovementDetail);
		// end Scanned Item

		// Begin Diff
		if($sqlScanned->num_rows() > 0){
			$dataBalance = $sqlBalance->result("array");

			$dataScaned = $sqlScanned->result("array");

			$PRIBalance = array_column($dataBalance,"t6011f001");
			$PRIBalance = array_unique($PRIBalance);

			$PRIScan = array_column($dataScaned,"IMEI");
			$PRIScan = array_unique($PRIScan);

			$vodiffItem = array_diff($PRIBalance,$PRIScan);
			$vodiffItem = array_unique($vodiffItem);
			
			if($vodiffItem){
				$this->db->order_by(T_MasterDataItem_Brand);
				$this->db->order_by(T_MasterDataItem_Model);
				$this->db->order_by(T_MasterDataItem_Color);
				$this->db->where(T_TRANSINOUTDETAIL_Flag,1);
				$this->db->where_in(T_TRANSINOUTDETAIL_IMEI,$vodiffItem);
				$this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TRANSINOUTDETAIL.".".T_TRANSINOUTDETAIL_ItemID);
				$this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
				$this->db->select("*");
				$query = $this->db->get(T_TRANSINOUTDETAIL);
				$voitemData = $query->result();
				// echo "<pre>";
				// print_r($voitemData); die;

				foreach($voitemData as $itemData)
				{
					if($lastItemID != $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}." ".$itemData->{T_TRANSINOUT_Status}){
						$diffItem[$itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}." ".$itemData->{T_TRANSINOUT_Status}] = array(
							'ItemID' => $itemData->{T_MasterDataItem_ItemID},
							'ItemName' => $itemData->{T_MasterDataItem_Brand}."  ".$itemData->{T_MasterDataItem_Model}." - ".$itemData->{T_MasterDataItem_Color},
							'Status' => $itemData->{T_TRANSINOUT_Status},
							'SN' => array(
								"RFID" => array($itemData->{T_TRANSINOUTDETAIL_RFID}),
								"IMEI" => array($itemData->{T_TRANSINOUTDETAIL_IMEI})
							)
						);
					}else{
						array_push($diffItem[$lastItemID]['SN']['RFID'],$itemData->{T_TRANSINOUTDETAIL_RFID}); 
						array_push($diffItem[$lastItemID]['SN']['IMEI'],$itemData->{T_TRANSINOUTDETAIL_IMEI}); 
					}
					$lastItemID = $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}." ".$itemData->{T_TRANSINOUT_Status};
				}
			}

		}else{
			$info->errorcode = 101;
			$info->msg = "Empty Data!";
		}
		// End Diff
		$qtyDiff = ($sqlScanned->num_rows()) ? $sqlScanned->num_rows() - $sqlBalance->num_rows() : 0;
		$info->qtyBalance = $sqlBalance->num_rows();
		$info->qtyScanned = $sqlScanned->num_rows();
		$info->qtyDifference = $qtyDiff;
		$info->qtyFound = $FoundCount;
		$info->qtyMiss = $qtyDiff;
		$info->dataMiss = $diffItem;
		$info->data = $sqlBalance->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	// public function readdetailStocktake()
	// {
	// 	$info = new stdClass();
	// 	$info->msg = "";
	// 	$info->errorcode = 0;
	// 	$qtyBalance = 0;
	// 	$qtyScanned = 0;

	// 	$id = $this->input->get('PRI');
	// 	$this->db->from(T_TransactionStockMovementDetail);
	// 	$this->db->where(T_TransactionStockMovementDetail_PRI,$id);
	// 	$get_stockmovement_detail = $this->db->get();
		
	// 	if($get_stockmovement_detail->num_rows() > 0)
	// 	{
	// 		$data_sm_detail = array();
	// 		$sn_scanned = array();
	// 		$epc_scanned = array();

	// 		$dataMiss = array();
	// 		foreach ($get_stockmovement_detail->result('array') as $temp_data_sm_detail) {
	// 			$this->db->from(T_TransactionStockMovementDetailSerialNo);
	// 			$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$temp_data_sm_detail[T_TransactionStockMovementDetail_RecordID]);
	// 			$get_stockmovement_sn = $this->db->get();

	// 			$this->db->from(T_MasterDataItem);
	// 			$this->db->where(T_MasterDataItem_ItemID, $temp_data_sm_detail[T_TransactionStockMovementDetail_ItemID]);
	// 			$get_master = $this->db->get();
	// 			$get_master = $get_master->first_row('array');

	// 			if($get_master[T_MasterDataItem_AutoIDType] == 33)//accessories TYPE SS
	// 			{
	// 				$qtyScanned += $temp_data_sm_detail[T_TransactionStockMovementDetail_Quantity1];
	// 				$epc_scanned += $temp_data_sm_detail[T_TransactionStockMovementDetail_EPC];

	// 			}else if($get_master[T_MasterDataItem_AutoIDType] == 30){//mobile type S1
	// 				$qtyScanned += $get_stockmovement_sn->num_rows();
	// 				foreach ($get_stockmovement_sn->result("array") as $temp_get_stockmovement_sn) {
	// 					$sn_scanned[] = $temp_get_stockmovement_sn[T_TransactionStockMovementDetailSerialNo_SerialNo];
	// 				}
	// 			}
	// 		}
	// 		//accessories TYPE SS
	// 		$this->db->from(T_TransactionStockBalanceHeader);
	// 		if(!empty($epc_scanned))
	// 		{
	// 			$this->db->where_not_in(T_TransactionStockBalanceHeader_EPC, $epc_scanned);
	// 		}
	// 		$data_ss = $this->db->get();

	// 		if($data_ss->num_rows() > 0){
	// 			foreach ($data_ss->result("array") as $temp_data_ss) {
	// 				$this->db->from(T_MasterDataItem);
	// 				$this->db->where(T_MasterDataItem_ItemID, $temp_data_ss[T_TransactionStockBalanceHeader_ItemID]);
	// 				$get_master = $this->db->get();
	// 				$get_master = $get_master->first_row('array');
					
	// 				$key = array_search($get_master[T_MasterDataItem_RecordID], array_column($dataMiss, T_MasterDataItem_RecordID));
	// 				$qtyBalance += 1;
	// 				if($get_master[T_MasterDataItem_AutoIDType] == 33)
	// 				{
	// 					if($key === false)
	// 					{
	// 						$array_data = array(
	// 							T_MasterDataItem_RecordID => $get_master[T_MasterDataItem_RecordID],
	// 							T_MasterDataItem_AutoIDType => $get_master[T_MasterDataItem_AutoIDType],
	// 							T_MasterDataItem_ItemID => $get_master[T_MasterDataItem_ItemID],
	// 							T_MasterDataItem_Brand => $get_master[T_MasterDataItem_Brand],
	// 							T_MasterDataItem_Model => $get_master[T_MasterDataItem_Model],
	// 							T_MasterDataItem_Color => $get_master[T_MasterDataItem_Color],
	// 							"data" => array(
	// 									array(
	// 										"RFID" => $temp_data_ss[T_TransactionStockBalanceHeader_EPC],
	// 										"IMEI" => ""
	// 									)
	// 								)
	// 						);

	// 						array_push($dataMiss, $array_data);
	// 					}else{
	// 						$new = array(
	// 							"RFID" => $temp_data_ss[T_TransactionStockBalanceHeader_EPC],
	// 							"IMEI" => ""
	// 						);
	// 						array_push($dataMiss[$key]["data"], $new);
	// 					}
	// 				}
	// 			}
	// 		}


	// 		//mobile TYPE S1
	// 		$this->db->from(T_TransactionStockBalanceDetail);
	// 		if(!empty($sn_scanned))
	// 		{
	// 			$this->db->where_not_in(T_TransactionStockBalanceDetail_SerialNo, $sn_scanned);
	// 		}
	// 		$data_sn = $this->db->get();
	// 		foreach ($data_sn->result("array") as $temp_data_sn) {
	// 			$this->db->from(T_TransactionStockBalanceHeader);
	// 			$this->db->where(T_TransactionStockBalanceHeader_RecordID, $temp_data_sn[T_TransactionStockBalanceDetail_PRI]);
	// 			$data_header = $this->db->get();
	// 			$data_header = $data_header->first_row("array");

	// 			$this->db->from(T_MasterDataItem);
	// 			$this->db->where(T_MasterDataItem_ItemID, $data_header[T_TransactionStockBalanceHeader_ItemID]);
	// 			$get_master = $this->db->get();
	// 			$get_master = $get_master->first_row('array');
				
	// 			$key = array_search($get_master[T_MasterDataItem_RecordID], array_column($dataMiss, T_MasterDataItem_RecordID));
	// 			$qtyBalance += 1;
				
	// 			if($key === false)
	// 			{
	// 				$array_data = array(
	// 					T_MasterDataItem_RecordID => $get_master[T_MasterDataItem_RecordID],
	// 					T_MasterDataItem_AutoIDType => $get_master[T_MasterDataItem_AutoIDType],
	// 					T_MasterDataItem_ItemID => $get_master[T_MasterDataItem_ItemID],
	// 					T_MasterDataItem_Brand => $get_master[T_MasterDataItem_Brand],
	// 					T_MasterDataItem_Model => $get_master[T_MasterDataItem_Model],
	// 					T_MasterDataItem_Color => $get_master[T_MasterDataItem_Color],
	// 					"data" => array(
	// 							array(
	// 								"RFID" => $temp_data_sn[T_TransactionStockBalanceDetail_SerialNo],
	// 								"IMEI" => $temp_data_sn[T_TransactionStockBalanceDetail_IMEI]
	// 							)
	// 						)
	// 				);
	// 				array_push($dataMiss, $array_data);
	// 			}else{
	// 				$new = array(
	// 					"RFID" => $temp_data_sn[T_TransactionStockBalanceDetail_SerialNo],
	// 					"IMEI" => $temp_data_sn[T_TransactionStockBalanceDetail_IMEI]
	// 				);
	// 				array_push($dataMiss[$key]["data"], $new);
	// 			}
	// 		}
			
	// 		$info->dataMiss = $dataMiss;
	// 	}else{
	// 		$query = "SELECT SUM(t1990f003) as Onhand FROM t1990";
	// 		$sql = $this->db->query($query);
	// 		$qtyBalance = $sql->first_row()->Onhand;
	// 	}


	// 	$info->qtyBalance = $qtyBalance;
	// 	$info->qtyScanned = $qtyScanned;
	// 	$info->qtyMiss = ($qtyBalance - $qtyScanned);

	// 	$this->output->set_content_type('application/json')->set_output(json_encode($info));
	// }

	public function readdetailStockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementDetail;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionStockMovementDetail_RecordID." as RecordID, ".
            T_TransactionStockMovementDetail_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementDetail_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementDetail_PRI." as PRI, ".
            T_TransactionStockMovementDetail_RowIndex." as RowIndex, ".
            T_TransactionStockMovementDetail_ItemID." as ItemID, ".
            T_MasterDataItem_ItemName." as ItemName, ".
            T_MasterDataItem_AutoIDType." as ItemTypeID, ".
			T_MasterDataGeneralTableValue_Key." as ItemType, ".
            T_MasterDataItem_UOMID." as ItemUOM, ".
            T_MasterDataItem_EPC." as EPC, ".
            T_MasterDataItem_Barcode." as Barcode, ".
            T_TransactionStockMovementDetail_LocationID1." as LocationID1, ".
            T_TransactionStockMovementDetail_LocationID2." as LocationID2, ".
            T_TransactionStockMovementDetail_Quantity1." as Quantity1, ".
            T_TransactionStockMovementDetail_Quantity2." as Quantity2, ".
            T_TransactionStockMovementDetail_Remarks." as Remarks ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		$DocSNCount = 0;
		$diffSNCount = 0;
		$PRI = $Customfilter['TransactionStockMovementDetail_PRI'];
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$dataDetail = $getList->result("array");
			$PRISNDoc = array_column($dataDetail,"RecordID");
            $PRISNDoc = array_unique($PRISNDoc);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);
			$PRI = $dataDetail[0]["PRI"];
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$diffItem = array();
		$lastSN = "";
		$this->db->where(T_TransactionStockMovementHeader_RecordID, $PRI);
		$SQLheaderData = $this->db->get(T_TransactionStockMovementHeader);
		$headerData = $SQLheaderData->first_row();
		$headerStatus = isset($headerData->{T_TransactionStockMovementHeader_RecordStatus}) ? $headerData->{T_TransactionStockMovementHeader_RecordStatus} : 0 ;
		
		$reffDocDO = isset($headerData->{T_TransactionStockMovementHeader_ReffDocNo}) ? $headerData->{T_TransactionStockMovementHeader_ReffDocNo} : 0 ;
		$this->db->select(T_TransactionDO_RecordID);
        $this->db->where(T_TransactionDO_DocNo,$reffDocDO);
        $sqlDO = $this->db->get(T_TransactionDO);
		$DOData = $sqlDO->first_row();

		$this->db->select(T_TransactionDODetail_EPC);
        $this->db->where(T_TransactionDODetail_PRI,$DOData->{T_TransactionDO_RecordID});
        $sqlAllSN = $this->db->get(T_TransactionDODetail);
        $allSN = $sqlAllSN->result("array");
        $allSN = array_column($allSN,T_TransactionDODetail_EPC);
		$allSNCount = count($allSN);

		if($headerStatus){
			$this->db->where(T_LogDiffEPC_PRI,$PRI);
			$sqlHMiss = $this->db->get(T_LogDiffEPC);
			$dataHMiss = $sqlHMiss->first_row();
			$allSNCount = $dataHMiss->{T_LogDiffEPC_Balance};
			$DocSNCount = $dataHMiss->{T_LogDiffEPC_Scanned};
			$diffSNCount = $dataHMiss->{T_LogDiffEPC_Diff};

			$this->db->where(T_LogDiffEPDetail_PRI,$PRI);
			$sqlMiss = $this->db->get(T_LogDiffEPDetail);
			$dataMiss = $sqlMiss->result("array");
			$diffSN  = array_column($dataMiss,T_LogDiffEPDetail_EPC);
			foreach($diffSN as $dataSN)
			{
				$snTag = substr($dataSN,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$query = $this->db->get(T_MasterDataItem);
				$itemData = $query->first_row();
				if($lastSN != $snTag){
					$diffItem[$snTag] = array(
						'ItemID' => $itemData->{T_MasterDataItem_ItemID},
						'ItemName' => $itemData->{T_MasterDataItem_ItemName},
						'SN' => array($dataSN)
					);
				}else{
					array_push($diffItem[$lastSN]['SN'],$dataSN); 
				}
				$lastSN = $snTag;
			}
		}elseif($getList->num_rows() == 0){
			$diffSNCount = 0 - $allSNCount;
			sort($allSN);
			foreach($allSN as $dataSN)
			{
				$snTag = substr($dataSN,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$query = $this->db->get(T_MasterDataItem);
				$itemData = $query->first_row();
				if($lastSN != $snTag){
					$diffItem[$snTag] = array(
						'ItemID' => $itemData->{T_MasterDataItem_ItemID},
						'ItemName' => $itemData->{T_MasterDataItem_ItemName},
						'SN' => array($dataSN)
					);
				}else{
					array_push($diffItem[$lastSN]['SN'],$dataSN); 
				}
				$lastSN = $snTag;
			}
		}else{
			if($DocSNCount != 0){
				if($allSNCount > $DocSNCount){
					$diffSN = array_diff($allSN,$DocSN);
					$diffSNCount = count($diffSN)*-1;
				}else{
					$diffSN = array_diff($DocSN,$allSN);
					$diffSNCount = count($diffSN);
				}
				sort($diffSN);
				foreach($diffSN as $dataSN)
				{
					$snTag = substr($dataSN,SNINDEX,SNLENGTH);
					$this->db->where(T_MasterDataItem_EPC,$snTag);
					$query = $this->db->get(T_MasterDataItem);
					$itemData = $query->first_row();
					if($lastSN != $snTag){
						$diffItem[$snTag] = array(
							'ItemID' => $itemData->{T_MasterDataItem_ItemID},
							'ItemName' => $itemData->{T_MasterDataItem_ItemName},
							'SN' => array($dataSN)
						);
					}else{
						array_push($diffItem[$lastSN]['SN'],$dataSN); 
					}
					$lastSN = $snTag;
				}

			}
		}
		$info->qtyDO = $allSNCount;
		$info->qtyScanned = $DocSNCount;
		$info->qtyDifference = $diffSNCount;
		$info->dataMiss = $diffItem;

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function readdetail()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementDetail;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionStockMovementDetail_RecordID." as RecordID, ".
            T_TransactionStockMovementDetail_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementDetail_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementDetail_PRI." as PRI, ".
            T_TransactionStockMovementDetail_RowIndex." as RowIndex, ".
            T_TransactionStockMovementDetail_ItemID." as ItemID, ".
            T_MasterDataItem_ItemName." as ItemName, ".
            T_MasterDataItem_AutoIDType." as ItemTypeID, ".
			T_MasterDataGeneralTableValue_Key." as ItemType, ".
            T_MasterDataItem_UOMID." as ItemUOM, ".
            T_MasterDataItem_EPC." as EPC, ".
            T_MasterDataItem_Barcode." as Barcode, ".
            T_TransactionStockMovementDetail_LocationID1." as LocationID1, ".
            T_TransactionStockMovementDetail_LocationID2." as LocationID2, ".
            T_TransactionStockMovementDetail_Quantity1." as Quantity1, ".
            T_TransactionStockMovementDetail_Quantity2." as Quantity2, ".
            T_TransactionStockMovementDetail_Remarks." as Remarks ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function updatedetail(){
		$this->db->trans_begin();
		try{
			$parentID = $this->input->post("ParentRecordID");
			if(check_column(T_TransactionStockMovementDetail_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementDetail_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementDetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementDetail_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementDetail_PRI => empty($parentID)? 0:$parentID,
					T_TransactionStockMovementDetail_RowIndex => empty($this->input->post("RowIndex"))? 0:$this->input->post("RowIndex"),
					T_TransactionStockMovementDetail_ItemID => empty($this->input->post("ItemID"))? ' ':$this->input->post("ItemID"),
					T_TransactionStockMovementDetail_LocationID1 => empty($this->input->post("LocationID1"))? ' ':$this->input->post("LocationID1"),
					T_TransactionStockMovementDetail_LocationID2 => empty($this->input->post("LocationID2"))? ' ':$this->input->post("LocationID2"),
					T_TransactionStockMovementDetail_Quantity1 => empty($this->input->post("Quantity1"))? 0:$this->input->post("Quantity1"),
					T_TransactionStockMovementDetail_Quantity2 => empty($this->input->post("Quantity2"))? 0:$this->input->post("Quantity2"),
					T_TransactionStockMovementDetail_EPC => empty($this->input->post("EPC"))? "":$this->input->post("EPC"),
					T_TransactionStockMovementDetail_Barcode => empty($this->input->post("Barcode"))? " ":$this->input->post("Barcode"),
					T_TransactionStockMovementDetail_Remarks => empty($this->input->post("Remarks")) ? " ":$this->input->post("Remarks")
				);

				$this->db->where(T_TransactionStockMovementDetail_RecordID, $data[T_TransactionStockMovementDetail_RecordID]);
				$this->db->update(T_TransactionStockMovementDetail,$data);
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function deletedetail()
	{
		$this->db->trans_begin();
		try{
			$RecordID = $this->input->post("RecordID");
			$this->db->delete(T_TransactionStockMovementDetail, array(T_TransactionStockMovementDetail_RecordID => $RecordID));
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function createserialno(){
		$this->db->trans_begin();
		try{
			$parentID = $this->input->post("ParentRecordID");
			$DocTypeID = $this->input->post("DocTypeID");
			$val = array(
				T_TransactionStockMovementDetailSerialNo_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_TransactionStockMovementDetailSerialNo_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
				T_TransactionStockMovementDetailSerialNo_PRI => empty($parentID)? 0:$parentID,
				T_TransactionStockMovementDetailSerialNo_SerialNo => empty($this->input->post("SerialNo"))? 0:$this->input->post("SerialNo")
			);
			$this->db->insert(T_TransactionStockMovementDetailSerialNo, $val);

			$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $parentID);
			$dataSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$totalSN = $dataSN->num_rows();

            if($DocTypeID=="IVST"){
                $value = array(
                    T_TransactionStockMovementDetail_Quantity2 => $totalSN,
                );
            }else{
                $value = array(
                    T_TransactionStockMovementDetail_Quantity1 => $totalSN,
                );
            }
			$this->db->where(T_TransactionStockMovementDetail_RecordID, $parentID);
			$this->db->update(T_TransactionStockMovementDetail, $value);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
			
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function readserialno()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockMovementDetailSerialNo;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionStockMovementDetailSerialNo_RecordID." as RecordID, ".
            T_TransactionStockMovementDetailSerialNo_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockMovementDetailSerialNo_RecordStatus." as RecordStatus, ".
            T_TransactionStockMovementDetailSerialNo_PRI." as PRI, ".
            T_TransactionStockMovementDetailSerialNo_SerialNo." as SerialNo ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function updateserialno(){
		$this->db->trans_begin();
		try{
			$parentID = $this->input->post("ParentRecordID");
			if(check_column(T_TransactionStockMovementDetailSerialNo_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementDetailSerialNo_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementDetailSerialNo_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementDetailSerialNo_PRI => empty($parentID)? 0:$parentID,
					T_TransactionStockMovementDetailSerialNo_SerialNo => empty($this->input->post("SerialNo"))? 0:$this->input->post("SerialNo")
				);

				$this->db->where(T_TransactionStockMovementDetailSerialNo_RecordID, $data[T_TransactionStockMovementDetailSerialNo_RecordID]);
				$this->db->update(T_TransactionStockMovementDetailSerialNo,$data);
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function deleteserialno()
	{
		$this->db->trans_begin();
		try{
			$RecordID = $this->input->post("RecordID");
			$parentID = $this->input->post("ParentRecordID");
			$DocTypeID = $this->input->post("DocTypeID");
			$this->db->delete(T_TransactionStockMovementDetailSerialNo, array(T_TransactionStockMovementDetailSerialNo_RecordID => $RecordID));

			$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $parentID);
			$dataSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$totalSN = $dataSN->num_rows();

            if($DocTypeID=="IVST"){
                $value = array(
                    T_TransactionStockMovementDetail_Quantity2 => $totalSN,
                );
            }else{
                $value = array(
                    T_TransactionStockMovementDetail_Quantity1 => $totalSN,
                );
            }
			$this->db->where(T_TransactionStockMovementDetail_RecordID, $parentID);
			$this->db->update(T_TransactionStockMovementDetail, $value);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function post()
	{
		$this->db->trans_begin();
		try{
			$voMsg = "";
			$RecordID = $this->input->post("RecordID");
			
			$this->db->from(T_TransactionStockMovementHeader);
			$this->db->where(T_TransactionStockMovementHeader_RecordID,$RecordID);
			$DataHeader = $this->db->get()->first_row("array");

			$this->db->from(T_TransactionStockMovementDetail);
			$this->db->where(T_TransactionStockMovementDetail_PRI,$RecordID);
			$DataDetail = $this->db->get()->result("array");

			//start update header
			$UpdateHeader = array(
				T_TransactionStockMovementHeader_RecordStatus=> 1
			);
			$this->db->where(T_TransactionStockMovementHeader_RecordID,$RecordID);
			$this->db->update(T_TransactionStockMovementHeader, $UpdateHeader);
			//end update header

			//start stock balance
			$doctype = $DataHeader[T_TransactionStockMovementHeader_DocTypeID];
			foreach($DataDetail as $detail_temp)
			{
				$this->db->from(T_TransactionStockBalanceHeader);
				$this->db->where(T_TransactionStockBalanceHeader_ItemID,$detail_temp[T_TransactionStockMovementDetail_ItemID]);
				$this->db->where(T_TransactionStockBalanceHeader_LocationID,$detail_temp[T_TransactionStockMovementDetail_LocationID1]);
				$DataStockBalance = $this->db->get()->first_row("array");

				if($doctype == "IMBB")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] + $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}else{
						$qty = $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						$InsertStockBalance = array(
							T_TransactionStockBalanceHeader_RecordStatus => 0,
							T_TransactionStockBalanceHeader_ItemID => $detail_temp[T_TransactionStockMovementDetail_ItemID],
							T_TransactionStockBalanceHeader_LocationID => $detail_temp[T_TransactionStockMovementDetail_LocationID1],
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->insert(T_TransactionStockBalanceHeader,$InsertStockBalance);
					}
				}

				if($doctype == "IVIO")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] - $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						if($qty < 0)
						{ 
							$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
						}else{
							// - on location1 start
							$UpdateStockBalance = array(
								T_TransactionStockBalanceHeader_Quantity => $qty
							);
							$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
							$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
							// - on location1 end

							// + on location2 start
							$this->db->from(T_TransactionStockBalanceHeader);
							$this->db->where(T_TransactionStockBalanceHeader_ItemID,$detail_temp[T_TransactionStockMovementDetail_ItemID]);
							$this->db->where(T_TransactionStockBalanceHeader_LocationID,$detail_temp[T_TransactionStockMovementDetail_LocationID2]);
							$DataStockBalance2 = $this->db->get()->first_row("array");
							
							if(count($DataStockBalance2) > 0)
							{
								$qty = $DataStockBalance2[T_TransactionStockBalanceHeader_Quantity] + $detail_temp[T_TransactionStockMovementDetail_Quantity1];
								$UpdateStockBalance = array(
									T_TransactionStockBalanceHeader_Quantity => $qty
								);
								$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance2[T_TransactionStockBalanceHeader_RecordID]);
								$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance2);
							}else{
								$qty = $detail_temp[T_TransactionStockMovementDetail_Quantity1];
								$InsertStockBalance = array(
									T_TransactionStockBalanceHeader_RecordStatus => 0,
									T_TransactionStockBalanceHeader_ItemID => $detail_temp[T_TransactionStockMovementDetail_ItemID],
									T_TransactionStockBalanceHeader_LocationID => $detail_temp[T_TransactionStockMovementDetail_LocationID2],
									T_TransactionStockBalanceHeader_Quantity => $qty
								);
								$this->db->insert(T_TransactionStockBalanceHeader,$InsertStockBalance);
							}
							// + on location2 end
						}
					}else{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}
				}

				if($doctype == "IVST")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty1 = $detail_temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
						$qty2 = $detail_temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
						$different = $qty2 - $qty1;
						$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] + $different;
						if($qty < 0)
						{
							$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
						}else{
							$UpdateStockBalance = array(
								T_TransactionStockBalanceHeader_Quantity => $qty
							);
							$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
							$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
						}
					}else{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}
				}

				if($doctype == "IVSA")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty1 = $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						$qty2 = $detail_temp[T_TransactionStockMovementDetail_Quantity2];
						$qty = $qty1 + $qty2;

						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}else{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}
				}
			}
			if($voMsg != ""){ throw new Exception($voMsg); }
			//end stock balance
			
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function unpost()
	{
		$this->db->trans_begin();
		try{
			$voMsg = "";
			$RecordID = $this->input->post("RecordID");
			
			$this->db->from(T_TransactionStockMovementHeader);
			$this->db->where(T_TransactionStockMovementHeader_RecordID,$RecordID);
			$DataHeader = $this->db->get()->first_row("array");

			$this->db->from(T_TransactionStockMovementDetail);
			$this->db->where(T_TransactionStockMovementDetail_PRI,$RecordID);
			$DataDetail = $this->db->get()->result("array");

			//start update header
			$UpdateHeader = array(
				T_TransactionStockMovementHeader_RecordStatus=> 0
			);
			$this->db->where(T_TransactionStockMovementHeader_RecordID,$RecordID);
			$this->db->update(T_TransactionStockMovementHeader, $UpdateHeader);
			//end update header

			//start stock balance
			$doctype = $DataHeader[T_TransactionStockMovementHeader_DocTypeID];
			foreach($DataDetail as $detail_temp)
			{
				$this->db->from(T_TransactionStockBalanceHeader);
				$this->db->where(T_TransactionStockBalanceHeader_ItemID,$detail_temp[T_TransactionStockMovementDetail_ItemID]);
				$this->db->where(T_TransactionStockBalanceHeader_LocationID,$detail_temp[T_TransactionStockMovementDetail_LocationID1]);
				$DataStockBalance = $this->db->get()->first_row("array");

				if($doctype == "IMBB")
				{
					$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] - $detail_temp[T_TransactionStockMovementDetail_Quantity1];
					if($qty < 0)
					{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}
				}

				if($doctype == "IVIO")
				{
					$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] + $detail_temp[T_TransactionStockMovementDetail_Quantity1];
					$UpdateStockBalance = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);

					// - on location2 start
					$this->db->from(T_TransactionStockBalanceHeader);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$detail_temp[T_TransactionStockMovementDetail_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$detail_temp[T_TransactionStockMovementDetail_LocationID2]);
					$DataStockBalance2 = $this->db->get()->first_row("array");

					$qty = $DataStockBalance2[T_TransactionStockBalanceHeader_Quantity] - $detail_temp[T_TransactionStockMovementDetail_Quantity1];
					if($qty < 0)
					{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID2]." \n ";
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance2[T_TransactionStockBalanceHeader_RecordID]);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}
					// - on location2 end
				}

				if($doctype == "IVST")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty1 = $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						$qty2 = $detail_temp[T_TransactionStockMovementDetail_Quantity2];
						$different = $qty2 - $qty1;
						$qty = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] - $different;
						if($qty < 0)
						{
							$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
						}else{
							$UpdateStockBalance = array(
								T_TransactionStockBalanceHeader_Quantity => $qty
							);
							$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
							$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
						}
					}else{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}
				}
				if($doctype == "IVSA")
				{
					if(count($DataStockBalance) > 0)
					{
						$qty1 = $detail_temp[T_TransactionStockMovementDetail_Quantity1];
						$qty2 = $detail_temp[T_TransactionStockMovementDetail_Quantity2];
						$qty = $qty1 + $qty2;
						$qtyUpdate = $DataStockBalance[T_TransactionStockBalanceHeader_Quantity] - $qty2;
						if($DataStockBalance[T_TransactionStockBalanceHeader_Quantity] != $qty)
						{
							$voMsg .= "Cant unpost ".$DataStockBalance[T_TransactionStockBalanceHeader_ItemID]." on location ".$DataStockBalance[T_TransactionStockBalanceHeader_LocationID]." cause Quantity has been modified on stock balance.\n"; 
						}else{
							$UpdateStockBalance = array(
								T_TransactionStockBalanceHeader_Quantity => $qtyUpdate
							);
							$this->db->where(T_TransactionStockBalanceHeader_RecordID,$DataStockBalance[T_TransactionStockBalanceHeader_RecordID]);
							$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);	
						}
					}else{
						$voMsg .= $detail_temp[T_TransactionStockMovementDetail_ItemID]." not found in Location ".$detail_temp[T_TransactionStockMovementDetail_LocationID1]." \n ";
					}
				}
			}
			if($voMsg != ""){ throw new Exception($voMsg); }
			//end stock balance

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function readstockbalance()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionStockBalanceHeader_RecordID." as RecordID, ".
            T_TransactionStockBalanceHeader_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionStockBalanceHeader_RecordStatus." as RecordStatus, ".
            T_TransactionStockBalanceHeader_ItemID." as ItemID, ".
            T_MasterDataItem_ItemName." as ItemName, ".
            T_MasterDataItem_UOMID." as UOMID, ".
			T_MasterDataGeneralTableValue_Key." as Type, ".
            T_MasterDataItem_EPC." as EPC, ".
            T_MasterDataItem_Barcode." as Barcode, ".
            T_MasterDataItem_Picture." as Picture, ".
            T_TransactionStockBalanceHeader_LocationID." as LocationID, ".
			T_MasterDataLocation_LocationName." as LocationName, ".
            T_TransactionStockBalanceHeader_Quantity." as Quantity ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		if($getList->num_rows() > 0){
			foreach($getList->result() as $item)
			{
				$data[] = array(
					'RecordID' => $item->RecordID,
					'RecordTimestamp' => $item->RecordTimestamp,
					'RecordStatus' => $item->RecordStatus,
					'ItemID' => $item->ItemID,
					'ItemName' => $item->ItemName,
					'UOMID' => $item->UOMID,
					'Type' => $item->Type,
					'EPC' => $item->EPC,
					'Barcode' => $item->Barcode,
					'Picture' => $item->Picture,
					'LocationID' => $item->LocationID,
					'LocationName' => $item->LocationName,
					'Quantity' => $item->Quantity,
					'SN' => $this->getstockbalanceSN($item->RecordID,0)
				);
			}
			$info->data = $data;
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getstockbalanceSN($id=0,$json=1)
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id = ($id) ? $id : $_GET['PRI'];
		$select = T_TransactionStockBalanceDetail_SerialNo." as SerialNo ";
		$this->db->select($select);
		$this->db->where(T_TransactionStockBalanceDetail_PRI,$id);
		$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
		$sql = $this->db->get(T_TransactionStockBalanceDetail);
		$data = $sql->result();
		if($json){
			if($sql->num_rows()==0){
				$info->msg = "Data not found";
				$info->errorcode = 101;
			}
			$info->data = $data;
			$this->output->set_content_type('application/json')->set_output(json_encode($info));
		}else{
			return $data;
		}
		
	}

	public function insertItem()
	{
		$this->db->trans_begin();
		try{
			$item = $_POST;
			$val = array(
				T_TransactionStockMovementDetail_PRI => empty($item['parentID'])? 0:$item['parentID'],
				T_TransactionStockMovementDetail_RowIndex => empty($item['RowIndex'])? 0:$item['RowIndex'],
				T_TransactionStockMovementDetail_ItemID => empty($item['ItemID'])? ' ':$item['ItemID'],
				T_TransactionStockMovementDetail_Quantity1 => empty($item['Qty1'])? 0:$item['Qty1'],
				T_TransactionStockMovementDetail_Quantity2 => empty($item['Qty2'])? 0:$item['Qty2'],
				T_TransactionStockMovementDetail_LocationID1 => empty($item['LocationID1'])? ' ':$item['LocationID1'],
				T_TransactionStockMovementDetail_LocationID2 => empty($item['LocationID2'])? ' ':$item['LocationID2'],
				T_TransactionStockMovementDetail_EPC => strtoupper(empty($item['EPC'])? ' ':$item['EPC']),
				T_TransactionStockMovementDetail_Barcode => empty($item['Barcode'])? ' ':$item['Barcode'],
				T_TransactionStockMovementDetail_Remarks => empty($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
			);
			$this->db->insert(T_TransactionStockMovementDetail, $val);
			$parentItemID = $this->db->insert_id();
			if(!empty($item['SN'])){
				foreach ($item['SN'] as $sub) {
					$valSub = array(
						T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
						T_TransactionStockMovementDetailSerialNo_SerialNo => isset($sub) ? $sub : ""
					);
					$this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
				}
			}
			$this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function updateItem()
	{
		$this->db->trans_begin();
		try{
			$item = $_POST;
			$val = array(
				T_TransactionStockMovementDetail_PRI => empty($item['parentID'])? 0:$item['parentID'],
				T_TransactionStockMovementDetail_RowIndex => empty($item['RowIndex'])? 0:$item['RowIndex'],
				T_TransactionStockMovementDetail_ItemID => empty($item['ItemID'])? ' ':$item['ItemID'],
				T_TransactionStockMovementDetail_Quantity1 => empty($item['Qty1'])? 0:$item['Qty1'],
				T_TransactionStockMovementDetail_Quantity2 => empty($item['Qty2'])? 0:$item['Qty2'],
				T_TransactionStockMovementDetail_LocationID1 => empty($item['LocationID1'])? ' ':$item['LocationID1'],
				T_TransactionStockMovementDetail_LocationID2 => empty($item['LocationID2'])? ' ':$item['LocationID2'],
				T_TransactionStockMovementDetail_EPC => strtoupper(empty($item['EPC'])? ' ':$item['EPC']),
				T_TransactionStockMovementDetail_Barcode => empty($item['Barcode'])? ' ':$item['Barcode'],
				T_TransactionStockMovementDetail_Remarks => empty($item["RemarksDetail"])? ' ':$item["RemarksDetail"],
			);

			$parentItemID =0;
            if(empty($item['RecordID'])){ // Insert New
                $this->db->insert(T_TransactionStockMovementDetail, $val);
                $parentItemID = $this->db->insert_id();
            }elseif(!empty($item['RecordID'])){
                $this->db->where(T_TransactionStockMovementDetail_RecordID,$item['RecordID']);
                $this->db->update(T_TransactionStockMovementDetail, $val);
                $parentItemID = $item['RecordID'];
            }

			if(!empty($item['SN'])){
				$this->db->delete(T_TransactionStockMovementDetailSerialNo,array(T_TransactionStockMovementDetailSerialNo_PRI=>$parentItemID));
				foreach ($item['SN'] as $sub) {
					$valSub = array(
						T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
						T_TransactionStockMovementDetailSerialNo_SerialNo => isset($sub) ? $sub : ""
					);
					$this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
				}
			}
			$this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function deleteItem()
	{
		$this->db->trans_begin();
		try{
			$RecordID = $_POST['RecordID'];
			$this->db->delete(T_TransactionStockMovementDetail,array(T_TransactionStockMovementDetail_RecordID=>$RecordID));
			$this->db->delete(T_TransactionStockMovementDetailSerialNo,array(T_TransactionStockMovementDetailSerialNo_PRI=>$RecordID));
			$this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function gettotalstock()
	{
		$this->db->trans_begin();
		try{
			$doctype = $this->input->get("DocTypeID");
			//$sql = $this->db->query("select SUM(".T_TransactionStockBalanceHeader_Quantity.") as AllQty FROM ".T_TransactionStockBalanceHeader);
			$sql = $this->db->query("select COUNT(".T_TRANSINOUTDETAIL_RFID.") as AllQty FROM ".T_TRANSINOUTDETAIL."  where ".T_TRANSINOUTDETAIL_Flag." = 1");
			$res = ($sql->first_row()->AllQty) ? $sql->first_row()->AllQty : 0;
			$docNo = substr(DocNo($doctype), 2);
            $output = array('errorcode' => 0, 'msg' => 'success', 'Qty' => $res,'DocNo'=>$docNo);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getItemDO()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionDO;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
  $select = T_TransactionDO_RecordID." as RecordID, ".
            T_TransactionDO_RecordTimestamp." as RecordTimestamp, ".
            T_TransactionDO_RecordStatus." as RecordStatus, ".
            T_TransactionDO_DocNo." as DocNo, ";
        
		$getList = $this->Stockmovement_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter,$select);

		if($getList->num_rows() > 0){
			foreach($getList->result() as $item)
			{
				$data[] = array(
					'RecordID' => $item->RecordID,
					'RecordTimestamp' => $item->RecordTimestamp,
					'RecordStatus' => $item->RecordStatus,
					'DocNo' => $item->DocNo,
					'SN' => $this->getDoSN($item->RecordID,0)
				);
			}
			$info->data = $data;
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Stockmovement_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getDoSN($id=0,$json=1)
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id = ($id) ? $id : $_GET['PRI'];
		$select = T_TransactionDODetail_EPC." as SerialNo ";
		$this->db->select($select);
		$this->db->where(T_TransactionDODetail_PRI,$id);
		$sql = $this->db->get(T_TransactionDODetail);
		$data = $sql->result("array");
		$data = array_column($data,"SerialNo");
		$lastSN = "";
		$diffItem = array();
		sort($data);
		foreach($data as $DetailDO)
		{
			$snTag = substr($DetailDO,SNINDEX,SNLENGTH);
			$this->db->where(T_MasterDataItem_EPC,$snTag);
			$query = $this->db->get(T_MasterDataItem);
			$itemData = $query->first_row();
			if($lastSN != $snTag){
				$diffItem[$snTag] = array(
					'ItemID' => $itemData->{T_MasterDataItem_ItemID},
					'ItemName' => $itemData->{T_MasterDataItem_ItemName},
					'SN' => array($DetailDO)
				);
			}else{
				array_push($diffItem[$lastSN]['SN'],$DetailDO); 
			}
			$lastSN = $snTag;
		}
		if($json){
			if($sql->num_rows()==0){
				$info->msg = "Data not found";
				$info->errorcode = 101;
			}
			$info->data = $diffItem;
			$this->output->set_content_type('application/json')->set_output(json_encode($info));
		}else{
			return $diffItem;
		}
		
	}

	public function StockTakePost(){
		$this->db->trans_begin();
		try{
			$voMsg = "";
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
			$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
			$sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
			$allSN = $sqlAllSN->result("array");
			$allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
			$allSNCount = count($allSN);
			
			$PRISNDoc = array_column($get_data["Detail"],T_TransactionStockMovementDetail_RecordID);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);
			
			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
					$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
					$different = $qty2 - $qty1;
					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $different;
					if($qty < 0)
					{
						$voMsg .= $temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}

				}else{
                    $different = $temp[T_TransactionStockMovementDetail_Quantity2];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_ItemID => $temp[T_TransactionStockMovementDetail_ItemID],
						T_TransactionStockBalanceHeader_LocationID => $temp[T_TransactionStockMovementDetail_LocationID1],
						T_TransactionStockBalanceHeader_Quantity => $temp[T_TransactionStockMovementDetail_Quantity2],
					);
					$this->db->insert(T_TransactionStockBalanceHeader,$data_stocklist);
					$parent_id = $this->db->insert_id();
				}

				if($typeItem == SSTYPE){
					// get sn document
					$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
					$get_serial_no = $this->db->get(T_TransactionStockMovementDetailSerialNo);

					if($get_serial_no->num_rows() > 0){
						// get sn stock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$StockSN = $this->db->get(T_TransactionStockBalanceDetail);
						foreach ($StockSN->result("array") as $dataStockSN) {
							$dataL = array(
								T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
								T_StockTakeLog_SN => $dataStockSN[T_TransactionStockBalanceDetail_SerialNo],
								T_StockTakeLog_StockFlag => $dataStockSN[T_TransactionStockBalanceDetail_StockFlag]
							);
							$this->db->insert(T_StockTakeLog,$dataL);
						}
						// update sn to unstock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							// get sn 
							$this->db->where(T_TransactionStockBalanceDetail_PRI,$parent_id);
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){ // insert if not exist in stock
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $parent_id,
									T_TransactionStockBalanceDetail_RecordFlag => 2,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{ // update if exist in stock
								$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different,
					T_TransactionStockBalanceLog_ItemGroup => $temp[T_TransactionStockMovementDetail_ItemGroup],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				if($voMsg != ""){ throw new Exception($voMsg); }
			}

			$msgCustom = "";
			$snMiss = array();
			$snMiss1 = array();
			$snMiss2 = array();

			$diffSN1 = array_diff($allSN,$DocSN);
			$diffSNCount1 = count($diffSN1)*-1;

			// begin of miss condition item
			sort($diffSN1);
			foreach($diffSN1 as $itemDiff)
			{
				// $snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				// $this->db->where(T_MasterDataItem_EPC,$snTag);
				// $SqlSNData = $this->db->get(T_MasterDataItem);
				// $SNData = $SqlSNData->first_row("array");

				$this->db->where(T_TransactionStockBalanceHeader_EPC,$itemDiff);
                $this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
                $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                $SqlSNData = $this->db->get(T_TransactionStockBalanceHeader);
                if($SqlSNData->num_rows() == 0){
                    $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$itemDiff);
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
                    $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $SqlSNData = $this->db->get(T_TransactionStockBalanceDetail);
				}
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_ItemID,$SNData[T_MasterDataItem_ItemID]);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

					$snMiss1[$SNData[T_MasterDataItem_ItemID]][] = $itemDiff;
				}
			}
				
			foreach($snMiss1 as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_ItemID,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					// $this->db->where(T_MasterDataItem_EPC,$key);
					// $SqlSNData = $this->db->get(T_MasterDataItem);
					// $SNData = $SqlSNData->first_row("array");
					$this->db->where(T_TransactionStockBalanceHeader_EPC,$itemDiff);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
					$this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
					$SqlSNData = $this->db->get(T_TransactionStockBalanceHeader);
					if($SqlSNData->num_rows() == 0){
						$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$itemDiff);
						$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
						$this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
						$this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
						$SqlSNData = $this->db->get(T_TransactionStockBalanceDetail);
					}
					$SNData = $SqlSNData->first_row("array");
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss1[$key]);
					$different = $Sqty - $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty*-1,
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}
			// end of miss condition item

			$diffSN2 = array_diff($DocSN,$allSN);
			$diffSNCount2 = count($diffSN2);

			// begin of new condition item
			sort($diffSN2);
			foreach($diffSN2 as $itemDiff)
			{
				$snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$SqlSNData = $this->db->get(T_MasterDataItem);
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 1));

					$snMiss2[$snTag][] = $itemDiff;
				}
			}
				
			foreach($snMiss2 as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					$this->db->where(T_MasterDataItem_EPC,$key);
					$SqlSNData = $this->db->get(T_MasterDataItem);
					$SNData = $SqlSNData->first_row("array");
					
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss2[$key]);
					$different = $Sqty - $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty,
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}
				// end of new condition item

			$diffSN = array_merge($diffSN1,$diffSN2);
			$diffSNCount = $diffSNCount2." | ".$diffSNCount1;
			$FoundCount =  $diffSNCount2;
			$MissCount = $diffSNCount1;

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => "success");
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	
	public function StockTakeUnpost(){
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->getDetailStockTake($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->UpdateHeaderStockTake($data,$id);

			$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
			$this->db->where(T_TransactionStockBalanceDetail_StockFlag,0);
			$sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
			$allSN = $sqlAllSN->result("array");
			$allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
			$allSNCount = count($allSN);

			$PRISNDoc = array_column($get_data["Detail"],T_TransactionStockMovementDetail_RecordID);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);

			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->item_existStockTake($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);


				$get_stocklist = $get_stocklist->first_row("array");
				$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

				$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];
				$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];
				$different = $qty2 - $qty1;
				$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $different;
				if($qty < 0)
				{
					$voMsg .= $temp[T_TransactionStockMovementDetail_EPC]." < 0 in Stockbalance \n "; 
				}else{
					$UpdateStockBalance = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
				}

				if($typeItem == SSTYPE){
					$this->db->where(T_StockTakeLog_DocNo, $get_data[T_TransactionStockMovementHeader_RecordID]);
					$get_serial_no = $this->db->get(T_StockTakeLog);

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_StockTakeLog_SN]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() != 0){
								$serialNoExistsData = $serialNoExists->first_row('array');
								$flag = $serial_no_temp[T_StockTakeLog_StockFlag];
								$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_StockTakeLog_SN]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>$flag));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different*-1,
					T_TransactionStockBalanceLog_ItemGroup => $temp[T_TransactionStockMovementDetail_ItemGroup],
					T_TransactionStockBalanceLog_Status => $temp[T_TransactionStockMovementDetail_Status],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);
			}

			$diffSN = array_diff($allSN,$DocSN);
			$msgCustom = "";
			$snMiss = array();
			foreach($diffSN as $itemDiff)
			{
				$snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$SqlSNData = $this->db->get(T_MasterDataItem);
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 1));

					$snMiss[$snTag] = $itemDiff;
				}
			}

			foreach($snMiss as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					$this->db->where(T_MasterDataItem_EPC,$key);
					$SqlSNData = $this->db->get(T_MasterDataItem);
					$SNData = $SqlSNData->first_row("array");
					
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss[$key]);
					$different = $Sqty + $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty,
						T_TransactionStockBalanceLog_ItemGroup => $SNData[T_MasterDataItem_ItemGroup],
						T_TransactionStockBalanceLog_Status => $SNData[T_MasterDataItem_Status],
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}

			$this->db->where(T_StockTakeLog_DocNo,$id);
			$this->db->delete(T_StockTakeLog);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function PosPost(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";

		// $data = array(
        //     'parentID' => 4,
        //     'LocationID' => 'LC-00001',
        //     'data' => array(
        //         '30000888eeee4000007000000003',
        //         '30000888eeee4000007000000004',
        //         '30000888eeee4000007000000006',
        //     )
        // );
		$this->db->trans_begin();
		try{
			
		//get data
		// $id = $this->input->post("RecordID");
		$dataPost = $_POST;
		$id = $dataPost['id'];
		
		//update header
		$dataHeader = array(
			T_SalesInvoiceHeader_DocDate => convert_datetime(empty($dataPost["DocDate"])? date("Y-m-d g:i:s",now()): $dataPost["DocDate"]),
			T_SalesInvoiceHeader_DocStatus => empty($dataPost["DocStatus"])? 0: $dataPost["DocStatus"],
			T_SalesInvoiceHeader_TermofPayment => empty($dataPost["TermofPayment"])? "":$dataPost["TermofPayment"],
			T_SalesInvoiceHeader_AmountSubtotal => empty($dataPost["AmountSubtotal"])? 0:$dataPost["AmountSubtotal"],
			T_SalesInvoiceHeader_AmountVAT => empty($dataPost["AmountVAT"])? '0.07':$dataPost["AmountVAT"],
			T_SalesInvoiceHeader_AmountTotal => empty($dataPost["AmountTotal"])? 0:$dataPost["AmountTotal"],
			T_SalesInvoiceHeader_Payment => empty($dataPost["Payment"])? "":$dataPost["Payment"],
			T_SalesInvoiceHeader_Remarks => empty($dataPost["Remarks"])? ' ':$dataPost["Remarks"],
			T_SalesInvoiceHeader_IC => empty($dataPost["IC"])? ' ':$dataPost["IC"],
			T_SalesInvoiceHeader_CustomerName => empty($dataPost["CustomerName"])? ' ':$dataPost["CustomerName"],
			T_SalesInvoiceHeader_Address => empty($dataPost["Address"])? ' ':$dataPost["Address"],
			T_SalesInvoiceHeader_Phone => empty($dataPost["Phone"])? ' ':$dataPost["Phone"],
			T_SalesInvoiceHeader_Email => empty($dataPost["Email"])? ' ':$dataPost["Email"],
			T_SalesInvoiceHeader_Outstanding => empty($dataPost["Outstanding"])? ' ':$dataPost["Outstanding"],
			T_SalesInvoiceHeader_PaymentMode => empty($dataPost["PaymentMode"])? ' ':$dataPost["PaymentMode"],
		);
		$this->db->where(T_SalesInvoiceHeader_RecordID,$id);
        $this->db->update(T_SalesInvoiceHeader, $dataHeader);

		$get_data = $this->getDetailPos($id);
		
		$data = array(
			T_SalesInvoiceHeader_RecordStatus   => 1,
		);
		$this->UpdateHeaderPos($data,$id);
		foreach ($get_data["Detail"] as $temp) {
			//$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

			$this->db->from(T_TransactionStockBalanceHeader);
			$this->db->join(T_SalesInvoiceDetail.' i', 
						'i.'.T_SalesInvoiceDetail_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID, 
						'left'
						);
			$this->db->where(T_TransactionStockBalanceHeader_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
			$this->db->where(T_TransactionStockBalanceHeader_LocationID, $temp[T_SalesInvoiceDetail_LocationID]);
			$get_stocklist = $this->db->get();

			//echo $this->db->last_query();die;
			//print_r($get_stocklist->num_rows());die;
			
			// Start Get Master Item
			$this->db->from(T_MasterDataItem);
			$this->db->where(T_MasterDataItem_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
			$get_master_item = $this->db->get()->first_row("array");
			// End Get Master Item
	
				if($get_stocklist->num_rows() == 0)
				{
					$voMsg .= $temp[T_SalesInvoiceDetail_ItemName]." not exists in Location ".$temp[T_SalesInvoiceDetail_LocationID]." \n ";
					$this->Pos_model->Delete($id);
				}else{
					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $temp[T_SalesInvoiceDetail_Quantity];
					if($qty < 0){
						$voMsg .= $temp[T_SalesInvoiceDetail_ItemName]." < 0 in Location ".$temp[T_SalesInvoiceDetail_LocationID]." \n ";
					}
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

					if($get_master_item[T_MasterDataItem_AutoIDType] == SSTYPE){
						$this->db->from(T_SalesInvoiceDetailSerialNo);
						$this->db->where(T_SalesInvoiceDetailSerialNo_PRI,$temp[T_SalesInvoiceDetail_RecordID]);
						$get_serial_no = $this->db->get()->result("array");

						foreach ($get_serial_no as $get_serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $get_serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
							$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>0));

							//delete t7990 by epc
							$this->db->where("t7990f002", $get_serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
							$this->db->delete('t7990');
						}
					}

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $stocklistPRI,
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_SalesInvoiceHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_SalesInvoiceHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_SalesInvoiceDetail_RecordID],
						T_TransactionStockBalanceLog_Quantity => $temp[T_SalesInvoiceDetail_Quantity]*-1,
						T_TransactionStockBalanceLog_ItemGroup => $temp[T_SalesInvoiceDetail_ItemGroup],
						T_TransactionStockBalanceLog_Status => $temp[T_SalesInvoiceDetail_Status],
					);
					
				}
				$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				if($voMsg != ""){ throw new Exception($voMsg); }
		}
	    $this->db->trans_commit();
		$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function mail_attachment() {
		
		$dataPost = $_POST;
		$id = $dataPost['id'];
		try{

		//$this->load->library('pdf');
		$pdf = $this->pdf->load();
		
		$data = $this->Pos_model->getDetailInvoice($id);

		ini_set('memory_limit', '256M'); 
		
		$html = $this->load->view('print_item', $data, true);

		$pdf->WriteHTML($html); // write the HTML into the PDF
		ob_clean();
		$output = $_SERVER["DOCUMENT_ROOT"].'/redwhite/assets/upload/invoice/'.'Invoice_'.$data{T_SalesInvoiceHeader_DocNo}.'.pdf';
		$this->db->where(T_SalesInvoiceHeader_RecordID,$id);
		$this->db->update(T_SalesInvoiceHeader, array(T_SalesInvoiceHeader_Pdf => $output));
		//$pdf->Output("$output", "D"); // Download
		$pdf->Output("$output", 'F'); // Save to dir

		//get invoice data
		$get_data = $this->getDetailInvoice($id);
		
		$gst=$get_data[T_SalesInvoiceHeader_AmountVAT] * 100; 
		$gsttotal=$get_data[T_SalesInvoiceHeader_AmountVAT] * $get_data[T_SalesInvoiceHeader_AmountSubtotal];
		
		//email
		$filename=substr($get_data[T_SalesInvoiceHeader_Pdf],45);
		//print_r($filename);
		$path = $_SERVER["DOCUMENT_ROOT"]."/redwhite/assets/upload/invoice/";
		$mailto=$get_data[T_SalesInvoiceHeader_Email];
		$from_mail="redwhitemobile@yahoo.com.sg";//manz.esem@gmail.com";
		$from_name="Red White Mobile";
		$replyto="redwhitemobile@yahoo.com.sg";
		$subject="Invoice ".$get_data[T_SalesInvoiceHeader_CustomerName];
		$message='
		<html>
			<head>
				<title>Invoice</title>
			</head>
			<body>
				<table cellspacing="0" cellpadding="0" border="0" width="600" align="center" style="border-collapse:collapse;background-color:#ffffff">
			<tbody>
				<tr>
					<td style="padding:15px 20px 10px" bgcolor="#e70012">
						<table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;color:#ffffff">
						<tbody>
							<tr>
								<td width="280">
									<a href="" target="" data-saferedirecturl=""><img src="https://redwhitemobile.com/wp-content/uploads/2012/10/rwm-logo2.png" alt="Logo" height="40px" class="CToWUd"></a>
								</td>
								<td width="280" align="right" style="font-size:20px">
									Transaction Done
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:20px;padding:30px 20px 0;font-weight:bold;color:#4e4e4e">Halo '.$get_data[T_SalesInvoiceHeader_CustomerName].',</td>
				</tr>
				<tr>
					<td style="padding:10px 20px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:15px">
						<tbody>
							<tr>
								<td style="line-height:25px;color:#4f4f4f">Congratulations, you'."'".'ve made a purchase at Redwhite Mobile with the following details :
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:10px 20px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:15px">
						<tbody>
							<tr>
								<td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Transaction No:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em"><a href="" style="color:#42b549;text-decoration:none" target="" data-saferedirecturl="">'.$get_data[T_SalesInvoiceHeader_DocNo].'</a></td>
							</tr>
							<tr>
								<td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">I/C:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">'.$get_data[T_SalesInvoiceHeader_IC].'</td>
							</tr>
							<tr>
								<td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Customer Name:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">'.$get_data[T_SalesInvoiceHeader_CustomerName].'</td>
							</tr>
							<tr>
								<td width="220" style="padding:10px 0;vertical-align:top;line-height:1.6em">Purchase Date:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;vertical-align:top;line-height:1.6em">'.$get_data[T_SalesInvoiceHeader_DocDate].'</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:15px 20px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;border-top:3px solid #c9c9c9;border-bottom:3px solid #c9c9c9;font-size:15px">
						<tbody>
							<tr>
								<td style="padding:20px 0;font-weight:bold;color:#4f4f4f">Purchase Detail</td>
							</tr>
							<tr>
								<td style="padding:5px 0">
									<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
									<tbody>
										<tr>
											<th align="left" style="padding:10px 0;border-bottom:1px solid #dddddd" width="280">Item Name</th>
											<th align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="112">Quantity</th>
											<th align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="168">Price</th>
										</tr>
									</tbody>
									<tbody style="vertical-align:top;line-height:1.6em">';

									if(!empty($get_data["Detail"])){
										foreach ($get_data["Detail"] as $temp) {

										$message .='<tr>
												<td align="left" style="padding:10px 0;border-bottom:1px solid #dddddd;table-layout:fixed;" width="280px">'.$temp[T_SalesInvoiceDetail_Brand].'&nbsp; '.$temp[T_SalesInvoiceDetail_ItemName].'&nbsp; '.$temp[T_SalesInvoiceDetail_Color].'</td>
												<td align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="112">'.$temp[T_SalesInvoiceDetail_Quantity].' Pcs</td>
												<td align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="168">SGD'.money_format("%!i", $temp[T_SalesInvoiceDetail_LineTotal]).'</td>
											</tr>';
										}
									}
									$message .='</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="padding:0 0 15px">
									<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
									<tbody>
										<tr>
											<td align="right" width="392" style="padding:5px 0">GST ('.$gst.' %): </td>
											<td align="right" width="168" style="padding:5px 0">SGD'.money_format("%!i",$gsttotal).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="padding:5px 0">Amount Total: </td>
											<td align="right" width="168" style="padding:5px 0">SGD'.money_format("%!i",$get_data[T_SalesInvoiceHeader_AmountTotal]).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="font-weight:bold;padding:5px 0">Total Payment: </td>
											<td align="right" width="168" style="font-weight:bold;padding:5px 0">SGD'.money_format("%!i",$get_data[T_SalesInvoiceHeader_Payment]).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="font-weight:bold;padding:5px 0">Outstanding: </td>
											<td align="right" width="168" style="font-weight:bold;padding:5px 0">SGD'.money_format("%!i",$get_data[T_SalesInvoiceHeader_Outstanding]).'</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="padding:10px;border-collapse:collapse;background-color:#fbe3e4;font-size:13px;color:#404040;line-height:16px;letter-spacing:0.1px;border-top:2px solid #fbb3b4;border-bottom:2px solid #fbb3b4">
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="">
									<tbody>
										<tr>
											<td align="left">
												<span>Address:</span>
												<span style="display:block;line-height:1.6em">'.$get_data[T_SalesInvoiceHeader_Address].'
												<br>
												Telp: <a href="tel:0896-9452-0083" value="+6289694520083" target="_blank">'.$get_data[T_SalesInvoiceHeader_Phone].'</a>
												</span>
												<br>
												<b>Invoice files are included in the attachment </b></a>
												</span>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:20px" align="center">
						<a href="https://redwhitemobile.com/" style="display:inline-block;padding:20px 30px;background-color:#e70012;color:#ffffff;text-decoration:none;font-size:15px;font-weight:bold;border-radius:3px;width:210px" target="_blank" data-saferedirecturl="https://redwhitemobile.com/">More Product</a>
					</td>
				</tr>
				<tr>
					<td>
						<table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;clear:both!important;background-color:transparent;margin:0 0 10px;padding:0" bgcolor="transparent">
						<tbody>
							<tr style="margin:0;padding:0">
								<td style="margin:0;padding:0"></td>
								<td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0px">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
									<tbody>
										<tr>
											
											<td width="280" align="right" style="padding:0px 20px">
												<table border="0" style="border-collapse:collapse;margin-top:0px">
												<tbody>
													<tr>
														<td style="font-size:13px;color:#999999;margin-bottom:10px" align="right"></td>
													</tr>
													<tr>
														<td style="padding:2px"></td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
															<tbody>
																<tr>
																	<td>
																		<a href="http://" style="padding:0 5px" target="_blank" data-saferedirecturl=""><img src="https://ci3.googleusercontent.com/proxy/TL2aWzcuExbXf3EGgD4hhxN1OgBoO900bJFXeHL9RPOV5enqM0muRFAcmFQQb95MNv3VlbyEZud3OCZtLBFlL_uvglTqC6fs2yS3u_qQ6S13LkgOz8L0Udb7p_ez=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307802.png" alt="Google plus" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px" target="_blank" data-saferedirecturl=""><img src="https://ci6.googleusercontent.com/proxy/hpkmiDtehvMUkzI6NQ6XxbIyirGT45tdKbdIzboposYocCcs_dWmXX3aNbXt8ROnlJvo8CA6LphY5vLGZcKuOk02xDtOMbqED9SbOrrgqeUuIgxqlnlAtza2iLFy=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307762.png" alt="Facebook" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px"><img src="https://ci3.googleusercontent.com/proxy/GIsQ666UaY-N1YHMNhXRYjUTObALsZDyqQ51u2t8uFNXI1TlYtWJ4-1fQrLdutgnyNgM9qu43Ang9jKuZ38oUrCBsSD4YPslXDPvP6scSXyIJmyQ6VTB2c0aGVlw=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307869.png" alt="Twitter" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px"><img src="https://ci4.googleusercontent.com/proxy/JOX0gbWvJ4vDUCHPWyne4LMiRgkAfgnuTnWfu68HnovFepHvdjChQ7hrOSwMbKA53IPW0Xd3XS1I-Q8amoGXRxNvRMDCnO4GVAH2coMKT0cn2QMu6FMiENJXzfMM=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307832.png" alt="Instagram" height="44" class="CToWUd"></a>
																	</td>
																</tr>
															</tbody>
															</table>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
						<table align="center">
						<tbody>
							<tr style="margin:0;padding:0 0 0 0">
								<td style="display:block!important;width:600px!important;clear:both!important;margin:0 auto;padding:0">
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f7f7f7;font-size:13px;color:#999999;border-top:1px solid #dddddd">
									<tbody>
										<tr>
											<td width="600" align="center" style="padding:30px 20px 0">If you need help, use the <a href="http://" style="color:#42b549;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://fapp1.tokopedia.com/linktrack/lt.pl?id%3D16114%3DIRlUVgZSAwlXHgEDVAgJUQMJVE4%3DWQRYHBkGQ1ALeFUOAwwISFQLX0RSVwEEVgoBUwMMUAENUg9QAQ%3D%3D%26fl%3DChEQFkReHRcLB11DSkxWClxJAVZdBBgFWA4fXQUXVw1PEBc%3D%26ext%3DdXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9Q05UQ1Q%3D&amp;source=gmail&amp;ust=1511338198167000&amp;usg=AFQjCNF4IE7wk5nlQgPqQyewLoROlnPH4A"> Contact Us page</a></td>
										</tr>
										<tr>
											<td width="600" align="center" style="padding:10px 20px 30px">© '.date("Y").', <span class="il">Redwhite Mobile</span>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>
			</body>
		</html>';

		$file = $path.$filename;
		//print_r($file);die;
		$file_size = filesize($file);
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		$content = chunk_split(base64_encode($content));
		$uid = md5(uniqid(time()));

		$header = "From: ".$from_name." <".$from_mail.">\n";
		//$header .= 'Cc: hikmat.fauzy@globaltrendasia.com' . "\n";
		//$header .= 'Cc: arif.rahmanto@globaltrendasia.com' . "\n";
		$header .= "Reply-To: ".$replyto."\n";
		$header .= "MIME-Version: 1.0\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
		$emessage= "--".$uid."\n";
		$emessage.= "Content-type:text/html; charset=iso-8859-1\n";
		$emessage.= "Content-Transfer-Encoding: 7bit\n\n";
		$emessage .= $message."\n\n";
		$emessage.= "--".$uid."\n";
		$emessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n"; // use different content types here
		$emessage .= "Content-Transfer-Encoding: base64\n";
		$emessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
		$emessage .= $content."\n\n";
		$emessage .= "--".$uid."--";
		
		mail($mailto,$subject,$emessage,$header);
		
		$output = array('errorcode' => 0, 'msg' => 'Success send to'.$get_data[T_SalesInvoiceHeader_Email]);
		}catch(Exception $e)
		{
			$info = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getDetailInvoice($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceHeader);
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItemInvoice($data[T_SalesInvoiceHeader_RecordID]);
        }
        return $data;
    }

    public function getDetailItemInvoice($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceDetail);
        // $this->db->join(T_MasterDataItem.' a', 
        //                 'a.'.T_MasterDataItem_ItemID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemID, 
        //                 'left'
        //                 );
        $this->db->join(T_MasterDataItemGroup.' i', 
                        'i.'.T_MasterDataItemGroup_ID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemGroup, 
                        'left'
                        );
        $this->db->where(T_SalesInvoiceDetail_PRI, $id);
        $this->db->order_by(T_SalesInvoiceDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
    }

	public function getDetailPos($id){
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceHeader);
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItemPos($data[T_SalesInvoiceHeader_RecordID]);
        }
        return $data;
    }

    public function getDetailItemPos($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceDetail);
        $this->db->join(T_MasterDataItem.' i', 
                        'i.'.T_MasterDataItem_ItemID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemID, 
                        'left'
                        );
        $this->db->join(T_MasterDataGeneralTableValue.' m', 
        'm.'.T_MasterDataGeneralTableValue_RecordID.'= i.'.T_MasterDataItem_AutoIDType, 
        'left'
        );
        $this->db->where(T_SalesInvoiceDetail_PRI, $id);
        $this->db->order_by(T_SalesInvoiceDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
	}
	
	public function UpdateHeaderPos($data,$id){
        $this->db->trans_begin();
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $this->db->update(T_SalesInvoiceHeader,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return TRUE;
        }
    }
    
    public function getDetailStockTake($id){
        $this->db->select('*');
        $this->db->from(T_TransactionStockMovementHeader);
        $this->db->where(T_TransactionStockMovementHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItemStockTake($data[T_TransactionStockMovementHeader_RecordID]);
        }
        return $data;
    }

    public function getDetailItemStockTake($id)
    {
        $this->db->select('*');
        $this->db->from(T_TransactionStockMovementDetail);
        $this->db->join(T_MasterDataItem.' i', 
                        'i.'.T_MasterDataItem_ItemID.'='.T_TransactionStockMovementDetail.'.'.T_TransactionStockMovementDetail_ItemID, 
                        'left'
                        );
        $this->db->join(T_MasterDataLocation.' l', 
                        'l.'.T_MasterDataLocation_LocationID.'='.T_TransactionStockMovementDetail.'.'.T_TransactionStockMovementDetail_LocationID1, 
                        'left'
                        );
        $this->db->join(T_MasterDataGeneralTableValue.' m', 'm.'.T_MasterDataGeneralTableValue_RecordID.'= i.'.T_MasterDataItem_AutoIDType, 'left' );
        $this->db->where(T_TransactionStockMovementDetail_PRI, $id);
        $this->db->order_by(T_TransactionStockMovementDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
	}

	public function UpdateHeaderStockTake($data,$id){
        $this->db->trans_begin();
        $this->db->where(T_TransactionStockMovementHeader_RecordID, $id);
        $this->db->update(T_TransactionStockMovementHeader,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return TRUE;
        }
	}
	
	public function item_existStockTake($itemid,$itemloc,$type = ""){
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $itemid);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, $itemloc);
        if($type!=""){ $this->db->where(T_ItemMovementStocklistHeader_Type, $type); }
        return $this->db->get();
    }
	
}
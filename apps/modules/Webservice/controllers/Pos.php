<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2014, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Inventory Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 07.04.2017	
 * @author	    Hikmat Fauzy Erdiyana
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2014, Creative Plus Studio
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Pos extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Pos_model');
	}

	public function GetData()
	{
		try{
			$id = $this->input->get("RecordID");
			// $id = 1;
			$this->db->from(T_SalesInvoiceHeader);
			$this->db->where(T_SalesInvoiceHeader_RecordID, $id);
			$data = $this->db->get();
			if($data->num_rows() > 0)
			{
				$data = $data->first_row("array");
				$this->db->from(T_SalesInvoiceDetail);
				$this->db->where(T_SalesInvoiceDetail_PRI, $id);
				$GetDetail = $this->db->get();

				$data["Detail"] = $GetDetail->result("array");
				$output = array('errorcode' => 0, 'msg' => "success", 'data'=>$data);
			}else{
				$output = array('errorcode' => 100, 'msg' => "data not found");
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetDetail()
	{
		try{
			$id = $this->input->get("RecordID");
			// $id = 1;
			$this->db->from(T_SalesInvoiceDetail);
			$this->db->where(T_SalesInvoiceDetail_RecordID, $id);
			$data = $this->db->get();
			if($data->num_rows() > 0)
			{
				$data = $data->first_row("array");
				$output = array('errorcode' => 0, 'msg' => "success", 'data'=>$data);
			}else{
				$output = array('errorcode' => 100, 'msg' => "data not found");
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UpdateDetail()
	{
		$this->db->trans_begin();
		try{
			$id = $this->input->post("RecordID");
			$pri = $this->input->post("PRI");

			$data = array(
                T_SalesInvoiceDetail_Quantity => empty($this->input->post('Quantity'))? 0:$this->input->post('Quantity'),
                T_SalesInvoiceDetail_UnitPrice => empty($this->input->post('SellingPrice'))? 0:$this->input->post('SellingPrice'),
                T_SalesInvoiceDetail_LineTotal => empty($this->input->post('LineTotal'))? 0:$this->input->post('LineTotal'),
                T_SalesInvoiceDetail_Remarks => empty($this->input->post('Remarks'))? ' ':$this->input->post('Remarks')
			);
			$this->Pos_model->UpdateDetail($id,$data);
			$this->Pos_model->SumLineTotal($pri);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => "success");
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function deletedetail()
	{
		$this->db->trans_begin();
		try{
			$RecordID = $this->input->post("RecordID");
			$this->db->delete(T_SalesInvoiceDetail, array(T_SalesInvoiceDetail_RecordID => $RecordID));
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arif
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Stockmovement Modules
 *
 * Automation Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arif
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Automation extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Beginbalance_model','Stockbalance_model'));
	}

	public function EPC()
	{	
        // $data = array(
        //     'PRI' => 5,
        //     'LocationID' => 'LC01',
        //     'data' => array(
        //         '2015231798490002',
        //         '201700087652',
        //         '2015231798480002',
        //         '201700087653',
        //         '2015231798490001',
        //         '201523198571',
        //         '2015231798480001',
        //         '2015231798480003',
        //         '2015231798490003'
        //     )
        // );
        $data = $_POST;
        
        $data['data'] = array_unique($data['data']);
        sort($data['data']);
        $lastEPC = "";
        $sn = array();
        // foreach($data['data'] as $key => $epc)
        // {
        //     if(strlen($epc)==LENGTHEPC)// serial no
        //     {
        //         $snTag = substr($epc,0,LENGTHSN);
        //         if($snTag == $lastEPC)
        //         {   
        //             array_push($sn[$lastEPC],$epc);
        //             $qty = count($sn[$lastEPC]);
        //             array_push($sn,$qty);                    
        //         }else{
        //             $sn[$snTag][] = $epc;
        //             $lastEPC = $snTag;
        //         }
        //     }else{
        //         $sn[$epc] = $epc;
        //     }
        // }
      
        // print_out($sn);
        // die;
        $this->db->trans_begin();
		try{
            $parentItemID = 0;
            foreach($data['data'] as $key => $epc)
            {
		        $epc = strtoupper($epc);
                // cutting EPC prefix
                $length = strlen($epc);
                $epc = substr($epc,CUTPREFIX,$length);
                // checking epc for serialno
                $snTag = substr($epc,SNINDEX,SNLENGTH);
                $this->db->where(T_MasterDataItem_EPC,$snTag);
                $sql = $this->db->get(T_MasterDataItem);

                $key = $key + 1;
                if($sql->num_rows() != 0)// serial no
                {
                    $epcExist = $sql->first_row();
                    if($snTag != $lastEPC){
                        $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                        $this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
                        $sqlInDoc = $this->db->get(T_TransactionStockMovementDetail);
                        if($sqlInDoc->num_rows() == 0){ // checking item is already in document
                            $val = array(
                                T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                T_TransactionStockMovementDetail_RowIndex => $key,
                                T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                T_TransactionStockMovementDetail_Quantity1 => 1,
                                T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                                T_TransactionStockMovementDetail_EPC => strtoupper(empty($snTag)? ' ':$snTag)
                            );
                            $this->db->insert(T_TransactionStockMovementDetail, $val);
                            $parentItemID = $this->db->insert_id();

                            $valSub = array(
                                T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                                T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                            );
                            $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                            $sn[$snTag][] = $epc;
                            $lastEPC = $snTag;
                        }
                    }else{
                        $valSub = array(
                            T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                            T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                        );
                        $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                        array_push($sn[$lastEPC],$epc);
                        $Qty = count($sn[$lastEPC]);

                        $val = array(
                            T_TransactionStockMovementDetail_Quantity1 => empty($Qty)? 0:$Qty
                        );
                        $this->db->where(T_TransactionStockMovementDetail_RecordID,$parentItemID);
                        $this->db->update(T_TransactionStockMovementDetail, $val);
                    }
                        
                }else{// not serial no
                    $this->db->where(T_MasterDataItem_EPC,$epc);
                    $sql = $this->db->get(T_MasterDataItem);
                    if($sql->num_rows() != 0){
                        $epcExist = $sql->first_row();
                        $Qty2 = ($epcExist->{T_MasterDataItem_AutoIDType}==4) ? 1 : 0;
                        $val = array(
                            T_TransactionStockMovementDetail_PRI => empty($data['parentID'])? 0:$data['parentID'],
                            T_TransactionStockMovementDetail_RowIndex => $key,
                            T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                            T_TransactionStockMovementDetail_Quantity1 => empty($Qty2)? 0:$Qty2,
                            T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                            T_TransactionStockMovementDetail_EPC => strtoupper(empty($epc)? ' ':$epc)
                        );
                        $this->db->insert(T_TransactionStockMovementDetail, $val);
                    }
                     
                }
            }
            $this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

    public function StockIn()
	{	
        // $data = array(
        //     'parentID' => 27,
        //     'LocationID' => 'LC01',
        //     'data' => array(
        //         '30000888EEEE4000002000000002',
        //         '30000888EEEE4000002000000003',
        //         '30000888EEEE4000003000000002',
        //         '30000888EEEE4000003000000003'
        //     )
        // );
        $data = $_POST;
        
        $data['data'] = array_unique($data['data']);
        sort($data['data']);
        $lastEPC = "";
        $sn = array();
        $PRISNDoc = array();

        $this->db->where(T_TransactionStockMovementHeader_RecordID, $data['parentID']);
		$SQLheaderData = $this->db->get(T_TransactionStockMovementHeader);
		$headerData = $SQLheaderData->first_row();

        $reffDocDO = $headerData->{T_TransactionStockMovementHeader_ReffDocNo};
		$this->db->select(T_TransactionDO_RecordID);
        $this->db->where(T_TransactionDO_DocNo,$reffDocDO);
        $sqlDO = $this->db->get(T_TransactionDO);
		$DOData = $sqlDO->first_row();

		$this->db->select(T_TransactionDODetail_EPC);
        $this->db->where(T_TransactionDODetail_PRI,$DOData->{T_TransactionDO_RecordID});
        $sqlAllSN = $this->db->get(T_TransactionDODetail);
        $allSN = $sqlAllSN->result("array");
        $allSN = array_column($allSN,T_TransactionDODetail_EPC);

        $this->db->trans_begin();
		try{
            $parentItemID = 0;
            foreach($data['data'] as $key => $epc)
            {
		        $epc = strtoupper($epc);
                // cutting EPC prefix
                $length = strlen($epc);
                $epc = substr($epc,CUTPREFIX,$length);
                // checking epc for serialno
                $snTag = substr($epc,SNINDEX,SNLENGTH);
                $this->db->where(T_MasterDataItem_EPC,$snTag);
                $sql = $this->db->get(T_MasterDataItem);

                $key = $key + 1;
                if($sql->num_rows() != 0)// serial no
                {
                    $epcExist = $sql->first_row();
                    if($snTag != $lastEPC){
                        $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                        $this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
                        $sqlInDoc = $this->db->get(T_TransactionStockMovementDetail);
                        if($sqlInDoc->num_rows() == 0){ // checking item is already in document
                            $val = array(
                                T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                T_TransactionStockMovementDetail_RowIndex => $key,
                                T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                T_TransactionStockMovementDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                                T_TransactionStockMovementDetail_Type => empty($epcExist->{T_MasterDataItem_AutoIDType})? ' ':$epcExist->{T_MasterDataItem_AutoIDType},
                                T_TransactionStockMovementDetail_EPC => strtoupper(empty($snTag)? ' ':$snTag),
                                T_TransactionStockMovementDetail_Barcode => empty($epcExist->{T_MasterDataItem_Barcode})? ' ':$epcExist->{T_MasterDataItem_Barcode},
                                T_TransactionStockMovementDetail_Quantity1 => 1,
                                T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                                T_TransactionStockMovementDetail_EPC => strtoupper(empty($snTag)? ' ':$snTag)
                            );
                            $this->db->insert(T_TransactionStockMovementDetail, $val);
                            $parentItemID = $this->db->insert_id();

                            $valSub = array(
                                T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                                T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                            );
                            $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                            $sn[$snTag][] = $epc;
                            $lastEPC = $snTag;
                        }else{
                            $SNINdoc = $sqlInDoc->first_row();

                            $this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$SNINdoc->{T_TransactionStockMovementDetail_RecordID});
                            $this->db->where(T_TransactionStockMovementDetailSerialNo_SerialNo,$epc);
                            $sqlexistSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
                            $existSN = $sqlexistSN->num_rows();
                            if($existSN == 0){
                                $valSub = array(
                                    T_TransactionStockMovementDetailSerialNo_PRI => $SNINdoc->{T_TransactionStockMovementDetail_RecordID},
                                    T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                                );
                                $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);

                                $realQTY = $SNINdoc->{T_TransactionStockMovementDetail_Quantity1} + 1;
                                $val = array(
                                    T_TransactionStockMovementDetail_Quantity1 => $realQTY
                                );
                                $this->db->where(T_TransactionStockMovementDetail_RecordID,$SNINdoc->{T_TransactionStockMovementDetail_RecordID});
                                $this->db->update(T_TransactionStockMovementDetail, $val);
                            }
                        }
                    }else{
                        $valSub = array(
                            T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                            T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                        );
                        $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                        array_push($sn[$lastEPC],$epc);
                        $Qty = count($sn[$lastEPC]);

                        $val = array(
                            T_TransactionStockMovementDetail_Quantity1 => empty($Qty)? 0:$Qty
                        );
                        $this->db->where(T_TransactionStockMovementDetail_RecordID,$parentItemID);
                        $this->db->update(T_TransactionStockMovementDetail, $val);
                    }
                        
                }else{// not serial no
                    $this->db->where(T_MasterDataItem_EPC,$epc);
                    $sql = $this->db->get(T_MasterDataItem);
                    
                    if($sql->num_rows() != 0){
                        $epcExist = $sql->first_row();
                        $Qty2 = ($epcExist->{T_MasterDataItem_AutoIDType}==4) ? 1 : 0;
                        $val = array(
                            T_TransactionStockMovementDetail_PRI => empty($data['parentID'])? 0:$data['parentID'],
                            T_TransactionStockMovementDetail_RowIndex => $key,
                            T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                            T_TransactionStockMovementDetail_Quantity1 => empty($Qty2)? 0:$Qty2,
                            T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                            T_TransactionStockMovementDetail_EPC => strtoupper(empty($epc)? ' ':$epc)
                        );
                        $this->db->insert(T_TransactionStockMovementDetail, $val);
                    }
                     
                }
            }

            $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
            $SQLDataDetailDoc = $this->db->get(T_TransactionStockMovementDetail);
            $DataDetailDoc = $SQLDataDetailDoc->result("array");
            $PRISNDoc = array_column($DataDetailDoc,T_TransactionStockMovementDetail_RecordID);
            $PRISNDoc = array_unique($PRISNDoc);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);

            $allSNCount = count($allSN);
            $DocSNCount = count($DocSN);
            if($allSNCount > $DocSNCount){
                $diffSN = array_diff($allSN,$DocSN);
                $qtyBalance = $allSNCount;
                $qtyScaned = $DocSNCount;
                $qtyDiff = count($diffSN)*-1;
            }else{
                $diffSN = array_diff($DocSN,$allSN);
                $qtyBalance = $allSNCount;
                $qtyScaned = $DocSNCount;
                $qtyDiff = count($diffSN);
            }
            $diffItem = array();
            $lastSN = "";
            sort($diffSN);

            $this->db->where(T_LogDiffEPC_PRI,$data['parentID']);
            $this->db->delete(T_LogDiffEPC);

            $this->db->where(T_LogDiffEPDetail_PRI,$data['parentID']);            
            $this->db->delete(T_LogDiffEPDetail);
            foreach($diffSN as $dataSN)
            {
                $snTag = substr($dataSN,SNINDEX,SNLENGTH);
                $this->db->where(T_MasterDataItem_EPC,$snTag);
				$query = $this->db->get(T_MasterDataItem);
                $itemData = $query->first_row();
                if($lastSN != $snTag){
                    $diffItem[$snTag] = array(
                        'ItemID' => $itemData->{T_MasterDataItem_ItemID},
                        'ItemName' => $itemData->{T_MasterDataItem_ItemName},
                        'SN' => array($dataSN)
                    );
                }else{
                    array_push($diffItem[$lastSN]['SN'],$dataSN); 
                }
                $dataLog = array(
                    T_LogDiffEPDetail_PRI => $data['parentID'],
                    T_LogDiffEPDetail_EPC => $dataSN
                );
                $this->db->insert(T_LogDiffEPDetail,$dataLog);
                $lastSN = $snTag;
            }

            $dataLog = array(
                T_LogDiffEPC_PRI => $data['parentID'],
                T_LogDiffEPC_Balance => $qtyBalance,
                T_LogDiffEPC_Scanned => $qtyScaned,
                T_LogDiffEPC_Diff => $qtyDiff
            );
            $this->db->insert(T_LogDiffEPC,$dataLog);
            
            $this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

    public function EPCout()
	{
        $data = $_POST;

        $data['data'] = array_unique($data['data']);
        sort($data['data']);
        $lastEPC = "";
        $sn = array();
        $this->db->trans_begin();
		try{
            $parentItemID = 0;
            foreach($data['data'] as $key => $epc)
            {
		        $epc = strtoupper($epc);
                // cutting EPC prefix
                $length = strlen($epc);
                $epc = substr($epc,CUTPREFIX,$length);
                // checking epc for serialno
                $snTag = substr($epc,SNINDEX,SNLENGTH);
                $this->db->where(T_MasterDataItem_EPC,$snTag);
                $sql = $this->db->get(T_MasterDataItem);
                

                $key = $key + 1;
                if($sql->num_rows() != 0)/// serial no
                {
                    $epcExist = $sql->first_row();
                    $this->db->where(T_TransactionStockBalanceHeader_ItemID,$epcExist->{T_MasterDataItem_ItemID});
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $sql2 = $this->db->get(T_TransactionStockBalanceHeader);
                    
                    if($sql2->num_rows() > 0){
                        $StockExist = $sql2->first_row();
                        if($snTag != $lastEPC){

                            $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                            $this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
                            $sqlInDoc = $this->db->get(T_TransactionStockMovementDetail);
                            if($sqlInDoc->num_rows() == 0){ // checking item is already in document

                                $val = array(
                                    T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                    T_TransactionStockMovementDetail_RowIndex => $key,
                                    T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                    T_TransactionStockMovementDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                                    T_TransactionStockMovementDetail_Type => empty($epcExist->{T_MasterDataItem_AutoIDType})? ' ':$epcExist->{T_MasterDataItem_AutoIDType},
                                    T_TransactionStockMovementDetail_EPC => strtoupper(empty($snTag)? ' ':$snTag),
                                    T_TransactionStockMovementDetail_Barcode => empty($epcExist->{T_MasterDataItem_Barcode})? ' ':$epcExist->{T_MasterDataItem_Barcode},
                                    T_TransactionStockMovementDetail_Quantity1 => 1,
                                    T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                                    T_TransactionStockMovementDetail_LocationID2 => !isset($data['LocationIDto'])? ' ':$data['LocationIDto']
                                );
                                $this->db->insert(T_TransactionStockMovementDetail, $val);
                                $parentItemID = $this->db->insert_id();

                                $valSub = array(
                                    T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                                    T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                                );
                                $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                                $sn[$snTag][] = $epc;
                                $lastEPC = $snTag;
                            }
                        }else{
                            $valSub = array(
                                T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                                T_TransactionStockMovementDetailSerialNo_SerialNo => $epc
                            );
                            $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                            array_push($sn[$lastEPC],$epc);
                            $Qty = count($sn[$lastEPC]);

                            $val = array(
                                T_TransactionStockMovementDetail_Quantity1 => empty($Qty)? 0:$Qty
                            );
                            $this->db->where(T_TransactionStockMovementDetail_RecordID,$parentItemID);
                            $this->db->update(T_TransactionStockMovementDetail, $val);
                        }
                    }
                    
                }else{// not serial no
                    $this->db->where(T_MasterDataItem_EPC,$epc);
                    $sql = $this->db->get(T_MasterDataItem);
                    
                    if($sql->num_rows() != 0){
                        $epcExist = $sql->first_row();
                        $this->db->where(T_TransactionStockBalanceHeader_ItemID,$epcExist->{T_MasterDataItem_ItemID});
                        $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                        $sql2 = $this->db->get(T_TransactionStockBalanceHeader);

                        if($sql2->num_rows() > 0){
                            $StockExist = $sql2->first_row();
                            $Qty2 = ($epcExist->{T_MasterDataItem_AutoIDType}==4) ? 1 : 0;
                            $val = array(
                                T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                    T_TransactionStockMovementDetail_RowIndex => $key,
                                    T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                    T_TransactionStockMovementDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                                    T_TransactionStockMovementDetail_Type => empty($epcExist->{T_MasterDataItem_AutoIDType})? ' ':$epcExist->{T_MasterDataItem_AutoIDType},
                                    T_TransactionStockMovementDetail_EPC => strtoupper(empty($snTag)? ' ':$snTag),
                                    T_TransactionStockMovementDetail_Barcode => empty($epcExist->{T_MasterDataItem_Barcode})? ' ':$epcExist->{T_MasterDataItem_Barcode},
                                    T_TransactionStockMovementDetail_Quantity1 => empty($Qty2)? 0:$Qty2,
                                    T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                                    T_TransactionStockMovementDetail_LocationID2 => !isset($data['LocationIDto'])? ' ':$data['LocationIDto']
                            );
                            $this->db->insert(T_TransactionStockMovementDetail, $val);
                        }
                    }
                     
                }
            }
            $this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
    
     public function StockTake()
     {
        /* $data = array(
            'parentID' => 1,
            'LocationID' => '',
            'data' => array(
                '3000000000000000000000000450',
                '3000000000000000000000000461',
                '3000000000000000000000000644',
                '3000000000000000000000003284',
                '3000000000000000000000003275',
                '3000000000000000000000003267',
                '3000000000000000000000003276',
                '3000000000000000000000003271',
                '3000000000000000000000003279',
                '3000000000000000000000003259',
                '3000000000000000000000003273',
                '3000000000000000000000000459',
                '3000000000000000000000003264',
                '3000000000000000000000003263',
                '3000000000000000000000003156',
                '3000000000000000000000000239',
                '3000000000000000000000003154',
                '3000000000000000000000000850',
                '3000000000000000000000000143',
                '3000000000000000000000000004',
                '3000000000000000000000000018',
                '3000000000000000000000000083',
                '3000000000000000000000000551',
                '3000000000000000000000001057',
                '3000000000000000000000000213',
                '3000000000000000000000000055',
                '3000000000000000000000000358',
                '3000000000000000000000000107',
                '3000000000000000000000000827',
                '3000000000000000000000000681',
                '3000000000000000000000000017',
                '3000000000000000000000000674',
                '3000000000000000000000003245',
                '3000000000000000000000000952',
                '3000000000000000000000000601',
                '3000000000000000000000000252',
                '3000000000000000000000000614',
                '3000000000000000000000000400',
                '3000000000000000000000001101',
                '3000000000000000000000000007',
                '3000000000000000000000000486',
                '3000000000000000000000001171',
                '3000000000000000000000000198',
                '3000000000000000000000000531',
                '3000000000000000000000003014',
                '3000000000000000000000001088',
                '3000000000000000000000003002',
                '3000000000000000000000000258',
                '3000000000000000000000001169',
                '3000000000000000000000000082',
                '3000000000000000000000000190',
                '3000000000000000000000001129',
                '3000000000000000000000000394',
                '3000000000000000000000000381',
                '3000000000000000000000003195',
                '3000000000000000000000001098',
                '3000000000000000000000003010',
                '3000000000000000000000003234',
                '3000000000000000000000000483',
                '3000000000000000000000003161',
                '3000000000000000000000003001',
                '3000000000000000000000000390',
                '3000000000000000000000000375',
                '3000000000000000000000001011',
                '3000000000000000000000003029',
                '3000000000000000000000003139',
                '3000000000000000000000003265',
                '3000000000000000000000000842',
                '3000000000000000000000001127',
                '3000000000000000000000003023',
                '3000000000000000000000000587',
                '3000000000000000000000000693',
                '3000000000000000000000000113',
                '3000000000000000000000003152',
                '3000000000000000000000000474',
                '3000000000000000000000003035',
                '3000000000000000000000001070',
                '3000000000000000000000000612',
                '3000000000000000000000000872',
                '3000000000000000000000000812',
                '3000000000000000000000001103',
                '3000000000000000000000000010',
                '3000000000000000000000000222',
                '3000000000000000000000001178',
                '3000000000000000000000000171',
                '3000000000000000000000000809',
                '3000000000000000000000000440',
                '3000000000000000000000000469',
                '3000000000000000000000000071',
                '3000000000000000000000000997',
                '3000000000000000000000000145',
                '3000000000000000000000001110',
                '3000000000000000000000000427',
                '3000000000000000000000000657',
                '3000000000000000000000003008',
                '3000000000000000000000001151',
                '3000000000000000000000000419',
                '3000000000000000000000000475',
                '3000000000000000000000000152',
                '3000000000000000000000000267',
                '3000000000000000000000000402',
                '3000000000000000000000003076',
                '3000000000000000000000000345',
                '3000000000000000000000000684',
                '3000000000000000000000000668',
                '3000000000000000000000000553',
                '3000000000000000000000001166',
                '3000000000000000000000003252',
                '3000000000000000000000001056',
                '3000000000000000000000000223',
                '3000000000000000000000003131',
                '3000000000000000000000000966',
                '3000000000000000000000000294',
                '3000000000000000000000000176',
                '3000000000000000000000000566',
                '3000000000000000000000000629',
                '3000000000000000000000000993',
                '3000000000000000000000000003',
                '3000000000000000000000000512',
                '3000000000000000000000001120',
                '3000000000000000000000001184',
                '3000000000000000000000000449',
                '3000000000000000000000000935',
                '3000000000000000000000001083',
                '3000000000000000000000003222',
                '3000000000000000000000001049',
                '3000000000000000000000001092',
                '3000000000000000000000003053',
                '3000000000000000000000003063',
                '3000000000000000000000003206',
                '3000000000000000000000003235',
                '3000000000000000000000003200',
                '3000000000000000000000003220',
                '3000000000000000000000003247',
                '3000000000000000000000003176',
                '3000000000000000000000003045',
                '3000000000000000000000001121',
                '3000000000000000000000003054',
                '3000000000000000000000003060'
            )
        ); */
        $data = $_POST;
        $data['data'] = array_unique($data['data']);
        $RFID = array();
        $diffItem = array();
        $lastItemID = "";
        $this->db->trans_begin();
        try{
            if(empty($data["data"])){ throw new Exception('No item scanned..!'); }
            foreach($data['data'] as $key => $epc)
            {
                $epc = strtoupper($epc);
                $length = strlen($epc);
                if($length > 24){
                    $dif = $length - 24;
                    $RFID[] = substr($epc,$dif,$length);
                }
            }
            $this->db->order_by(T_MasterDataItem_Brand);
			$this->db->order_by(T_MasterDataItem_Model);
			$this->db->order_by(T_MasterDataItem_Color);
            $this->db->where(T_TRANSINOUTDETAIL_Flag,1);
            $this->db->where_in(T_TRANSINOUTDETAIL_RFID,$RFID);
            $this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TRANSINOUTDETAIL.".".T_TRANSINOUTDETAIL_ItemID);
            $this->db->select("*");
            $query = $this->db->get(T_TRANSINOUTDETAIL);
            $voitemData = $query->result();

            // Begin insert item scanned 
            if($query->num_rows() > 0)
            {
                $key = 1;
                foreach ($voitemData as $Item) {

                    $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                    $this->db->where(T_TransactionStockMovementDetail_EPC,$Item->{T_TRANSINOUTDETAIL_RFID});
                    $this->db->where(T_TransactionStockMovementDetail_IMEI,$Item->{T_TRANSINOUTDETAIL_IMEI});
                    $sqlItemExist = $this->db->get(T_TransactionStockMovementDetail);
                    if($sqlItemExist->num_rows() > 0){

                        $val = array(
                                T_TransactionStockMovementDetail_Quantity1 => 1,
                                T_TransactionStockMovementDetail_Quantity2 => 1,
                        );
                        $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                        $this->db->where(T_TransactionStockMovementDetail_EPC,$Item->{T_TRANSINOUTDETAIL_RFID});
                        $this->db->where(T_TransactionStockMovementDetail_IMEI,$Item->{T_TRANSINOUTDETAIL_IMEI});
                        $this->db->update(T_TransactionStockMovementDetail, $val);

                    }else{
                        $val = array(
                            T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                T_TransactionStockMovementDetail_RowIndex => $key,
                                T_TransactionStockMovementDetail_ItemID => empty($Item->{T_MasterDataItem_ItemID}) ? ' ':$Item->{T_MasterDataItem_ItemID},
                                T_TransactionStockMovementDetail_ItemName => empty($Item->{T_MasterDataItem_ItemName}) ? ' ':$Item->{T_MasterDataItem_ItemName},
                                T_TransactionStockMovementDetail_Type => empty($Item->{T_MasterDataItem_AutoIDType}) ? ' ':$Item->{T_MasterDataItem_AutoIDType},
                                T_TransactionStockMovementDetail_EPC => empty($Item->{T_TRANSINOUTDETAIL_RFID}) ? "":$Item->{T_TRANSINOUTDETAIL_RFID},
                                T_TransactionStockMovementDetail_Barcode => empty($Item->{T_MasterDataItem_Barcode}) ? ' ':$Item->{T_MasterDataItem_Barcode},
                                T_TransactionStockMovementDetail_Quantity1 => 1,
                                T_TransactionStockMovementDetail_Quantity2 => 1,
                                T_TransactionStockMovementDetail_LocationID1 => empty($dsata['LocationID']) ? ' ':$data['LocationID'],
                                T_TransactionStockMovementDetail_IMEI => empty($Item->{T_TRANSINOUTDETAIL_IMEI}) ? "":$Item->{T_TRANSINOUTDETAIL_IMEI},
                                T_TransactionStockMovementDetail_Brand => empty($Item->{T_MasterDataItem_Brand}) ? ' ':$Item->{T_MasterDataItem_Brand},
                                T_TransactionStockMovementDetail_Model => empty($Item->{T_MasterDataItem_Model}) ? ' ':$Item->{T_MasterDataItem_Model},
                                T_TransactionStockMovementDetail_Color => empty($Item->{T_MasterDataItem_Color}) ? ' ':$Item->{T_MasterDataItem_Color},
                                T_TransactionStockMovementDetail_Status => empty($Item->{T_MasterDataItem_Status}) ? ' ':$Item->{T_MasterDataItem_Status},
                                T_TransactionStockMovementDetail_ItemGroup => empty($Item->{T_MasterDataItem_GroupID}) ? ' ':$Item->{T_MasterDataItem_GroupID},
                        );
                        $this->db->insert(T_TransactionStockMovementDetail, $val);
                    }
                    $key++;
                }
            }
            // End insert item scanned

            // Begin Balance

            $this->db->where(T_TRANSINOUTDETAIL_Flag,1);
            if($this->input->post('Brand') && $this->input->post('Brand') != "All") {
                $this->db->where("t6010f004", $this->input->post('Brand'));
            }
            if($this->input->post('Model') && $this->input->post('Model') != "All") {
                $this->db->where("t6010f005", $this->input->post('Model'));
            }
            if($this->input->post('Color') && $this->input->post('Color') != "All" ) {
                $this->db->where("t6010f006", $this->input->post('Color'));
            }
            if($this->input->post('Status') && $this->input->post('Status') != "All") {
                $this->db->where("UPPER(t6010f007)", $this->input->post('Status'));
            }
            $this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TRANSINOUTDETAIL.".".T_TRANSINOUTDETAIL_ItemID);
            $this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
            $this->db->select("*");
            $sqlBalance = $this->db->get(T_TRANSINOUTDETAIL);
            // $fn = "D://log.txt";
            // $fp = fopen($fn, "a");
            // $rawPostData = file_get_contents('php://input');
            // fwrite($fp, date("l F d, Y, h:i A") . "," . $rawPostData . "\n".$this->db->last_query()); 
            // fclose($fp);
            $qtyDiff = ($query->num_rows() != 0) ? $query->num_rows() - $sqlBalance->num_rows() : 0;
            // End Balance

            // Begin Diff
            $dataBalance = $sqlBalance->result("array");
            $dataScaned = $query->result("array");
            
			$PRIBalance = array_column($dataBalance,"t6011f001");
            $PRIBalance = array_unique($PRIBalance);
            
			$PRIScan = array_column($dataScaned,"t6011f001");
            $PRIScan = array_unique($PRIScan);
            
            $vodiffItem = array_diff($PRIBalance,$PRIScan);
            if($vodiffItem){
                $this->db->order_by(T_MasterDataItem_Brand);
                $this->db->order_by(T_MasterDataItem_Model);
                $this->db->order_by(T_MasterDataItem_Color);
                $this->db->where(T_TRANSINOUTDETAIL_Flag,1);
                $this->db->where_in(T_TRANSINOUTDETAIL_IMEI,$vodiffItem);
                $this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".T_TRANSINOUTDETAIL.".".T_TRANSINOUTDETAIL_ItemID);
                $this->db->select("*");
                $query2 = $this->db->get(T_TRANSINOUTDETAIL);
                $poitemData = $query2->result();
                
                if($query2->num_rows() > 0)
                {
                    $i = $key;
                    foreach($poitemData as $itemData)
                    {
                        $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                        $this->db->where(T_TransactionStockMovementDetail_EPC,$itemData->{T_TRANSINOUTDETAIL_RFID});
                        $this->db->where(T_TransactionStockMovementDetail_IMEI,$itemData->{T_TRANSINOUTDETAIL_IMEI});
                        $sqlItemExist2 = $this->db->get(T_TransactionStockMovementDetail);
                        
                        if($sqlItemExist2->num_rows() == 0){
                            $val2 = array(
                                T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                    T_TransactionStockMovementDetail_RowIndex => $key,
                                    T_TransactionStockMovementDetail_ItemID => empty($itemData->{T_MasterDataItem_ItemID}) ? ' ':$itemData->{T_MasterDataItem_ItemID},
                                    T_TransactionStockMovementDetail_ItemName => empty($itemData->{T_MasterDataItem_ItemName}) ? ' ':$itemData->{T_MasterDataItem_ItemName},
                                    T_TransactionStockMovementDetail_Type => empty($itemData->{T_MasterDataItem_AutoIDType}) ? ' ':$itemData->{T_MasterDataItem_AutoIDType},
                                    T_TransactionStockMovementDetail_EPC => empty($itemData->{T_TRANSINOUTDETAIL_RFID}) ? "":$itemData->{T_TRANSINOUTDETAIL_RFID},
                                    T_TransactionStockMovementDetail_Barcode => empty($itemData->{T_MasterDataItem_Barcode}) ? ' ':$itemData->{T_MasterDataItem_Barcode},
                                    T_TransactionStockMovementDetail_Quantity1 => 1,
                                    T_TransactionStockMovementDetail_Quantity2 => 0,
                                    T_TransactionStockMovementDetail_LocationID1 => empty($dsata['LocationID']) ? ' ':$data['LocationID'],
                                    T_TransactionStockMovementDetail_IMEI => empty($itemData->{T_TRANSINOUTDETAIL_IMEI}) ? "":$itemData->{T_TRANSINOUTDETAIL_IMEI},
                                    T_TransactionStockMovementDetail_Brand => empty($itemData->{T_MasterDataItem_Brand}) ? ' ':$itemData->{T_MasterDataItem_Brand},
                                    T_TransactionStockMovementDetail_Model => empty($itemData->{T_MasterDataItem_Model}) ? ' ':$itemData->{T_MasterDataItem_Model},
                                    T_TransactionStockMovementDetail_Color => empty($itemData->{T_MasterDataItem_Color}) ? ' ':$itemData->{T_MasterDataItem_Color},
                                    T_TransactionStockMovementDetail_Status => empty($itemData->{T_MasterDataItem_Status}) ? ' ':$itemData->{T_MasterDataItem_Status},
                                    T_TransactionStockMovementDetail_ItemGroup => empty($itemData->{T_MasterDataItem_GroupID}) ? ' ':$itemData->{T_MasterDataItem_GroupID},
                            );
                            $this->db->insert(T_TransactionStockMovementDetail, $val2);
                        }
                        
                        if($lastItemID != $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}){
                            $diffItem[$itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}] = array(
                                'ItemID' => $itemData->{T_MasterDataItem_ItemID},
                                'ItemName' => $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color}." (-)",
                                'SN' => array(
                                    "RFID" => array($itemData->{T_TRANSINOUTDETAIL_RFID}),
                                    "IMEI" => array($itemData->{T_TRANSINOUTDETAIL_IMEI})
                                )
                            );
                        }else{
                            array_push($diffItem[$lastItemID]['SN']['RFID'],$itemData->{T_TRANSINOUTDETAIL_RFID}); 
                            array_push($diffItem[$lastItemID]['SN']['IMEI'],$itemData->{T_TRANSINOUTDETAIL_IMEI}); 
                        }
                        $lastItemID = $itemData->{T_MasterDataItem_Brand}." ".$itemData->{T_MasterDataItem_Model}." ".$itemData->{T_MasterDataItem_Color};
                        $i++;
                    }
                }
            }
            //End Diff

            $this->db->trans_commit();

            $output = array('errorcode' => 0, 'msg' => 'success', "qtyBalance" => $sqlBalance->num_rows(), "qtyScanned" => $query->num_rows(), "qtyDifference" => $qtyDiff, "dataMiss" => $diffItem, "id" => $data['parentID'] );
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
     }
     public function StockTakeOLD()
	{
        // $data = array(
        //     'parentID' => 5,
        //     'LocationID' => 'LC-00001',
        //     'data' => array(
        //         '30000888eeee4000007000000005',
        //         '30000888eeee4000007000000006',
        //         //'30000888eeee4000007000000004',
        //     )
        // );

        $data = $_POST;
      
        $data['data'] = array_unique($data['data']);
        sort($data['data']);
        $lastItemID = "";
        $ItemIDSN = array();
        $PRISNDoc = array();

        $this->db->select(T_TransactionStockBalanceDetail_SerialNo);
        $this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
        $sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
        $allSN = $sqlAllSN->result("array");
        $allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);

        $this->db->trans_begin();
		try{
            $parentItemID = 0;
            foreach($data['data'] as $key => $epc)
            {
		        $epc = strtoupper($epc);
                $length = strlen($epc);
                if($length > 24){
                    $dif = $length - 24;
                    $epc = substr($epc,$dif,$length);
                }
                $SSType = false;
                // checking epc for serialno OLDER
                // $snTag = substr($epc,SNINDEX,SNLENGTH);
                // $this->db->where(T_MasterDataItem_EPC,$snTag);
                // $sql = $this->db->get(T_MasterDataItem); END OLDER

                $this->db->where(T_TransactionStockBalanceHeader_EPC,$epc);
                $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                $sql = $this->db->get(T_TransactionStockBalanceHeader);
                if($sql->num_rows() == 0){
                    $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$epc);
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $sql = $this->db->get(T_TransactionStockBalanceDetail);
                    $SSType = true;
                }
                //print_r($sql->first_row()); die;
                $key = $key + 1;
                if($SSType)/// serial no
                {
                    $epcExist = $sql->first_row();
                    if($epcExist->{T_MasterDataItem_ItemID} != $lastItemID){
                        
                        $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
                        $this->db->where(T_TransactionStockMovementDetail_EPC,$epc);
                        $sqlInDoc = $this->db->get(T_TransactionStockMovementDetail);
                        if($sqlInDoc->num_rows() == 0){ // checking item is already in document
                            $val = array(
                                T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                T_TransactionStockMovementDetail_RowIndex => $key,
                                T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                T_TransactionStockMovementDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                                T_TransactionStockMovementDetail_Type => empty($epcExist->{T_MasterDataItem_AutoIDType})? ' ':$epcExist->{T_MasterDataItem_AutoIDType},
                                T_TransactionStockMovementDetail_EPC => "",
                                T_TransactionStockMovementDetail_Barcode => empty($epcExist->{T_MasterDataItem_Barcode})? ' ':$epcExist->{T_MasterDataItem_Barcode},
                                T_TransactionStockMovementDetail_Quantity1 => isset($epcExist->{T_TransactionStockBalanceHeader_Quantity}) ? $epcExist->{T_TransactionStockBalanceHeader_Quantity} : 0,
                                T_TransactionStockMovementDetail_Quantity2 => 1,
                                T_TransactionStockMovementDetail_LocationID1 => empty($data['LocationID'])? ' ':$data['LocationID'],
                                T_TransactionStockMovementDetail_LocationID2 => !isset($data['LocationIDto'])? ' ':$data['LocationIDto'],
                                T_TransactionStockMovementDetail_IMEI => "",
                                T_TransactionStockMovementDetail_Brand => empty($epcExist->{T_MasterDataItem_Brand})? ' ':$epcExist->{T_MasterDataItem_Brand},
                                T_TransactionStockMovementDetail_Model => empty($epcExist->{T_MasterDataItem_Model})? ' ':$epcExist->{T_MasterDataItem_Model},
                                T_TransactionStockMovementDetail_Color => empty($epcExist->{T_MasterDataItem_Color})? ' ':$epcExist->{T_MasterDataItem_Color},
                                T_TransactionStockMovementDetail_Status => empty($epcExist->{T_MasterDataItem_Status})? ' ':$epcExist->{T_MasterDataItem_Status},
                                T_TransactionStockMovementDetail_ItemGroup => empty($epcExist->{T_MasterDataItem_GroupID})? ' ':$epcExist->{T_MasterDataItem_GroupID},
                            );
                            $this->db->insert(T_TransactionStockMovementDetail, $val);
                            $parentItemID = $this->db->insert_id();
                            
                            $valSub = array(
                                T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                                T_TransactionStockMovementDetailSerialNo_SerialNo => $epc,
                                T_TransactionStockMovementDetailSerialNo_IMEI => $epcExist->{T_TransactionStockBalanceDetail_IMEI},
                            );
                            $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                            $ItemIDSN[$epcExist->{T_MasterDataItem_ItemID}][] = $epc;
                            $lastItemID = $epcExist->{T_MasterDataItem_ItemID};
                        }else{
                            $SNINdoc = $sqlInDoc->first_row();

                            $this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$SNINdoc->{T_TransactionStockMovementDetail_RecordID});
                            $this->db->where(T_TransactionStockMovementDetailSerialNo_SerialNo,$epc);
                            $sqlexistSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
                            $existSN = $sqlexistSN->num_rows();
                            if($existSN == 0){
                                $valSub = array(
                                    T_TransactionStockMovementDetailSerialNo_PRI => $SNINdoc->{T_TransactionStockMovementDetail_RecordID},
                                    T_TransactionStockMovementDetailSerialNo_SerialNo => $epc,
                                    T_TransactionStockMovementDetailSerialNo_IMEI => $epcExist->{T_TransactionStockBalanceDetail_IMEI},
                                );
                                $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);

                                $realQTY = $SNINdoc->{T_TransactionStockMovementDetail_Quantity2} + 1;
                                $val = array(
                                    T_TransactionStockMovementDetail_Quantity1 => isset($epcExist->{T_TransactionStockBalanceHeader_Quantity}) ? $epcExist->{T_TransactionStockBalanceHeader_Quantity} : 0,
                                    T_TransactionStockMovementDetail_Quantity2 => $realQTY
                                );
                                $this->db->where(T_TransactionStockMovementDetail_RecordID,$SNINdoc->{T_TransactionStockMovementDetail_RecordID});
                                $this->db->update(T_TransactionStockMovementDetail, $val);
                            }
                        }
                    }else{
                        $valSub = array(
                            T_TransactionStockMovementDetailSerialNo_PRI => $parentItemID,
                            T_TransactionStockMovementDetailSerialNo_SerialNo => $epc,
                            T_TransactionStockMovementDetailSerialNo_IMEI => $epcExist->{T_TransactionStockBalanceDetail_IMEI}
                        );
                        $this->db->insert(T_TransactionStockMovementDetailSerialNo,$valSub);
                        array_push($ItemIDSN[$lastItemID],$epc);
                        $Qty = count($ItemIDSN[$lastItemID]);

                        $val = array(
                            T_TransactionStockMovementDetail_Quantity1 => isset($epcExist->{T_TransactionStockBalanceHeader_Quantity}) ? $epcExist->{T_TransactionStockBalanceHeader_Quantity} : 0,
                            T_TransactionStockMovementDetail_Quantity2 => empty($Qty)? 0:$Qty
                        );
                        $this->db->where(T_TransactionStockMovementDetail_RecordID,$parentItemID);
                        $this->db->update(T_TransactionStockMovementDetail, $val);
                    }
                }else{// not serial no
                    // $this->db->where(T_MasterDataItem_EPC,$epc);
                    // $sql = $this->db->get(T_MasterDataItem);
                   
                    if($sql->num_rows() != 0){
                        $epcExist = $sql->first_row();
                        $Qty2 = ($epcExist->{T_MasterDataItem_AutoIDType}==4) ? 1 : 0;
                        $val = array(
                            T_TransactionStockMovementDetail_PRI => empty($data['parentID']) ? 0 :  $data['parentID'],
                                T_TransactionStockMovementDetail_RowIndex => $key,
                                T_TransactionStockMovementDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                                T_TransactionStockMovementDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                                T_TransactionStockMovementDetail_Type => empty($epcExist->{T_MasterDataItem_AutoIDType})? ' ':$epcExist->{T_MasterDataItem_AutoIDType},
                                T_TransactionStockMovementDetail_EPC => strtoupper(empty($epc)? ' ':$epc),
                                T_TransactionStockMovementDetail_Barcode => empty($epcExist->{T_MasterDataItem_Barcode})? ' ':$epcExist->{T_MasterDataItem_Barcode},
                                T_TransactionStockMovementDetail_Quantity1 => isset($epcExist->{T_TransactionStockBalanceHeader_Quantity}) ? $epcExist->{T_TransactionStockBalanceHeader_Quantity} : 0,
                                T_TransactionStockMovementDetail_Quantity2 => empty($Qty2)? 0:$Qty2,
                                T_TransactionStockMovementDetail_LocationID1 => empty($dsata['LocationID'])? ' ':$data['LocationID'],
                                T_TransactionStockMovementDetail_LocationID2 => !isset($data['LocationIDto'])? ' ':$data['LocationIDto'],
                                T_TransactionStockMovementDetail_IMEI => empty($epcExist->{T_TransactionStockBalanceDetail_IMEI}) ? "":$epcExist->{T_TransactionStockBalanceDetail_IMEI},
                                T_TransactionStockMovementDetail_Brand => empty($epcExist->{T_MasterDataItem_Brand})? ' ':$epcExist->{T_MasterDataItem_Brand},
                                T_TransactionStockMovementDetail_Model => empty($epcExist->{T_MasterDataItem_Model})? ' ':$epcExist->{T_MasterDataItem_Model},
                                T_TransactionStockMovementDetail_Color => empty($epcExist->{T_MasterDataItem_Color})? ' ':$epcExist->{T_MasterDataItem_Color},
                                T_TransactionStockMovementDetail_Status => empty($epcExist->{T_MasterDataItem_Status})? ' ':$epcExist->{T_MasterDataItem_Status},
                                T_TransactionStockMovementDetail_ItemGroup => empty($epcExist->{T_MasterDataItem_GroupID})? ' ':$epcExist->{T_MasterDataItem_GroupID},
                        );
                        $this->db->insert(T_TransactionStockMovementDetail, $val);
                    }
                     
                }
                //$PRISNDoc[] = $parentItemID;
            }

            $this->db->where(T_TransactionStockMovementDetail_PRI,$data['parentID']);
            $SQLDataDetailDoc = $this->db->get(T_TransactionStockMovementDetail);
            $DataDetailDoc = $SQLDataDetailDoc->result("array");
            $PRISNDoc = array_column($DataDetailDoc,T_TransactionStockMovementDetail_RecordID);
            $PRISNDoc = array_unique($PRISNDoc);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);

			$DocSNCount = count($DocSN);
            $allSNCount = count($allSN);
            $diffItem = array();
            $lastItemID = "";
            // if($allSNCount == $DocSNCount){ // balance qty
                $this->db->where(T_LogDiffEPC_PRI,$data['parentID']);
                $this->db->delete(T_LogDiffEPC);

                $this->db->where(T_LogDiffEPDetail_PRI,$data['parentID']);            
                $this->db->delete(T_LogDiffEPDetail);

                $diffSN1 = array_diff($allSN,$DocSN);
				$qtyDiff1 = count($diffSN1)*-1;

                sort($diffSN1);
                foreach($diffSN1 as $dataSN)
                {
                    // $snTag = substr($dataSN,SNINDEX,SNLENGTH);
                    // $this->db->where(T_MasterDataItem_EPC,$snTag);
                    // $query = $this->db->get(T_MasterDataItem);
                    // $itemData = $query->first_row();

                    $this->db->where(T_TransactionStockBalanceHeader_EPC,$dataSN);
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $query = $this->db->get(T_TransactionStockBalanceHeader);
                    if($query->num_rows() == 0){
                        $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$dataSN);
                        $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                        $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                        $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                        $query = $this->db->get(T_TransactionStockBalanceDetail);
                    }
                    $itemData = $query->first_row();
                    if($lastItemID != $itemData->{T_MasterDataItem_ItemID}){
                        $diffItem[$itemData->{T_MasterDataItem_ItemID}] = array(
                            'ItemID' => $itemData->{T_MasterDataItem_ItemID},
                            'ItemName' => $itemData->{T_MasterDataItem_ItemName},
                            'SN' => array($dataSN)
                        );
                    }else{
                        array_push($diffItem[$lastItemID]['SN'],$dataSN); 
                    }
                    $dataLog = array(
                        T_LogDiffEPDetail_PRI => $data['parentID'],
                        T_LogDiffEPDetail_EPC => $dataSN
                    );
                    $this->db->insert(T_LogDiffEPDetail,$dataLog);
                    $lastItemID = $itemData->{T_MasterDataItem_ItemID};
                }

                $diffSN2 = array_diff($DocSN,$allSN);
				$qtyDiff2 = count($diffSN2);

                sort($diffSN2);
                foreach($diffSN2 as $dataSN)
                {
                    // $snTag = substr($dataSN,SNINDEX,SNLENGTH);
                    // $this->db->where(T_MasterDataItem_EPC,$snTag);
                    // $query = $this->db->get(T_MasterDataItem);
                    // $itemData = $query->first_row();

                    $this->db->where(T_TransactionStockBalanceHeader_EPC,$dataSN);
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                    $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $query = $this->db->get(T_TransactionStockBalanceHeader);
                    if($query->num_rows() == 0){
                        $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$dataSN);
                        $this->db->where(T_TransactionStockBalanceHeader_LocationID,$data['LocationID']);
                        $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                        $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                        $query = $this->db->get(T_TransactionStockBalanceDetail);
                    }
                    $itemData = $query->first_row();
                    if($lastItemID != $itemData->{T_MasterDataItem_ItemID}){
                        $diffItem[$itemData->{T_MasterDataItem_ItemID}] = array(
                            'ItemID' => $itemData->{T_MasterDataItem_ItemID},
                            'ItemName' => $itemData->{T_MasterDataItem_ItemName},
                            'SN' => array($dataSN)
                        );
                    }else{
                        array_push($diffItem[$lastItemID]['SN'],$dataSN); 
                    }
                    $dataLog = array(
                        T_LogDiffEPDetail_RecordStatus => 1,
                        T_LogDiffEPDetail_PRI => $data['parentID'],
                        T_LogDiffEPDetail_EPC => $dataSN
                    );
                    $this->db->insert(T_LogDiffEPDetail,$dataLog);
                    $lastItemID = $itemData->{T_MasterDataItem_ItemID};
                }

                $qtyDiff = $qtyDiff2." and ".$qtyDiff1;

            $dataLog = array(
                T_LogDiffEPC_PRI => $data['parentID'],
                T_LogDiffEPC_Balance => $allSNCount,
                T_LogDiffEPC_Scanned => $DocSNCount,
                T_LogDiffEPC_Diff => $qtyDiff
            );
            $this->db->insert(T_LogDiffEPC,$dataLog);
            
            $this->db->trans_commit();
           
            $output = array('errorcode' => 0, 'msg' => 'success', "qtyBalance" => $allSNCount, "qtyScanned" => $DocSNCount, "qtyDifference" => $qtyDiff, "dataMiss" => $diffItem,"id"=>$data['parentID']);
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

    public function Pos()
	{
        $this->db->trans_begin();
		try{
            // $data = array(
            //     'parentID' => 4,
            //     'LocationID' => 'LC-00001',
            //     'data' => array(
            //         '30000888eeee4000007000000003',
            //         '30000888eeee4000007000000004',
            //         '30000888eeee4000007000000006',
            //     )
            // );
            $data = $_POST;
            //print_r($data);die;
            if(!empty($data['data'])){
                $dataHeader = array(
                    T_SalesInvoiceHeader_DocTypeID => "SLPS",
                    T_SalesInvoiceHeader_DocNo => substr(DocNo("SLPS"), 2),
                    T_SalesInvoiceHeader_DocDate => convert_datetime(empty($data["DocDate"])? date("Y-m-d g:i:s",now()): $data["DocDate"]),
                    T_SalesInvoiceHeader_DocStatus => empty($data["DocStatus"])? 0: $data["DocStatus"],
                    T_SalesInvoiceHeader_TermofPayment => empty($data["TermofPayment"])? "":$data["TermofPayment"],
                    T_SalesInvoiceHeader_CurrencyID => empty($data["CurrencyID"])? "":$data["CurrencyID"],
                    T_SalesInvoiceHeader_ExchangeRate => empty($data["ExchRate"])? 0:$data["ExchRate"],
                    T_SalesInvoiceHeader_SalespersonID => empty($data["SalespersonID"])? "":$data["SalespersonID"],
                    T_SalesInvoiceHeader_BizPartnerID => empty($data["BizPartnerID"])? "":$data["BizPartnerID"],
                    T_SalesInvoiceHeader_AmountSubtotal => empty($data["AmountSubtotal"])? 0:$data["AmountSubtotal"],
                    T_SalesInvoiceHeader_AmountVAT => empty($data["AmountVAT"])? '0.07':$data["AmountVAT"],
                    T_SalesInvoiceHeader_AmountTotal => empty($data["AmountTotal"])? 0:$data["AmountTotal"],
                    T_SalesInvoiceHeader_Payment => empty($data["Payment"])? "":$data["Payment"],
                    T_SalesInvoiceHeader_Remarks => empty($data["Remarks"])? ' ':$data["Remarks"],
                    T_SalesInvoiceHeader_IC => empty($data["IC"])? ' ':$data["IC"],
                    T_SalesInvoiceHeader_CustomerName => empty($data["CustomerName"])? ' ':$data["CustomerName"],
                    T_SalesInvoiceHeader_Address => empty($data["Address"])? ' ':$data["Address"],
                    T_SalesInvoiceHeader_Phone => empty($data["Phone"])? ' ':$data["Phone"],
                    T_SalesInvoiceHeader_Email => empty($data["Email"])? ' ':$data["Email"],
                    T_SalesInvoiceHeader_Outstanding => empty($data["Outstanding"])? ' ':$data["Outstanding"],
                    T_SalesInvoiceHeader_PaymentMode => empty($data["PaymentMode"])? ' ':$data["PaymentMode"],
                );

                $this->db->insert(T_SalesInvoiceHeader, $dataHeader);
                $parentID = $this->db->insert_id();
                UpdateDocNo("SLPS");

                $this->db->trans_commit();
    			$this->db->trans_begin();

                foreach($data['data'] as $key => $epc)
                {
                    $key = $key + 1;
                    $rfid=substr($epc,4);
                    
                    //$this->db->where(T_MasterDataItem_EPC,$snTag);
                    //$sql = $this->db->get(T_MasterDataItem);

                    $this->db->select('*');
                    $this->db->from(T_TransactionStockBalanceHeader);
                    $this->db->join(T_MasterDataItem.' i','i.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $this->db->where('i.'.T_MasterDataItem_EPC, $rfid);
                    //$this->db->where(T_TransactionStockBalanceHeader_Quantity, '> 0');
                    $sql = $this->db->get();

                    //print_r($sql);die;
                    //echo $this->db->last_query();die;
                    
                    if($sql->num_rows() != 0)
                    {
                        $epcExist = $sql->first_row();
                        $this->db->where(T_TransactionStockBalanceHeader_ItemID,$epcExist->{T_MasterDataItem_ItemID});
                        $this->db->join(T_MasterDataItem.' i','i.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                        $sql2 = $this->db->get(T_TransactionStockBalanceHeader);
                        //print_r($sql2);die;   
                        if($sql2->num_rows() > 0){
                            $StockExist = $sql2->first_row();
                        }
                        $Qty2 = ($epcExist->{T_MasterDataItem_AutoIDType}==30) ? 1 : 0;
                        $val = array(
                            T_SalesInvoiceDetail_PRI => $parentID,
                            T_SalesInvoiceDetail_RowIndex => $key,
                            T_SalesInvoiceDetail_ItemID => empty($epcExist->{T_MasterDataItem_ItemID})? ' ':$epcExist->{T_MasterDataItem_ItemID},
                            T_SalesInvoiceDetail_ItemName => empty($epcExist->{T_MasterDataItem_ItemName})? ' ':$epcExist->{T_MasterDataItem_ItemName},
                            T_SalesInvoiceDetail_EPC => empty($epcExist->{T_MasterDataItem_EPC})? ' ':$epcExist->{T_MasterDataItem_EPC},
                            T_SalesInvoiceDetail_Quantity => "",
                            T_SalesInvoiceDetail_LocationID => empty($StockExist->{T_TransactionStockBalanceHeader_LocationID})? ' ':$StockExist->{T_TransactionStockBalanceHeader_LocationID},
                            T_SalesInvoiceDetail_IMEI => empty($epcExist->{T_MasterDataItem_IMEI})? ' ':$epcExist->{T_MasterDataItem_IMEI},
                            T_SalesInvoiceDetail_Brand => empty($epcExist->{T_MasterDataItem_Brand})? ' ':$epcExist->{T_MasterDataItem_Brand},
                            T_SalesInvoiceDetail_Model => empty($epcExist->{T_MasterDataItem_Model})? ' ':$epcExist->{T_MasterDataItem_Model},
                            T_SalesInvoiceDetail_Color => empty($epcExist->{T_MasterDataItem_Color})? ' ':$epcExist->{T_MasterDataItem_Color},
                            T_SalesInvoiceDetail_Status => empty($epcExist->{T_MasterDataItem_Status})? ' ':$epcExist->{T_MasterDataItem_Status},
                            T_SalesInvoiceDetail_ItemGroup => empty($epcExist->{T_MasterDataItem_GroupID})? ' ':$epcExist->{T_MasterDataItem_GroupID},
                        );
                        //print_r($val);die;  
                        $this->db->insert(T_SalesInvoiceDetail, $val);
                        //echo $this->db->last_query();die;
                    }
                }
                    
                $this->db->trans_commit();
                $output = array('errorcode' => 0, 'msg' => 'success', 'id'=> $parentID);
            }else{
                throw new Exception("No epc found. Please scan epc first before send.");
            }
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    public function StockSearch()
    {
        $data = $_POST;
        try{
            //$this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			//$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    public function testinsert()
    {
        $data = $_POST;
        try{
            foreach($data['data'] as $EPC){
                $val = array(
                    "EPC" => $EPC,
                    "LocationID" => $data["LocationID"],
                    "parentID" => $data["parentID"]
                );
                $this->db->insert('EPC', $val);
            }
            $this->db->trans_commit();
            $output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

}

/* End of file Automation.php */
/* Location: ./app/modules/master/controllers/Automation.php */

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Delete_model extends CI_Model
{
//Master Item
    public function DeleteItem($id){
        $this->db->trans_begin();
        $this->db->where('t8010.t8010r001', $id);
        $this->db->delete('t8010');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Master Location
    public function DeleteLocation($id){
        $this->db->trans_begin();
        $this->db->where('t8030.t8030r001', $id);
        $this->db->delete('t8030');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }    

//Master General List
    public function DeleteGeneralList($id){
        $this->db->trans_begin();
        $this->db->delete('t8000', array('t8000r001' => $id));
        $this->db->delete('t8001', array('t8001f002' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Stock Balance
    public function DeleteStockBalance($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Delete Stock Transfer
    public function DeleteStockTransfer($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Delete Stock Take
    public function DeleteStockTake($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Delete Stock Adjust
    public function DeleteStockAdjust($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Sales
    public function DeleteSales($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Purchase
    public function DeletePurchase($id){
        $this->db->trans_begin();
        $this->db->delete('t1010', array('t1010r001' => $id));
        $this->db->delete('t1011', array('t1011f001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}

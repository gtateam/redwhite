<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pos_model extends CI_Model
{

    public function UpdateDetail($id,$data)
    {
        $this->db->where(T_SalesInvoiceDetail_RecordID, $id);
        $this->db->update(T_SalesInvoiceDetail, $data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function SumLineTotal($pri)
    {
        $this->db->select('SUM('.T_SalesInvoiceDetail_LineTotal.') as total');
        $this->db->where(T_SalesInvoiceDetail_PRI, $pri);
        $this->db->from(T_SalesInvoiceDetail);

        $SubTotal = $this->db->get()->first_row()->total;
        $this->UpdateHeader($pri,$SubTotal);
    }

    public function UpdateHeader($pri,$SubTotal)
    {
        $this->db->set(T_SalesInvoiceHeader_Outstanding,0);
        $this->db->set(T_SalesInvoiceHeader_Payment,0);
        $this->db->set(T_SalesInvoiceHeader_AmountSubtotal,$SubTotal);
        $this->db->set(T_SalesInvoiceHeader_AmountTotal, $SubTotal."*".T_SalesInvoiceHeader_AmountVAT."+".$SubTotal, FALSE);
        $this->db->where(T_SalesInvoiceHeader_RecordID, $pri);
        $this->db->update(T_SalesInvoiceHeader);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stockmovement_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter,$select){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_".$col);
                $this->db->where($col,$val);
            }
        }
        if(!empty($select)){
            if($table == T_TransactionStockBalanceHeader){
                $this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".$table.".".T_TransactionStockBalanceHeader_ItemID);  
                $this->db->join(T_MasterDataGeneralTableValue, T_MasterDataGeneralTableValue.".".T_MasterDataGeneralTableValue_RecordID." = ".T_MasterDataItem.".".T_MasterDataItem_AutoIDType);  
                $this->db->join(T_MasterDataLocation, T_MasterDataLocation.".".T_MasterDataLocation_LocationID." = ".T_TransactionStockBalanceHeader.".".T_TransactionStockBalanceHeader_LocationID);  
            }  
            if($table==T_TransactionStockMovementDetail){
                $this->db->join(T_MasterDataItem, T_MasterDataItem.".".T_MasterDataItem_ItemID." = ".$table.".".T_TransactionStockMovementDetail_ItemID);  
                $this->db->join(T_MasterDataGeneralTableValue, T_MasterDataGeneralTableValue.".".T_MasterDataGeneralTableValue_RecordID." = ".T_MasterDataItem.".".T_MasterDataItem_AutoIDType);  
            }  
            $this->db->select($select);
        }
        return $this->db->get($table);
    }

    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_".$col);
                $this->db->where($col,$val);
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function Insert($data){
        
    }

    public function Update($data){
        
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_TransactionStockMovementHeader, array(T_TransactionStockMovementHeader_RecordID => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Update_model extends CI_Model
{

//Master Item
    public function UpdateItem($RecordID, $data){
        $this->db->trans_begin();
            $this->db->where('t8010.t8010r001', $RecordID);
            $this->db->update("t8010",$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Stock Balance
    public function UpdateStockBalance($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['LocationID'],
                't1011f009' => $item['QuantityItem'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateStockBalanceJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($detail as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->LocationID,
                't1011f009' => $item->QuantityItem,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Stock Transfer 
    public function UpdateStockTransfer($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['FromLocationID'],
                't1011f008' => $item['ToLocationID'],
                't1011f009' => $item['QuantityST'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateStockTransferJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($detail as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->FromLocationID,
                't1011f008' => $item->ToLocationID,
                't1011f009' => $item->QuantityST,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Stock Take
    public function UpdateStockTake($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['LocationID'],
                't1011f009' => $item['QuantityCount'],
                't1011f010' => $item['QuantitySystem'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateStockTakeJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($detail as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->LocationID,
                't1011f009' => $item->QuantityCount,
                't1011f010' => $item->QuantitySystem,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Stock Adjust
    public function UpdateStockAdjust($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['LocationID'],
                't1011f009' => $item['QuantityCurrent'],
                't1011f010' => $item['QuantityAdjust'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateStockAdjustJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($detail as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->LocationID,
                't1011f009' => $item->QuantityCurrent,
                't1011f010' => $item->QuantityAdjust,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Sales
    public function UpdateSales($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['LocationID'],
                't1011f009' => $item['QuantityItem'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdateSalesJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->LocationID,
                't1011f009' => $item->QuantityItem,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

//Purchase
        public function UpdatePurchase($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item['RowIndex'],
                't1011f003' => $item['ItemID'],
                't1011f004' => $item['ItemName'],
                't1011f005' => $item['EPC'],
                't1011f006' => $item['Barcode'],
                't1011f007' => $item['LocationID'],
                't1011f009' => $item['QuantityItem'],
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function UpdatePurchaseJson($RecordID, $t1010r002, $t1010r003, $t1010f003, $t1010f004, $t1010f005, $data){
        $this->db->trans_begin();
        $this->db->set('t1010r002', $t1010r002);
        $this->db->set('t1010r003', $t1010r003);
        $this->db->set('t1010f003', $t1010f003);
        $this->db->set('t1010f004', $t1010f004);
        $this->db->set('t1010f005', $t1010f005);
        $this->db->where('t1010r001', $RecordID);
        $this->db->update('t1010');
        $this->db->delete('t1011', array('t1011f001' => $RecordID));
        foreach($data['detail'] as $item){
            $val = array(
                't1011r002' => date("Y-m-d g:i:s",now()),
                't1011r003' => 0,
                't1011f001' => $RecordID,
                't1011f002' => $item->RowIndex,
                't1011f003' => $item->ItemID,
                't1011f004' => $item->ItemName,
                't1011f005' => $item->EPC,
                't1011f006' => $item->Barcode,
                't1011f007' => $item->LocationID,
                't1011f009' => $item->QuantityItem,
            );
            $this->db->insert("t1011",$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}

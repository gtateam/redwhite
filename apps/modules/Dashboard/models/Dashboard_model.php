<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'='.$table.'.'.T_TransactionStockBalanceHeader_LocationID, 'left');
        // if(($startdate != "") || ($enddate != "")){
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
        // }
        $this->db->where($table.'.t1990f003 !=0');
        //echo $this->db->last_query($this->db->get($table));die;
        return $this->db->get($table);
    }

    //Count Data
    public function getListCount($table){
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function GetDataSearch($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        /*
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        if($this->input->get('DocTypeName'))
        {
            $this->db->where("t1010f001", $this->input->get('DocTypeName'));
        }
        if($this->input->get('DocDate'))
        {
            $stamp = strtotime($this->input->get('DocDate'));
            $date = date("Y-m-d",$stamp); 
            $this->db->where("t1010f003",$date);
        }
        if($this->input->get('SerialNo'))
        {
            $this->db->where($this->input->get('CodeType'),$this->input->get('SerialNo'));
        }
        if($this->input->get('Status'))
        {
            $this->db->where("t1011f017", $this->input->get('Status'));
        }
        
        return $this->db->get($table);
        */

        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');  
        $this->db->join('t9050', 't1010.t1010f001 = t9050.t9050f001', 'left');                

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        if($this->input->get('DocTypeName')) {
            $this->db->where("t1010f001", $this->input->get('DocTypeName'));
        }
        if($this->input->get('DocDate')) {
            $stamp = strtotime($this->input->get('DocDate'));
            $date = date("Y-m-d", $stamp); 
            $this->db->where("DATE_FORMAT(t1010f003,'%Y-%m-%d')", $date);
        }
        if($this->input->get('SerialNo')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('SerialNo'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t1011f017)", $this->input->get('Status'));
        }

        $query = $this->db->get();

        return $query;        
    }

    //Count Data
    public function GetDataSearchCount($table){
        /*
        if($this->input->get('DocTypeName'))
        {
            $this->db->where("t1010f001", $this->input->get('DocTypeName'));
        }
        if($this->input->get('DocDate'))
        {
            $stamp = strtotime($this->input->get('DocDate'));
            $date = date("Y-m-d",$stamp); 
            //$this->db->where("t1010f003",$date);
            $this->db->where("STR_TO_DATE('t1010f003','%Y-%m-%d')", $date);
        }
        if($this->input->get('SerialNo'))
        {
            $this->db->where($this->input->get('CodeType'),$this->input->get('SerialNo'));
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
        */

        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        if($this->input->get('DocTypeName')) {
            $this->db->where("t1010f001", $this->input->get('DocTypeName'));
        }
        if($this->input->get('DocDate')) {
            $stamp = strtotime($this->input->get('DocDate'));
            $date = date("Y-m-d", $stamp); 
            $this->db->where("DATE_FORMAT(t1010f003,'%Y-%m-%d')", $date);
        }
        if($this->input->get('SerialNo')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('SerialNo'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t1011f017)", $this->input->get('Status'));
        }

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
        
    }

    public function GetListStock($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField)
    {
        if($this->input->get('TransType') == 'BAL') {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6011');
            $this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
        } else {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6010');
            $this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');  // Update 20183101
        }
        
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        if($this->input->get('TransType') == 'BAL') {
            $this->db->where("t6011f005", 1);
            $this->db->where("t6010f001", 'IN');
        } else {
            if($this->input->get('TransType')) {
                $this->db->where("t6010f001", $this->input->get('TransType'));
            }
            if($this->input->get('TransDate')) {
                $stamp = strtotime($this->input->get('TransDate'));
                $date = date("Y-m-d", $stamp); 
                $this->db->where("DATE_FORMAT(t6010f002,'%Y-%m-%d')", $date);
            }
        }
        // Begin Update 20180502
        if($this->input->get('Brand')) {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model')) {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color')) {
            $this->db->where("t6010f006", $this->input->get('Color'));
        }
        // End Update 20180502

        if($this->input->get('Serialno')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
        }

        $query = $this->db->get();

        return $query;        
    }

    // Begin Update 20180502
    public function GetTotalStockCount($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField)
    {
        if($this->input->get('TransType') == 'BAL') {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6011');
            $this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');
        } else {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6010');
            $this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left');  // Update 20183101
        }

        if($this->input->get('TransType') == 'BAL') {
            $this->db->where("t6011f005", 1);
            $this->db->where("t6010f001", 'IN');
        } else {
            if($this->input->get('TransType')) {
                $this->db->where("t6010f001", $this->input->get('TransType'));
            }
            if($this->input->get('TransDate')) {
                $stamp = strtotime($this->input->get('TransDate'));
                $date = date("Y-m-d", $stamp); 
                $this->db->where("DATE_FORMAT(t6010f002,'%Y-%m-%d')", $date);
            }
        }

        if($this->input->get('Brand')) {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model')) {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color')) {
            $this->db->where("t6010f006", $this->input->get('Color'));
        }

        if($this->input->get('Serialno')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
        }

        $query = $this->db->get();

        return $query->num_rows();        
    }
    // End Update 20180502

    public function GetTotalStock($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField)
    {
        if($this->input->get('TransType') == 'BAL') {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6011');
            $this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008', 'left');

            $this->db->where("t6011f005", 1);
            $this->db->where("t6010f001", 'IN');
        } else {
            $this->db->select('t6010f001 AS transtype, t6010f002 AS transdate, t6010f003 AS itemid, t6010f004 AS brand, t6010f005 AS model, t6010f006 AS color, t6010f007 AS status, t6010f008 AS imei, t6010f009 AS rfid, t6010f010 AS costprice, t6010f011 AS remarks');
            $this->db->from('t6010');
            $this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left'); // Update 20183101

            if($this->input->get('TransType')) {
                $this->db->where("t6010f001", $this->input->get('TransType'));
            }
            if($this->input->get('TransDate')) {
                $stamp = strtotime($this->input->get('TransDate'));
                $date = date("Y-m-d", $stamp); 
                $this->db->where("DATE_FORMAT(t6010f002,'%Y-%m-%d')", $date);
            }
        }

        if($this->input->get('Serialno')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
        }

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;        
    }

    public function GetTotalStockIn()
    {
        if(($this->input->get('TransType') == 'BAL') || ($this->input->get('TransType') == 'OUT')) {
            $rowcount = 0;
        } else {
            $this->db->select('COUNT(t6010r001) AS totalcount');
            $this->db->from('t6010');
            $this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left'); // Update 20183101
            $this->db->where('t6010f001', 'IN');

            if($this->input->get('TransDate')) {
                $stamp = strtotime($this->input->get('TransDate'));
                $date = date("Y-m-d", $stamp); 
                $this->db->where("DATE_FORMAT(t6010f002,'%Y-%m-%d')", $date);
            }
            if($this->input->get('Serialno')) {
                $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
            }
            if($this->input->get('Status')) {
                $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
            }
            // Begin Update 20180502
            if($this->input->get('Brand')) {
                $this->db->where("t6010f004", $this->input->get('Brand'));
            }
            if($this->input->get('Model')) {
                $this->db->where("t6010f005", $this->input->get('Model'));
            }
            if($this->input->get('Color')) {
                $this->db->where("t6010f006", $this->input->get('Color'));
            }
            // end Update 20180502
            $query = $this->db->get();
            $rowcount = $query->first_row()->totalcount;
        }

        return $rowcount;
    }

    public function GetTotalStockOut()
    {
        if(($this->input->get('TransType') == 'BAL') || ($this->input->get('TransType') == 'IN')) {
            $rowcount = 0;
        } else {
            $this->db->select('COUNT(t6010r001) AS totalcount');
            $this->db->from('t6010');
            $this->db->join('t6011', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009', 'left'); // Update 20183101
            $this->db->where('t6010f001', 'OUT');

            if($this->input->get('TransDate')) {
                $stamp = strtotime($this->input->get('TransDate'));
                $date = date("Y-m-d", $stamp); 
                $this->db->where("DATE_FORMAT(t6010f002,'%Y-%m-%d')", $date);
            }
            if($this->input->get('Serialno')) {
                $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
            }
            if($this->input->get('Status')) {
                $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
            }
            // Begin Update 20180502
            if($this->input->get('Brand')) {
                $this->db->where("t6010f004", $this->input->get('Brand'));
            }
            if($this->input->get('Model')) {
                $this->db->where("t6010f005", $this->input->get('Model'));
            }
            if($this->input->get('Color')) {
                $this->db->where("t6010f006", $this->input->get('Color'));
            }
            // End Update 20180502
            $query = $this->db->get();
            $rowcount = $query->first_row()->totalcount;
        }

        return $rowcount;
    }

    public function GetTotalStockOnHand()
    {
        $this->db->select('COUNT(t6011f001) AS totalcount');
        $this->db->from('t6011');
        $this->db->join('t6010', 't6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009 ', 'left');
        $this->db->where("t6011f005", 1);
        $this->db->where("t6010f001", 'IN');

        if($this->input->get('Serialno')) {
            $this->db->where($this->input->get('CodeType'), $this->input->get('Serialno'));
        }
        if($this->input->get('Status')) {
            $this->db->where("UPPER(t6010f007)", $this->input->get('Status'));
        }
        // Begin Update 20180502
        if($this->input->get('Brand')) {
            $this->db->where("t6010f004", $this->input->get('Brand'));
        }
        if($this->input->get('Model')) {
            $this->db->where("t6010f005", $this->input->get('Model'));
        }
        if($this->input->get('Color')) {
            $this->db->where("t6010f006", $this->input->get('Color'));
        }
        // End Update 20180502
        $query = $this->db->get();
        $rowcount = $query->first_row()->totalcount;
        return $rowcount;
    }

    public function getListstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){                
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function getListstockInCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function getListstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function GridStockSoldAccessories($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $startdate,$enddate,$datenow){
        $this->db->select("sum(v1991.t1991f005) as qty, v1991.*, a.*, i.*");
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_TransactionStockMovementHeader.' a', 'a.'.T_TransactionStockMovementHeader_DocNo.'='.$table.'.t1010f002', 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
        $this->db->where_in($table.'.t1991f002',array('IVSO','SLPS','IVST'));
        if(($startdate != "") || ($enddate != "")){
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}
        $this->db->where($table.'.t1991f006','IG-00002');
        $this->db->group_by("t8010f001");
        return $this->db->get($table);
    }

    public function getListstockSoldCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function getListAlert($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->where("DATE_FORMAT(".T_VIEWAlert_Seen.", '%Y-%m-%d') = ", "'".date("Y-m-d")."'", false);
        $this->db->where(T_VIEWAlert_FLAG,0);
        $this->db->where(T_VIEWAlert_ItemID." != ''");
        return $this->db->get($table);
    }

    public function getListAlertCount($table){
        $this->db->where("DATE_FORMAT(".T_VIEWAlert_Seen.", '%Y-%m-%d') = ", "'".date("Y-m-d")."'", false);
        $this->db->where(T_VIEWAlert_FLAG,0);
        $this->db->where(T_VIEWAlert_ItemID." != ''");
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }






    //AccessoriesDashboard
    public function getListAccesoriesOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'='.$table.'.'.T_TransactionStockBalanceHeader_LocationID, 'left');
        // if(($startdate != "") || ($enddate != "")){
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
        // }
        $this->db->where(T_MasterDataItem_GroupID,'IG-00002');
        $this->db->where($table.'.t1990f003 !=0');
        return $this->db->get($table);
    }

    public function getListAccessoriesstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        $this->db->select("sum(v1991.t1991f005) as qty, v1991.*, a.*, i.*");
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_TransactionStockMovementHeader.' a', 'a.'.T_TransactionStockMovementHeader_DocNo.'='.$table.'.t1010f002', 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
        $this->db->where_in($table.'.t1991f002',array('IVSO','SLPS','IVST'));
        if(($startdate != "") || ($enddate != "")){
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}
        $this->db->where($table.'.t1991f006','IG-00002');
        $this->db->group_by("t8010f001");
        return $this->db->get($table);
    }

    public function getListAccessoriesstockSoldCount($table){
        $this->db->join(T_TransactionStockMovementHeader.' a', 'a.'.T_TransactionStockMovementHeader_DocNo.'='.$table.'.t1010f002', 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
        $this->db->where_in($table.'.t1991f002',array('IVSO','SLPS','IVST'));
        $this->db->where(T_MasterDataItem_GroupID,'IG-00002');
        $this->db->group_by("t8010f001");
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListAccessoriesstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $searchField,$startdate,$enddate,$datenow){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_TransactionStockMovementHeader.' a', 'a.'.T_TransactionStockMovementHeader_DocNo.'='.$table.'.t1010f002', 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
        $this->db->where_in($table.'.t1991f002',array('IMBB','IVSI'));
        if(($startdate != "") || ($enddate != "")){
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where($table.".t1991r002 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}
        $this->db->where(T_MasterDataItem_GroupID,'IG-00002');
        return $this->db->get($table);
    }
    
    public function getListAccessoriesstockInCount($table){
        $this->db->join(T_TransactionStockMovementHeader.' a', 'a.'.T_TransactionStockMovementHeader_DocNo.'='.$table.'.t1010f002', 'left');
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
        $this->db->where_in($table.'.t1991f002',array('IMBB','IVSI'));
        $this->db->where(T_MasterDataItem_GroupID,'IG-00002');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    //END-AccessoriesDashboard






    //BrandNewDashboard
    public function getListNewStockOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'='.$table.'.'.T_TransactionStockBalanceHeader_LocationID, 'left');
        // if(($startdate != "") || ($enddate != "")){
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
        // }
        $this->db->where(T_MasterDataItem_Status,'New');
        $this->db->where($table.'.t1990f003 !=0');
        return $this->db->get($table);
    }

    public function getListNewStockOnhandCount($table){
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->where(T_MasterDataItem_Status,'New');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListNewStockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        $this->db->where('UPPER(t1011.t1011f017)', 'NEW');        
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function getListNewStockSoldCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        $this->db->where('UPPER(t1011.t1011f017)', 'NEW');         
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function getListNewstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        $this->db->where('UPPER(t1011.t1011f017)', 'NEW');        
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function getListNewstockInCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        $this->db->where('UPPER(t1011.t1011f017)', 'NEW');         
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    //END - BrandNewDashboard





    
    //BrandUsedDashboard
    public function getListUsedStockOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->join(T_MasterDataLocation.' l', 'l.'.T_MasterDataLocation_LocationID.'='.$table.'.'.T_TransactionStockBalanceHeader_LocationID, 'left');
        // if(($startdate != "") || ($enddate != "")){
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
        // }
        $this->db->where(T_MasterDataItem_Status,'Used');
        $this->db->where($table.'.t1990f003 !=0');
        return $this->db->get($table);
    }

    public function getListUsedStockOnhandCount($table){
        $this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'='.$table.'.'.T_TransactionStockBalanceHeader_ItemID, 'left');
        $this->db->where(T_MasterDataItem_Status,'Used');
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getListUsedStockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        $this->db->where('UPPER(t1011.t1011f017)', 'USED');        
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function getListUsedStockSoldCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSO');
        $this->db->where('UPPER(t1011.t1011f017)', 'USED');         
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    public function getListUsedstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        

        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }

        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        $this->db->where('UPPER(t1011.t1011f017)', 'USED');        
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();

        return $query;
    }

    public function getListUsedstockInCount($table,$startdate,$enddate,$datenow){
        $this->db->select('*');
        $this->db->from('t1011');
        $this->db->join('t1012', 't1012.t1012f001 = t1011.t1011r001', 'left');
        $this->db->join('t1010', 't1010.t1010r001 = t1011.t1011f001', 'left');        
        $this->db->where('t1010.t1010r003', 1);
        $this->db->where('t1010.t1010f001', 'IVSI');
        $this->db->where('UPPER(t1011.t1011f017)', 'USED');         
        if(($startdate != "") || ($enddate != "")){
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		}else{
            $this->db->where("t1010f003 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
		}

        $query = $this->db->get();
        $rowcount = $query->num_rows();

        return $rowcount;
    }
    //END - BrandUsedDashboard
}
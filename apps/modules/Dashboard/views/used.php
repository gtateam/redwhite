<style type="text/css">.mrg{margin-bottom:5px;}
[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:15;
	font-size: 0.75em;
	height:28px;
    width:65px;
	line-height:18px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<div class="col-md-12">
    <div class="col-md-4 text-left">
        <div class="form-group">
            <div class="col-md-6">
                <div class="row">
                    <label class="col-md-3 form-label">
                        <button id="IntervalStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="myInterval(1)" style="display:none;">Start</button>
                        <button id="IntervalStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="myInterval(0)">Stop</button>
                    </label>
                    <div class="col-md-6">
                        <span data-tip="From Date"><input type="text" id="StartDate" class="KendoDatePicker" /></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <label class="col-md-2 form-label"></label>
                    <div class="col-md-6">
                        <span data-tip="To Date"><input type="text" style="width:160px;" id="EndDate" class="KendoDatePicker" /></span>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top:10px;">
<div class="row">
    <div class="col-md-4">
        <section class="panel">
            <div class="panel-body bg-success">
                <div class="widget-summary">
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Stock-In Qty</h4>
                            <div class="info">
                                <strong class="amount" id="stockIn">0</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a class="text-uppercase" onclick="viewstockIn()">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-4">
        <section class="panel">
            <div class="panel-body bg-tertiary">
                <div class="widget-summary">
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Stock-Out Qty</h4>
                            <div class="info">
                                <strong class="amount" id="stockSold">0</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a class="text-uppercase" onclick="viewstockSold()">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-4">
        <section class="panel">
            <div class="panel-body bg-primary">
                <div class="widget-summary">
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Total-Stock Qty</h4>
                            <div class="info">
                                <strong class="amount" id="stockOnhand">0</strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a class="text-uppercase" onclick="viewstockOnhand()">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
</div>
<div class="col-md-12" id="gridStockInview">
<section class="panel panel-success">
    <header class="panel-heading">
        <div class="panel-actions">
            <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
        </div>
        <h2 class="panel-title">Stock In</h2>
    </header>
    <div class="panel-body">

    <div id="gridStockIn"></div>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#StartDate").kendoDatePicker({format: DateFormat});
        $("#EndDate").kendoDatePicker({format: DateFormat});
    //Grid
        $("#gridStockIn").kendoGrid({
            height: "330px",
            width: "100%",
            columns: [
                {
                "title":"ItemID",
                "width":"100px",
                "field":"t1011f003",
                },
                {
                "title":"Brand",
                "width":"100px",
                "field":"t1011f014",
                },
                {
                "title":"Model",
                "width":"100px",
                "field":"t1011f015",
                },
                {
                "title":"Color",
                "width":"100px",
                "field":"t1011f016",
                },
                {
                "title":"Status",
                "width":"100px",
                "field":"t1011f017",
                },
                {
                "title":"IMEI",
                "width":"100px",
                "field":"t1012f003",
                },
            ],                     
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: 't1011'},
                        url: function() {
                            return site_url('Dashboard/UsedstockIn?StartDate='+$("#StartDate").val()+'&EndDate='+$("#EndDate").val()).slice(0,-5);
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridStockIn').data('kendoGrid').dataSource.read();
                    $('#gridStockIn').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t1011r001",
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:false,     
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });
    });
</script>
</div>
</section>
</div>
<div class="col-md-12" id="gridStockOnHandview" style="display: none">
<section class="panel panel-primary">
    <header class="panel-heading">
        <div class="panel-actions">
            <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
        </div>
        <h2 class="panel-title">Total Stock</h2>
    </header>
    <div class="panel-body">

    <div id="gridStockOnHand"></div>
    <script type="text/javascript">
    $(document).ready(function() {
    //Grid
        $("#gridStockOnHand").kendoGrid({
            height: "330px",
            width: "100%",
            columns: [
            {
            "title":"ItemID",
            "width":"100px",
            "field":"<?php echo T_TransactionStockBalanceHeader_ItemID; ?>",
            },
            {
            "title":"Brand",
            "width":"100px",
            "field":"<?php echo T_MasterDataItem_Brand; ?>",
            },
            {
            "title":"Model",
            "width":"100px",
            "field":"<?php echo T_MasterDataItem_Model; ?>",
            },
            {
            "title":"Color",
            "width":"100px",
            "field":"<?php echo T_MasterDataItem_Color; ?>",
            },
            {
            "title":"Status",
            "width":"100px",
            "field":"<?php echo T_MasterDataItem_Status; ?>",
            },                        
            {
            "title":"Qty",
            "width":"100px",
            "field":"<?php echo T_TransactionStockBalanceHeader_Quantity; ?>",
            },
            {
            "title":"UOM",
            "width":"100px",
            "field":"<?php echo T_MasterDataItem_UOMID; ?>",
            },
            // {
            // "title":"Location Name",
            // "width":"100px",
            // "field":"<?php echo T_MasterDataLocation_LocationName; ?>",
            // }
            ],                     
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: 't1011'},
                        url: function() {
                            return site_url('Dashboard/UsedstockOnHand?StartDate='+$("#StartDate").val()+'&EndDate='+$("#EndDate").val()).slice(0,-5);
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridStockOnHand').data('kendoGrid').dataSource.read();
                    $('#gridStockOnHand').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t1010r001",
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:false,     
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });
    });
</script>
</div>
</section>
</div>
<div class="col-md-12" id="gridStockSoldView" style="display: none">
<section class="panel panel-tertiary" >
    <header class="panel-heading">
        <div class="panel-actions">
            <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
        </div>
        <h2 class="panel-title">Stock Out</h2>
    </header>
    <div class="panel-body">
    <div id="gridStockSold"></div>
    <script type="text/javascript">
    $(document).ready(function() {
    //Grid
        $("#gridStockSold").kendoGrid({
            height: "330px",
            width: "100%",
            columns: [
                {
                "title":"ItemID",
                "width":"100px",
                "field":"t1011f003",
                },
                {
                "title":"Brand",
                "width":"100px",
                "field":"t1011f014",
                },
                {
                "title":"Model",
                "width":"100px",
                "field":"t1011f015",
                },
                {
                "title":"Color",
                "width":"100px",
                "field":"t1011f016",
                },
                {
                "title":"Status",
                "width":"100px",
                "field":"t1011f017",
                },
                {
                "title":"IMEI",
                "width":"100px",
                "field":"t1012f003",
                },
            ],                   
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: 't1011'},
                        url: function() {
                            return site_url('Dashboard/UsedStockSold?StartDate='+$("#StartDate").val()+'&EndDate='+$("#EndDate").val()).slice(0,-5);
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridStockSold').data('kendoGrid').dataSource.read();
                    $('#gridStockSold').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t1011r001",
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:false,   
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });
    });
</script>
</div>
</section>
</div>

<script type="text/javascript">
$(document).ready(function() {
viewstockOnhand();
var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MM-yyyy');
$("#StartDate").val(todayDate);
$("#EndDate").val(todayDate);
});
var realTime = setInterval(function(){ getData(); }, 1000);

function getData(){
    var StartDate=$("#StartDate").val();
    var EndDate=$("#EndDate").val();
$.ajax({
    type: 'POST',
    url: site_url('Dashboard/stockUsedOnHand?StartDate='+StartDate+'&EndDate='+EndDate).slice(0,-5),
    dataType: "json",
        success: function (data) {
        $("#stockOnhand").text(data.Qty);
        $('#gridStockOnHand').data('kendoGrid').refresh();
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
});

$.ajax({
    type: 'POST',
    url: site_url('Dashboard/stockUsedSoldCount?StartDate='+StartDate+'&EndDate='+EndDate).slice(0,-5),
    dataType: "json",
        success: function (data) {
        $("#stockSold").text(data.Qty);
        $('#gridStockSold').data('kendoGrid').dataSource.read();
        $('#gridStockSold').data('kendoGrid').refresh();
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
});

$.ajax({
    type: 'POST',
    url: site_url('Dashboard/stockUsedInCount?StartDate='+StartDate+'&EndDate='+EndDate).slice(0,-5),
    dataType: "json",
        success: function (data) {
        $("#stockIn").text(data.Qty);
        $('#gridStockIn').data('kendoGrid').dataSource.read();
        $('#gridStockIn').data('kendoGrid').refresh();
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
});

}
function viewstockIn(){
$('#gridStockIn').data('kendoGrid').dataSource.read();
$('#gridStockIn').data('kendoGrid').refresh();
$("#gridStockInview").removeAttr('style');
$("#gridStockOnHandview").attr('style', 'display:none');
$("#gridStockSoldView").attr('style', 'display:none');
}
function viewstockOnhand(){
$('#gridStockOnHand').data('kendoGrid').dataSource.read();
$('#gridStockOnHand').data('kendoGrid').refresh();
$("#gridStockOnHandview").removeAttr('style');
$("#gridStockSoldView").attr('style', 'display:none');
$("#gridStockInview").attr('style', 'display:none');
}

function viewstockSold(){
$('#gridStockSold').data('kendoGrid').dataSource.read();
$('#gridStockSold').data('kendoGrid').refresh();
$("#gridStockSoldView").removeAttr('style');
$("#gridStockOnHandview").attr('style', 'display:none');
$("#gridStockInview").attr('style', 'display:none');
}
function myInterval(i) {
if(i){
    $("#IntervalStart").hide();
    $("#IntervalStop").show();
    realTime = setInterval(function(){ getData(); }, 1000);
}else{
    $("#IntervalStart").show();
    $("#IntervalStop").hide();
    clearInterval(realTime);
}
}
</script>
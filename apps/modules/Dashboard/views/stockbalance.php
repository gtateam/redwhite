<div class="col-md-6">
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Type</label>
        <div class="col-md-4">
            <?php echo form_dropdown('TransType', array(0 => 'All', 'IN' => 'Stock In', 'OUT' => 'Stock Out', 'BAL' => 'Stock Balance'), (isset($category_id) ? $category_id : ''), 'class="form-control select2" id="TransType" style="width:250px;" '); ?>
        </div>
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
            <label class="col-md-3 form-label">Date </label>
            <div class="col-md-4">
                <span data-tip="Trans Date">
                    <input type="text" id="TransDate" class="KendoDatePicker" style="width:250px;" >
                </span>
            </div>
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Brand</label>
        <label class="col-md-4 form-label">
        <?php 
            echo form_dropdown('Brand', $this->Options_model->get_lookup('9',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key), (isset($Brand) ? $Brand : '') ,'class="form-control select2" id="Brand" style="width:250px;" ');
        ?>
        </label>    
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Model</label>
        <label class="col-md-4 form-label">
        <?php 
            echo form_dropdown('Model', $this->Options_model->get_lookup('6',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key), (isset($Brand) ? $Brand : '') ,'class="form-control select2" id="Model" style="width:250px;" ');
        ?>
        </label>    
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Color</label>
        <label class="col-md-4 form-label">
        <?php 
            echo form_dropdown('Color', $this->Options_model->get_lookup('5',T_MasterDataGeneralTableValue,T_MasterDataGeneralTableValue_PRI,T_MasterDataGeneralTableValue_Description,T_MasterDataGeneralTableValue_Key), (isset($Brand) ? $Brand : '') ,'class="form-control select2" id="Color" style="width:250px;" ');
        ?>
        </label>    
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">
        <?php 
            echo form_dropdown('CodeType', array( "t6011f001" => 'IMEI', "t6011f002" => 'RFID'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="CodeType" ' ); // Update 20183101
             ?></label>
        <div class="col-md-4">
            <?php
            $items = array( 'id' => 'Serialno', 'class' => 'form-control', 'style' => 'width:250px;');
            echo form_input($items);
            ?>  
        </div>
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Status</label>
        <label class="col-md-4 form-label">
        <?php 
            echo form_dropdown('Status', array( 0 => "All", "NEW" => 'NEW', "USED" => 'USED'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="Status" style="width:250px;" ' );
        ?>
        </label>    
    </div>

    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label"><button style="width:150px" onclick="FilterData(); return false;">Search</button></label>
        <div class="col-md-4">
        </div>
    </div>
    
</div>
<div class="col-md-2">
    <section class="panel">
        <div class="panel-body bg-success">
            <div class="widget-summary">
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Total Stock-In</h4>
                        <div class="info">
                            <strong class="amount" id="TotalStockIn">0</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="col-md-2">
    <section class="panel">
        <div class="panel-body bg-tertiary">
            <div class="widget-summary">
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Total Stock-Out</h4>
                        <div class="info">
                            <strong class="amount" id="TotalStockOut">0</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="col-md-2">
    <section class="panel">
        <div class="panel-body bg-primary">
            <div class="widget-summary">
                <div class="widget-summary-col">
                    <div class="summary">
                        <h4 class="title">Total Stock On-Hand</h4>
                        <div class="info">
                            <strong class="amount" id="TotalStockOnHand">0</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<br>
<div class="col-md-12">
<div id="gridSearch"></div>
</div>
    <script type="text/javascript">
    function FilterData()
    {
        if ($("#TransType").val() == 'BAL') {
            $("#TransDate").val('');
        }

        var SN = $("#Serialno").val();
        var length = SN.length;
        if($("#CodeType").val() == "t6011f002"){
            if (SN != ""){
                if(length < 24){
                    var prefixs = '000000000000000000000000';
                    var min = prefixs.substr(length);
                    var res = min.concat(SN);
                    $("#Serialno").val(res);
                }else{
                    $("#Serialno").val();
                }
            }
        }

        var voData = {
            TransType : $("#TransType").val(),
            TransDate : $("#TransDate").val(),            
            Brand : $("#Brand").val(),            
            Model : $("#Model").val(),            
            Color : $("#Color").val(),            
            CodeType : $("#CodeType").val(),
            Serialno :  $("#Serialno").val(),
            Status : $("#Status").val()
        };
        console.log(voData);
        $("#gridSearch").data("kendoGrid").dataSource.page(1);
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.data = {TransType: voData.TransType, TransDate: voData.TransDate, Brand:voData.Brand,Model:voData.Model,Color:voData.Color, CodeType: voData.CodeType, Serialno: voData.Serialno, Status: voData.Status};
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.url = site_url("Dashboard/GetListStock");
        $("#gridSearch").data("kendoGrid").dataSource.read();

        fGetTotalStockIn();
        fGetTotalStockOut();
        fGetTotalStockOnHand();
    }
    function fGetTotalStockIn()
    {
        var voData = {
            TransType : $("#TransType").val(),
            TransDate : $("#TransDate").val(),
            Brand : $("#Brand").val(),            
            Model : $("#Model").val(),            
            Color : $("#Color").val(),          
            CodeType : $("#CodeType").val(),
            Serialno :  $("#Serialno").val(),
            Status : $("#Status").val()
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Dashboard/GetTotalStockIn'),
            beforeSend: function(){
            },
            success: function (result) {
            if (result.errorcode != 0) {
                
            } else {
                $("#TotalStockIn").html(result.TotalStockIn);
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
    function fGetTotalStockOut()
    {
        var voData = {
            TransType : $("#TransType").val(),
            TransDate : $("#TransDate").val(),
            Brand : $("#Brand").val(),            
            Model : $("#Model").val(),            
            Color : $("#Color").val(),              
            CodeType : $("#CodeType").val(),
            Serialno :  $("#Serialno").val(),
            Status : $("#Status").val()
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Dashboard/GetTotalStockOut'),
            beforeSend: function(){
            },
            success: function (result) {
            if (result.errorcode != 0) {
                
            } else {
                $("#TotalStockOut").html(result.TotalStockOut);
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });

    }
    function fGetTotalStockOnHand()
    {
        var voData = {
            TransType : $("#TransType").val(),
            TransDate : $("#TransDate").val(),   
            Brand : $("#Brand").val(),            
            Model : $("#Model").val(),            
            Color : $("#Color").val(),         
            CodeType : $("#CodeType").val(),
            Serialno :  $("#Serialno").val(),
            Status : $("#Status").val()
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Dashboard/GetTotalStockOnHand'),
            beforeSend: function(){
            },
            success: function (result) {
                if (result.errorcode != 0) {
                    
                } else {
                    $("#TotalStockOnHand").html(result.TotalStockOnHand);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }    
    $(document).ready(function() {
        $("#TransType").val('BAL');
        $("#TransDate").kendoDatePicker({format: DateFormat});
        // var vToday = kendo.toString(kendo.parseDate(new Date()), 'dd-MM-yyyy');
        // $("#TransDate").val(vToday);

        $("#gridSearch").kendoGrid({
            toolbar: ["excel"],
            excel: {
                fileName: "stockbalance.xlsx",
                filterable: true
            },            
            height: "500px",
            width: "100%",
            columns: [
            {
            "title":"Type",
            "width":"100px",
            "field":"transtype",
            },
            {
            "title":"Date",
            "width":"100px",
            "field":"transdate",
            },
            {
            "title":"Item ID",
            "width":"100px",
            "field":"itemid",
            },
            {
            "title":"Brand",
            "width":"150px",
            "field":"brand",
            },
            {
            "title":"Model",
            "width":"150px",
            "field":"model",
            },
            {
            "title":"Color",
            "width":"100px",
            "field":"color",
            },
            {
            "title":"Status",
            "width":"100px",
            "field":"status",
            },
            {
            "title":"IMEI",
            "width":"200px",
            "field":"imei",
            },
            {
            "title":"RFID",
            "width":"200px",
            "field":"rfid",
            },
            {
            "title":"Cost Price",
            "width":"100px",
            "field":"costprice",
            },            
            {
            "title":"Remarks",
            "width":"200px",
            "field":"remarks",
            }            
            ],                     
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: ''},
                        url: function() {
                            return site_url('Dashboard/GetListStock');
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridSearch').data('kendoGrid').dataSource.read();
                    $('#gridSearch').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t6010r001",
                    }
                },
                pageSize: 30,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:false,     
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });

        var voData = {
            TransType : $("#TransType").val(),
            TransDate : $("#TransDate").val(),
            Brand : $("#Brand").val(),            
            Model : $("#Model").val(),            
            Color : $("#Color").val(),              
            CodeType : $("#CodeType").val(),
            Serialno :  $("#Serialno").val(),
            Status : $("#Status").val()
        };

        $("#gridSearch").data("kendoGrid").dataSource.page(1);
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.data = {TransType: voData.TransType, Brand:voData.Brand, Model:voData.Model, Color:voData.Color, TransDate: voData.TransDate, CodeType: voData.CodeType, Serialno: voData.Serialno, Status: voData.Status};
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.url = site_url("Dashboard/GetListStock");
        $("#gridSearch").data("kendoGrid").dataSource.read();

        fGetTotalStockOnHand();

        $("#TransType").kendoDropDownList({
            filter: "startswith"
        });
        $("#CodeType").kendoDropDownList({
            filter: "startswith"
        });
        $("#Status").kendoDropDownList({
            filter: "startswith"
        });
        $("#Brand").kendoDropDownList({
            filter: "startswith"
        });
        $("#Model").kendoDropDownList({
            filter: "startswith"
        });
        $("#Color").kendoDropDownList({
            filter: "startswith"
        });
    });
</script>
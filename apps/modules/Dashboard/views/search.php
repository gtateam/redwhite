<div class="col-md-6">
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Doc Type Name</label>
        <div class="col-md-4">
            <?php echo form_dropdown('DocTypeName', array(0 => 'All', 'IVSI' => 'Stock In', 'IVSO' => 'Stock Out'), (isset($category_id) ? $category_id : ''), 'class="form-control select2" id="DocTypeName" style="width:200px;" '); ?>
        </div>
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
            <label class="col-md-3 form-label">Doc Date </label>
            <div class="col-md-4">
                <span data-tip="Doc Date">
                    <input type="text" id="DocDate" class="KendoDatePicker" /input>
                </span>
            </div>
    </div>
</div>
<div class="col-md-6">
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">
        <?php 
            echo form_dropdown('CodeType', array( "t1012f003" => 'IMEI', "t1012f002" => 'RFID'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="CodeType" ' );
             ?></label>
        <div class="col-md-4">
            <?php
            $items = array(
            'id' => 'SerialNo',
            'class' => 'form-control',
            'style' => 'width:250px;'
            );
            echo form_input($items);
            ?>  
        </div>
    </div>
    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label">Status</label>
        <label class="col-md-4 form-label">
        <?php 
            echo form_dropdown('Status', array( 0 => "All", "NEW" => 'NEW', "USED" => 'USED'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="Status" ' );
        ?>
        </label>    
    </div>


    <div class="row mrg" style="margin-bottom:10px;">
        <label class="col-md-3 form-label"><button onclick="FIlterData(); return false;">Search</button></label>
        <div class="col-md-4">
        </div>
    </div>
</div>
<br>
<div class="col-md-12">
<div id="gridSearch"></div>
</div>
    <script type="text/javascript">
    function FIlterData()
    {
        var SN = $("#SerialNo").val();
        var length = SN.length;
        if($("#CodeType").val() == "t1012f002"){
            if (SN != ""){
                if(length < 24){
                    var prefixs = '000000000000000000000000';
                    var min = prefixs.substr(length);
                    var res = min.concat(SN);
                    $("#SerialNo").val(res);
                }else{
                    $("#SerialNo").val();
                }
            }
        }

        var voData = {
            DocTypeName : $("#DocTypeName").val(),
            DocDate : $("#DocDate").val(),
            CodeType : $("#CodeType").val(),
            SerialNo :  $("#SerialNo").val(),
            Status : $("#Status").val()
        };
        $("#gridSearch").data("kendoGrid").dataSource.page(1);
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.data = { DocTypeName: voData.DocTypeName, DocDate: voData.DocDate,CodeType: voData.CodeType,SerialNo: voData.SerialNo, Status: voData.Status };
        $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.url = site_url("Dashboard/GetDataSearch");
        $("#gridSearch").data("kendoGrid").dataSource.read();
    }
    $(document).ready(function() {
        $("#DocDate").kendoDatePicker({format: DateFormat});
        $("#gridSearch").kendoGrid({
            height: "500px",
            width: "100%",
            columns: [
            {
            "title":"Doc Type Name",
            "width":"130px",
            "field":"t9050f002",
            },
            {
            "title":"Doc No ",
            "width":"100px",
            "field":"t1010f002",
            },
            {
            "title":"Doc Date",
            "width":"100px",
            "field":"t1010f003",
            },
            {
            "title":"ItemID",
            "width":"00px",
            "field":"t1011f003",
            },
            {
            "title":"Brand",
            "width":"90px",
            "field":"t1011f014",
            },
            {
            "title":"Model",
            "width":"100px",
            "field":"t1011f015",
            },
            {
            "title":"Color",
            "width":"100px",
            "field":"t1011f016",
            },
            {
            "title":"Status",
            "width":"100px",
            "field":"t1011f017",
            },
            {
            "title":"IMEI",
            "width":"180px",
            "field":"t1012f003",
            },
            {
            "title":"RFID",
            "width":"195px",
            "field":"t1012f002",
            },
            {
            "title":"Cost Price",
            "width":"100px",
            "field":"<?php echo T_TransactionStockMovementDetailSerialNo_CostPrice; ?>",
            },            
            {
            "title":"Remarks",
            "width":"100px",
            "field":"t1012f005",
            }            
            ],                     
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: 't1011'},
                        url: function() {
                            return site_url('Dashboard/GetDataSearch');
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridSearch').data('kendoGrid').dataSource.read();
                    $('#gridSearch').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t1011r001",
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:false,     
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });
    });
</script>
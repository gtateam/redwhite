<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2016, Global trend asia PT.
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Component Dashboard
 *
 * Dashboard Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 14.12.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2016, Global trend asia PT.
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Dashboard extends BC_Controller 
{
	
	function __construct()
    {
    	parent::__construct();
		$this->load->model(array('Dashboard_model','Options_model'));
	}

	public function inout()
	{
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render('index', $data);
	}

	public function Accessories()
	{
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render('accessories', $data);
	}

	public function BrandNew()
	{
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render('brandnew', $data);
	}

	public function Used()
	{
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render('used', $data);
	}

	public function alert($id=0)
	{
		$view = ($id) ? 'alert' : "alertnew";
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render($view, $data);
	}

	public function Search($id=0)
	{
		$data = '';
		$this->modules->render("search", $data);
	}

	public function StockBalance($id=0)
	{
		$data = '';
		$this->modules->render("stockbalance", $data);
	}

	public function GetDataSearch()
	{
		/*
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = 'v1010';
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1012r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
			$getList = $this->Dashboard_model->GetDataSearch($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->GetDataSearchCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
		*/

		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1010f003, t1010f001, t1010f002, t1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->GetDataSearch($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->GetDataSearchCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function GetListStock()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't6010f002, t6010f003, t6010f004, t6010f005, t6010f006, t6010f007, t6010f008';
   			$sortdir = 'ASC';
		}

		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}

		$getList = $this->Dashboard_model->GetListStock($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);		

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->GetTotalStockCount($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function GetTotalStockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		//$query = $this->Dashboard_model->GetTotalStockIn();
		//$info->TotalStockIn = ($query->first_row()->totalstockin) ? $query->first_row()->totalstockin : 0;
		$info->TotalStockIn = $this->Dashboard_model->GetTotalStockIn();
		
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetTotalStockOut()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$info->TotalStockOut = $this->Dashboard_model->GetTotalStockOut();

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}	

	public function GetTotalStockOnHand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$info->TotalStockOnHand = $this->Dashboard_model->GetTotalStockOnHand();

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockOnhand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
			$getList = $this->Dashboard_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
		
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockSold()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListstockSoldCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));	
	}

	public function GridStockSoldAccessories()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "v1991";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1991r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->GridStockSoldAccessories($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListstockSoldCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListstockInCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function stocAccessoriesOnhand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAccesoriesOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function AccessoriesstockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "v1991";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1991r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAccessoriesstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListAccessoriesstockInCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function AccessoriesstockSold()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "v1991";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1991r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAccessoriesstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListAccessoriesstockSoldCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function alertData()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_VIEWAlert;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAlert($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListAlertCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function NewStockOnhand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListNewStockOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListNewStockOnhandCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function NewstockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListNewstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListNewstockInCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function NewStockSold()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListNewstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListNewstockSoldCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function UsedStockOnhand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListUsedStockOnhand($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListUsedStockOnhandCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function UsedstockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListUsedstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListUsedstockInCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function UsedStockSold()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011f014, t1011f015, t1011f016, t1011f017, t1012f003';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListUsedstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$startdate,$enddate,$datenow);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListUsedstockSoldCount($table,$startdate,$enddate,$datenow);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function resetAlert()
	{
		$this->db->update(T_Alert,array(T_Alert_FLAG => 1));
	}

	public function stockOnhandCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$table = T_TransactionStockBalanceHeader;
		$this->db->select('SUM('.T_TransactionStockBalanceHeader_Quantity.') as QtyIn');
		// if(($startdate != "") || ($enddate != "")){
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		// }
		// else{
        //     $this->db->where($table.".t1990r002 BETWEEN STR_TO_DATE('$datenow 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')");
        // }
		$sql = $this->db->get($table);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockSoldCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListstockSoldCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockAccessoriesSoldCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");
		//print_r($enddate);die;

		//$query = "Select SUM(t1011f010) as QtyOut from t1010 JOIN t1011 on t1011.t1011f001 = t1010.t1010r001 where t1010f001 in ('IVSO') AND t1010r003 = 1";
		$query = "SELECT 
		SUM(t1991f005) AS QtyOut 
		FROM v1991 
		WHERE t1991f002 IN ('IVSO','SLPS','IVST')
		AND t1991r002 BETWEEN STR_TO_DATE(";
		if(($startdate != "") || ($enddate != "")){
			$query .= "'$startdate 00:00:01','%Y-%m-%d %H:%i:%s')
			AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')";
		}else{
			$query .= "'$datenow 00:00:01','%Y-%m-%d %H:%i:%s')
			AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')";
		}
		$query .= "AND t1991f006 ='IG-00002'";

		$sql = $this->db->query($query);
		$info->Qty = ($sql->first_row()->QtyOut) ? $sql->first_row()->QtyOut : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function alertCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$query = "Select t7011f003 as Qty, t8010f001 as ItemID from v7011 WHERE	DATE_FORMAT(t7011r002, '%Y-%m-%d') = '".date("Y-m-d")."' AND t7011f005 = 0";
		$sql = $this->db->query($query);
		$info->Qty = empty($sql->first_row()->ItemID) ? 0 : $sql->first_row()->Qty;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockInCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListstockInCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockAccessoriesInCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		// $query = "SELECT
		// 	SUM(t1011f010) AS QtyIn
		// FROM
		// 	t1011
		// JOIN t1010 ON t1010.t1010r001 = t1011f001
		// WHERE
		// 	t1010.t1010f001 IN ('IMBB', 'IVSI')
		// 	AND t1010.t1010r002 BETWEEN STR_TO_DATE(";
		// if(($startdate != "") || ($enddate != "")){
		// 	$query .= "'$startdate 00:00:01','%Y-%m-%d %H:%i:%s')
		// 	AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')";
		// }else{
		// 	$query .= "'$datenow 00:00:01','%Y-%m-%d %H:%i:%s')
		// 	AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')";
		// }
		// $query .="AND t1010.t1010r003 = 1 AND t1011.t1011f018 = 'IG-00002'";
		$query = "SELECT 
			SUM(t1991f005) AS QtyIn 
		FROM v1991 
		WHERE t1991f002 IN ('IVSI')
		AND t1991r002 BETWEEN STR_TO_DATE(";
		if(($startdate != "") || ($enddate != "")){
			$query .= "'$startdate 00:00:01','%Y-%m-%d %H:%i:%s')
			AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')";
		}else{
			$query .= "'$datenow 00:00:01','%Y-%m-%d %H:%i:%s')
			AND STR_TO_DATE('$datenow 23:59:59','%Y-%m-%d %H:%i:%s')";
		}
		$query .= "AND t1991f006 ='IG-00002'";

		$sql = $this->db->query($query);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockAccessoriesOnHand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$table = T_TransactionStockBalanceHeader;
		$this->db->select('SUM('.T_TransactionStockBalanceHeader_Quantity.') as QtyIn');
		$this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
		// if(($startdate != "") || ($enddate != "")){
        //     $this->db->where("t1990.t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		// }
		$this->db->where("i.t8010f009 = 'IG-00002'");

		$sql = $this->db->get($table);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockNewOnHand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$table = T_TransactionStockBalanceHeader;
		$this->db->select('SUM('.T_TransactionStockBalanceHeader_Quantity.') as QtyIn');
		$this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
		// if(($startdate != "") || ($enddate != "")){
        //     $this->db->where("t1990.t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		// }
		$this->db->where("i.t8010f014 = 'New'");

		$sql = $this->db->get($table);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockNewInCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListNewstockInCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockNewSoldCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListNewstockSoldCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockUsedOnHand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$table = T_TransactionStockBalanceHeader;
		$this->db->select('SUM('.T_TransactionStockBalanceHeader_Quantity.') as QtyIn');
		$this->db->join(T_MasterDataItem.' i', 'i.'.T_MasterDataItem_ItemID.'= '.$table.'.t1990f001', 'left');
		// if(($startdate != "") || ($enddate != "")){
        //     $this->db->where("t1990.t1990r002 BETWEEN STR_TO_DATE('$startdate 00:00:01','%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('$enddate 23:59:59','%Y-%m-%d %H:%i:%s')");
		// }
		$this->db->where("i.t8010f014 = 'Used'");

		$sql = $this->db->get($table);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockUsedInCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListUsedstockInCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockUsedSoldCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "t1011";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$startdate = ($this->input->get('StartDate') != "") ? date('Y-m-d',strtotime($this->input->get('StartDate'))) : "";
		$enddate = ($this->input->get('EndDate') != "") ? date('Y-m-d',strtotime($this->input->get('EndDate'))) : "";
		$datenow = date("Y-m-d");

		$rowcount = $this->Dashboard_model->getListUsedstockSoldCount($table, $startdate, $enddate, $datenow);
		$info->Qty = $rowcount;
		// if($rowcount > 0){
		// 	$info->Qty = $rowcount;
		// }else{
		// 	$info->errorcode = 32;
		// 	$info->msg = "Data not found!";
		// }
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file dashboard.php */
/* Location: ./app/modules/transaction/controllers/dashboard.php */

	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Expenses Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Expenses extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Expenses_model'));
	}

	public function index($id='')
	{
		$data = $this->Expenses_model->getDetail($id);
		$this->modules->render('/index', $data);
	}

	public function form($id='')
	{
		$sql1 = $this->db->get(T_SystemGST);
		$GST = $sql1->first_row()->{T_SystemGST_Value};
		$GST_text = $sql1->first_row()->{T_SystemGST_Text};
		$data = $this->Expenses_model->getDetail($id);
		$data["GST"] = $GST;
		$data["GST_text"] = $GST_text;
		$this->modules->render('/form', $data);
	}

	public function FormDetail($id='')
	{
		$sql1 = $this->db->get(T_SystemGST);
		$GST = $sql1->first_row()->{T_SystemGST_Value};
		$GST_text = $sql1->first_row()->{T_SystemGST_Text};
		$data = $this->Expenses_model->getDetail($id);
		$data["GST"] = $GST;
		$data["GST_text"] = $GST_text;
		$this->modules->render('/FormDetail', $data);
	}

	public function FormPrint($id='')
	{
		$this->template->set_layout('content_only');
		$sql1 = $this->db->get(T_SystemGST);
		$GST = $sql1->first_row()->{T_SystemGST_Value};
		$GST_text = $sql1->first_row()->{T_SystemGST_Text};
		$data = $this->Expenses_model->getDetail($id);
		$data["GST"] = $GST;
		$data["GST_text"] = $GST_text;
		$this->modules->render('/Print', $data);
	}

	public function Insert(){
		$this->db->trans_begin();
		try{
			$data = array(
				T_Expenses_Name => empty($this->input->post("Name"))? "":$this->input->post("Name"),
				T_Expenses_Description => empty($this->input->post("Description")) ? "" : $this->input->post("Description"),
				T_Expenses_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
				T_Expenses_AmountPaid => empty($this->input->post("AmountPaid")) ? 0 : $this->input->post("AmountPaid"),
				T_Expenses_GSTAmount => empty($this->input->post("GSTAmount")) ? 0 : $this->input->post("GSTAmount"),
				T_Expenses_MOP => empty($this->input->post("MOP")) ? "" : $this->input->post("MOP"),
				T_Expenses_GST => empty($this->input->post("GST")) ? "" : $this->input->post("GST")
			);
			$this->Expenses_model->Insert($data);
			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "SLPS";
			if(check_column(T_Expenses_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_Expenses_RecordID => $this->input->post("RecordID"),
					T_Expenses_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_Expenses_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_Expenses_Name => empty($this->input->post("Name")) ? "" : $this->input->post("Name"),
					T_Expenses_Description => empty($this->input->post("Description")) ? "" : $this->input->post("Description"),
					T_Expenses_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_Expenses_AmountPaid => empty($this->input->post("AmountPaid")) ? 0 : $this->input->post("AmountPaid"),
					T_Expenses_GSTAmount => empty($this->input->post("GSTAmount")) ? 0 : $this->input->post("GSTAmount"),
					T_Expenses_MOP => empty($this->input->post("MOP")) ? "" : $this->input->post("MOP"),
					T_Expenses_GST => empty($this->input->post("GST")) ? "" : $this->input->post("GST")
				);
				$this->Expenses_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Expenses_model->Delete($this->input->post("RecordID"));
			
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Expenses.php */
/* Location: ./app/modules/Expenses/controllers/Expenses.php */

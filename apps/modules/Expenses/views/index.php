<div id="gridStockIn"></div>
<script>
$(document).ready(function() {
    var gridIn = $("#gridStockIn").kendoGrid({
        //toolbar: ["excel"],
        toolbar: kendo.template($("#templateIn").html()),
        excel: {
            fileName: kendo.toString(new Date, "dd/MM/yyyy HH:mm") +"_Expenses.xlsx",
            filterable: true
        },
        height: "580px",
        width: "100%",
        columns: [
        { 
            "width": "100px",
            "template":
            ''
            +'<a class="k-button k-button-icontext k-grid-edit" href="'+site_url('Expenses/Form/#=t2020r001#')+'"><span class="k-icon k-edit"></span></a>'
            +'<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)" onclick="RemoveData(#=t2020r001#)"><span class="k-icon k-delete"></span></a>'
            +'<a class="k-button k-button-icontext" href="'+site_url('Expenses/FormDetail/#=t2020r001#')+'"><span class="k-icon k-i-hbars"></span></a>'
        },
        {
            "title":"Date",
            "width":"90px",
            "field":"<?php echo T_Expenses_DocDate; ?>",
        },
        {
            "title":"Name",
            "width":"150px",
            "field":"<?php echo T_Expenses_Name; ?>",
        },
        {
            "title":"Description",
            "width":"200px",
            "field":"<?php echo T_Expenses_Description; ?>",
        },
        {
            "title":"Mode of Payment",
            "width":"100px",
            "field":"<?php echo T_Expenses_MOP; ?>",
        },
        {
            "title":"Amount Paid",
            "width":"150px",
            "field":"<?php echo T_Expenses_AmountPaid; ?>",
        },
        {
            "title":"GST Amount",
            "width":"100px",
            "field":"<?php echo T_Expenses_GSTAmount; ?>",
        },
        {
            "title":"GST",
            "width":"100px",
            "field":"<?php echo T_Expenses_GST; ?>",
        }
        ],
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    data: {
                        table: "<?php echo T_Expenses; ?>",
                    },
                    url: site_url('Webservice/Read/Getlist'),
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#gridStockIn').data('kendoGrid').dataSource.read();
                $('#gridStockIn').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.count;
                },
                model: {
                    id: "<?php echo T_Expenses_RecordID; ?>",
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });

    $("#monthpicker").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM",
        dateInput: false,
        change: onChange,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;
            if (calendar.view().name === "year") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "year") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    $("#yearspicker").kendoDatePicker({
        start: "decade",
        depth: "decade",
        format: "yyyy",
        dateInput: false,
        change: onChange,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;

            if (calendar.view().name === "decade") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "decade") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });
});

function onChange() {
    var vomonth = $("#monthpicker").data("kendoDatePicker").value();
    var voyear = $("#yearspicker").data("kendoDatePicker").value();
    var month = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'MM' );
    var defYear = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'yyyy' );
    var year = kendo.toString(kendo.parseDate(voyear,'yyyy-MM-dd'), 'yyyy' );
    var TransDate = (month == null) ? year : year+'-'+month;
    TransDate = (year == null) ? defYear+'-'+month : TransDate;
    TransDate = (year == null && month == null) ? null : TransDate;
    $("#gridStockIn").data("kendoGrid").dataSource.page(1);
    $("#gridStockIn").data("kendoGrid").dataSource.transport.options.read.data = {table: "<?php echo T_Expenses; ?>", TransDate: TransDate};
    $("#gridStockIn").data("kendoGrid").dataSource.transport.options.read.url = site_url("Webservice/Read/Getlist");
    $("#gridStockIn").data("kendoGrid").dataSource.read();
}

function RemoveData(id)
    {
        var result = confirm("Delete this record?");
        if (result) {
            if(id){
                var voData = {
                    RecordID: id
                };
                $.ajax({
                    type: 'POST',
                    data: voData,
                    url:  site_url('Expenses/Delete'),
                    beforeSend: function(){
                    },
                    success: function (result) {
                    if (result.errorcode != 0) {
                        new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                    } else {
                        new PNotify({ title: "Success", text: "Remove data", type: 'success', shadow: true });
                        $('#gridStockIn').data('kendoGrid').dataSource.read();
                        $('#gridStockIn').data('kendoGrid').refresh();
                    }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
            }
        }
    }
</script>
<script type="text/x-kendo-template" id="templateIn">
    <div style="display: inline-block;">
        <a class="k-button k-button-icontext k-grid-add" href="#=site_url('Expenses/Form')#"><span class="k-icon k-add"></span>Add New</a>
        <a class="k-button k-button-icontext k-grid-excel" href="\\#"><span class="k-icon k-i-excel"></span>Export to Excel</a>
    </div>
    <div style="float: right;">
        <label class="category-label" for="category">Show by :</label>
        <input id="monthpicker" style="width: 150px"/>
        <input id="yearspicker" style="width: 150px"/>
    </div>
</script>
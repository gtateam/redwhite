<style type="text/css">.mrg{margin-bottom:5px;}</style>
<div class="row">
    <div class="col-md-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
                        <a data-panel-dismiss="" class="panel-action panel-action-dismiss" href="#"></a>
                    </div>
                    <h2 class="panel-title">Detail</h2>
                </header>
                <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_Expenses_RecordID}) ? ${T_Expenses_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_Expenses_RecordTimestamp}) ? ${T_Expenses_RecordTimestamp} : ''; ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row mrg">
                                <label class="col-md-3 form-label">Date</label>
                                <div class="col-md-6">
                                    <?php $items=array(
                                        'id'=> 'DocDate',
                                        'class' => 'KendoDatePicker',
                                        'style' => 'width:150px;',
                                        'readonly' => true,
                                        'value' => isset(${T_Expenses_DocDate}) ? ${T_Expenses_DocDate} : date(FORMATDATE),
                                    ); 
                                    echo form_input($items); ?>
                                </div>
                            </div>
                            <div class="row mrg">
                                <label class="col-md-3 form-label">Name</label>
                                <div class="col-md-6">
                                    <?php $items=array(
                                        'id'=> 'Name',
                                        'class' => '',
                                        'style' => 'width:250px;',
                                        'readonly' => true,
                                        'value' => isset(${T_Expenses_Name}) ? ${T_Expenses_Name} : "",
                                    ); 
                                    echo form_input($items); ?>
                                </div>
                            </div>
                            <div class="row mrg">
                                <label class="col-md-3 form-label">Description</label>
                                <div class="col-md-6">
                                    <?php $items=array(
                                        'id'=> 'Description',
                                        'class' => 'k-input k-textbox',
                                        'style' => 'width:350px;',
                                        'readonly' => true,
                                        'value' => isset(${T_Expenses_Description}) ? ${T_Expenses_Description} : "",
                                    ); 
                                    echo form_input($items); ?>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="row mrg">
                                <label class="col-md-3 form-label">Amount Paid</label>
                                <div class="col-md-6">
                                    <?php $items=array(
                                        'id'=> 'AmountPaid',
                                        'class' => '',
                                        'style' => 'width:150px;',
                                        'readonly' => true,
                                        'value' => isset(${T_Expenses_AmountPaid}) ? ${T_Expenses_AmountPaid} : "",
                                    ); 
                                    echo form_input($items); ?>
                                </div>
                            </div>
                            <div class="row mrg">
                                <label class="col-md-3 form-label">GST (<?php echo $GST_text; ?>)</label>
                                <div class="col-md-6">
                                    <?php
                                    $items = array(
                                    'id' => 'GST',
                                    'class' => '',
                                    'style' => 'width:150px;',
                                    'readonly' => true,
                                    'value' => isset(${T_Expenses_GSTAmount}) ? ${T_Expenses_GSTAmount} : ''
                                    );
                                    echo form_input($items);
                                    ?>
                                    <input type="checkbox" id="include_gst" onclick="GSTAmount();" readonly/> include
                                </div>
                            </div>
                            <div class="row mrg">
                                <label class="col-md-3 form-label">Mode of Payment</label>
                                <div class="col-md-6">
                                    <?php $items=array(
                                        'id'=> 'MOP',
                                        'class' => '',
                                        'style' => 'width:250px;',
                                        'readonly' => true,
                                        'value' => isset(${T_Expenses_MOP}) ? ${T_Expenses_MOP} : "",
                                    ); 
                                    echo form_input($items); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="panel-footer">
                    <button class="btn btn-default" type="button" onclick="goBack();">Back</button>
                </footer>
            </section>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#GST").kendoNumericTextBox({ });
    $("#AmountPaid").kendoNumericTextBox({
        change: GSTAmount,
        spin: GSTAmount
    });

    $(".KendoDatePicker").kendoDatePicker({format:DateFormat});

    $("#Name").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 13},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });

    $("#MOP").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 14},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });

    if($("#GST").val() != ""){
        document.getElementById("include_gst").checked = true;
    }

});

function GSTAmount()
{
    var include_gst = document.getElementById("include_gst");
    var Amount = $("#AmountPaid").val();
    var GST = <?php echo $GST; ?>;
    var GSTAmount = Amount * GST;
    if(include_gst.checked == true){
        $("#GST").val(GSTAmount);
        $("#GST").data("kendoNumericTextBox").value(GSTAmount);
    }else{
        $("#GST").val('');
        $("#GST").data("kendoNumericTextBox").value('');
    }
}

function insert()
{
    var voData = {
        DocDate: $('#DocDate').val(),
        Name: $('#Name').val(),
        Description : $('#Description').val(),
        AmountPaid : $('#AmountPaid').val(),
        GSTAmount : $('#GST').val(),
        MOP : $('#MOP').val(),
        GST : <?php echo $GST; ?>
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('Expenses/insert'),
            success: function (result) {
            if (result.errorcode != 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
            } else {
                new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                window.location.replace(site_url('Expenses'));
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function update()
{
    var voData = {
        RecordID: $('#RecordID').val(),
        TimeStamp: $('#TimeStamp').val(),
        DocDate: $('#DocDate').val(),
        Name: $('#Name').val(),
        Description : $('#Description').val(),
        AmountPaid : $('#AmountPaid').val(),
        GSTAmount : $('#GST').val(),
        MOP : $('#MOP').val(),
        GST : <?php echo $GST; ?>
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('Expenses/update'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(site_url('Expenses'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function checkForm(voData) {
    var valid = 1;
    var msg = "";
    if (voData.Name == "") { valid = 0; msg += "Name is required" + "\r\n"; }
    if (voData.AmountPaid == "") { valid = 0; msg += "AmountPaid is required" + "\r\n"; }
    var voRes = {valid: valid, msg: msg};
    return voRes;
}
</script>
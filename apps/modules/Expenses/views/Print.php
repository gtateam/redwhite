
        <div class="invoice">
            <header class="clearfix">
                <div class="row">
                    <?php //$this->navigations->kopsurat(); ?>
                </div>
            </header>
            <div class="clearfix">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Date:</span>
                                <span class="value"><?php echo date(FORMATDATEREPORT,strtotime(${T_Expenses_DocDate})); ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-Left">
                            <p class="mb-none">
                                <span class="text-dark">Name:</span>
                                <span class="value"><?php echo ${T_Expenses_Name}; ?></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Amount Paid:</span>
                                <span class="value"><?php echo ${T_Expenses_AmountPaid}; ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-Left">
                            <p class="mb-none">
                                <span class="text-dark">GST (<?php echo $GST_text; ?>):</span>
                                <span class="value"><?php echo ${T_Expenses_GSTAmount}; ?></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Made of Payment:</span>
                                <span class="value"><?php echo ${T_Expenses_MOP}; ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-Left">
                            <p class="mb-none">
                                <span class="text-dark">Description:</span>
                                <span class="value"><?php echo ${T_Expenses_Description}; ?></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<script>
    window.print();
</script>
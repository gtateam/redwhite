<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Expenses_model extends CI_Model
{
    public function getDetail($id){
        $this->db->select('*');
        $this->db->from(T_Expenses);
        $this->db->where(T_Expenses_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        return $data;
    }

    public function Insert($data){
        $this->db->trans_begin();
        $this->db->insert(T_Expenses, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
        return true;
    }

    public function Update($data){
        $this->db->trans_begin();
        $parentID = $data[T_Expenses_RecordID];
        $this->db->where(T_Expenses_RecordID,$parentID);
        $this->db->update(T_Expenses, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_Expenses, array(T_Expenses_RecordID => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}

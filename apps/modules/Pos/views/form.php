<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-inserts', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Point of Sale</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_SalesInvoiceHeader_RecordID}) ? ${T_SalesInvoiceHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_SalesInvoiceHeader_RecordTimestamp}) ? ${T_SalesInvoiceHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <!--<div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-textbox', 'value' => 'SLPS',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>-->
                        <div class="row">
                            <label class="col-md-3 form-label">Transaction No:</label>
                            <div class="col-md-9">
                                 <?php $items=array( 'id'=> '', 'class' => 'k-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : substr(DocNo('SLPS'), 2), 'style' => 'background-color:#eee;', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Salesperson</label>
                            <div class="col-md-9">
                                <input type="hidden" id="SalespersonID" />
                                <?php $items=array( 'id'=> 'SalespersonName', 'class' => 'k-input k-textbox', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "", ); echo form_input($items); ?>
                                <div class="k-button" id="LookupEventSalespersonID"> <div class="k-icon k-i-search"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Transaction Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Customer</label>
                            <div class="col-md-9">
                                <input type="hidden" id="BizPartnerID" />
                                <?php $items=array( 'id'=> 'BizPartnerName', 'class' => 'k-input k-textbox', 'value' => '', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                                <div class="k-button" id="LookupEventBizPartnerID"> <div class="k-icon k-i-search"></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Term Of Payment</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'TermofPayment', 'class' => 'k-input k-textbox', 'value' => 'Cash' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount VAT</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountVAT', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountVAT}) ? ${T_SalesInvoiceHeader_AmountVAT} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Currency</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'CurrencyID', 'class' => 'k-input k-textbox', 'value' => 'IDR',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount Subtotal</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountSubtotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? ${T_SalesInvoiceHeader_AmountSubtotal} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Exchange Rate</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'ExchRate', 'class' => 'k-input', 'value' => '1',  'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Amount Total</label>
                            <div class="col-md-9">
                                <?php //$items=array( 'id'=> 'AmountTotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountTotal}) ? ${T_SalesInvoiceHeader_AmountTotal} : "", ); echo form_input($items); ?>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                       
                    </div>
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                 <textarea class="k-input k-textbox" id="Remarks"><?php echo (isset(${T_SalesInvoiceHeader_Remarks}))? ${T_SalesInvoiceHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-9">
                        <div style="">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">Doc No</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'DocNo', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : Substr(DocNo('SLPS'),2),'style' => 'background-color:#eee;', 'readonly' => 'TRUE'); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Date</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;IMEI</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'IMEI', 'class' => 'K-input k-textbox', 'value' =>  "", "onkeypress" => "return imeilookup(event);" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Email</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'Email','onblur'=>'ValidateEmail()', 'style' => 'width:250px;', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;I/C No</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'IC', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3" style="margin-left:-3px;">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Phone</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'Phone','style'=>'margin-left:-25px;', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!-- <div class="form-group">
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">Cust Name</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'CustomerName', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Address</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'Address', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Email</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'Email','onblur'=>'ValidateEmail()', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3" style="margin-left:-3px;">
                                    <div class="row">
                                        <label class="col-md-5 form-label">&nbsp;&nbsp;&nbsp;Remarks</label>
                                        <div class="col-md-7">
                                            <?php $items=array( 'id'=> 'Remarks','style'=>'margin-left:-25px;', 'class' => 'K-input k-textbox', 'value' => isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "" ); echo form_input($items); ?>
                                        </div>
                                    </div>
                                </div>
                            </div><br> -->
                            <br>
                            <div>
                                <!--<a id="DetailModal" class="mb-xs mt-xs mr-xs btn btn-xs btn-success"><i class="fa fa-plus"></i> &nbsp;Add New</a>-->
                                <a id="removeAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detail');"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
                                <a id="scanTagStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="scanTag(1);"><i class="fa fa-barcode"></i> &nbsp;Start Scan</a>
                                <a id="scanTagStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="scanTag(0);" style="display:none;"><i class="fa fa-barcode"></i> &nbsp;Stop Scan</a>
                                <div style="display: inline-block; margin: 10px;">IMEI : <?php $items=array( 'id'=> 'IMEI', 'style' => 'width:250px;', 'class' => 'K-input k-textbox', 'value' =>  "", "onkeypress" => "return imeilookup(event);" ); echo form_input($items); ?></div> 
                            </div>
                            <div style="overflow:scroll;height:250px;">
                            <input id="DoRemoveID" type="hidden" />
                                <table id="table-detail" class="table table-bordered mb-none">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="80px"><input type="checkbox" id="detailCheckAll" onclick="CheckAll('detail');"> Action</th>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Item ID</th>
                                            <th data-col="ItemName" style="display:none;">Item Name</th>
                                            <th data-col="IMEI">IMEI</th>
                                            <th data-col="EPC">RFID</th>
                                            <th data-col="ItemGroup">Type</th>
                                            <th data-col="Brand">Brand</th>
                                            <th data-col="Model">Model</th>
                                            <th data-col="Color">Color</th>
                                            <th data-col="Status">Status</th>
                                            <th data-col="qtyf" style="display:none;">Quantity</th>
                                            <th data-col="UnitPrice">Selling Price</th>
                                            <th data-col="LineTotal" style="display:none;">Line Total</th>
                                            <th data-col="LocationID" style="display:none;">LocationID</th>
                                            <th data-col="LocationName" style="display:none;">LocationNamel</th>
                                            <th data-col="RecordIDDetail" style="display:none;"></th>
                                            <th data-col="RecordFlag" style="display:none;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"> <a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a> <a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemID]. '">'.$item[T_SalesInvoiceDetail_ItemID]. '</td>
                                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailItemGroupv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailBrandv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailModelv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailColorv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailStatusv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_ItemName]. '</td>
                                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Quantity]. '">'.$item[T_SalesInvoiceDetail_Quantity]. '</td>
                                        <td id="detailUnitPricev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_UnitPrice]. '">'.$item[T_SalesInvoiceDetail_UnitPrice]. '</td>
                                        <td id="detailLineTotalv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_LineTotal]. '">'.$item[T_SalesInvoiceDetail_LineTotal]. '</td>
                                        <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Remarks]. '">'.$item[T_SalesInvoiceDetail_Remarks]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_LocationID]. '">'.$item[T_SalesInvoiceDetail_LocationID]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_LocationName]. '">'.$item[T_SalesInvoiceDetail_LocationName]. '</td>
                                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_RecordID]. '" style="display:none;">'.$item[T_SalesInvoiceDetail_RecordID]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <table id="table-summary" class="table table-bordered mb-none">
                            <thead id="head-summary"><tr><th colspan="2">Summary</th></tr></thead>
                            <tbody>
                            <tr><td>Subtotal</td><td><?php $items=array( 'id'=> 'AmountSubtotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? ${T_SalesInvoiceHeader_AmountSubtotal} : "", ); echo form_input($items); ?></td></tr>
                            <tr><td>GST</td><td><?php $items=array( 'id'=> 'AmountVAT', 'class' => '', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountVAT}) ? ${T_SalesInvoiceHeader_AmountVAT} : "", ); echo form_input($items); ?></td></tr>
                            <tr><td>Total</td><td><?php $items=array( 'id'=> 'AmountTotal', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => isset(${T_SalesInvoiceHeader_AmountTotal}) ? ${T_SalesInvoiceHeader_AmountTotal} : "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                            <thead id="head-cash"><tr><th colspan="2">Payment Mode</th></tr></thead>
                            <tbody>
                            <tr><td>Payment Mode</td><td><?php $items=array( 'id'=> 'PaymentMode', 'class' => '', 'value' =>  "", ); echo form_input($items); ?></td></tr>
                            <tr><td>Payment</td><td><?php $items=array( 'id'=> 'Payment', 'class' => 'k-input', 'value' =>  "", ); echo form_input($items); ?></td></tr>
                            <tr><td>Outstanding</td><td><?php $items=array( 'id'=> 'Change', 'class' => 'k-input', 'readonly' => 'TRUE', 'value' => "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                            <thead style="display:none;" id="head-credit"><tr><th colspan="2">Credit</th></tr></thead>
                            <tbody style="display:none;">
                            <tr><td>Credit Card No</td><td><?php $items=array( 'id'=> 'CreditCardNo', 'class' => 'k-input', 'value' =>  "", ); echo form_input($items); ?></td></tr>
                            </tbody>
                        </table>
                    </div>  
                </div>
            </div>
            <footer class="panel-footer text-right">
                <button class="btn btn-primary" type="button" onclick="insert(); return false;">Tender</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-5">
                <input id="RecordFlag" type="hidden" />
                <input id="RecordIDDetail" type="hidden" />
                <div class="k-edit-label">#</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="RowIndex" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Item ID</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemID" datarequired="0" readonly />
                    <div class="k-button" id="LookupEventItemID"> <div class="k-icon k-i-search"></div> </div>
                </div>
                <div class="k-edit-label">Item Name</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemName" datarequired="0" readonly />
                    <input type="hidden" id="ItemType" class="k-input k-textbox" datarequired="1"  readonly/>
                </div>
                 <div class="k-edit-label">Qty</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="Qty" datarequired="0" />
                </div>
                <div class="k-edit-label">UOM</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="UOM" datarequired="0" readonly/>
                </div>
            </div>
            <div class="col-md-5">
                <div class="k-edit-label">Unit Price</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="UnitPrice" datarequired="0" />
                </div>
                <div class="k-edit-label">Line Total</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="LineTotal" datarequired="0" />
                </div>
                <div class="k-edit-label">Remarks</div>
                <div class="k-edit-field">
                    <textarea class="k-textbox" id="RemarksDetail"></textarea>
                </div>
            </div>
        </div>
        <a id="DetailModalSub" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" style="display:none;" onclick="openDetailModal();"><i class="fa fa-plus"></i> &nbsp;Add New</a>
        <a id="detailSubRemoveAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detailSub');" style="display:none;"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
        <div id="tableDetailModalSub" style="overflow:auto; display:none;">
            <table id="table-detailSub" class="table table-bordered mb-none">
                <thead id="head-detailSub">
                    <tr>
                        <th width="80px"><input type="checkbox" id="detailSubCheckAll" onclick="CheckAll('detailSub');"> Action</th>
                        <th data-col="SerialNo">SerialNo</th>
                    </tr>
                </thead>
                <tbody id="list-detailSub">
                </tbody>
            </table>
        </div>

        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="fa fa-cancel"></i> &nbsp;Cancel</button>
        </div>
    </div>
</div>

<div style="display:none;" id="SendEmail" data-loading-overlay>
    <div class="k-edit-form-container" align="center">
        <img width="30px" src="assets/img/loading.gif" />
    </div>
</div>

<!--  End Modal Form Detail -->

<?php
 $dataItem = array(
    array('field' => T_TransactionStockBalanceHeader_ItemID, 'title' => 'Item ID', 'width' => '50px'),
    array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '80px'),
    array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location', 'width' => '80px'),
    array('field' => T_TransactionStockBalanceHeader_Quantity, 'title' => 'Qty', 'width' => '50px'),
    array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Type', 'width' => '50px'),
    array('field' => T_MasterDataItem_EPC, 'title' => 'EPC', 'width' => '80px'),
    array('field' => T_MasterDataItem_Barcode, 'title' => 'Barcode', 'width' => '80px')
);
//Double Click Throw Data to Form
$columnItem = array(
    array('id' => 'RecordIDDetail2', 'column' => T_TransactionStockBalanceHeader_RecordID),        
    array('id' => 'ItemID', 'column' => T_TransactionStockBalanceHeader_ItemID),
    array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
    array('id' => 'ItemType', 'column' => T_MasterDataGeneralTableValue_Key),
    array('id' => 'EPC', 'column' => T_MasterDataItem_EPC),
    array('id' => 'Barcode', 'column' => T_MasterDataItem_Barcode),
    array('id' => 'UnitPrice', 'column' => T_MasterDataItem_UnitPrice),
    array('id' => 'UOM', 'column' => T_MasterDataItem_UOMID),
);
$filter = array('');

//id, title, size, URL, data field in database, Throw Data To form when click
echo kendoModalLookup("ItemID", "Data Item", "700px", "Stockmovement/Stocktransfer/GetStockList", $dataItem, $columnItem,T_TransactionStockBalanceHeader,'',$filter);

//Location Lookup
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("Location", "Data Location", "500px", "Webservice/Read/Getlist", $dataLocation, $columnLocation,T_MasterDataLocation);

    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataperson_PersonID, 'title' => 'ID', 'width' => '200px'),
        array('field' => T_MasterDataperson_PersonName, 'title' => 'Name', 'width' => '200px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'SalespersonID', 'column' => T_MasterDataperson_PersonID),
        array('id' => 'SalespersonName', 'column' => T_MasterDataperson_PersonName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("SalespersonID", "Sales Person", "700px", "Webservice/Read/getlist", $dataItem, $columnItem,T_MasterDataperson);

    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataBizPartnerHeader_BizPartnerID, 'title' => 'ID', 'width' => '200px'),
        array('field' => T_MasterDataBizPartnerHeader_BizPartnerName, 'title' => 'Name', 'width' => '200px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'BizPartnerID', 'column' => T_MasterDataBizPartnerHeader_BizPartnerID),
        array('id' => 'BizPartnerName', 'column' => T_MasterDataBizPartnerHeader_BizPartnerName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("BizPartnerID", "Customer", "700px", "Webservice/Read/getlist", $dataItem, $columnItem,T_MasterDataBizPartnerHeader);

?>

<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 

if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>";
var SCAN = "<?php echo $scan; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {
    var realTime = "";
    $("#RowIndex").kendoNumericTextBox(); 
    $("#RowIndex2").kendoNumericTextBox(); 
    $("#ExchRate").kendoNumericTextBox(); 
    $("#AmountVAT").kendoNumericTextBox({
        format: "p0",
        min: 0,
        max: 1,
        step: 0.01,
        change: sumTotal,
        spin: sumTotal
    }); 
    $("#AmountSubtotal").kendoNumericTextBox({spinners: false}); 
    $("#AmountTotal").kendoNumericTextBox({spinners: false}); 
    $("#Change").kendoNumericTextBox({spinners: false}); 
    $("#CreditCardNo").kendoMaskedTextBox({
        mask: "0000 0000 0000 0000"
    });
    $("#Payment").kendoNumericTextBox({
        change:totalPayment,
        spinners: false
    });
    $("#UnitPrice").kendoNumericTextBox(); 
    $("#LineTotal").kendoNumericTextBox(); 

    $("#SendEmail").kendoWindow({
        width: "400px",
        title: "Sending invoice ...",
        visible: false,
        modal: true,
        actions: [
        "Close"
        ],
    });

    kendoModal("detailForm","Add Detail","850px");
    kendoModal("detailSubForm","Add Item","400px");

     $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        cleardetail("detailSub", 0);
    });
    sumTotal('detail');
    $("#kgSerialNo").delegate("tbody>tr", "dblclick", dblclickSerialNo);

    if(SCAN==1){
        $("#scanTagStart").hide();
        $("#scanTagStop").show();
    }else{
        $("#scanTagStart").show();
        $("#scanTagStop").hide();
    }
    $("#PaymentMode").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Mode",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 10},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    //amountVAT();
});
function ItemExist(ItemID,indx)
{
    var exist = getTotal('detail','string',indx,2);
    if(exist){
        if(exist.indexOf(ItemID) == -1){
            return true;
        }
        return false;
    }else{
        return true;
    }
}

function imeilookup(e) {
    if (e.keyCode == 13) {
        var imei = $("#IMEI").val();
        getdataScanImei(imei);
    }
}

function amountVAT(gst){
    $('#AmountVAT').data("kendoNumericTextBox").value(gst);
}
function scanTag(id)
{
    var msg ="";
    var voData = {
        id: id,
    };
    if(id){
        var msg = {
            Title : "Web Triger",
            Text : "Start Scan"
        }
        var typeData = { broadType : "Gate Start", data : msg};
        conn.sendMsg(typeData);
        $("#scanTagStart").hide();
        $("#scanTagStop").show();
    }else{
        var msg = {
            Title : "Web Triger",
            Text : "Stop Scan"
        }
        var typeData = { broadType : "Gate Stop", data : msg};
        conn.sendMsg(typeData);
        $("#scanTagStart").show();
        $("#scanTagStop").hide();
    }

    // $.ajax({
    //     type: 'POST',
    //     data: voData,
    //     url:  site_url('Pos/TrigerReader'),
    //     success: function (result) {
    //     if (result.errorcode > 0) {
    //         new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
    //     } else {
    //         if(id){
    //             $("#scanTagStart").hide();
    //             $("#scanTagStop").show();
    //             msg = "Start"; 
    //             realTime = setInterval(function(){ getdataScan(); }, 1000);
    //         }else{
    //             $("#scanTagStart").show();
    //             $("#scanTagStop").hide();
    //             msg = "Stop";         
    //             // $("#list-detail").html(result.html);
    //             // sumTotal('detail');
    //             clearInterval(realTime);
    //         }
    //         new PNotify({ title: "Reader "+msg, text: result.msg , type: 'info', shadow: true });
    //     }
    //     },
    //     error: function (jqXHR, textStatus, errorThrown) {
    //         alert(jQuery.parseJSON(jqXHR.responseText));
    //     }
    // });
}
function insert()
    {
        var detail = getDetailSubItemPOS('detail');
        var voData = {
            DocDate: $('#DocDate').val(),
            DocStatus: $('#DocStatus').val(),
            //TermofPayment: $('#TermofPayment').val(),
            //CurrencyID: $('#CurrencyID').val(),
            //ExchRate: $('#ExchRate').val(),
            //SalespersonID: $('#SalespersonID').val(),
            //BizPartnerID: $('#BizPartnerID').val(),
            AmountVAT: $('#AmountVAT').val(),
            AmountSubtotal: $('#AmountSubtotal').val(),
            AmountTotal: $('#AmountTotal').val(),
            Payment: $('#Payment').val(),
            IC: $('#IC').val(),
            CustomerName: $('#CustomerName').val(),
            Address: $('#Address').val(),
            Phone: $('#Phone').val(),
            Email: $('#Email').val(),
            PaymentMode: $('#PaymentMode').val(),
            Outstanding: $('#Change').val(),
            Remarks: $('#Remarks').val(),
            detail: detail
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Pos/insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    window.open(site_url('Pos/print_item/'+result.id));
                    //rewriteChecking();)
                    setTimeout(function(){
                        var id = {
                            RecorID : result.id,
                        };
                        $.ajax({
                            type: 'POST',
                            data: id,
                            url: site_url('Pos/mail_attachment/'+result.id),
                            beforeSend: function(){
                                $('#SendEmail').trigger('loading-overlay:show');
                                $("#SendEmail").data("kendoWindow").center().open();
                            },
                            success: function(resultemail){
                                if(resultemail.errorcode > 0){
                                    new PNotify({ title: "warning", text: "Email not Sent", type: 'warning', shadow: true });
                                }else{
                                    new PNotify({ title: "Info", text: "Email has been Sent", type: 'info', shadow: true });
                                    $("#SendEmail").data("kendoWindow").center().close();
                                    window.location.replace(current_url());
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jQuery.parseJSON(jqXHR.responseText));
                            }
                        });
                        new PNotify({ title: "Success", text: "Transaction done", type: 'success', shadow: true });

                    }, 4000);
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailSubItem('detail');
     var voData = {
        RecordID: ID,
        TimeStamp: $('#TimeStamp').val(),
        DocNo: $('#DocNo').val(),
        DocDate: $('#DocDate').val(),
        DocStatus: $('#DocStatus').val(),
        TermofPayment: $('#TermofPayment').val(),
        CurrencyID: $('#CurrencyID').val(),
        ExchRate: $('#ExchRate').val(),
        SalespersonID: $('#SalespersonID').val(),
        BizPartnerID: $('#BizPartnerID').val(),
        AmountVAT: $('#AmountVAT').val(),
        AmountSubtotal: $('#AmountSubtotal').val(),
        AmountTotal: $('#AmountTotal').val(),
        Payment: $('#Payment').val(),
        DoRemoveID : $("#DoRemoveID").val(),
        detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Pos/update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(site_url('Pos'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.Email == "") { valid = 0; msg += "Email is required" + "\r\n"; }
    if (voData.DocDate == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.Outstanding == "") { valid = 0; msg += "Please insert correct payment" + "\r\n"; }
    if (voData.detail == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }


    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}

//Sum Total
function sumTotal(target){
    var subTotal = getTotal("detail","float",12,2);
    var GST = $("#AmountVAT").data("kendoNumericTextBox").value();
    var VAT = GST * subTotal;
    var Total = VAT + subTotal;
    $("#AmountSubtotal").data("kendoNumericTextBox").value(subTotal);
    $("#AmountTotal").data("kendoNumericTextBox").value(Total);
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);
        Calculate();
    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

function rewriteChecking()
{
    var msg ="";
    var voData = {
        id: true,
    };
    $.ajax({
        type: 'GET',
        data: voData,
        url:  site_url('Pos/rewriteChecking'),
        success: function (result) {
        if (result.errorcode > 0) {
            new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
        } else {
            if(result.rewrite == "1"){
                rewriteChecking();
            }else{
                new PNotify({ title: "Success", text: "Transaction done.", type: 'success', shadow: true });
                setTimeout(function(){
                    window.location.replace(current_url());
                }, 4000);
            }
        }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
}
function GetDataSub(id)
    {
        var voData = {
            RecordID: id
        }; 
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stocktransfer/GetSubItem')+"?editable=1",
            success: function (result) {
                $('#list-detailSub').append(result.html);
                var qty = document.getElementById("list-detailSub").rows.length;
                $('#Qty').data('kendoNumericTextBox').value(qty);
                var target = "detailSub";
                var RowIndex = $("#RowIndex").data("kendoNumericTextBox").value();
                var htmlUpdate = $('#list-'+target).html();
                var ID = current_url()+target+RowIndex;
                localStorage[ID] = htmlUpdate;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#detailSubForm").kendoWindow({
            width: "500px",
            title: "Serial NO",
            visible: false,
            modal: true,
            actions: [
            "Close"
            ],
        });

        $("#kgSerialNo").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                    },
                },
                sync: function(e) {
                    // $("#kgLogStocking").data("kendoGrid").dataSource.read();
                    // $("#kgLogStocking").data("kendoGrid").refresh();
                },
                schema: {
                    data: function(datas){
                        return datas.data;
                    },
                    total: function(datas){
                        return datas.count;
                    },
                    model: {
                        id: "RecordID",
                    }
                },                        
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
                autoBind: false,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                    height: "500px",
                    width: "100%",
                    columns: [{"field":"<?php echo T_TransactionStockBalanceDetail_SerialNo; ?>","title":"Serial NO","width":"50px",},
                    ],
            });

    });
    function openDetailModal()
    {
        var id = $("#RecordIDDetail2").val();
        $.ajax(
        {
            type: 'GET',
            url: site_url("Webservice/Read/Getlist"),
            dataType: 'json',
            data: { table: "t1992", customfilter: {t1992f001:id,t1992f003:1}  },
            success: function (result) {
                $("#kgSerialNo").data("kendoGrid").dataSource.data(result.data);
            }
        });
    }

    function dblclickSerialNo(e)
    {
        var grid = $("#kgSerialNo").data("kendoGrid");
        var voRow = grid.dataItem(grid.select());
        var SNexist = getTotal('detailSub','string',1,2);
        if(SNexist){
            if(SNexist.indexOf(voRow.t1992f002) == -1){
                GetDataSub(voRow.t1992r001);
            }
        }else{
            GetDataSub(voRow.t1992r001);
        }
    }

    function calculating(i,qty){
        var price = $("#pricef-"+i).val();
        $("#pricef-"+i).attr("data-val",price);
        var UP = $("#detailUnitPricev-"+i).attr("data-val");
        var qtypcs = $("#qtyf-"+i).val();
        var LT = price * qtypcs;
        var LNMusk = accounting.formatNumber(LT,2,",");
        $("#detailUnitPricev-"+i).attr("data-val",price);
        $("#detailUnitPricev-"+i).attr("data-def",price);
        $("#detailLineTotalv-"+i).attr("data-val",LT);
        $("#detailQtyv-"+i).attr("data-val",qtypcs);
        $("#detailQtyv-"+i).attr("data-def",qtypcs);
        $("#detailLineTotalv-"+i).html(LNMusk);

        $('#Payment').data('kendoNumericTextBox').value("");
        $('#AmountTotal').data('kendoNumericTextBox').value("");

        sumTotal('detail');
    }

    function calculating2(i,qty){
        var qty = $("#pricef-"+i).val();
        calculating(i,qty);
    }

    function totalPayment()
    {
        var payment = $('#Payment').data('kendoNumericTextBox').value();
        var AmountTotal = $('#AmountTotal').data('kendoNumericTextBox').value();
        if(AmountTotal == '0'){
            new PNotify({ title: "Info", text: "Amount total still 0", type: 'warning', shadow: true });
            return false;
        }else{
            if(payment < AmountTotal){
                new PNotify({ title: "Info", text: "Amount paid shall not be less than the amount total ", type: 'warning', shadow: true });
                $('#Payment').focus();
                return false;
            }else{
                var change = payment - AmountTotal;
                $('#Change').data('kendoNumericTextBox').value(change);
            }
        }
    }

    function getdataScan()
    {
        var msg ="";
        var voData = {
            id: true,
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Pos/getdataScan'),
            success: function (result) {
                //PNotify.removeAll();
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    $("#list-detail").html(result.html);
                    sumTotal('detail');
                    //new PNotify({ title: "Reader "+msg, text: result.msg , type: 'info', shadow: true });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
    function getdataScanSocket(epc)
    {
        var msg ="";
        var status ="";
        var gst =0;
        var rowCount = document.getElementById("list-detail").rows.length + 1;
        var voData = {
            id: rowCount,
            EPC:epc,
            type:"epc"
        };
        if(ItemExist(epc,5)){
            $.ajax({
                type: 'GET',
                data: voData,
                url:  site_url('Pos/getdataScanSocket'),
                success: function (result) {
                    PNotify.removeAll();
                    if(result.errorcode > 0) {
                        new PNotify({ title: "info", text: result.msg, type: 'warning', shadow: true });
                    } else {
                        if(result.status == "NEW")
                        {
                            gst = <?php echo $GST; ?>;
                        }else{
                            gst = 0;
                        }
                        if(ItemExist(result.status,10) == false || getTotal('detail','string',10,2) == 0){
                            amountVAT(gst);
                            $("#list-detail").append(result.html);
                            sumTotal('detail');
                        }else{
                            new PNotify({ title: "info", text: "Item scaned is "+result.status, type: 'warning', shadow: true });
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Info", text: "Item is Already exist", type: 'warning', shadow: true });
        }
    }
    function getdataScanImei(imei)
    {
        var msg ="";
        var status ="";
        var gst =0;
        var rowCount = document.getElementById("list-detail").rows.length + 1;
        var voData = {
            id: rowCount,
            EPC:imei,
            type:"imei"
        };
        if(ItemExist(imei,4)){
            $.ajax({
                type: 'GET',
                data: voData,
                url:  site_url('Pos/getdataScanSocket'),
                success: function (result) {
                    PNotify.removeAll();
                    if(result.errorcode > 0) {
                        new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                    } else {
                        if(result.status == "NEW")
                        {
                            gst = <?php echo $GST; ?>;
                        }else{
                            gst = 0;
                        }
                        if(ItemExist(result.status,10) == false || getTotal('detail','string',10,2) == 0){
                            amountVAT(gst);
                            $("#list-detail").append(result.html);
                            sumTotal('detail');
                        }else{
                            new PNotify({ title: "info", text: "Item scaned is "+result.status, type: 'warning', shadow: true });
                        }
                        $("#IMEI").val('');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Info", text: "Item is Already exist", type: 'warning', shadow: true });
        }
    }
    function ValidateEmail(){  
    var email = $("#Email").val();
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
        if(email!=""){
             if(email.match(mailformat)){  
            //document.form1.text1.focus();  
            return true;  
            }else{  
                new PNotify({ title: "Invalid Email", text: "You have entered an invalid email address!", type: 'warning', shadow: true }); 
                $("#Email").focus();  
                return false;  
            }
        }
    }
</script>
<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Detail</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordID}) ? ${T_TransactionStockMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordTimestamp}) ? ${T_TransactionStockMovementHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-textbox', 'value' => 'IMBB', 'style' => 'background-color:#eee', 'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocName', 'class' => 'k-textbox', 'value' => 'Stock Begin Balance', 'style' => 'background-color:#eee', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc No</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-textbox', 'value' => isset(${T_TransactionStockMovementHeader_DocNo}) ? ${T_TransactionStockMovementHeader_DocNo} : substr(DocNo('IMBB'), 2), 'style' => 'background-color:#eee;', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate','readonly' => 'TRUE', 'class' => 'KendoDatePicker', 'value' => isset(${T_TransactionStockMovementHeader_DocDate}) ? ${T_TransactionStockMovementHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocStatus', 'value' => isset(${T_TransactionStockMovementHeader_DocStatus}) ? ${T_TransactionStockMovementHeader_DocStatus} : '0', 'readonly' => true, 'style' => 'margin-top:-9px;' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-textbox" id="Remarks" readonly="true" style="width: 300px;"><?php echo (isset(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <div style="overflow:auto;">
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="50px">Action</th>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Item ID</th>
                                            <th data-col="ItemName">Item Name</th>
                                            <th data-col="ItemType">Item Type</th>
                                            <th data-col="Qty">Qty</th>
                                            <th data-col="LocationID">Loc ID</th>
                                            <th data-col="LocationName">Loc Name</th>
                                            <th data-col="EPC">EPC</th>
                                            <th data-col="Barcode">Barcode</th>
                                            <th data-col="RemarksDetail">Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions">-</td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                       <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
                                        <td id="detailItemTypev-'.$i. '" data-val="'.$item[T_MasterDataGeneralTableValue_Key]. '">'.$item[T_MasterDataGeneralTableValue_Key]. '</td>
                                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                                        <td id="detailBarcodev-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Barcode]. '">'.$item[T_TransactionStockMovementDetail_Barcode]. '</td>
                                        <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Remarks]. '">'.$item[T_TransactionStockMovementDetail_Remarks]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<?php
//Item Lookup
    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataItem_ItemID, 'title' => 'Item ID', 'width' => '80px'),
        array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '150px'),
        array('field' => T_MasterDataItem_AutoIDType, 'title' => 'Type', 'width' => '50px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'ItemID', 'column' => T_MasterDataItem_ItemID),
        array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("ItemID", "Data Item", "600px", "Webservice/Read/Getlistitem", $dataItem, $columnItem, T_MasterDataItem);

//Location Lookup
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("Location", "Data Location", "500px", "Webservice/Read/Getlist", $dataLocation, $columnLocation,T_MasterDataLocation);

?>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>
<script type="text/javascript" src="assets/js/apps.js"></script>

<?php 

if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {

    $("#RowIndex").kendoNumericTextBox(); 
    $("#RowIndex2").kendoNumericTextBox(); 
    $("#Qty").kendoNumericTextBox(); 

    kendoModal("detailForm","Add Detail","800px");
    kendoModal("detailSubForm","Add Item","400px");

     $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        cleardetail("detailSub", 0);
    });

});

</script>
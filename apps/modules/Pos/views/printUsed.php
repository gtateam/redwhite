<html>
	<head>
		<title>RedWhite - Invoice Print</title>
        <base href="<?php echo base_url(); ?>" />
		<!-- Web Fonts  -->

		<!-- Vendor CSS -->
		<!-- <link rel="stylesheet" href="assets/backend/vendor/bootstrap/css/bootstrap.css" /> -->

		<!-- Invoice Print Style -->
		<link rel="stylesheet" href="assets/backend/stylesheets/invoice-print.css" />

        <style>
            .row {
                margin-left: -15px;
                margin-right: -15px;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12{
                float: left;
            }
            .text-right {
                text-align: right;
            }
            .text-left {
                text-align: left;
            }
            .invoice address {
                color: #5f5f5f;
                line-height: 1em;
            }
        </style>
	</head>
	<body>
		<div class="invoice">
			<header class="clearfix">
				<div class="row">
					<div class="col-sm-6 mt-md">
						<h5 class="h5 mt-none mb-sm text-dark text-weight-bold">TAX INVOICE</h5>
					</div>
					<div class="col-sm-6 text-right mt-md mb-md">
						<div class="ib">
							<img src="assets/backend/images/logos.png" height="65" alt="RedWhite" />
						</div>
					</div>
				</div>
			</header>
			<div class="bill-info">
				<div class="row">
					<div class="col-md-6">
						<div class="bill-to">
							<p class="h7 mb-xs text-dark text-weight-semibold">To:</p>
							<address>
								<?php echo isset(${T_SalesInvoiceHeader_Email}) ? ${T_SalesInvoiceHeader_Email} : "";?>
							</address>
                            <!-- <p class="mb-none">
								<span class="text-dark">Invoice No:</span>
								<span class="value"><?php echo isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "";?></span>
							</p>
							<p class="mb-none">
								<span class="text-dark">Date:</span>
								<span class="value"><?php echo isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "";?></span>
							</p> -->
                            <table>
                                <tr style="font-size: 14px;"><td class="text-dark">Invoice No</td><td class="value"> : <?php echo isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "";?></td></tr>
                                <tr style="font-size: 14px;"><td class="text-dark">Date</td><td class="value"> : <?php echo isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "";?></td></tr>
                            </table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bill-data text-right">
                            <p class="h7 mb-xs text-dark text-weight-semibold">From:</p>
                            <address class="ib mr-xlg">
                                Red White Mobile Pte Ltd 
                                <br/>
                                420 North Bridgt Road, #01-
                                <br/>
                                20 North Bridgt Centre
                                <br/>
                                SINGAPORE 188727
                                <br/>
                                UEN & GST Regn No.2013135778K
                            </address>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<table class="table invoice-items">
					<thead>
						<tr class="text-dark">
							<th id="cell-desc" style="border-bottom: 1px solid; border-bottom-width: 2px;" class="text-left text-weight-semibold">Description</th>
							<th id="cell-qty"  style="border-bottom: 1px solid; border-bottom-width: 2px;" class="text-right text-weight-semibold">Quantity</th>
							<th id="cell-price" style="border-bottom: 1px solid; border-bottom-width: 2px;" class="text-right text-weight-semibold">Unit Price</th>
							<th id="cell-total" style="border-bottom: 1px solid; border-bottom-width: 2px;" class="text-right text-weight-semibold">Amount SGD</th>
						</tr>
					</thead>
					<tbody>
                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                            foreach($Detail as $item): 
                            $LT = $item[T_SalesInvoiceDetail_UnitPrice] * $item[T_SalesInvoiceDetail_Quantity];
                            echo '<tr id="detail-'.$i. '">
                            <td style="border-bottom: 1px solid;" class="text-weight-semibold text-dark">Decription of Goods : USED - Brand: '.$item[T_SalesInvoiceDetail_Brand].' Model : '.$item[T_SalesInvoiceDetail_Model].' Colour : '.$item[T_SalesInvoiceDetail_Color].' IMEI No : '.$item[T_SalesInvoiceDetail_IMEI].'</td>
							<td style="border-bottom: 1px solid;" class="text-right">1</td>
							<td style="border-bottom: 1px solid;" class="text-right">'.$item[T_SalesInvoiceDetail_UnitPrice]. '</td>
							<td style="border-bottom: 1px solid;" class="text-right">'.$LT.'</td>
                        </tr>'; 
                        $i++; endforeach; endif; ?>
					</tbody>
				</table>
			</div>
		
			<div class="invoice-summary">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-8">
						<table class="table text-dark">
							<tbody>
                                <tr>
									<td colspan="1" class="text-right">Invoice Total SGD</td>
									<td class="text-right"><?php echo isset(${T_SalesInvoiceHeader_AmountTotal}) ? money_format('%!i',${T_SalesInvoiceHeader_AmountTotal}): "";?></td>
								</tr>
								<tr class="b-top-none">
									<td style="border-bottom: 1px solid;" class="text-right" colspan="1" class="text-right">Total Net Payments SGD</td>
									<td style="border-bottom: 1px solid;" class="text-right" class="text-right"><?php echo isset(${T_SalesInvoiceHeader_AmountTotal}) ? money_format('%!i',${T_SalesInvoiceHeader_AmountTotal}): "";?></td>
								</tr>
								<tr>
									<td style="font-weight: bold;" colspan="1" class="text-right">Amount Due SGD</td>
									<td style="font-weight: bold;" class="text-right">0.00</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
            
		</div>
	</body>
</html>
<style>
.borderered .table thead tr th, .table tbody tr td {
border: none;
text-align:center;
font-size: 10px;
}
.bordereds .table thead tr th, .table tbody tr td .table tfoot tr td {
    text-align:center;
}
.footertable{font-size: 10px;}
</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <div class="panel-body">
                <div class="col-md-8" >
                    <div class="row">
                        <div style=""><h2><b>Red White Mobile Private Limited</b></h2></div>
                        <table id="table-summary" class="borderered" >
                            <tbody>
                                <tr>
                                    <td colspan="7"><b>470 North Bridge Road #03-22 Bugis Cube Singapore 188735</b></td>
                                </tr>
                                <tr>
                                    <td width="120px">Telephone</td>
                                    <td width="10px">:</td>
                                    <td width="300px">(65) 67354811</td>
                                    <td width="100px"></td>
                                    <td width="100px">&nbsp;&nbsp;&nbsp;Date</td>
                                    <td width="10px">:</td>
                                    <td width="200px" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "";?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>redwhitemobile@yahoo.com.sg</td>
                                    <td width=""></td>
                                    <td>&nbsp;&nbsp;&nbsp;Terms</td>
                                    <td>:</td>
                                    <td style="border-bottom: solid 2px;"></td>
                                </tr>
                                <tr>
                                    <td>Customer Name</td>
                                    <td>:</td>
                                    <td colspan="2" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_CustomerName}) ? ${T_SalesInvoiceHeader_CustomerName} : "";?></td>
                                    <td>&nbsp;&nbsp;&nbsp;Doc No</td>
                                    <td>:</td>
                                    <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "";?></td>
                                </tr>
                                <tr>
                                    <td rowspan="2">Address</td>
                                    <td rowspan="2">:</td>
                                    <td colspan="2" rowspan="2" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_Address}) ? ${T_SalesInvoiceHeader_Address} : "";?></td>
                                    <td>&nbsp;&nbsp;&nbsp;I/C No</td>
                                    <td>:</td>
                                    <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_IC}) ? ${T_SalesInvoiceHeader_IC} : "";?></td>
                                    <td width=""></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: bottom">&nbsp;&nbsp;&nbsp;Telephone</td>
                                    <td>:</td>
                                    <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_Phone}) ? ${T_SalesInvoiceHeader_Phone} : "";?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>             
                </div>
                
                <div class="col-md-8" style="margin-top:10px;">
                    <table class="table table-bordered mb-none" style="border-collapse: collapse">
                        <thead id="head-detail">
                            <tr>
                                <th style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="RowIndex">#</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="ItemID">Item</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="ItemName">Description</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="ItemGroup">Type</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse"  data-col="IMEI">IMEI</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="Status">Status</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="Qty">Quantity</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="UnitPrice">Unit Price</th>
                                <th class="center" style="font-size: 10px;border:1px solid;border-collapse: collapse" data-col="LineTotal">Amount</th>
                            </tr>
                        </thead>
                        <tbody id="list-detail">
                            <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                            foreach($Detail as $item): 
                            $LT = $item[T_SalesInvoiceDetail_UnitPrice] * $item[T_SalesInvoiceDetail_Quantity];
                            echo '<tr id="detail-'.$i. '">
                            <td style="border:1px solid;border-collapse: collapse" id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailItemIDv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Brand]. '">'.$item[T_SalesInvoiceDetail_Brand]. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailItemNamev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_Model].','.$item[T_SalesInvoiceDetail_ItemName]. ' ,'.$item[T_SalesInvoiceDetail_Color].'</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailItemGroupv-'.$i. '" data-val="'.$item[T_MasterDataItemGroup_Name]. '">'.$item[T_MasterDataItemGroup_Name]. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailIMEIv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_IMEI]. '">'.$item[T_SalesInvoiceDetail_IMEI]. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailStatusv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Status]. '">'.$item[T_SalesInvoiceDetail_Status]. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailQtyv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Quantity]. '">'.$item[T_SalesInvoiceDetail_Quantity]. ' (Pcs)</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailUnitPricev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_UnitPrice]. '">'.$item[T_SalesInvoiceDetail_UnitPrice]. '</td>
                            <td style="border:1px solid;border-collapse: collapse" id="detailLineTotalv-'.$i. '" data-val="'.$LT. '">'.$LT. '</td>
                        </tr>'; 
                        $i++; endforeach; endif; ?>
                        </tbody>
                        <tfoot> <tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td></tr><tr> <td colspan="6" rowspan="2" style="border:1px solid;" >&nbsp;<h5>Goods sold are not refundable or exchangable</h5></td><td class="center" colspan="2" style="font-size: 10px;border:1px solid;border-collapse: collapse" >&nbsp;Payment Mode</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" >&nbsp;<?php echo isset(${T_SalesInvoiceHeader_PaymentMode}) ? ${T_SalesInvoiceHeader_PaymentMode}: "";?></td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="font-size: 10px;font-size: 10px;border:1px solid;border-collapse: collapse" >&nbsp;Subtotal</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" id="subtotal" >&nbsp;<?php echo isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? "SGD".money_format('%!i', ${T_SalesInvoiceHeader_AmountSubtotal}) . "\n": "";?></td></tr><tr> <td colspan="6" rowspan="2" style="border:1px solid;" >&nbsp;<h5>No 7 days exchange policy</h5></td><td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" >&nbsp;GST</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" id="GST" >&nbsp;<?php $a=100; $percent=${T_SalesInvoiceHeader_AmountVAT} * ${T_SalesInvoiceHeader_AmountSubtotal}; $GST=100 * ${T_SalesInvoiceHeader_AmountVAT}; echo isset(${T_SalesInvoiceHeader_AmountVAT}) ? "SGD".money_format('%!i', $percent) . "\n"." (".$GST ."%)": "";?></td></tr><tr> <td style="border:1px solid;border-collapse: collapse" >&nbsp;</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" >&nbsp;Total Amount</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse" id="AmountTotal">&nbsp;<?php echo isset(${T_SalesInvoiceHeader_AmountTotal}) ? "<b>SGD".money_format('%!i',${T_SalesInvoiceHeader_AmountTotal}) . "\n"."</b>": "";?></td></tr><tr> <td colspan="7" style="">&nbsp;</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse">&nbsp;Payment</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse">&nbsp;<?php echo isset(${T_SalesInvoiceHeader_Payment}) ? "SGD".money_format('%!i', ${T_SalesInvoiceHeader_Payment}) . "\n": "";?></td></tr><tr> <td colspan="7" style="">&nbsp;</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse">&nbsp;Outstanding</td><td style="font-size: 10px;border:1px solid;border-collapse: collapse">&nbsp; <?php echo isset(${T_SalesInvoiceHeader_Outstanding}) ? "SGD".money_format('%!i', ${T_SalesInvoiceHeader_Outstanding}) . "\n" : "";?></td></tr></tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="assets/backend/vendor/jquery/jquery.js"></script>
<script src="assets/Content/assets/vendor/accounting/accounting.min.js"></script>
<script>

$(document).ready(function() {
    //Numeric
});
</script>
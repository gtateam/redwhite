<div id="grid"></div>
<div id="window"></div>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#grid").kendoGrid({
            toolbar: ["excel"],
            excel: {
                fileName: "POS.xlsx",
                filterable: true
            },
            height: "580px",
            width: "100%",
            columns: [
            { 
                "width": "30px",
                "template":
                ''
                +'<a class="k-button" href="javascript:void(0)" onclick="PrintDoc(\'#=t2040f026#\')"><span class="fa fa-print"></span></a> <a class="k-button" href="javascript:void(0)" onclick="RemoveData(#=t2040r001#)"><span class="fa fa-trash"></span></a>'
            },
            {
                "title":"Type",
                "width":"80px",
                "field":"<?php echo T_SalesInvoiceHeader_DocNo; ?>",
            },
            {
                "title":"Date",
                "width":"100px",
                "field":"<?php echo T_SalesInvoiceHeader_DocDate; ?>",
            },
            {
                "title":"Email",
                "width":"140px",
                "field":"<?php echo T_SalesInvoiceHeader_Email; ?>",
            }
            ],
            dataSource: {
                transport: {            
                    read: {
                        type:"GET",
                        url: site_url('Pos/getlist'),
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                },

                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "<?php echo T_TRANSINOUT_RecordID; ?>",
                    }
                },
                pageSize: 30,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            sortable: true,
            pageable: true,
            groupable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            filterable: {
                mode: "row",
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });

        $("#window").kendoWindow({
            width: "1015px",
            height: "515px",
            title: "Print invoice",
            visible: false,
            iframe: true,
            content: "assets/upload/invoice/Invoice_PS-17-00014.pdf"
        });
    });

    function RemoveData(id)
    {
        var result = confirm("Delete this record?");
        if (result) {
            if(id){
                var voData = {
                    RecordID: id
                };
                $.ajax({
                    type: 'POST',
                    data: voData,
                    url:  site_url('Pos/Delete'),
                    beforeSend: function(){
                    },
                    success: function (result) {
                    if (result.errorcode != 0) {
                        new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                    } else {
                        new PNotify({ title: "Success", text: "Remove Item", type: 'success', shadow: true });
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                    }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
            }
        }
    }
    function PrintDoc(file)
    {
        var dialog = $("#window").data("kendoWindow");
        var url = 'assets/upload/invoice/'+file;
        $(".k-content-frame").attr('src', url);
        dialog.center();
        dialog.open();
    }
</script>
</div>
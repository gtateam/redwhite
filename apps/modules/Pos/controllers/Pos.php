	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Balance Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Pos extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Pos_model')); //,'Pos_model'
	}

	public function index($id='')
	{
		$this->template->set_layout('menucollapse');
		$sql = $this->db->get('t1013');
		$scan = $sql->first_row()->t1013f001;
		$data["scan"] = $scan;

		$sql1 = $this->db->get(T_SystemGST);
		$GST = $sql1->first_row()->{T_SystemGST_Value};
		$data["GST"] = $GST;
		$this->modules->render('/form',$data);
	}

	public function listData($id='')
	{
		$data = "";
		$this->modules->render('index', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Pos_model->getDetail($id);
		$this->modules->render('/formPrint', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Pos_model->getDetail($id);
		$this->modules->render('/formDetail', $data);
	}

	public function getDetail()
	{	$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id =  $this->input->get('RecordID');
		$data = $this->Pos_model->getDetail($id);
		if(!empty($data)){
			$info->data = $data;
		}else{
			$info->data = null;
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_SalesInvoiceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Pos_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Pos_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		$this->db->trans_begin();
		try{
			$doctype = "SLPS";
			if (check_column(T_SalesInvoiceHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_SalesInvoiceHeader_DocTypeID => $doctype,
					T_SalesInvoiceHeader_DocNo => substr(DocNo($doctype), 2),
					T_SalesInvoiceHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_SalesInvoiceHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_SalesInvoiceHeader_TermofPayment => empty($this->input->post("TermofPayment"))? "":$this->input->post("TermofPayment"),
					T_SalesInvoiceHeader_CurrencyID => empty($this->input->post("CurrencyID"))? "":$this->input->post("CurrencyID"),
					T_SalesInvoiceHeader_ExchangeRate => empty($this->input->post("ExchRate"))? 0:$this->input->post("ExchRate"),
					T_SalesInvoiceHeader_SalespersonID => empty($this->input->post("SalespersonID"))? "":$this->input->post("SalespersonID"),
					T_SalesInvoiceHeader_BizPartnerID => empty($this->input->post("BizPartnerID"))? "":$this->input->post("BizPartnerID"),
					T_SalesInvoiceHeader_AmountSubtotal => empty($this->input->post("AmountSubtotal"))? 0:$this->input->post("AmountSubtotal"),
					T_SalesInvoiceHeader_AmountVAT => empty($this->input->post("AmountVAT"))? 0:$this->input->post("AmountVAT"),
					T_SalesInvoiceHeader_AmountTotal => empty($this->input->post("AmountTotal"))? 0:$this->input->post("AmountTotal"),
					T_SalesInvoiceHeader_Payment => empty($this->input->post("Payment"))? 0:$this->input->post("Payment"),
					T_SalesInvoiceHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					T_SalesInvoiceHeader_IC => empty($this->input->post("IC"))? ' ':$this->input->post("IC"),
					T_SalesInvoiceHeader_CustomerName => empty($this->input->post("CustomerName"))? ' ':$this->input->post("CustomerName"),
					T_SalesInvoiceHeader_Address => empty($this->input->post("Address"))? ' ':$this->input->post("Address"),
					T_SalesInvoiceHeader_Phone => empty($this->input->post("Phone"))? ' ':$this->input->post("Phone"),
					T_SalesInvoiceHeader_Email => empty($this->input->post("Email"))? ' ':$this->input->post("Email"),
					T_SalesInvoiceHeader_Outstanding => empty($this->input->post("Outstanding"))? ' ':$this->input->post("Outstanding"),
					T_SalesInvoiceHeader_PaymentMode => empty($this->input->post("PaymentMode"))? ' ':$this->input->post("PaymentMode"),
					'detail' => $this->input->post("detail")
				);

				$id = $this->Pos_model->Insert($data);
				//print_r($id); die;
				$this->db->trans_commit();
				$this->db->trans_begin();
				// print_r($id); die;
				$this->Post($id);

				//$this->db->update("t1013",array("t1013f002" => 1));

				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success', 'id'=>$id);
			}
		}catch(Exception $e)
		{
			$this->Pos_model->Delete($id);
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "SLPS";
			if(check_column(T_SalesInvoiceHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SalesInvoiceHeader_RecordID => $this->input->post("RecordID"),
					T_SalesInvoiceHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_SalesInvoiceHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_SalesInvoiceHeader_DocTypeID => $doctype,
					T_SalesInvoiceHeader_DocNo => $this->input->post("DocNo"),
					T_SalesInvoiceHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_SalesInvoiceHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_SalesInvoiceHeader_TermofPayment => empty($this->input->post("TermofPayment"))? "":$this->input->post("TermofPayment"),
					T_SalesInvoiceHeader_CurrencyID => empty($this->input->post("CurrencyID"))? 0:$this->input->post("CurrencyID"),
					T_SalesInvoiceHeader_ExchangeRate => empty($this->input->post("ExchRate"))? 0:$this->input->post("ExchRate"),
					T_SalesInvoiceHeader_SalespersonID => empty($this->input->post("SalespersonID"))? "":$this->input->post("SalespersonID"),
					T_SalesInvoiceHeader_BizPartnerID => empty($this->input->post("BizPartnerID"))? "":$this->input->post("BizPartnerID"),
					T_SalesInvoiceHeader_AmountSubtotal => empty($this->input->post("AmountVAT"))? 0:$this->input->post("AmountVAT"),
					T_SalesInvoiceHeader_AmountVAT => empty($this->input->post("AmountSubtotal"))? 0:$this->input->post("AmountSubtotal"),
					T_SalesInvoiceHeader_AmountTotal => empty($this->input->post("AmountTotal"))? 0:$this->input->post("AmountTotal"),
					T_SalesInvoiceHeader_Payment => empty($this->input->post("Payment"))? 0:$this->input->post("Payment"),
					T_SalesInvoiceHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail")
				);

				$this->Pos_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Pos_model->Delete($this->input->post("RecordID"));
			
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function TrigerReader()
	{
		try{
			$html="";
			$msg="";
			$sn = array();
			$id = $this->input->post("id");
			$this->db->update("t1013",array("t1013f001" => $id));
			if($id==1)
			{
				$this->db->truncate("t1014");
				$msg="Scaning...";
			}else{
				$msg="Stop Scaning...";
			}
			
			$output = array('errorcode' => 0, 'msg' => $msg, 'html' => $html);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getdataScan()
	{
		try{
			$html="";
			$msg="";
			$sn = array();
			$this->db->select("t1014f001");
			$sql = $this->db->get("t1014");
			$dataEPC = $sql->result();
			$res ='';
			if($sql->num_rows() > 0){
				$i=1;
				foreach($dataEPC as $epc)
				{
					// checking epc for serialno
					//$snTag = substr($epc->t1014f001,SNINDEX,SNLENGTH);
					$snTag = $epc->t1014f001;
					//print_r($snTag);die;
					$this->db->where(T_MasterDataItem_EPC,$snTag);
					$sql2 = $this->db->get(T_MasterDataItem);
					$epcExist = $sql2->first_row();
					
					if($sql2->num_rows() != 0)// serial no
					{
						if($sql->num_rows()==$i){
							$res .= '"'.$snTag.'"';
						}else{
							$res .= '"'.$snTag.'",';
						}
						$sn[$snTag][] = $epc->t1014f001;
					}else{
						if($sql->num_rows()==$i){
							$res .= '"'.$epc->t1014f001.'"';
						}else{
							$res .= '"'.$epc->t1014f001.'",';
						}
					}
					
					$i++;
				}
			}
			if($res!=""){
				$epcL = explode(",",$res);
				$countQTY = array_count_values($epcL);
				$query = 'SELECT * FROM t8010 join t1011 ON t8010.t8010f001=t1011.t1011f003 where t8010f005 in('.$res.')';
				$sql2 = $this->db->query($query);
				$Detail = $sql2->result('array');
				
				$i=1; $target="'detail'"; 
				//$msg = "empty data";
				if(isset($Detail) && !empty($Detail)):  
					foreach($Detail as $item): 
					$tag = $item[T_MasterDataItem_EPC];
					$type = substr($tag,TYPEINDEX,TYPELENGTH);
					if($type==SSTYPE)
					{
						$qty = $countQTY['"'.$tag.'"'];
						$listSN = $sn[$tag];
						$listSN = implode(",",$listSN);
					}else{
						$qty = $countQTY['"'.$tag.'"'];
						$listSN = "";
					}
					
					$LT = $item[T_MasterDataItem_UnitPrice] * $qty;
					$html .= '<tr id="detail-'.$i. '">
					<td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
					<td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
					<td id="detailItemIDv-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemID]. '">'.$item[T_MasterDataItem_ItemID]. '</td>
					<td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
					<td id="detailIMEIv-'.$i. '" data-val="'.$item[T_MasterDataItem_IMEI]. '">'.$item[T_MasterDataItem_IMEI]. '</td>
					<td id="detailItemGroupv-'.$i. '" data-val="'.$item[T_MasterDataItem_GroupID]. '">'.$item[T_MasterDataItem_GroupID]. '</td>
					<td id="detailBrandv-'.$i. '" data-val="'.$item[T_MasterDataItem_Brand]. '">'.$item[T_MasterDataItem_Brand]. '</td>
					<td id="detailModelv-'.$i. '" data-val="'.$item[T_MasterDataItem_Model]. '">'.$item[T_MasterDataItem_Model]. '</td>
					<td id="detailColorv-'.$i. '" data-val="'.$item[T_MasterDataItem_Color]. '">'.$item[T_MasterDataItem_Color]. '</td>
					<td id="detailStatusv-'.$i. '" data-val="'.$item[T_MasterDataItem_Status]. '">'.$item[T_MasterDataItem_Status]. '</td>
					<td id="detailQtyv-'.$i. '" data-val="'.$qty.'" ><input style="display:none;" id="serialno-'.$i.'" type="text" value="'.$listSN.'" /><input';
					if($item[T_MasterDataItem_GroupID] == 'IG-00001'){
						$html .= ' readonly ';
					}
					$html .=' id="qtyf-'.$i.'" type="number" value="'.$qty.'" style="width:50px;" onchange="calculating('.$i.',this.value)"></td>
					<td id="detailUnitPricev-'.$i. '" data-val="" data-def=""><input id="pricef-'.$i.'" type="number" value="" style="width:100px;" onchange="calculating2('.$i.',this.value);"></td>
					<td id="detailLineTotalv-'.$i. '" data-val="0">0</td>
					<td id="detailLocationIDv-'.$i. '" style="display:none;" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
					<td id="detailLocationNamev-'.$i. '" style="display:none;" data-val="'.$item[T_TransactionStockMovementDetail_LocationID2]. '">'.$item[T_TransactionStockMovementDetail_LocationID2]. '</td>
					<td id="detailRecordIDDetailv-'.$i. '" data-val="" style="display:none;"></td>
					<td id="detailRecordFlagv-'.$i. '" data-val="" style="display:none;"></td>
				</tr>'; $i++; endforeach;
				$msg = "found ".$sql2->num_rows()." data";
				endif;
			}else{
				$msg = "empty data in stock balance";
			}
			$output = array('errorcode' => 0, 'msg' => $msg, 'html' => $html);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getdataScanSocket()
	{
		try{
			$html="";
			$msg="";
			$status ="";
			if($_GET['EPC']!="" || $_GET['imei'] !=''){
				// $query = 'SELECT * FROM t8010 join t1011 ON t8010.t8010f001=t1011.t1011f003 where t8010f005 ="'.$_GET['EPC'].'"';
				if($_GET['type'] == 'epc'){
					$query = 'SELECT * FROM t8010 t1 JOIN t6010 t2 ON t1.t8010f001 = t2.t6010f003 JOIN t6011 t3 ON t2.t6010f008 = t3.t6011f001 AND t2.t6010f009 = t3.t6011f002 AND t3.t6011f005 = 1 WHERE t2.t6010f009 = "'.$_GET['EPC'].'"';
				}else{
					$query = 'SELECT * FROM t8010 t1 JOIN t6010 t2 ON t1.t8010f001 = t2.t6010f003 JOIN t6011 t3 ON t2.t6010f008 = t3.t6011f001 AND t2.t6010f009 = t3.t6011f002 AND t3.t6011f005 = 1 WHERE t2.t6010f008 = "'.$_GET['EPC'].'"';
				}
				$sql2 = $this->db->query($query);
				$Detail = $sql2->result('array');
				
				$i=$_GET['id']; $target="'detail'"; 
				//$msg = "empty data";
				if(isset($Detail) && !empty($Detail)){  
					foreach($Detail as $item): 
					$tag = $item[T_MasterDataItem_EPC];
					$type = substr($tag,TYPEINDEX,TYPELENGTH);
					if($type==SSTYPE)
					{
						$qty = 1;
						// $listSN = $sn[$tag];
						// $listSN = implode(",",$listSN);
						$listSN = "";
					}else{
						$qty = 1;
						$listSN = "";
					}
					$status = $item[T_MasterDataItem_Status];
					$LT = $item[T_MasterDataItem_UnitPrice] * $qty;
					$html .= '<tr id="detail-'.$i. '">
						<td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
						<td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
						<td id="detailItemIDv-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemID]. '">'.$item[T_MasterDataItem_ItemID]. '</td>
						<td id="detailItemNamev-'.$i. '" style="display:none;" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
						<td id="detailIMEIv-'.$i. '" data-val="'.$item[T_TRANSINOUTDETAIL_IMEI]. '">'.$item[T_TRANSINOUTDETAIL_IMEI]. '</td>
						<td id="detailRFIDv-'.$i. '" data-val="'.$item[T_TRANSINOUTDETAIL_RFID]. '">'.$item[T_TRANSINOUTDETAIL_RFID]. '</td>
						<td id="detailItemGroupv-'.$i. '" data-val="'.$item[T_MasterDataItem_GroupID]. '">'.$item[T_MasterDataItem_GroupID]. '</td>
						<td id="detailBrandv-'.$i. '" data-val="'.$item[T_MasterDataItem_Brand]. '">'.$item[T_MasterDataItem_Brand]. '</td>
						<td id="detailModelv-'.$i. '" data-val="'.$item[T_MasterDataItem_Model]. '">'.$item[T_MasterDataItem_Model]. '</td>
						<td id="detailColorv-'.$i. '" data-val="'.$item[T_MasterDataItem_Color]. '">'.$item[T_MasterDataItem_Color]. '</td>
						<td id="detailStatusv-'.$i. '" data-val="'.$item[T_MasterDataItem_Status]. '">'.$item[T_MasterDataItem_Status]. '</td>
						<td id="detailQtyv-'.$i. '" style="display:none;" data-val="'.$qty.'" ><input style="display:none;" id="serialno-'.$i.'" type="text" value="'.$listSN.'" /><input';
						if($item[T_MasterDataItem_GroupID] == 'IG-00001'){
							$html .= ' readonly ';
						}
						$html .=' id="qtyf-'.$i.'" type="number" value="'.$qty.'" style="width:50px;" onchange="calculating('.$i.',this.value)"></td>
						<td id="detailUnitPricev-'.$i. '" data-val="'.$item[T_TRANSINOUTDETAIL_CostPrice]. '" data-def="'.$item[T_TRANSINOUTDETAIL_CostPrice]. '">'.$item[T_TRANSINOUTDETAIL_CostPrice]. '</td>
						<td id="detailLineTotalv-'.$i. '" style="display:none;" data-val="0">0</td>
						<td id="detailLocationIDv-'.$i. '" style="display:none;" data-val=""></td>
						<td id="detailLocationNamev-'.$i. '" style="display:none;" data-val=""></td>
						<td id="detailRecordIDDetailv-'.$i. '" data-val="" style="display:none;"></td>
						<td id="detailRecordFlagv-'.$i. '" data-val="" style="display:none;"></td>
					</tr>'; $i++; endforeach;
					$msg = "found ".$sql2->num_rows()." data";
				}else{
					throw new Exception('This Item already out!');	
				}
			}else{
				$msg = "empty data in stock balance";
			}
			$output = array('errorcode' => 0, 'msg' => $msg, 'html' => $html, 'status' => $status);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function Post($id){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";
		$get_data = $this->Pos_model->getDetail($id);
		$data = array(
			T_SalesInvoiceHeader_RecordStatus   => 1,
		);
		$this->Pos_model->UpdateHeader($data,$id);
		foreach ($get_data["Detail"] as $temp) {

			$sql = "SELECT t6010f008 FROM t6010 LEFT JOIN t6011 ON (t6010f008 = t6011f001 AND t6011.t6011f002 = t6010.t6010f009) WHERE (t6010f008 = '" . $temp[T_SalesInvoiceDetail_IMEI] . "') AND (t6011f005 = 0)";	// update 20180210
			$result =$this->db->query($sql);
			if($result->num_rows()){
				throw new Exception('This IMEI already out!');					
			}

			$date = strtotime($get_data[T_SalesInvoiceHeader_DocDate]);
			$data = array(
				T_TRANSINOUT_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_TRANSINOUT_RecordStatus => 0,
				T_TRANSINOUT_Type => "Out",
				T_TRANSINOUT_Date => empty($get_data[T_SalesInvoiceHeader_DocDate])? " ": date("Y-m-d",$date),
				T_TRANSINOUT_ItemID => empty($temp[T_SalesInvoiceDetail_ItemID])? " ":$temp[T_SalesInvoiceDetail_ItemID],
				T_TRANSINOUT_Brand => empty($temp[T_SalesInvoiceDetail_Brand])? " ":$temp[T_SalesInvoiceDetail_Brand],
				T_TRANSINOUT_Model => empty($temp[T_SalesInvoiceDetail_Model])? " ":$temp[T_SalesInvoiceDetail_Model],
				T_TRANSINOUT_Color => empty($temp[T_SalesInvoiceDetail_Color])? " ":$temp[T_SalesInvoiceDetail_Color],
				T_TRANSINOUT_Status => empty($temp[T_SalesInvoiceDetail_Status])? " ":$temp[T_SalesInvoiceDetail_Status],
				T_TRANSINOUT_IMEI => empty($temp[T_SalesInvoiceDetail_IMEI])? " ":$temp[T_SalesInvoiceDetail_IMEI],
				T_TRANSINOUT_RFID => empty($temp[T_SalesInvoiceDetail_EPC])? " ":$temp[T_SalesInvoiceDetail_EPC],
				T_TRANSINOUT_CostPrice => empty($temp[T_SalesInvoiceDetail_UnitPrice])? 0:$temp[T_SalesInvoiceDetail_UnitPrice],
				T_TRANSINOUT_SellingPrice => empty($temp[T_SalesInvoiceDetail_UnitPrice])? 0:$temp[T_SalesInvoiceDetail_UnitPrice],
				T_TRANSINOUT_Payment => empty($get_data[T_SalesInvoiceHeader_PaymentMode])? " ":$get_data[T_SalesInvoiceHeader_PaymentMode],
				T_TRANSINOUT_Vendor => "RedWhite",
				T_TRANSINOUT_GST => ($temp[T_SalesInvoiceDetail_Status] == "NEW")? "Inclusive GST": "no GST",
				T_TRANSINOUT_InvoiceNO => empty($get_data[T_SalesInvoiceHeader_DocNo])? " ": $get_data[T_SalesInvoiceHeader_DocNo],
				T_TRANSINOUT_Remarks => empty($temp[T_SalesInvoiceDetail_Remarks])? ' ':$temp[T_SalesInvoiceDetail_Remarks]
			);

			$InsertData = $this->db->insert(T_TRANSINOUT,$data);
			if (!$InsertData)
			{
				throw new Exception('Duplicate Data');
			}  
			$id = $this->db->insert_id();

			
			$data = array(
				T_TRANSINOUTDETAIL_Flag => 0
			);
			$this->db->where(T_TRANSINOUTDETAIL_RFID,$temp[T_SalesInvoiceDetail_EPC]);
			$this->db->where(T_TRANSINOUTDETAIL_IMEI,$temp[T_SalesInvoiceDetail_IMEI]);
			$this->db->where(T_TRANSINOUTDETAIL_Flag,1);
			$this->db->Update(T_TRANSINOUTDETAIL,$data);
			
			
			if($voMsg != ""){ throw new Exception($voMsg); }
		}
	}
	public function Post_old($id){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";
		// $this->db->trans_begin();
		// try{
			
            //get data
			// $id = $this->input->post("RecordID");
			$get_data = $this->Pos_model->getDetail($id);
            
			$data = array(
				T_SalesInvoiceHeader_RecordStatus   => 1,
			);
			$this->Pos_model->UpdateHeader($data,$id);
            foreach ($get_data["Detail"] as $temp) {
				//$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

				$this->db->from(T_TransactionStockBalanceHeader);
				$this->db->join(T_SalesInvoiceDetail.' i', 
							'i.'.T_SalesInvoiceDetail_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID, 
							'left'
							);
				$this->db->where(T_TransactionStockBalanceHeader_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
				$this->db->where(T_TransactionStockBalanceHeader_LocationID, $temp[T_SalesInvoiceDetail_LocationID]);
				$get_stocklist = $this->db->get();

				//echo $this->db->last_query();die;
				//print_r($get_stocklist->num_rows());die;
				
				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item
		
					if($get_stocklist->num_rows() == 0)
					{
						$voMsg .= $temp[T_SalesInvoiceDetail_ItemName]." not exists in Location ".$temp[T_SalesInvoiceDetail_LocationID]." \n ";
						$this->Pos_model->Delete($id);
					}else{
						$get_stocklist = $get_stocklist->first_row("array");
						$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

						$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $temp[T_SalesInvoiceDetail_Quantity];
						if($qty < 0){
							$voMsg .= $temp[T_SalesInvoiceDetail_ItemName]." < 0 in Location ".$temp[T_SalesInvoiceDetail_LocationID]." \n ";
						}
						$data_stocklist = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
						$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

						if($get_master_item[T_MasterDataItem_AutoIDType] == SSTYPE){
							$this->db->from(T_SalesInvoiceDetailSerialNo);
							$this->db->where(T_SalesInvoiceDetailSerialNo_PRI,$temp[T_SalesInvoiceDetail_RecordID]);
							$get_serial_no = $this->db->get()->result("array");

							foreach ($get_serial_no as $get_serial_no_temp) {
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $get_serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>0));

								//delete t7990 by epc
								$this->db->where("t7990f002", $get_serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
								$this->db->delete('t7990');
							}
						}

						$log_data = array(
							T_TransactionStockBalanceLog_PRI => $stocklistPRI,
							T_TransactionStockBalanceLog_DocTypeID => $get_data[T_SalesInvoiceHeader_DocTypeID],
							T_TransactionStockBalanceLog_RefRecordID => $get_data[T_SalesInvoiceHeader_RecordID],
							T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_SalesInvoiceDetail_RecordID],
							T_TransactionStockBalanceLog_Quantity => $temp[T_SalesInvoiceDetail_Quantity]*-1,
							T_TransactionStockBalanceLog_ItemGroup => $temp[T_SalesInvoiceDetail_ItemGroup],
							T_TransactionStockBalanceLog_Status => $temp[T_SalesInvoiceDetail_Status],
						);
						
					}
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
					if($voMsg != ""){ throw new Exception($voMsg); }
			}
			// $this->db->trans_commit();
		// 	$output = array('errorcode' => 0, 'msg' => 'success');
		// }catch(Exception $e)
		// {
		// 	$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		// 	$this->db->trans_rollback();
		// }

		// $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Pos_model->getDetail($id);
			$data = array(
				T_SalesInvoiceHeader_RecordStatus   => 0,
			);
			$this->Pos_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
				$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_SalesInvoiceDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $temp[T_SalesInvoiceDetail_Quantity];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

				}

				if($get_master_item[T_MasterDataItem_AutoIDType] == SSTYPE){
					$this->db->from(T_SalesInvoiceDetailSerialNo);
					$this->db->where(T_SalesInvoiceDetailSerialNo_PRI, $temp[T_SalesInvoiceDetail_RecordID]);
					$get_serial_no = $this->db->get();

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $stocklistPRI,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_SalesInvoiceDetailSerialNo_SN],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_SalesInvoiceDetailSerialNo_SN]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $stocklistPRI,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_SalesInvoiceHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_SalesInvoiceHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_SalesInvoiceDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $temp[T_SalesInvoiceDetail_Quantity],
					T_TransactionStockBalanceLog_ItemGroup => $temp[T_SalesInvoiceDetail_ItemGroup],
					T_TransactionStockBalanceLog_Status => $temp[T_SalesInvoiceDetail_Status],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

			}

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function rewriteChecking()
	{
		try{
			$html="";
			$msg="";
			$sql = $this->db->get("t1013");
			$data = $sql->first_row();
			$rewrite = $data->t1013f002;
			$output = array('errorcode' => 0, 'msg' => $msg, 'rewrite' => $rewrite);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getSubItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->get("RecordID");
 		$data = $this->Pos_model->getSubItem($id);
 		$html = "";
 		if(!empty($data)){
            $target = "'detailSub'";
			$a = 1;
            foreach ($data as $item) {
            	if($this->input->get("editable")){
	                $html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions">-</td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SN]. '">'.$item[T_SalesInvoiceDetailSerialNo_SN]. '</td>
	                </tr>'; 
	            }else if($this->input->get("LookupTranfer")){
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SN]. '">'.$item[T_SalesInvoiceDetailSerialNo_SN]. '</td>
	                </tr>'; 
	            }else{
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="editdetail('.$target.','.$a.',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_SalesInvoiceDetailSerialNo_SN]. '">'.$item[T_SalesInvoiceDetailSerialNo_SN]. '</td>
	                </tr>'; 
	            }
				$a++;
            }
        }

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data, 'html' => $html);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function print_preview($id)
    {
		
		$data = $this->Pos_model->getDetailInvoice($id);
		
		// $this->load->view('print_item', $data);
		$this->load->view('printNew', $data);

	}

	public function print_item($id)
    {
        //$this->load->library('pdf');
		$pdf = $this->pdf->load();
		
		$data = $this->Pos_model->getDetailInvoice($id);

		$sql1 = $this->db->get(T_SystemGST);
		$GST = $sql1->first_row()->{T_SystemGST_Value};
		$GST_Text = $sql1->first_row()->{T_SystemGST_Text};
		$data["GST"] = $GST;
		$data["GST_Text"] = $GST_Text;

		ini_set('memory_limit', '256M'); 
		if($data['Detail'][0]['t2041f017']=="NEW"){
			$html = $this->load->view('printNew', $data, true);
			$pdf->SetHTMLFooter('
                <p style="font-weight: bold;">Disclaimer :</p>
                <p>Buyers understand that the amount stated on invoice is correct and they are responsible for checking the item before leaving shop.
                Red White Mobile Pte Ltd will not be responsible for any loss of items, defects, damages, claims made afterwards. No 1 to 1 exchange applicable and items sold are not not refundable.
                Used items comes with minimum 2 weeks waranty by Red White Mobile Pte Ltd unless item is still under waranty by local service centre.</p>
            ');
		}else{
			$html = $this->load->view('printUsed', $data, true);
			$pdf->SetHTMLFooter('
                <p style="font-weight: bold;">Disclaimer :</p>
                <p>Buyers understand that the amount stated on invoice is correct and they are responsible for checking the item before leaving shop.
                Red White Mobile Pte Ltd will not be responsible for any loss of items, defects, damages, claims made afterwards. No 1 to 1 exchange applicable and items sold are not not refundable.
                Used items comes with minimum 2 weeks waranty by Red White Mobile Pte Ltd unless item is still under waranty by local service centre.</p>
                <p style="font-weight: bold;">The goods sold on this invoice are sold under the GST Gross-Margin Scheme. Both the seller and buyer are not allowed to make any input tax claim on the goods</p>
            ');
		}
		
		$pdf->WriteHTML($html); // write the HTML into the PDF
		ob_clean();
		$output = $_SERVER["DOCUMENT_ROOT"].'/redwhite/assets/upload/invoice/'.'Invoice_'.$data{T_SalesInvoiceHeader_DocNo}.'.pdf';
		// $output = $_SERVER["DOCUMENT_ROOT"].'\redwhite\assets\upload\invoice\\'.'Invoice_'.$data{T_SalesInvoiceHeader_DocNo}.'.pdf';
		$this->db->where(T_SalesInvoiceHeader_RecordID,$id);
		$this->db->update(T_SalesInvoiceHeader, array(T_SalesInvoiceHeader_Pdf => 'Invoice_'.$data{T_SalesInvoiceHeader_DocNo}.'.pdf'));
		$pdf->Output($output,"D"); // Download
		$pdf->Output($output,"F"); // Save to dir
        // exit();
	}

	public function sendEmail($id='')
    {	
		$config = Array(
		'protocol' => 'smtp',
		//'mailpath' => '/usr/sbin/sendmail',
		'smtp_host' => 'smtp.gmail.com',
		'smtp_user' => 'cimb.notification.system@gmail.com',
		'smtp_pass' => '132457689hilman',
		'smtp_port' => '587',
		'smtp_timeout' => '5',
		'wordwrap' => 'TRUE',
		'mailtype' => 'text',
		'charset' => 'iso-8859-1',
		'newline' => "\r\n",
		'wordwrap' => TRUE,
		//'validation' => FALSE,
		//securesally@gmail.com
		//dndojarzowgpfcde
		);
		$attched_file= "file:///C:/xampp/htdocs/redwhite/assets/upload/invoice/Invoice_PS-17-00047.pdf";
		//print_r($attched_file);die;
		$this->load->library('Email');
		$this->email->initialize($config);

		$this->email->from('manz.esem@gmail.com', 'Hilman SM');
		$this->email->to('hilman.murtadhoh@globaltrendasia.com'); 
		//$this->email->cc('arif.rahmanto@globaltrendasia.com'); 
		$this->email->bcc('them@their-example.com'); 

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');	
		$this->email->attach($attched_file);
		$this->email->send();

		echo $this->email->print_debugger();
	}

	public function mail_attachment($id='') {
		//phpinfo(); die;
		try{
		//get invoice data
		$get_data = $this->Pos_model->getDetailInvoice($id);

		$gst=$get_data[T_SalesInvoiceHeader_AmountVAT] * 100; 
		$gsttotal=$get_data[T_SalesInvoiceHeader_AmountVAT] * $get_data[T_SalesInvoiceHeader_AmountSubtotal];
		
		//email
		// $filename=substr($get_data[T_SalesInvoiceHeader_Pdf],45);
		$filename = $get_data[T_SalesInvoiceHeader_Pdf];
		$path = $_SERVER["DOCUMENT_ROOT"]."/redwhite/assets/upload/invoice/";
		// $path = $_SERVER["DOCUMENT_ROOT"]."\redwhite\assets\upload\invoice\\";
		$mailto=$get_data[T_SalesInvoiceHeader_Email];
		$from_mail="no-reply@redwhitemobile.com";
		$from_name="Red White Mobile";
		// $replyto="no-reply@redwhitemobile.com";
		$subject="Invoice ".$get_data[T_SalesInvoiceHeader_CustomerName];
		$message='
		<html>
			<head>
				<title>Invoice</title>
			</head>
			<body>
				<table cellspacing="0" cellpadding="0" border="0" width="600" align="center" style="border-collapse:collapse;background-color:#ffffff">
			<tbody>
				<tr>
					<td style="padding:15px 20px 10px" bgcolor="#e70012">
						<table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:collapse;color:#ffffff">
						<tbody>
							<tr>
								<td width="280">
									<a href="" target="" data-saferedirecturl=""><img src="https://redwhitemobile.com/wp-content/uploads/2012/10/rwm-logo2.png" alt="Logo" height="40px" class="CToWUd"></a>
								</td>
								<td width="280" align="right" style="font-size:20px">
									Transaction Done
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:20px;padding:30px 20px 0;font-weight:bold;color:#4e4e4e">Halo '.$get_data[T_SalesInvoiceHeader_CustomerName].',</td>
				</tr>
				<tr>
					<td style="padding:10px 20px">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;font-size:15px">
						<tbody>
							<tr>
								<td style="line-height:25px;color:#4f4f4f">Congratulations, you'."'".'ve made a purchase at Redwhite Mobile with the following details :
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:10px 20px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:15px">
						<tbody>
							<tr>
								<td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Invoice No:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em"><a href="" style="color:#42b549;text-decoration:none" target="" data-saferedirecturl="">'.$get_data[T_SalesInvoiceHeader_DocNo].'</a></td>
							</tr>
							<tr>
								<td width="220" style="padding:10px 0;vertical-align:top;line-height:1.6em">Date:</td>
								<td width="340" style="font-weight:bold;padding:10px 0;vertical-align:top;line-height:1.6em">'.$get_data[T_SalesInvoiceHeader_DocDate].'</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:15px 20px">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;border-top:3px solid #c9c9c9;border-bottom:3px solid #c9c9c9;font-size:15px">
						<tbody>
							<tr>
								<td style="padding:20px 0;font-weight:bold;color:#4f4f4f">Purchase Detail</td>
							</tr>
							<tr>
								<td style="padding:5px 0">
									<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
									<tbody>
										<tr>
											<th align="left" style="padding:10px 0;border-bottom:1px solid #dddddd" width="280">Description</th>
											<th align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="112">Quantity</th>
											<th align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="168">Price</th>
										</tr>
									</tbody>
									<tbody style="vertical-align:top;line-height:1.6em">';

									if(!empty($get_data["Detail"])){
										foreach ($get_data["Detail"] as $temp) {
										$Outstanding = ($get_data[T_SalesInvoiceHeader_Outstanding] != 0) ? money_format("%!i",$get_data[T_SalesInvoiceHeader_Outstanding]) : 0.00;
										$message .='<tr>
												<td align="left" style="padding:10px 0;border-bottom:1px solid #dddddd;table-layout:fixed;" width="280px">'.$temp[T_SalesInvoiceDetail_Brand].'&nbsp; '.$temp[T_SalesInvoiceDetail_Model].'&nbsp; '.$temp[T_SalesInvoiceDetail_Color].'&nbsp; '.$temp[T_SalesInvoiceDetail_IMEI].'</td>
												<td align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="112">'.$temp[T_SalesInvoiceDetail_Quantity].' Pcs</td>
												<td align="right" style="padding:10px 0;border-bottom:1px solid #dddddd" width="168">SGD '.money_format("%!i", $temp[T_SalesInvoiceDetail_UnitPrice]).'</td>
											</tr>';
										}
									}
									$message .='</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="padding:0 0 15px">
									<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse">
									<tbody>
										<tr>
											<td align="right" width="392" style="padding:5px 0">GST ('.$gst.' %): </td>
											<td align="right" width="168" style="padding:5px 0">SGD '.money_format("%!i",$gsttotal).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="padding:5px 0">Amount Total: </td>
											<td align="right" width="168" style="padding:5px 0">SGD '.money_format("%!i",$get_data[T_SalesInvoiceHeader_AmountTotal]).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="font-weight:bold;padding:5px 0">Total Payment: </td>
											<td align="right" width="168" style="font-weight:bold;padding:5px 0">SGD '.money_format("%!i",$get_data[T_SalesInvoiceHeader_Payment]).'</td>
										</tr>
										<tr>
											<td align="right" width="392" style="font-weight:bold;padding:5px 0">Outstanding: </td>
											<td align="right" width="168" style="font-weight:bold;padding:5px 0">SGD '.$Outstanding.'</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="padding:10px;border-collapse:collapse;background-color:#fbe3e4;font-size:13px;color:#404040;line-height:16px;letter-spacing:0.1px;border-top:2px solid #fbb3b4;border-bottom:2px solid #fbb3b4">
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="">
									<tbody>
										<tr>
											<td align="left">
												<span style="display:block;line-height:1.6em">
												<b>Invoice files are included in the attachment </b>
												</span>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding:20px" align="center">
						<a href="https://redwhitemobile.com/" style="display:inline-block;padding:20px 30px;background-color:#e70012;color:#ffffff;text-decoration:none;font-size:15px;font-weight:bold;border-radius:3px;width:210px" target="_blank" data-saferedirecturl="https://redwhitemobile.com/">More Product</a>
					</td>
				</tr>
				<tr>
					<td>
						<table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;clear:both!important;background-color:transparent;margin:0 0 10px;padding:0" bgcolor="transparent">
						<tbody>
							<tr style="margin:0;padding:0">
								<td style="margin:0;padding:0"></td>
								<td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0px">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
									<tbody>
										<tr>
											
											<td width="280" align="right" style="padding:0px 20px">
												<table border="0" style="border-collapse:collapse;margin-top:0px">
												<tbody>
													<tr>
														<td style="font-size:13px;color:#999999;margin-bottom:10px" align="right"></td>
													</tr>
													<tr>
														<td style="padding:2px"></td>
													</tr>
													<tr>
														<td>
															<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse">
															<tbody>
																<tr>
																	<td>
																		<a href="http://" style="padding:0 5px" target="_blank" data-saferedirecturl=""><img src="https://ci3.googleusercontent.com/proxy/TL2aWzcuExbXf3EGgD4hhxN1OgBoO900bJFXeHL9RPOV5enqM0muRFAcmFQQb95MNv3VlbyEZud3OCZtLBFlL_uvglTqC6fs2yS3u_qQ6S13LkgOz8L0Udb7p_ez=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307802.png" alt="Google plus" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px" target="_blank" data-saferedirecturl=""><img src="https://ci6.googleusercontent.com/proxy/hpkmiDtehvMUkzI6NQ6XxbIyirGT45tdKbdIzboposYocCcs_dWmXX3aNbXt8ROnlJvo8CA6LphY5vLGZcKuOk02xDtOMbqED9SbOrrgqeUuIgxqlnlAtza2iLFy=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307762.png" alt="Facebook" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px"><img src="https://ci3.googleusercontent.com/proxy/GIsQ666UaY-N1YHMNhXRYjUTObALsZDyqQ51u2t8uFNXI1TlYtWJ4-1fQrLdutgnyNgM9qu43Ang9jKuZ38oUrCBsSD4YPslXDPvP6scSXyIJmyQ6VTB2c0aGVlw=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307869.png" alt="Twitter" height="44" class="CToWUd"></a>
																	</td>
																	<td>
																		<a href="http://" style="padding:0 5px"><img src="https://ci4.googleusercontent.com/proxy/JOX0gbWvJ4vDUCHPWyne4LMiRgkAfgnuTnWfu68HnovFepHvdjChQ7hrOSwMbKA53IPW0Xd3XS1I-Q8amoGXRxNvRMDCnO4GVAH2coMKT0cn2QMu6FMiENJXzfMM=s0-d-e1-ft#https://s3.amazonaws.com/www.betaoutcdn.com/300452016/11/1480307832.png" alt="Instagram" height="44" class="CToWUd"></a>
																	</td>
																</tr>
															</tbody>
															</table>
														</td>
													</tr>
												</tbody>
												</table>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
						<table align="center">
						<tbody>
							<tr style="margin:0;padding:0 0 0 0">
								<td style="display:block!important;width:600px!important;clear:both!important;margin:0 auto;padding:0">
									<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f7f7f7;font-size:13px;color:#999999;border-top:1px solid #dddddd">
									<tbody>
										<tr>
											<td width="600" align="center" style="padding:30px 20px 0">If you need help, use the <a href="http://" style="color:#42b549;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://fapp1.tokopedia.com/linktrack/lt.pl?id%3D16114%3DIRlUVgZSAwlXHgEDVAgJUQMJVE4%3DWQRYHBkGQ1ALeFUOAwwISFQLX0RSVwEEVgoBUwMMUAENUg9QAQ%3D%3D%26fl%3DChEQFkReHRcLB11DSkxWClxJAVZdBBgFWA4fXQUXVw1PEBc%3D%26ext%3DdXRtX3NvdXJjZT1sUFZhUWk2TiZhbXA7dXRtX21lZGl1bT1qTmF5clF3TyZhbXA7dXRtX2NhbXBhaWduPUNPX1RYLUJVWS1TVEktRklOXzBfQk9fMCZhbXA7dXRtX2NvbnRlbnQ9Q05UQ1Q%3D&amp;source=gmail&amp;ust=1511338198167000&amp;usg=AFQjCNF4IE7wk5nlQgPqQyewLoROlnPH4A"> Contact Us page</a></td>
										</tr>
										<tr>
											<td width="600" align="center" style="padding:10px 20px 30px">© '.date("Y").', <span class="il">Redwhite Mobile</span>
											</td>
										</tr>
									</tbody>
									</table>
								</td>
							</tr>
						</tbody>
						</table>
					</td>
				</tr>
			</tbody>
			</table>
			</body>
		</html>';

		$file = $path.$filename;
		$file_size = filesize($file);
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		$content = chunk_split(base64_encode($content));
		$uid = md5(uniqid(time()));

		$header = "From: ".$from_name." <".$from_mail.">\r\n";
		//$header .= 'Cc: hikmat.fauzy@globaltrendasia.com' . "\n";
		//$header .= 'Cc: arif.rahmanto@globaltrendasia.com' . "\n";
		// $header .= "Reply-To: ".$replyto."\n";
		$header .= "Return-Path: ".$from_mail."\r\n";
		$header .= "MIME-Version: 1.0\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
		$emessage= "--".$uid."\n";
		$emessage.= "Content-type:text/html; charset=iso-8859-1\n";
		$emessage.= "Content-Transfer-Encoding: 7bit\n\n";
		$emessage .= $message."\n\n";
		$emessage.= "--".$uid."\n";
		$emessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n"; // use different content types here
		$emessage .= "Content-Transfer-Encoding: base64\n";
		$emessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
		$emessage .= $content."\n\n";
		$emessage .= "--".$uid."--";
		
		mail($mailto,$subject,$emessage,$header);
		
		$output = array('errorcode' => 0, 'msg' => 'Success send to '.$get_data[T_SalesInvoiceHeader_Email]);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file stockbalance.php */
/* Location: ./app/modules/master/controllers/stockbalance.php */

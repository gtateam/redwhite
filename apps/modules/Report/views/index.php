<style>
.borderered .table thead tr th, .table tbody tr td {
border: none;
}
.bordered .table thead tr th, .table tbody tr td {
    border: none;
}
</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_SalesInvoiceHeader_RecordID}) ? ${T_SalesInvoiceHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_SalesInvoiceHeader_RecordTimestamp}) ? ${T_SalesInvoiceHeader_RecordTimestamp} : ''; ?>">
                
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div style=""><h2><b>Red White Mobile Private Limited</b></h2></div>
                            <table id="table-summary" class=" borderered" >
                                <tbody>
                                    <tr>
                                        <td colspan="7"><b>470 North Bridge Road #03-22 Bugis Cube Singapore 188735</b></td>
                                    </tr>
                                    <tr>
                                        <td width="120px">Telephone</td>
                                        <td width="10px">:</td>
                                        <td width="300px">(65) 67354811</td>
                                        <td width="150px"></td>
                                        <td width="100px">&nbsp;&nbsp;&nbsp;Date</td>
                                        <td width="10px">:</td>
                                        <td width="200px" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_DocDate}) ? ${T_SalesInvoiceHeader_DocDate} : "";?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>redwhitemobile@yahoo.com.sg</td>
                                        <td width=""></td>
                                        <td>&nbsp;&nbsp;&nbsp;Terms</td>
                                        <td>:</td>
                                        <td style="border-bottom: solid 2px;"></td>
                                    </tr>
                                    <tr>
                                        <td>Customer Name</td>
                                        <td>:</td>
                                        <td colspan="2" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_CustomerName}) ? ${T_SalesInvoiceHeader_CustomerName} : "";?></td>
                                        <td>&nbsp;&nbsp;&nbsp;Doc No</td>
                                        <td>:</td>
                                        <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_DocNo}) ? ${T_SalesInvoiceHeader_DocNo} : "";?></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">Address</td>
                                        <td rowspan="2">:</td>
                                        <td colspan="2" rowspan="2" style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_Address}) ? ${T_SalesInvoiceHeader_Address} : "";?></td>
                                        <td>&nbsp;&nbsp;&nbsp;I/C No</td>
                                        <td>:</td>
                                        <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_IC}) ? ${T_SalesInvoiceHeader_IC} : "";?></td>
                                        <td width=""></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: bottom">&nbsp;&nbsp;&nbsp;Telephone</td>
                                        <td>:</td>
                                        <td style="border-bottom: solid 2px;"><?php echo isset(${T_SalesInvoiceHeader_Phone}) ? ${T_SalesInvoiceHeader_Phone} : "";?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>             
                </div>
                
                <div class="col-md-8" style="margin-top:10px;">
                    <table id="table-detail" class="table table-bordered mb-none">
                        <thead id="head-detail">
                            <tr>
                                <th style="border:1px solid;" data-col="RowIndex">#</th>
                                <th style="border:1px solid;" data-col="ItemID">Item</th>
                                <th style="border:1px solid;" data-col="ItemName">Description</th>
                                <th style="border:1px solid;" data-col="ItemGroup">Type</th>
                                <th style="border:1px solid;"  data-col="IMEI">IMEI</th>
                                <th style="border:1px solid;" data-col="Status">Status</th>
                                <th style="border:1px solid;" data-col="Qty">Quantity</th>
                                <th style="border:1px solid;" data-col="UnitPrice">Unit Price</th>
                                <th style="border:1px solid;" data-col="LineTotal">Amount</th>
                            </tr>
                        </thead>
                        <tbody id="list-detail">
                            <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                            foreach($Detail as $item): 
                            $LT = $item[T_SalesInvoiceDetail_UnitPrice] * $item[T_SalesInvoiceDetail_Quantity];
                                $detail .= '<tr id="detail-'.$i. '">
                            <td style="border:1px solid;" id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                            <td style="border:1px solid;" id="detailItemIDv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Brand]. '">'.$item[T_SalesInvoiceDetail_Brand]. '</td>
                            <td style="border:1px solid;" id="detailItemNamev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_ItemName]. '">'.$item[T_SalesInvoiceDetail_Model].','.$item[T_SalesInvoiceDetail_ItemName]. ' ,'.$item[T_SalesInvoiceDetail_Color].'</td>
                            <td style="border:1px solid;" id="detailItemGroupv-'.$i. '" data-val="'.$item[T_MasterDataItemGroup_Name]. '">'.$item[T_MasterDataItemGroup_Name]. '</td>
                            <td style="border:1px solid;" id="detailIMEIv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_IMEI]. '">'.$item[T_SalesInvoiceDetail_IMEI]. '</td>
                            <td style="border:1px solid;" id="detailStatusv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Status]. '">'.$item[T_SalesInvoiceDetail_Status]. '</td>
                            <td style="border:1px solid;" id="detailQtyv-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_Quantity]. '">'.$item[T_SalesInvoiceDetail_Quantity]. ' (Pcs)</td>
                            <td style="border:1px solid;" id="detailUnitPricev-'.$i. '" data-val="'.$item[T_SalesInvoiceDetail_UnitPrice]. '">'.$item[T_SalesInvoiceDetail_UnitPrice]. '</td>
                            <td style="border:1px solid;" id="detailLineTotalv-'.$i. '" data-val="'.$LT. '">'.$LT. '</td>
                        </tr>'; 
                        $i++; endforeach; endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="6" rowspan="2" style="border:1px solid;" >&nbsp;<h4>Goods sold are not refundable or exchangable</h4></td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;Payment : <?php echo isset(${T_SalesInvoiceHeader_PaymentMode}) ? ${T_SalesInvoiceHeader_PaymentMode} : "";?></td>
                                <td style="border:1px solid;" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;Subtotal</td>
                                <td style="border:1px solid;" id="subtotal" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="6" rowspan="2" style="border:1px solid;" >&nbsp;<h4>No 7 days exchange policy</h4></td>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;GST</td>
                                <td style="border:1px solid;" id="GST" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border:1px solid;" >&nbsp;</td>
                                <td style="border:1px solid;" >&nbsp;Total Amount ($SGD)</td>
                                <td style="border:1px solid;" id="AmountTotal">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <footer class="panel-footer">
                <!--  -->
            </footer>
        </section>
    </div>
</div>

<script type="text/javascript" src="assets/js/apps-invoice.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset($t2040r001)) {
    $ID = $t2040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>"; 
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    }
];
$(document).ready(function() {
    var LNMusk = accounting.formatNumber("<?php echo isset(${T_SalesInvoiceHeader_AmountSubtotal}) ? ${T_SalesInvoiceHeader_AmountSubtotal} : "";?>",2,",");
    $("#subtotal").html("$"+LNMusk);
    var GST = accounting.formatNumber("<?php $percent=${T_SalesInvoiceHeader_AmountVAT} * ${T_SalesInvoiceHeader_AmountSubtotal}; echo isset(${T_SalesInvoiceHeader_AmountVAT}) ?  $percent: "";?>",2,",");
    $("#GST").html("$"+GST+"<?php $a=100; $percent=${T_SalesInvoiceHeader_AmountVAT} * ${T_SalesInvoiceHeader_AmountSubtotal}; $GST=${T_SalesInvoiceHeader_AmountVAT} * $a;  echo " (".$GST ."%)";?>");
    var AmountTotal = accounting.formatNumber("<?php echo isset(${T_SalesInvoiceHeader_AmountTotal}) ?  ${T_SalesInvoiceHeader_AmountTotal}: "";?>",2,",");
    $("#AmountTotal").html("<b>$"+AmountTotal+"</b>");
    //Numeric
});
</script>
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Item Model
 *
 * @package     App
 * @subpackage  Modules
 * @category    Module Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Report_model extends CI_Model
{

    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceHeader);
        $this->db->where(T_SalesInvoiceHeader_RecordID, $id);
        $query = $this->db->get();
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($data[T_SalesInvoiceHeader_RecordID]);
        }
        return $data;
    }

    public function getDetailItem($id)
    {
        $this->db->select('*');
        $this->db->from(T_SalesInvoiceDetail);
        // $this->db->join(T_MasterDataItem.' a', 
        //                 'a.'.T_MasterDataItem_ItemID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemID, 
        //                 'left'
        //                 );
        $this->db->join(T_MasterDataItemGroup.' i', 
                        'i.'.T_MasterDataItemGroup_ID.'='.T_SalesInvoiceDetail.'.'.T_SalesInvoiceDetail_ItemGroup, 
                        'left'
                        );
        $this->db->where(T_SalesInvoiceDetail_PRI, $id);
        $this->db->order_by(T_SalesInvoiceDetail_RecordID, 'ASC');
        $query = $this->db->get();
        $data = $query->result("array");
        return $data;
    }


}

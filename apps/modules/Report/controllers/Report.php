<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Item Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Report extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Report_model');
	}

	public function index($id='')
	{
		//	$this->template->set_layout('content_only');
		$data = $this->Report_model->getDetail($id);
		//print_r($data);die;
		$this->modules->render('/index',$data);
	}

	function print_item($id='')
    {
        $this->load->library('pdf');
		$pdf = $this->pdf->load();
		
		$data = $this->Report_model->getDetail($id);

		ini_set('memory_limit', '256M'); 
		
		$html = $this->load->view('print_item', $data, true);
		$pdf->WriteHTML($html); // write the HTML into the PDF
		ob_clean();
		$output = 'assets/upload/invoice/'.'Invoice_'.$data{T_SalesInvoiceHeader_DocNo}.'.pdf';
		$this->db->where(T_SalesInvoiceHeader_RecordID,$id);
		$this->db->update(T_SalesInvoiceHeader, array(T_SalesInvoiceHeader_Pdf => $output));
		$pdf->Output("$output", 'F'); // Save to dir
		$pdf->Output("$output", "D"); // Download
        //exit();
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Report_model->getDetail($id);
		//print_r($data);die;
		$this->modules->render('/print_item', $data);
	}
}


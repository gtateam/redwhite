<script type="text/javascript">
    $(document).ready(function() {
        $('.k-button').hide();
    });
</script>
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_DataScanned_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_DataScanned_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_DataScanned_Brand  => array(1,1,'50px','Brand',0,'string',0),
    T_DataScanned_ItemID  => array(1,1,'50px','Item ID',1,'string',1),
    T_DataScanned_ItemName  => array(1,1,'150px','Model',0,'string',0),
    T_DataScanned_Status => array(1,1,'50px','Status',0,'string',0),
    T_DataScanned_IMEI => array(1,1,'100px','Imei No',0,'string',0),
    T_MasterDataGeneralTableValue_Key => array(1,1,'50px','Category',0,'string',0),
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    // T_DataScanned_UOMID => array('UOMID','Scanned/Generaltable/getItemType/1',T_ScannedGeneralTableValue_Key),
    // T_ScannedGeneralTableValue_RecordID => array('AutoIDType','Scanned/Generaltable/getItemBiz/2',T_ScannedGeneralTableValue_Key)

);
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'actBTN' => "1px",
    'postBTN' => "0",
    'table' => T_DataScanned,
    'tools' => array(
        T_DataScanned_RecordID,
        T_DataScanned_RecordTimestamp ,
        T_DataScanned_Brand ,
        T_DataScanned_ItemID  ,
        T_DataScanned_ItemName,
        T_DataScanned_Status,
        T_DataScanned_IMEI,
        T_DataScanned_Category),
    'column' => $column,
    'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Scanned/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Scanned/Update',
        'destroy' => 'Scanned/Delete',
        'form' => 'Scanned/Form',
        'post' => '',
        'unpost' => ''
    )
);
// generate gridView
echo justGridView($attr); 
?>
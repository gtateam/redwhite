<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_DataScanned_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_DataScanned_RecordID}) ? ${T_DataScanned_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_DataScanned_RecordTimestamp}) ? ${T_DataScanned_RecordTimestamp} : ''; ?>">
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Item ID</label>
                            <div class="col-md-9">
                                <?php $items=array(
                                    'id'=> 'ItemID', 
                                    'class' => 'k-input k-textbox', 
                                    'value' => isset(${T_DataScanned_ItemID}) ? ${T_DataScanned_ItemID} : "",
                                    //'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventGroupID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">IMEI</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'IMEI', 
                                    'class' => 'k-input k-textbox   ',
                                    'value' => isset(${T_DataScanned_IMEI}) ? ${T_DataScanned_IMEI} : "",
                                    'style' => 'width:250px',
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Brand</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'Brand', 
                                    'class' => 'k-input k-textbox',
                                    'value' => isset(${T_DataScanned_Brand}) ? ${T_DataScanned_Brand} : "",
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Category</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'Category', 
                                    'class' => '',
                                    'value' => isset(${T_DataScanned_Category}) ? ${T_DataScanned_Category} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Model</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'ItemName', 
                                    'class' => 'k-input k-textbox',
                                    'value' => isset(${T_DataScanned_ItemName}) ? ${T_DataScanned_ItemName} : "",
                                    'style' => 'width:250px',
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                           <!--  -->
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_DataScanned_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>
<?php
//Person Type Lookup
    //field in database data to load
    $dataItemGroup = array(
        array('field' => T_MasterDataItem_ItemID, 'title' => 'Item ID', 'width' => '50px'),
        array('field' => T_MasterDataItem_Brand, 'title' => 'Brand', 'width' => '50px'),
        array('field' => T_MasterDataItem_ItemName, 'title' => 'Model', 'width' => '100px'),
        array('field' => T_MasterDataItem_IMEI, 'title' => 'IMEI', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnItemGroup = array(
        array('id' => 'ItemID', 'column' => T_MasterDataItem_ItemID),
        array('id' => 'ItemName', 'column' => T_MasterDataItem_ItemName),
        array('id' => 'Brand', 'column' => T_MasterDataItem_Brand),
        array('id' => 'IMEI', 'column' => T_MasterDataItem_IMEI),
    );

    $customfilter = array("");

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("GroupID", "Stock Balance", "700px", "Stockmovement/Stockbalance/GetList", $dataItemGroup, $columnItemGroup,T_TransactionStockBalanceHeader,"",$customfilter);
    
//Person Type Lookup
    //field in database data to load
    $dataIdentity = array(
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Key', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTableValue_Description, 'title' => 'Description', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnIdentity = array(
        array('id' => 'IdentityTypeID', 'column' => T_MasterDataGeneralTableValue_RecordID),
        array('id' => 'IdentityTypeName', 'column' => T_MasterDataGeneralTableValue_Description),
    );

    $customfilter2 = array(
        T_MasterDataGeneralTableValue_PRI => "5",
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("IdentityID", "Identity", "500px", "Webservice/Read/Getlist", $dataIdentity, $columnIdentity,T_MasterDataGeneralTableValue,"",$customfilter2);

?>

<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataItem_RecordID}) ? ${T_MasterDataItem_RecordID} : 0; ?>;
$(document).ready(function() {
    $("#UnitPrice").kendoNumericTextBox().val();
    $("#SellingPrice").kendoNumericTextBox().val();
    $("#Category").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_RecordID; ?>",
        optionLabel: "Select Category",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 8},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            ItemID: $('#ItemID').val(),
            ItemName: $('#ItemName').val(),
            Category: $('#Category').val(),
            IMEI: $('#IMEI').val(),
            Brand: $('#Brand').val(),

        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Scanned/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        ItemID: $('#ItemID').val(),
        ItemName: $('#ItemName').val(),
        Category: $('#Category').val(),
        IMEI: $('#IMEI').val(),
        Brand: $('#Brand').val(),
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Scanned/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Scanned'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ItemID == "") { valid = 0; msg += "Item ID is required" + "\r\n"; }
    if (voData.ItemName == "") { valid = 0; msg += "Model is required" + "\r\n"; }
    if (voData.AutoIDType == "") { valid = 0; msg += "Auto id Type is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock In / Out Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Stockout extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Stockout_model','Stockbalance_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/stockout/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Stockout_model->getDetail($id);
		//print_r($data);die;
		$this->modules->render('/stockout/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Stockout_model->getDetail($id);
		$this->modules->render('/stockout/formDetail', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Stockout_model->getDetail($id);
		$this->modules->render('/stockout/formPrint', $data);
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			//$sortfield = $table.'r001';
			$sortfield = "t1010f003, t1010f002";
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Stockout_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Stockout_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetStockList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		$IMEI = $this->input->get('IMEI');
		$table = T_TransactionStockBalanceHeader;
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = T_TransactionStockBalanceHeader_ItemID;
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		
		$getList = $this->Stockout_model->getStockList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $IMEI);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->countHeader = $this->Stockout_model->getListStockCount($table,$IMEI);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetItem()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$type = "";
		$html = "";
		$htmlsub = "";

		$IMEI = $this->input->get('IMEI');
		$RFID = $this->input->get('RFID');
		$id = $this->input->get('id');
		$subID = $this->input->get('subID');
		if($IMEI != null){
			$type = "IMEI"." : ".$IMEI;
			$this->db->where(T_TransactionStockBalanceDetail_IMEI,$IMEI);
		}
		if($RFID != null){
			$type = "RFID"." : ".$RFID;
			$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$RFID);
		}
		$this->db->join(T_TransactionStockBalanceHeader.' sth','sth.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'inner');
		$this->db->join(T_MasterDataLocation.' dl','dl.'.T_MasterDataLocation_LocationID.'= sth.'.T_TransactionStockBalanceHeader_LocationID,'inner');
		$this->db->join(T_MasterDataItem.' dt','dt.'.T_MasterDataItem_ItemID.'= sth.'.T_TransactionStockBalanceHeader_ItemID,'inner');
		$this->db->join(T_MasterDataItemGroup.' dtg','dtg.'.T_MasterDataItemGroup_ID.'= dt.'.T_MasterDataItem_GroupID,'inner');
		$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
		$sql = $this->db->get(T_TransactionStockBalanceDetail);
		if($sql->num_rows() > 0 ){
			$voData = $sql->first_row();
			$info->Data = $voData;
			if($voData->{T_MasterDataItem_AutoIDType} == SSTYPE)
			{
				$html = '<tr id="detail-'.$id.'" style="background: beige; display: table-row;">
						<td class="actions"><span class="label label-primary new">new</span>
							<input type="checkbox" class="detailCheck" value="'.$id.'"> <a href="javascript:void(0);" onclick="editdetail(\'detail\','.$id.',0,1);"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removedetail(\'detail\','.$id.');"><i class="fa fa-trash-o"></i></a>
						</td>
						<td id="detailRowIndexv-'.$id.'" data-def="'.$id.'" data-val="'.$id.'">'.$id.'</td>
						<td id="detailItemIDv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_ItemID}.'" data-val="'.$voData->{T_MasterDataItem_ItemID}.'">'.$voData->{T_MasterDataItem_ItemID}.'</td>
						<td id="detailBrandv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Brand}.'" data-val="'.$voData->{T_MasterDataItem_Brand}.'">'.$voData->{T_MasterDataItem_Brand}.'</td>
						<td id="detailModelv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Model}.'" data-val="'.$voData->{T_MasterDataItem_Model}.'">'.$voData->{T_MasterDataItem_Model}.'</td>
						<td id="detailColorv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Color}.'" data-val="'.$voData->{T_MasterDataItem_Color}.'">'.$voData->{T_MasterDataItem_Color}.'</td>
						<td id="detailStatusv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Status}.'" data-val="'.$voData->{T_MasterDataItem_Status}.'">'.$voData->{T_MasterDataItem_Status}.'</td>
						<td id="detailItemGroupv-'.$id.'" data-def="'.$voData->{T_MasterDataItemGroup_ID}.'" data-val="'.$voData->{T_MasterDataItemGroup_ID}.'">'.$voData->{T_MasterDataItemGroup_Name}.'</td>
						<td id="detailEPCv-'.$id.'" data-def="" data-val=""></td>
						<td id="detailQtyv-'.$id.'" data-def="1" data-val="1">1</td>
						<td id="detailLocationIDv-'.$id.'" data-def="'.$voData->{T_MasterDataLocation_LocationID}.'" data-val="'.$voData->{T_MasterDataLocation_LocationID}.'">'.$voData->{T_MasterDataLocation_LocationID}.'</td>
						<td id="detailLocationNamev-'.$id.'" data-def="'.$voData->{T_MasterDataLocation_LocationName}.'" data-val="'.$voData->{T_MasterDataLocation_LocationName}.'">'.$voData->{T_MasterDataLocation_LocationName}.'</td>
						<td id="detailRecordIDDetailv-'.$id.'" data-def="" data-val="" style="display:none;"></td>
						<td id="detailRecordFlagv-'.$id.'" data-def="" data-val=""></td>
					</tr>';
				$htmlsub = '<tr id="detailSub-'.$subID.'">
					<td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$subID.'"> <a onclick="removedetail(\'detailSub\','.$subID.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
					<td id="detailSubIMEIv-'.$subID.'" data-val="'.$voData->{T_TransactionStockBalanceDetail_IMEI}.'">'.$voData->{T_TransactionStockBalanceDetail_IMEI}.'</td>
					<td id="detailSubSerialNov-'.$subID.'" data-val="'.$voData->{T_TransactionStockBalanceDetail_SerialNo}.'">'.$voData->{T_TransactionStockBalanceDetail_SerialNo}.'</td>
				</tr>';
			}else{
				$html = '<tr id="detail-'.$id.'" style="background: beige; display: table-row;">
						<td class="actions"><span class="label label-primary new">new</span>
							<input type="checkbox" class="detailCheck" value="'.$id.'"> <a href="javascript:void(0);" onclick="editdetail(\'detail\','.$id.',0,1);"><i class="fa fa-pencil"></i></a><a class="delete-row" href="javascript:void(0);" onclick="removedetail(\'detail\','.$id.');"><i class="fa fa-trash-o"></i></a>
						</td>
						<td id="detailRowIndexv-'.$id.'" data-def="'.$id.'" data-val="'.$id.'">'.$id.'</td>
						<td id="detailItemIDv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_ItemID}.'" data-val="'.$voData->{T_MasterDataItem_ItemID}.'">'.$voData->{T_MasterDataItem_ItemID}.'</td>
						<td id="detailBrandv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Brand}.'" data-val="'.$voData->{T_MasterDataItem_Brand}.'">'.$voData->{T_MasterDataItem_Brand}.'</td>
						<td id="detailModelv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Model}.'" data-val="'.$voData->{T_MasterDataItem_Model}.'">'.$voData->{T_MasterDataItem_Model}.'</td>
						<td id="detailColorv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Color}.'" data-val="'.$voData->{T_MasterDataItem_Color}.'">'.$voData->{T_MasterDataItem_Color}.'</td>
						<td id="detailStatusv-'.$id.'" data-def="'.$voData->{T_MasterDataItem_Status}.'" data-val="'.$voData->{T_MasterDataItem_Status}.'">'.$voData->{T_MasterDataItem_Status}.'</td>
						<td id="detailItemGroupv-'.$id.'" data-def="'.$voData->{T_MasterDataItemGroup_ID}.'" data-val="'.$voData->{T_MasterDataItemGroup_ID}.'">'.$voData->{T_MasterDataItemGroup_Name}.'</td>
						<td id="detailEPCv-'.$id.'" data-def="'.$voData->{T_TransactionStockBalanceHeader_EPC}.'" data-val="'.$voData->{T_TransactionStockBalanceHeader_EPC}.'">'.$voData->{T_TransactionStockBalanceHeader_EPC}.'</td>
						<td id="detailQtyv-'.$id.'" data-def="1" data-val="1">1</td>
						<td id="detailLocationIDv-'.$id.'" data-def="'.$voData->{T_MasterDataLocation_LocationID}.'" data-val="'.$voData->{T_MasterDataLocation_LocationID}.'">'.$voData->{T_MasterDataLocation_LocationID}.'</td>
						<td id="detailLocationNamev-'.$id.'" data-def="'.$voData->{T_MasterDataLocation_LocationName}.'" data-val="'.$voData->{T_MasterDataLocation_LocationName}.'">'.$voData->{T_MasterDataLocation_LocationName}.'</td>
						<td id="detailRecordIDDetailv-'.$id.'" data-def="" data-val="" style="display:none;"></td>
						<td id="detailRecordFlagv-'.$id.'" data-def="" data-val=""></td>
					</tr>';
			}
			
			//print_r($sql->first_row()); die;
		}else{
			$info->msg = "Item not found with ".$type;
			$info->errorcode = 200;
		}
		$info->html = $html;
		$info->htmlsub = $htmlsub;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function autoLocation(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "T_MasterDataLocation";

		$getList = $this->Stockout_model->autoLocation($table);
		$info->data = $getList->first_row();
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		$this->db->trans_begin();
		try{
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo($this->input->post("DocType")), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($this->input->post("DocType")), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => 0,
					T_TransactionStockMovementHeader_DocTypeID => empty($this->input->post("DocType"))? 0:$this->input->post("DocType"),
					T_TransactionStockMovementHeader_DocNo => substr(DocNo($this->input->post("DocType")), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remark"))? ' ':$this->input->post("Remark"),
					'detail' => $this->input->post("detail"),
					);

				$id = $this->Stockout_model->Insert($data);

				//print_r($id);die;

				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success', 'id' => $id);
			}
		}catch(Exception $e)
		{
			$this->Stockout_model->Delete($id);
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		$this->db->trans_begin();
		try{
			if (check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $this->input->post("DocType"),
					T_TransactionStockMovementHeader_DocNo => $this->input->post("DocNo"),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remark"))? ' ':$this->input->post("Remark"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail"),
					);

				$id = $this->Stockout_model->update($data);
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success', 'id' => $id);
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Stockout_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post($id=''){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";
		$this->db->trans_begin();
		try{
			
            //get data
			($id=='') ? $id=$this->input->post("RecordID") : $id;
			//print_r($id);die;
			$get_data = $this->Stockout_model->getDetail($id);
            
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Stockout_model->UpdateHeader($data,$id);
            foreach ($get_data["Detail"] as $temp) {
				//$get_stocklist = $this->Pos_model->item_exist($temp[T_SalesInvoiceDetail_ItemID]);

				$this->db->from(T_TransactionStockBalanceHeader);
				$this->db->join(T_TransactionStockMovementDetail.' i', 
							'i.'.T_TransactionStockMovementDetail_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID, 
							'left'
							);
				$this->db->where(T_TransactionStockBalanceHeader_ItemID, $temp[T_TransactionStockMovementDetail_ItemID]);
				$this->db->where(T_TransactionStockBalanceHeader_LocationID, $temp[T_TransactionStockMovementDetail_LocationID1]);
				$get_stocklist = $this->db->get();

				//echo $this->db->last_query();die;
				//print_r($get_stocklist->num_rows());die;
				
				// Start Get Master Item
				$this->db->from(T_MasterDataItem);
				$this->db->where(T_MasterDataItem_ItemID, $temp[T_TransactionStockMovementDetail_ItemID]);
				$get_master_item = $this->db->get()->first_row("array");
				// End Get Master Item
		
				if($get_stocklist->num_rows() == 0)
				{
					$voMsg .= $temp[T_TransactionStockMovementDetail_Brand].' '.$temp[T_TransactionStockMovementDetail_Model]." not exists in Location ".$temp[T_TransactionStockMovementDetail_LocationID2]." \n ";
					$this->Stockut_model->Delete($id);
				}else{
					$get_stocklist = $get_stocklist->first_row("array");
					$stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $temp[T_TransactionStockMovementDetail_Quantity1];
					if($qty < 0){
						$voMsg .= $temp[T_TransactionStockMovementDetail_Brand].' '.$temp[T_TransactionStockMovementDetail_Model]." < 0 in Location ".$temp[T_TransactionStockMovementDetail_LocationID2]." \n ";
					}
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
					$this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

					if($get_master_item[T_MasterDataItem_AutoIDType] == SSTYPE ){
						$this->db->from(T_TransactionStockMovementDetailSerialNo);
						$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI,$temp[T_TransactionStockMovementDetail_RecordID]);
						$get_serial_no = $this->db->get()->result("array");

						foreach ($get_serial_no as $get_serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $get_serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>0));
						}
					}
					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $stocklistPRI,
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
						T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1]*-1,
						T_TransactionStockBalanceLog_ItemGroup => $temp[T_TransactionStockMovementDetail_ItemGroup],
						T_TransactionStockBalanceLog_Status => $temp[T_TransactionStockMovementDetail_Status],
					);
					
				}
				$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				if($voMsg != ""){ throw new Exception($voMsg); }
			}

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$voMsg = "";
		$this->db->trans_begin();
		try{
			//get data
			$id = $this->input->post("RecordID");
			$id = $this->input->post("RecordID");
			$get_data = $this->Stockout_model->getDetail($id);
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->Stockout_model->UpdateHeader($data,$id);

			foreach ($get_data["Detail"] as $temp) {
                    $get_stocklist = $this->Stockbalance_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

                    // Start Get Master Item
                    $this->db->from(T_MasterDataItem);
                    $this->db->where(T_MasterDataItem_ItemID, $temp[T_TransactionStockMovementDetail_ItemID]);
                    $get_master_item = $this->db->get()->first_row("array");
                    // End Get Master Item
					
                    if($get_stocklist->num_rows() > 0){
                        $get_stocklist = $get_stocklist->first_row("array");
                        $stocklistPRI = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];
						
                        $qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $temp[T_TransactionStockMovementDetail_Quantity1];
                        $data_stocklist = array(
                            T_TransactionStockBalanceHeader_Quantity => $qty
                        );
                        $this->db->where(T_TransactionStockBalanceHeader_RecordID, $stocklistPRI);
                        $this->db->update(T_TransactionStockBalanceHeader, $data_stocklist);

                    }

                    if($get_master_item[T_MasterDataItem_AutoIDType] == SSTYPE){
                        $this->db->from(T_TransactionStockMovementDetailSerialNo);
                        $this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
                        $get_serial_no = $this->db->get();

                        if($get_serial_no->num_rows() > 0){
                            foreach ($get_serial_no->result("array") as $serial_no_temp) {
                                $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
                                $serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
                                
                                if($serialNoExists->num_rows() == 0){
                                    $data_serial_no = array(
                                        T_TransactionStockBalanceDetail_PRI => $stocklistPRI,
                                        T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
                                        T_TransactionStockBalanceDetail_StockFlag => 1
                                    );
                                    $this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
                                }else{
                                    $this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
                                    $this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
                                }
                            }
                        }
                    }

                    $log_data = array(
                        T_TransactionStockBalanceLog_PRI => $stocklistPRI,
                        T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
                        T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
                        T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
						T_TransactionStockBalanceLog_Quantity => $temp[T_TransactionStockMovementDetail_Quantity1],
						T_TransactionStockBalanceLog_ItemGroup => $temp[T_TransactionStockMovementDetail_ItemGroup],
						T_TransactionStockBalanceLog_Status => $temp[T_TransactionStockMovementDetail_Status],
                    );

                    $this->db->insert(T_TransactionStockBalanceLog,$log_data);
					if($voMsg != ""){ throw new Exception($voMsg); }
                }

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function getSubItem(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		
 		$id = $this->input->get("RecordID");
 		$data = $this->Stockout_model->getSubItem($id);
 		$html = "";
 		if(!empty($data)){
            $target = "'detailSub'";
			$a = 1;
            foreach ($data as $item) {
            	if($this->input->get("editable")){
	                $html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions">-</td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else if($this->input->get("LookupTranfer")){
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }else{
	            	$html .= '<tr id="detailSub-'.$a.'">
	                    <td class="actions"><input type="checkbox" class="detailSubCheck" value="'.$a.'"> <a onclick="editdetail('.$target.','.$a.',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$a.');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
	                        <td id="detailSubRowIndex2v-'.$a.'" data-val="'.$a.'">'.$a.'</td>
	                        <td id="detailSubSerialNov-'.$a.'" data-val="'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '">'.$item[T_TransactionStockMovementDetailSerialNo_SerialNo]. '</td>
	                </tr>'; 
	            }
				$a++;
            }
        }

 		$output = array('errorcode' => 0, 'msg' => 'success', 'data' => $data, 'html' => $html);
 		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Stockout.php */
/* Location: ./app/modules/master/controllers/Stockout.php */

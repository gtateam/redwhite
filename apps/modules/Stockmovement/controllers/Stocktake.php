<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Salman Fariz
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Take Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Stocktake extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Stocktake_model','StockList_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/stocktake/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Stocktake_model->getDetail($id);
		$this->modules->render('/stocktake/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Stocktake_model->getHeader($id);
		
		$this->modules->render('/stocktake/form', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Stocktake_model->getDetail($id);
		$this->modules->render('/stocktake/formPrint', $data);
	}

	public function GetListDetail()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$id = $this->input->get('PRI');
		$table = "";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1011r001';
   			$sortdir = 'ASC';
		}

		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}

		$getList = $this->Stocktake_model->GetListDetail($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$id);		

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Stocktake_model->GetListDetailCount($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$id);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));		
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->get('table');
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		$TransDate = $this->input->get('TransDate');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Stocktake_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$TransDate);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Stocktake_model->getListCount($table,$TransDate);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetListRow(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$item = $this->input->post('ItemID');
		$location = $this->input->post('LocationID');
		$getList = $this->Stocktake_model->GetListRow($item, $location);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Ditemukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function Insert(){
		try{
			$doctype = "IVST";
			if (check_column(T_TransactionStockMovementHeader_DocNo, substr(DocNo($doctype), 2)) == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.substr(DocNo($doctype), 2).' sudah ada');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocNo => substr(DocNo($doctype), 2),
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'detail' => $this->input->post("detail")
				);

				$this->Stocktake_model->Insert($data);
				
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			$doctype = "IVST";
			if(check_column(T_TransactionStockMovementHeader_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_TransactionStockMovementHeader_RecordID => $this->input->post("RecordID"),
					T_TransactionStockMovementHeader_RecordTimestamp => date("Y-m-d g:i:s",now()),
					T_TransactionStockMovementHeader_RecordStatus => empty($this->input->post("RecordStatus"))? 0:$this->input->post("RecordStatus"),
					T_TransactionStockMovementHeader_DocTypeID => $doctype,
					T_TransactionStockMovementHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
					T_TransactionStockMovementHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
					T_TransactionStockMovementHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
					'DoRemoveID' => empty($this->input->post("DoRemoveID"))? ' ':$this->input->post("DoRemoveID"),
					'detail' => $this->input->post("detail")
				);

				$this->Stocktake_model->Update($data);
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Stocktake_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
				);

			activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function Post(){
		$this->db->trans_begin();
		try{
			$voMsg = "";
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
			$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
			$sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
			$allSN = $sqlAllSN->result("array");
			$allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
			$allSNCount = count($allSN);
			
			$PRISNDoc = array_column($get_data["Detail"],T_TransactionStockMovementDetail_RecordID);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);
			
			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
					$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
					$different = $qty2 - $qty1;
					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $different;
					if($qty < 0)
					{
						$voMsg .= $temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}

				}else{
                    $different = $temp[T_TransactionStockMovementDetail_Quantity2];
					$data_stocklist = array(
						T_TransactionStockBalanceHeader_ItemID => $temp[T_TransactionStockMovementDetail_ItemID],
						T_TransactionStockBalanceHeader_LocationID => $temp[T_TransactionStockMovementDetail_LocationID1],
						T_TransactionStockBalanceHeader_Quantity => $temp[T_TransactionStockMovementDetail_Quantity2],
					);
					$this->db->insert(T_TransactionStockBalanceHeader,$data_stocklist);
					$parent_id = $this->db->insert_id();
				}

				if($typeItem == SSTYPE){
					// get sn document
					$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
					$get_serial_no = $this->db->get(T_TransactionStockMovementDetailSerialNo);

					if($get_serial_no->num_rows() > 0){
						// get sn stock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$StockSN = $this->db->get(T_TransactionStockBalanceDetail);
						foreach ($StockSN->result("array") as $dataStockSN) {
							$dataL = array(
								T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
								T_StockTakeLog_SN => $dataStockSN[T_TransactionStockBalanceDetail_SerialNo],
								T_StockTakeLog_StockFlag => $dataStockSN[T_TransactionStockBalanceDetail_StockFlag]
							);
							$this->db->insert(T_StockTakeLog,$dataL);
						}
						// update sn to unstock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							// get sn 
							$this->db->where(T_TransactionStockBalanceDetail_PRI,$parent_id);
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){ // insert if not exist in stock
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $parent_id,
									T_TransactionStockBalanceDetail_RecordFlag => 2,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{ // update if exist in stock
								$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different,
					T_TransactionStockBalanceLog_ItemGroup => $temp[T_TransactionStockMovementDetail_ItemGroup],
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				if($voMsg != ""){ throw new Exception($voMsg); }
			}

			$msgCustom = "";
			$snMiss = array();
			$snMiss1 = array();
			$snMiss2 = array();

			$diffSN1 = array_diff($allSN,$DocSN);
			$diffSNCount1 = count($diffSN1)*-1;

			// begin of miss condition item
			sort($diffSN1);
			foreach($diffSN1 as $itemDiff)
			{
				// $snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				// $this->db->where(T_MasterDataItem_EPC,$snTag);
				// $SqlSNData = $this->db->get(T_MasterDataItem);
				// $SNData = $SqlSNData->first_row("array");

				$this->db->where(T_TransactionStockBalanceHeader_EPC,$itemDiff);
                $this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
                $this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
                $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
                $SqlSNData = $this->db->get(T_TransactionStockBalanceHeader);
                if($SqlSNData->num_rows() == 0){
                    $this->db->where(T_TransactionStockBalanceDetail_SerialNo,$itemDiff);
                    $this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
                    $this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
                    $this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
                    $SqlSNData = $this->db->get(T_TransactionStockBalanceDetail);
				}
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_ItemID,$SNData[T_MasterDataItem_ItemID]);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

					$snMiss1[$SNData[T_MasterDataItem_ItemID]][] = $itemDiff;
				}
			}
				
			foreach($snMiss1 as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_ItemID,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					// $this->db->where(T_MasterDataItem_EPC,$key);
					// $SqlSNData = $this->db->get(T_MasterDataItem);
					// $SNData = $SqlSNData->first_row("array");
					$this->db->where(T_TransactionStockBalanceHeader_EPC,$itemDiff);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->join(T_TransactionStockBalanceDetail.' i','i.'.T_TransactionStockBalanceDetail_PRI.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_RecordID,'left');
					$this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'='.T_TransactionStockBalanceHeader.'.'.T_TransactionStockBalanceHeader_ItemID,'left');
					$SqlSNData = $this->db->get(T_TransactionStockBalanceHeader);
					if($SqlSNData->num_rows() == 0){
						$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$itemDiff);
						$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
						$this->db->join(T_TransactionStockBalanceHeader.' i','i.'.T_TransactionStockBalanceHeader_RecordID.'='.T_TransactionStockBalanceDetail.'.'.T_TransactionStockBalanceDetail_PRI,'left');
						$this->db->join(T_MasterDataItem.' a','a.'.T_MasterDataItem_ItemID.'=i.'.T_TransactionStockBalanceHeader_ItemID,'left');
						$SqlSNData = $this->db->get(T_TransactionStockBalanceDetail);
					}
					$SNData = $SqlSNData->first_row("array");
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss1[$key]);
					$different = $Sqty - $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty*-1,
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}
			// end of miss condition item

			$diffSN2 = array_diff($DocSN,$allSN);
			$diffSNCount2 = count($diffSN2);

			// begin of new condition item
			sort($diffSN2);
			foreach($diffSN2 as $itemDiff)
			{
				$snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$SqlSNData = $this->db->get(T_MasterDataItem);
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 1));

					$snMiss2[$snTag][] = $itemDiff;
				}
			}
				
			foreach($snMiss2 as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					$this->db->where(T_MasterDataItem_EPC,$key);
					$SqlSNData = $this->db->get(T_MasterDataItem);
					$SNData = $SqlSNData->first_row("array");
					
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss2[$key]);
					$different = $Sqty - $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty,
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}
				// end of new condition item

			$diffSN = array_merge($diffSN1,$diffSN2);
			$diffSNCount = $diffSNCount2." | ".$diffSNCount1;
			$FoundCount =  $diffSNCount2;
			$MissCount = $diffSNCount1;

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => "success");
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	public function PostOLD(){
		$this->db->trans_begin();
		try{
			$voMsg = "";
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 1,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
			$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
			$sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
			$allSN = $sqlAllSN->result("array");
			$allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
			$allSNCount = count($allSN);

			$PRISNDoc = array_column($get_data["Detail"],T_TransactionStockMovementDetail_RecordID);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);

			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				if($get_stocklist->num_rows() > 0){
					$get_stocklist = $get_stocklist->first_row("array");
					$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

					$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
					$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
					$different = $qty2 - $qty1;
					$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] + $different;
					if($qty < 0)
					{
						$voMsg .= $temp[T_TransactionStockMovementDetail_ItemID]." < 0 in Location ".$temp[T_TransactionStockMovementDetail_LocationID1]." \n "; 
					}else{
						$UpdateStockBalance = array(
							T_TransactionStockBalanceHeader_Quantity => $qty
						);
						$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
						$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
					}

				}else{
					$voMsg .= "item ".$temp[T_TransactionStockMovementDetail_ItemID]." not found in Stockbalance \n ";
				}

				if($typeItem == SSTYPE){
					// get sn document
					$this->db->where(T_TransactionStockMovementDetailSerialNo_PRI, $temp[T_TransactionStockMovementDetail_RecordID]);
					$get_serial_no = $this->db->get(T_TransactionStockMovementDetailSerialNo);

					if($get_serial_no->num_rows() > 0){
						// get sn stock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$StockSN = $this->db->get(T_TransactionStockBalanceDetail);
						foreach ($StockSN->result("array") as $dataStockSN) {
							$dataL = array(
								T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
								T_StockTakeLog_SN => $dataStockSN[T_TransactionStockBalanceDetail_SerialNo],
								T_StockTakeLog_StockFlag => $dataStockSN[T_TransactionStockBalanceDetail_StockFlag]
							);
							$this->db->insert(T_StockTakeLog,$dataL);
						}
						// update sn to unstock
						$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
						$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							// get sn 
							$this->db->where(T_TransactionStockBalanceDetail_PRI,$parent_id);
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() == 0){ // insert if not exist in stock
								$data_serial_no = array(
									T_TransactionStockBalanceDetail_PRI => $parent_id,
									T_TransactionStockBalanceDetail_RecordFlag => 2,
									T_TransactionStockBalanceDetail_SerialNo => $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo],
									T_TransactionStockBalanceDetail_StockFlag => 1
								);
								$this->db->insert(T_TransactionStockBalanceDetail,$data_serial_no);
							}else{ // update if exist in stock
								$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_TransactionStockMovementDetailSerialNo_SerialNo]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>1));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				if($voMsg != ""){ throw new Exception($voMsg); }
			}

			$diffSN = array_diff($allSN,$DocSN);
			$msgCustom = "";
			$snMiss = array();
			foreach($diffSN as $itemDiff)
			{
				$snTag = substr($itemDiff,SNINDEX,SNLENGTH);
				$this->db->where(T_MasterDataItem_EPC,$snTag);
				$SqlSNData = $this->db->get(T_MasterDataItem);
				$SNData = $SqlSNData->first_row("array");
				$msgCustom .= $SNData[T_MasterDataItem_ItemID]."(".$itemDiff."), \n";
				
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$snTag);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();

					$this->db->where(T_TransactionStockBalanceDetail_PRI,$current->{T_TransactionStockBalanceHeader_RecordID});
					$sqlSN = $this->db->get(T_TransactionStockBalanceDetail);
					$currentSN = $sqlSN->first_row();

					$dataL = array(
						T_StockTakeLog_DocNo => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_StockTakeLog_SN => $itemDiff,
						T_StockTakeLog_StockFlag => $currentSN->{T_TransactionStockBalanceDetail_StockFlag}
					);
					$this->db->insert(T_StockTakeLog,$dataL);

					$this->db->where(T_TransactionStockBalanceDetail_PRI, $current->{T_TransactionStockBalanceHeader_RecordID});
					$this->db->update(T_TransactionStockBalanceDetail,array(T_TransactionStockBalanceDetail_StockFlag => 0));

					$snMiss[$snTag] = $itemDiff;
				}
			}

			foreach($snMiss as $key => $itemMiss)
			{
				$this->db->where(T_TransactionStockMovementDetail_PRI,$get_data[T_TransactionStockMovementHeader_RecordID]);
				$this->db->where(T_TransactionStockMovementDetail_EPC,$key);
				$sqla = $this->db->get(T_TransactionStockMovementDetail);

				if($sqla->num_rows() == 0){

					$this->db->where(T_MasterDataItem_EPC,$key);
					$SqlSNData = $this->db->get(T_MasterDataItem);
					$SNData = $SqlSNData->first_row("array");
					
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$sql = $this->db->get(T_TransactionStockBalanceHeader);
					$current = $sql->first_row();
					$Sqty = $current->{T_TransactionStockBalanceHeader_Quantity};

					$missQty = count($snMiss[$key]);
					$different = $Sqty - $missQty;
					$UpdateSB = array(
						T_TransactionStockBalanceHeader_Quantity => $different
					);
					$this->db->where(T_TransactionStockBalanceHeader_ItemID,$SNData[T_MasterDataItem_ItemID]);
					$this->db->where(T_TransactionStockBalanceHeader_LocationID,$get_data["Detail"][0][T_TransactionStockMovementDetail_LocationID1]);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateSB);

					$log_data = array(
						T_TransactionStockBalanceLog_PRI => $current->{T_TransactionStockBalanceHeader_RecordID},
						T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
						T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
						T_TransactionStockBalanceLog_RefDetailRecordID => 0,
						T_TransactionStockBalanceLog_Quantity => $missQty*-1,
					);
					$this->db->insert(T_TransactionStockBalanceLog,$log_data);
				}
			}
			$diffSNCount = count($diffSN);
			if($allSNCount > $DocSNCount)
			{
				$msg = "in stock balance (".$allSNCount.")\n scanned result (".$DocSNCount.")\n missing (".$diffSNCount.") item :\n".$msgCustom; 
			}else{
				$msg = "in stock balance :".$allSNCount."\n scanned result :".$DocSNCount."\n new ".$diffSNCount." item found :\n".$msgCustom; 
			}

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => $msg);
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		$this->db->trans_begin();
		try{
			$voMsg = "";
			//get data
			$id = $this->input->post("RecordID");
			$get_data = $this->Stocktake_model->getDetail($id);
			
			$data = array(
				T_TransactionStockMovementHeader_RecordStatus   => 0,
			);
			$this->Stocktake_model->UpdateHeader($data,$id);

			$this->db->select(T_TransactionStockBalanceDetail_SerialNo);
			$this->db->where(T_TransactionStockBalanceDetail_StockFlag,1);
			$sqlAllSN = $this->db->get(T_TransactionStockBalanceDetail);
			$allSN = $sqlAllSN->result("array");
			$allSN = array_column($allSN,T_TransactionStockBalanceDetail_SerialNo);
			$allSNCount = count($allSN);
			
			$PRISNDoc = array_column($get_data["Detail"],T_TransactionStockMovementDetail_RecordID);
			$this->db->where_in(T_TransactionStockMovementDetailSerialNo_PRI,$PRISNDoc);
			$sqlDocSN = $this->db->get(T_TransactionStockMovementDetailSerialNo);
			$DocSN = $sqlDocSN->result("array");
			$DocSN = array_column($DocSN,T_TransactionStockMovementDetailSerialNo_SerialNo);
			$DocSNCount = count($DocSN);
			
			foreach ($get_data["Detail"] as $temp) {
				$this->db->select(T_MasterDataItem_AutoIDType);
				$this->db->where(T_MasterDataItem_ItemID,$temp[T_TransactionStockMovementDetail_ItemID]);
				$query = $this->db->get(T_MasterDataItem);
				$typeItem = $query->first_row();
				$typeItem = $typeItem->{T_MasterDataItem_AutoIDType};

				$get_stocklist = $this->StockList_model->item_exist($temp[T_TransactionStockMovementDetail_ItemID],$temp[T_TransactionStockMovementDetail_LocationID1]);

				$get_stocklist = $get_stocklist->first_row("array");
				$parent_id = $get_stocklist[T_TransactionStockBalanceHeader_RecordID];

				$qty1 = $temp[T_TransactionStockMovementDetail_Quantity1];//QtySistem
				$qty2 = $temp[T_TransactionStockMovementDetail_Quantity2];//QtyReal
				$different = $qty2 - $qty1;
				$qty = $get_stocklist[T_TransactionStockBalanceHeader_Quantity] - $different;
				if($qty < 0)
				{
					$voMsg .= $temp[T_TransactionStockMovementDetail_EPC]." < 0 in Stockbalance \n "; 
				}else{
					$UpdateStockBalance = array(
						T_TransactionStockBalanceHeader_Quantity => $qty
					);
					$this->db->where(T_TransactionStockBalanceHeader_RecordID,$parent_id);
					$this->db->update(T_TransactionStockBalanceHeader, $UpdateStockBalance);
				}

				if($typeItem == SSTYPE){
					$this->db->where(T_StockTakeLog_DocNo, $get_data[T_TransactionStockMovementHeader_RecordID]);
					$get_serial_no = $this->db->get(T_StockTakeLog);

					if($get_serial_no->num_rows() > 0){
						foreach ($get_serial_no->result("array") as $serial_no_temp) {
							$this->db->where(T_TransactionStockBalanceDetail_SerialNo,$serial_no_temp[T_StockTakeLog_SN]);
							$serialNoExists = $this->db->get(T_TransactionStockBalanceDetail);
							
							if($serialNoExists->num_rows() != 0){
								$serialNoExistsData = $serialNoExists->first_row('array');
								$flag = $serial_no_temp[T_StockTakeLog_StockFlag];
								$this->db->where(T_TransactionStockBalanceDetail_PRI, $parent_id);
								$this->db->where(T_TransactionStockBalanceDetail_SerialNo, $serial_no_temp[T_StockTakeLog_SN]);
								$this->db->update(T_TransactionStockBalanceDetail, array(T_TransactionStockBalanceDetail_StockFlag=>$flag));
							}
						}
					}
				}

				$log_data = array(
					T_TransactionStockBalanceLog_PRI => $parent_id,
					T_TransactionStockBalanceLog_DocTypeID => $get_data[T_TransactionStockMovementHeader_DocTypeID],
					T_TransactionStockBalanceLog_RefRecordID => $get_data[T_TransactionStockMovementHeader_RecordID],
					T_TransactionStockBalanceLog_RefDetailRecordID => $temp[T_TransactionStockMovementDetail_RecordID],
					T_TransactionStockBalanceLog_Quantity => $different*-1,
				);

				$this->db->insert(T_TransactionStockBalanceLog,$log_data);

				if($voMsg != ""){ throw new Exception($voMsg); }
			}

			

			$this->db->where(T_StockTakeLog_DocNo,$id);
			$this->db->delete(T_StockTakeLog);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => "success");
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
			$this->db->trans_rollback();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file stocktake.php */
/* Location: ./app/modules/master/controllers/stocktake.php */

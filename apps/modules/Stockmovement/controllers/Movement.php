<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Stockmovement Modules
 *
 * Stock In Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 29.12.2017	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Movement extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('Stockin_model','Stockbalance_model'));
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/stockin/index',$data);
	}

	public function Insert(){		
		$this->db->trans_begin();
		try{
			$PRI = 0;
			if($this->input->post("Type") == "Out") {
				$check = $this->GetDataIn2($this->input->post("IMEI"), "IMEI");
				if ($check) {
					$check = $this->GetDataIn2($this->input->post("RFID"), "RFID");

					if (!$check) {
						throw new Exception('RFID not found!');
					}

				} else {
					throw new Exception('IMEI not found!');
				}
			}

			if($this->input->post("RecordID")){ 
				// Begin Edit Data
				if(check_column(T_TRANSINOUT_RecordTimestamp, 'TimeStamp') == FALSE)
				{
					$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
				}else{
					$date = strtotime($this->input->post("DocDate"));
					$data = array(
						T_TRANSINOUT_UpdatedOn => date("Y-m-d g:i:s",now()),
						T_TRANSINOUT_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_TRANSINOUT_UpdatedAt  => $this->input->ip_address(),
						T_TRANSINOUT_Type => empty($this->input->post("Type"))? " ":$this->input->post("Type"),
						T_TRANSINOUT_Date => empty($this->input->post("DocDate"))? " ": date("Y-m-d",$date) ,
						T_TRANSINOUT_ItemID => empty($this->input->post("ItemID"))? " ":$this->input->post("ItemID"),
						T_TRANSINOUT_Brand => empty($this->input->post("Brand"))? " ":$this->input->post("Brand"),
						T_TRANSINOUT_Model => empty($this->input->post("Model"))? " ":$this->input->post("Model"),
						T_TRANSINOUT_Color => empty($this->input->post("Color"))? " ":$this->input->post("Color"),
						T_TRANSINOUT_Status => empty($this->input->post("Status"))? " ":$this->input->post("Status"),
						T_TRANSINOUT_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
						T_TRANSINOUT_RFID => empty($this->input->post("RFID"))? " ":$this->input->post("RFID"),
						T_TRANSINOUT_CostPrice => empty($this->input->post("CostPrice"))? 0:$this->input->post("CostPrice"),
						T_TRANSINOUT_Payment => empty($this->input->post("payment"))? " " : $this->input->post("payment"),
						T_TRANSINOUT_Vendor => empty($this->input->post("Vendor"))? " " : $this->input->post("Vendor"),
						T_TRANSINOUT_GST => empty($this->input->post("GST"))? " " : $this->input->post("GST"),
						T_TRANSINOUT_SellingPrice => empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice"),
						T_TRANSINOUT_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks")
					);
					$this->db->where(T_TRANSINOUT_RecordID,$this->input->post("RecordID"));
					$UpdateData = $this->db->update(T_TRANSINOUT,$data);

					if (!$UpdateData)
					{
						throw new Exception('Update failed');
					}  
					$id = $this->db->insert_id();

					$data2 = array(
						T_TRANSINOUTDETAIL_UpdatedOn => date("Y-m-d g:i:s",now()),
						T_TRANSINOUTDETAIL_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_TRANSINOUTDETAIL_UpdatedAt  => $this->input->ip_address(),
						T_TRANSINOUTDETAIL_CostPrice => empty($this->input->post("CostPrice"))? 0:$this->input->post("CostPrice"),
						T_TRANSINOUTDETAIL_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
						T_TRANSINOUTDETAIL_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
						T_TRANSINOUTDETAIL_RFID => empty($this->input->post("RFID"))? " ":$this->input->post("RFID")
					);
					$this->db->where(T_TRANSINOUTDETAIL_RFID,$this->input->post("RFID"));
					$this->db->where(T_TRANSINOUTDETAIL_IMEI,$this->input->post("IMEI"));
					$this->db->Update(T_TRANSINOUTDETAIL,$data2);
					
					$this->db->trans_commit();
					$output = array('errorcode' => 0, 'msg' => 'success', 'id' => $id);
				}
				// End Edit Data
			}else{ 
				// Begin Insert Data
				//$status =1;
				//if($this->input->post("Type") == "Out")
				//{
				//	$status = 0;
				//}
				//$query = "SELECT t6010.t6010f003 as ItemID,t6010.t6010f004 as Brand,t6010.t6010f005 as Model,t6010.t6010f006 as Color,t6010.t6010f007 as Status,t6010.t6010f008 as IMEI,t6010.t6010f009 as RFID, t6010.t6010f010 as CostPrice FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f005 = ".$status." WHERE t6010.t6010f008 = '".$this->input->post("IMEI")."' ";				
				//$sql =$this->db->query($query);
				//if($sql->num_rows()){
				//	throw new Exception('Duplicate IMEI');
				//}
				//$query = "SELECT t6010.t6010f003 as ItemID,t6010.t6010f004 as Brand,t6010.t6010f005 as Model,t6010.t6010f006 as Color,t6010.t6010f007 as Status,t6010.t6010f008 as IMEI,t6010.t6010f009 as RFID, t6010.t6010f010 as CostPrice FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f005 = ".$status." WHERE t6010.t6010f009 = '".$this->input->post("RFID")."' ";
				//$sql =$this->db->query($query);
				//if($sql->num_rows()){
				//	throw new Exception('Duplicate RFID');
				//}
				
				if($this->input->post("Type") == "IN") {
					$sql = "SELECT t6010r001 FROM t6010 LEFT JOIN t6011 ON (t6010f008 = t6011f001 AND t6011.t6011f002 = t6010.t6010f009) WHERE (t6010f008 = '" . $this->input->post("IMEI") . "') AND (t6011f005 = 1)";	// update 20180210			
					$result =$this->db->query($sql);
					if($result->num_rows()){
						throw new Exception('This IMEI already in!');					
					}

					$sql = "SELECT t6010r001 FROM t6010 LEFT JOIN t6011 ON (t6010f008 = t6011f001 AND t6011.t6011f002 = t6010.t6010f009) WHERE (t6010f009 = '" . $this->input->post("RFID") . "') AND (t6011f005 = 1)";	// update 20180210
					$result =$this->db->query($sql);
					if($result->num_rows()){
						throw new Exception('This RFID already assigned!');					
					}

					$date = strtotime($this->input->post("DocDate"));
					$data = array(
						T_TRANSINOUT_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_TRANSINOUT_RecordStatus => 0,
						T_TRANSINOUT_Type => empty($this->input->post("Type"))? " ":$this->input->post("Type"),
						T_TRANSINOUT_Date => empty($this->input->post("DocDate"))? " ": date("Y-m-d",$date) ,
						T_TRANSINOUT_ItemID => empty($this->input->post("ItemID"))? " ":$this->input->post("ItemID"),
						T_TRANSINOUT_Brand => empty($this->input->post("Brand"))? " ":$this->input->post("Brand"),
						T_TRANSINOUT_Model => empty($this->input->post("Model"))? " ":$this->input->post("Model"),
						T_TRANSINOUT_Color => empty($this->input->post("Color"))? " ":$this->input->post("Color"),
						T_TRANSINOUT_Status => empty($this->input->post("Status"))? " ":$this->input->post("Status"),
						T_TRANSINOUT_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
						T_TRANSINOUT_RFID => empty($this->input->post("RFID"))? " ":$this->input->post("RFID"),
						T_TRANSINOUT_CostPrice => empty($this->input->post("CostPrice"))? 0:$this->input->post("CostPrice"),
						T_TRANSINOUT_Payment => empty($this->input->post("payment"))? " " : $this->input->post("payment"),
						T_TRANSINOUT_Vendor => empty($this->input->post("Vendor"))? " " : $this->input->post("Vendor"),
						T_TRANSINOUT_GST => empty($this->input->post("GST"))? " " : $this->input->post("GST"),
						T_TRANSINOUT_SellingPrice => empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice"),
						T_TRANSINOUT_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks")
					);

				} else {
					$sql = "SELECT t6010f008 FROM t6010 LEFT JOIN t6011 ON (t6010f008 = t6011f001 AND t6011.t6011f002 = t6010.t6010f009) WHERE (t6010f008 = '" . $this->input->post("IMEI") . "') AND (t6011f005 = 0)";	// update 20180210
					$result =$this->db->query($sql);
					if($result->num_rows()){
						throw new Exception('This IMEI already out!');					
					}

					// = POS region
					$doctype = "SLPS";
					$SellingPrice = empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice");
					$sql1 = $this->db->get(T_SystemGST);
					$GST = $sql1->first_row()->{T_SystemGST_Value};
					if($this->input->post("Status") == "NEW" )
					{
						$AmountVAT = $GST;
						$GSTAmount = $SellingPrice * $GST;
						$AmountTotal = $SellingPrice + $GSTAmount;
					}else{
						$AmountVAT = 0;
						$AmountTotal = $SellingPrice;
					}
					$data = array(
						T_SalesInvoiceHeader_DocTypeID => $doctype,
						T_SalesInvoiceHeader_DocNo => substr(DocNo($doctype), 2),
						T_SalesInvoiceHeader_DocDate => convert_datetime(empty($this->input->post("DocDate"))? date("Y-m-d g:i:s",now()):$this->input->post("DocDate")),
						T_SalesInvoiceHeader_DocStatus => empty($this->input->post("DocStatus"))? 0:$this->input->post("DocStatus"),
						T_SalesInvoiceHeader_TermofPayment => empty($this->input->post("TermofPayment"))? "":$this->input->post("TermofPayment"),
						T_SalesInvoiceHeader_CurrencyID => empty($this->input->post("CurrencyID"))? "": $this->input->post("CurrencyID"),
						T_SalesInvoiceHeader_ExchangeRate => empty($this->input->post("ExchRate"))? 0: $this->input->post("ExchRate"),
						T_SalesInvoiceHeader_SalespersonID => empty($this->input->post("SalespersonID"))? "": $this->input->post("SalespersonID"),
						T_SalesInvoiceHeader_BizPartnerID => empty($this->input->post("BizPartnerID"))? "": $this->input->post("BizPartnerID"),
						T_SalesInvoiceHeader_AmountSubtotal => $SellingPrice,
						T_SalesInvoiceHeader_AmountVAT => $AmountVAT,
						T_SalesInvoiceHeader_AmountTotal => $AmountTotal,
						T_SalesInvoiceHeader_Payment => 0,
						T_SalesInvoiceHeader_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
						T_SalesInvoiceHeader_IC => empty($this->input->post("IC"))? ' ':$this->input->post("IC"),
						T_SalesInvoiceHeader_CustomerName => empty($this->input->post("CustomerName"))? ' ':$this->input->post("CustomerName"),
						T_SalesInvoiceHeader_Address => empty($this->input->post("Address"))? ' ':$this->input->post("Address"),
						T_SalesInvoiceHeader_Phone => empty($this->input->post("Phone"))? ' ':$this->input->post("Phone"),
						T_SalesInvoiceHeader_Email => empty($this->input->post("Email"))? ' ':$this->input->post("Email"),
						T_SalesInvoiceHeader_Outstanding => 0,
						T_SalesInvoiceHeader_PaymentMode => empty($this->input->post("payment"))? ' ':$this->input->post("payment")
					);
					$this->db->insert(T_SalesInvoiceHeader,$data);
					$PRI = $this->db->insert_id();

					$val = array(
						T_SalesInvoiceDetail_PRI => !isset($PRI)? 0:$PRI,
						T_SalesInvoiceDetail_RowIndex => 1,
						T_SalesInvoiceDetail_ItemID => empty($this->input->post("ItemID")) ? " ":$this->input->post("ItemID"),
						T_SalesInvoiceDetail_ItemName => !isset($item['ItemName'])? ' ':$item['ItemName'],
						T_SalesInvoiceDetail_AutoIDType => !isset($item['AutoIDType'])? ' ':$item['AutoIDType'],
						T_SalesInvoiceDetail_EPC => empty($this->input->post("RFID"))? " ":$this->input->post("RFID"),
						T_SalesInvoiceDetail_Quantity => 1,
						T_SalesInvoiceDetail_UoMID => "PCS",
						T_SalesInvoiceDetail_UnitPrice => empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice"),
						T_SalesInvoiceDetail_LineTotal => empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice"),
						T_SalesInvoiceDetail_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
						T_SalesInvoiceDetail_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
						T_SalesInvoiceDetail_Brand => empty($this->input->post("Brand"))? " ":$this->input->post("Brand"),
						T_SalesInvoiceDetail_Model => empty($this->input->post("Model"))? " ":$this->input->post("Model"),
						T_SalesInvoiceDetail_Color => empty($this->input->post("Color"))? " ":$this->input->post("Color"),
						T_SalesInvoiceDetail_Status => empty($this->input->post("Status"))? " ":$this->input->post("Status"),
						T_SalesInvoiceDetail_ItemGroup => !isset($item["ItemGroup"])? ' ':$item["ItemGroup"],
						T_SalesInvoiceDetail_LocationID => !isset($item["LocationID"])? ' ':$item["LocationID"],
						T_SalesInvoiceDetail_LocationName => !isset($item["LocationName"])? ' ':$item["LocationName"],
					);
					$this->db->insert(T_SalesInvoiceDetail, $val);

					UpdateDocNo($doctype);
					// = POS endregion

					$date = strtotime($this->input->post("DocDate"));
					$data = array(
						T_TRANSINOUT_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_TRANSINOUT_RecordStatus => 0,
						T_TRANSINOUT_Type => empty($this->input->post("Type"))? " ":$this->input->post("Type"),
						T_TRANSINOUT_Date => empty($this->input->post("DocDate"))? " ": date("Y-m-d",$date) ,
						T_TRANSINOUT_ItemID => empty($this->input->post("ItemID"))? " ":$this->input->post("ItemID"),
						T_TRANSINOUT_Brand => empty($this->input->post("Brand"))? " ":$this->input->post("Brand"),
						T_TRANSINOUT_Model => empty($this->input->post("Model"))? " ":$this->input->post("Model"),
						T_TRANSINOUT_Color => empty($this->input->post("Color"))? " ":$this->input->post("Color"),
						T_TRANSINOUT_Status => empty($this->input->post("Status"))? " ":$this->input->post("Status"),
						T_TRANSINOUT_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
						T_TRANSINOUT_RFID => empty($this->input->post("RFID"))? " ":$this->input->post("RFID"),
						T_TRANSINOUT_CostPrice => empty($this->input->post("CostPrice"))? 0:$this->input->post("CostPrice"),
						T_TRANSINOUT_PaymentOut => empty($this->input->post("payment"))? " " : $this->input->post("payment"),
						T_TRANSINOUT_Vendor => empty($this->input->post("Vendor"))? " " : $this->input->post("Vendor"),
						T_TRANSINOUT_GSTOut => empty($this->input->post("GST"))? " " : $this->input->post("GST"),
						T_TRANSINOUT_SellingPrice => empty($this->input->post("SellingPrice"))? 0 : $this->input->post("SellingPrice"),
						T_TRANSINOUT_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks")
					);
				}

				$InsertData = $this->db->insert(T_TRANSINOUT,$data);
				if (!$InsertData)
				{
					throw new Exception('Duplicate Data');
				}  
				$id = $this->db->insert_id();

				if($this->input->post("Type") == "Out")
				{
					$data = array(
						T_TRANSINOUTDETAIL_Flag => 0
					);
					$this->db->where(T_TRANSINOUTDETAIL_RFID,$this->input->post("RFID"));
					$this->db->where(T_TRANSINOUTDETAIL_IMEI,$this->input->post("IMEI"));
					$this->db->where(T_TRANSINOUTDETAIL_Flag,1);
					$this->db->Update(T_TRANSINOUTDETAIL,$data);
				}else{
					$sql = "SELECT t6011f001 FROM t6011 WHERE (t6011f001 = '" . $this->input->post("IMEI") . "')";
					$result =$this->db->query($sql);
					if($result->num_rows()){
						$data = array(
							T_TRANSINOUTDETAIL_Flag => 1,
							T_TRANSINOUTDETAIL_RFID => $this->input->post("RFID")
						);
						$this->db->where(T_TRANSINOUTDETAIL_IMEI,$this->input->post("IMEI"));
						$this->db->Update(T_TRANSINOUTDETAIL,$data);
					} else {
						$data = array(
							T_TRANSINOUTDETAIL_RecordTimestamp => date("Y-m-d g:i:s",now()),
							T_TRANSINOUTDETAIL_RecordStatus => 0,
							T_TRANSINOUTDETAIL_IMEI => empty($this->input->post("IMEI"))? " ":$this->input->post("IMEI"),
							T_TRANSINOUTDETAIL_RFID => empty($this->input->post("RFID"))? " ":$this->input->post("RFID"),
							T_TRANSINOUTDETAIL_CostPrice => empty($this->input->post("CostPrice"))? 0:$this->input->post("CostPrice"),
							T_TRANSINOUTDETAIL_Remarks => empty($this->input->post("Remarks"))? ' ':$this->input->post("Remarks"),
							T_TRANSINOUTDETAIL_Flag => 1,
							T_TRANSINOUTDETAIL_ItemID => empty($this->input->post("ItemID"))? " ":$this->input->post("ItemID")												
						);						
					
					$this->db->insert(T_TRANSINOUTDETAIL,$data);
					}
				}
				// End Insert Data
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success', 'id' => $id, 'PRI' => $PRI);
			}
			
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function RemoveItem()
	{
		$this->db->trans_begin();
		try{
			$RecordID = $this->input->post("RecordID");

			$query = "SELECT t6010.t6010r001 AS RecordID, t6010.t6010r002 AS RecordTimeStamp, t6010.t6010f001 AS Type, t6010.t6010f002 AS Date, t6010.t6010f003 AS ItemID, t6010.t6010f004 AS Brand, t6010.t6010f005 AS Model, t6010.t6010f006 AS Color, t6010.t6010f007 AS Status, t6010.t6010f008 AS IMEI, t6010.t6010f009 AS RFID, t6010.t6010f010 AS CostPrice, t6011.t6011f005 AS Flag, t6010.t6010f011 AS Remarks FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f001 = t6010.t6010f008 WHERE t6010.t6010r001 = ".$RecordID." ";
			$sql =$this->db->query($query);
			$voData = $sql->first_row();

			if($voData->Type == "IN"){
				$query1 = "SELECT t6010r001 FROM t6010 LEFT JOIN t6011 ON (t6010.t6010f008 = t6011.t6011f001) WHERE (t6010r001 = ".$RecordID.") AND (t6011f005 = 1)";
				$sql1 = $this->db->query($query1);
				if($sql1->num_rows() > 0){
					//$query = "DELETE t6010 FROM t6010 LEFT JOIN t6011 ON (t6010.t6010f008 = t6011.t6011f001) WHERE (t6010r001 = ".$RecordID.") AND (t6011f005 = 1)";
					// $query = "DELETE FROM t6010 WHERE (t6010r001 = $recordid)";
					// $this->db->query($query);

					//t6011
					$this->db->where(T_TRANSINOUTDETAIL_IMEI,$voData->IMEI);
					//$this->db->where(T_TRANSINOUTDETAIL_RFID,$voData->RFID);
					//$this->db->where(T_TRANSINOUTDETAIL_Flag,1);
					$this->db->delete(T_TRANSINOUTDETAIL);

				}else{
					throw new Exception('Stock-Out transaction exist. Delete the transaction first!');
				}
				
			}else{
				$data = array(
					T_TRANSINOUTDETAIL_Flag => 1
				);
				//$this->db->where(T_TRANSINOUTDETAIL_RFID,$voData->RFID);
				$this->db->where(T_TRANSINOUTDETAIL_IMEI,$voData->IMEI);
				$this->db->where(T_TRANSINOUTDETAIL_Flag,0);
				$this->db->Update(T_TRANSINOUTDETAIL,$data);
			}

			$this->db->where(T_TRANSINOUT_RecordID,$RecordID);
			$this->db->delete(T_TRANSINOUT);

			$this->db->trans_commit();
			$output = array('errorcode' => 0, 'msg' => 'success');

		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TRANSINOUT;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');

		$Customfilter = $this->input->get('customfilter');
		$TransDate = $this->input->get('TransDate');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}

		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');

			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}

		$getList = $this->Stockin_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter, $TransDate);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			//$info->count = $this->Stockin_model->getListCount($table);
			$info->count = $this->Stockin_model->getListCount($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter, $TransDate);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function GetDataIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$epc = $this->input->get('epc');
		$type = $this->input->get('type');
		$query = "SELECT t6010.t6010f001 as Type, t6010.t6010f003 as ItemID,t6010.t6010f004 as Brand,t6010.t6010f005 as Model,t6010.t6010f006 as Color,t6010.t6010f007 as Status,t6010.t6010f008 as IMEI,t6010.t6010f009 as RFID, t6010.t6010f010 as CostPrice, t6010.t6010f012 as Payment, t6010.t6010f013 as Vendor, t6010.t6010f014 as GST  FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009 AND t6011.t6011f005 = 1 WHERE t6010.t6010f009 = '".$epc."' OR t6010.t6010f008 = '".$epc."' ";
		$sql =$this->db->query($query);
		if($sql->num_rows()){
			$info->data = $sql->first_row();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found with ".$type." (".$epc.") ";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
	public function GetDataIn2($psValue, $psType)
	{
		$result = true;

		$epc = $psValue;
		$type = $psType;
		$query = "SELECT t6010.t6010f003 as ItemID,t6010.t6010f004 as Brand,t6010.t6010f005 as Model,t6010.t6010f006 as Color,t6010.t6010f007 as Status,t6010.t6010f008 as IMEI,t6010.t6010f009 as RFID, t6010.t6010f010 as CostPrice FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f002 = t6010.t6010f009 AND t6011.t6011f005 = 1 WHERE t6010.t6010f009 = '".$epc."' OR t6010.t6010f008 = '".$epc."' ";
		$sql =$this->db->query($query);
		if($sql->num_rows()){
			$result = true;
		}else{
			$result = false;
		}
		return $result;
	}


	public function GetDataEdit()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$id = $this->input->get('RecordID');
		$query = "SELECT t6010.t6010r001 AS RecordID, t6010.t6010r002 AS RecordTimeStamp, t6010.t6010f001 AS Type, t6010.t6010f002 AS Date, t6010.t6010f003 AS ItemID, t6010.t6010f004 AS Brand, t6010.t6010f005 AS Model, t6010.t6010f006 AS Color, t6010.t6010f007 AS Status, t6010.t6010f008 AS IMEI, t6010.t6010f009 AS RFID, t6010.t6010f010 AS CostPrice, t6011.t6011f005 AS Flag, t6010.t6010f011 AS Remarks FROM t6010 JOIN t6011 ON t6011.t6011f001 = t6010.t6010f008 AND t6011.t6011f001 = t6010.t6010f008 WHERE t6010.t6010r001 = ".$id." ";
		$sql =$this->db->query($query);
		if($sql->num_rows()){
			$info->data = $sql->first_row();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data not found";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file Stockin.php */
/* Location: ./app/modules/Stockmovement/controllers/Stockin.php */

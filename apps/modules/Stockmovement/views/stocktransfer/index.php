<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_TransactionStockMovementHeader_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_TransactionStockMovementHeader_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_TransactionStockMovementHeader_RecordStatus  => array(0,1,'50px','RS',1,'string',1),
    T_TransactionStockMovementHeader_DocTypeID  => array(1,1,'100px','Doc Type',1,'string',1),
    T_TransactionStockMovementHeader_DocNo  => array(1,1,'100px','Doc No',0,'string',0),
    T_TransactionStockMovementHeader_DocDate  => array(1,1,'100px','Doc Date',0,'datetime',0),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_TransactionStockMovementHeader,
    'tools' => array(T_TransactionStockMovementHeader_RecordID,T_TransactionStockMovementHeader_RecordTimestamp,T_TransactionStockMovementHeader_RecordStatus,T_TransactionStockMovementHeader_DocTypeID,T_TransactionStockMovementHeader_DocNo,T_TransactionStockMovementHeader_DocDate),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Stockmovement/Stocktransfer/insert',
        'read' => 'Stockmovement/Stocktransfer/getlist',
        'update' => 'Stockmovement/Stocktransfer/update',
        'destroy' => 'Stockmovement/Stocktransfer/delete',
        'form' => 'Stockmovement/Stocktransfer/form',
        'post' => 'Stockmovement/Stocktransfer/post',
        'unpost' => 'Stockmovement/Stocktransfer/unPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>
<div id="grid"></div>
<script>
$(document).ready(function() {
    var gridIn = $("#grid").kendoGrid({
        //toolbar: ["excel"],
        toolbar: kendo.template($("#templateIn").html()),
        excel: {
            fileName: kendo.toString(new Date, "dd/MM/yyyy HH:mm") +"_Expenses.xlsx",
            filterable: true
        },
        height: "580px",
        width: "100%",
        columns: [
        { 
            "width": "100px",
            "template":
            ''
            +'<a class="k-button k-button-icontext" href="'+site_url('Stockmovement/Stocktake/formDetail/#=t1010r001#')+'"><span class="k-icon k-i-hbars"></span></a>'
            +'<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)" onclick="RemoveData(#=t1010r001#)"><span class="k-icon k-delete"></span></a>'
        },
        {
            "title":"Doc No",
            "width":"90px",
            "field":"<?php echo T_TransactionStockMovementHeader_DocNo; ?>",
        },
        {
            "title":"Doc Date",
            "width":"150px",
            "field":"<?php echo T_TransactionStockMovementHeader_DocDate; ?>",
            "template": '#= kendo.toString(<?php echo T_TransactionStockMovementHeader_DocDate; ?>,"dd-MM-yyyy") #'
        },
        {
            "title":"Remarks",
            "width":"200px",
            "field":"<?php echo T_TransactionStockMovementHeader_Remarks; ?>",
        }
        ],
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    data: {
                        table: "<?php echo T_TransactionStockMovementHeader; ?>",
                    },
                    url: site_url('Stockmovement/Stocktake/getlist'),
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.count;
                },
                model: {
                    id: "<?php echo T_Expenses_RecordID; ?>",
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });

    $("#monthpicker").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM",
        dateInput: false,
        change: onChange,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;
            if (calendar.view().name === "year") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "year") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    $("#yearspicker").kendoDatePicker({
        start: "decade",
        depth: "decade",
        format: "yyyy",
        dateInput: false,
        change: onChange,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;

            if (calendar.view().name === "decade") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "decade") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });
});

function onChange() {
    var vomonth = $("#monthpicker").data("kendoDatePicker").value();
    var voyear = $("#yearspicker").data("kendoDatePicker").value();
    var month = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'MM' );
    var defYear = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'yyyy' );
    var year = kendo.toString(kendo.parseDate(voyear,'yyyy-MM-dd'), 'yyyy' );
    var TransDate = (month == null) ? year : year+'-'+month;
    TransDate = (year == null) ? defYear+'-'+month : TransDate;
    TransDate = (year == null && month == null) ? null : TransDate;
    $("#grid").data("kendoGrid").dataSource.page(1);
    $("#grid").data("kendoGrid").dataSource.transport.options.read.data = {table: "<?php echo T_TransactionStockMovementHeader; ?>", TransDate: TransDate};
    $("#grid").data("kendoGrid").dataSource.transport.options.read.url = site_url("Stockmovement/Stocktake/getlist");
    $("#grid").data("kendoGrid").dataSource.read();
}

function RemoveData(id)
    {
        var result = confirm("Delete this record?");
        if (result) {
            if(id){
                var voData = {
                    RecordID: id
                };
                $.ajax({
                    type: 'POST',
                    data: voData,
                    url:  site_url('Stockmovement/Stocktake/Delete'),
                    beforeSend: function(){
                    },
                    success: function (result) {
                    if (result.errorcode != 0) {
                        new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                    } else {
                        new PNotify({ title: "Success", text: "Remove data", type: 'success', shadow: true });
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();
                    }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
            }
        }
    }
</script>
<script type="text/x-kendo-template" id="templateIn">
    <div style="display: inline-block;">
    </div>
    <div style="float: right;">
        <label class="category-label" for="category">Show by :</label>
        <input id="monthpicker" style="width: 150px"/>
        <input id="yearspicker" style="width: 150px"/>
    </div>
</script>
<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Detail</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordID}) ? ${T_TransactionStockMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordTimestamp}) ? ${T_TransactionStockMovementHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6" style="display:none;">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-input k-textbox', 'value' => 'IVST', 'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="display:none;">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type Name</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocName', 'class' => 'k-input k-textbox', 'value' => 'Stock Take', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc No</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-input k-textbox', 'value' => isset(${T_TransactionStockMovementHeader_DocNo}) ? ${T_TransactionStockMovementHeader_DocNo} : substr(DocNo('IVST'), 2), 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;margin-bottom: 30px;">
                            <label class="col-md-3 form-label">Doc Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker','readonly' => 'TRUE', 'value' => isset(${T_TransactionStockMovementHeader_DocDate}) ? ${T_TransactionStockMovementHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row col-md-3" style="margin-top: 10px; margin-left: 5px; cursor: pointer;" onclick="filterData(0);">
                            <section class="panel">
                                <div class="panel-body bg-success">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title" style="text-align: center;">ON-HAND</h4>
                                                <div class="info" style="text-align: center;">
                                                    <strong class="amount" id="onhandqty"><?php echo $onhand; ?></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row col-md-3" style="margin-top: 10px; margin-left: 5px; cursor: pointer;" onclick="filterData(1);">
                            <section class="panel">
                                <div class="panel-body bg-warning">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title" style="text-align: center;">SCAN</h4>
                                                <div class="info" style="text-align: center;">
                                                    <strong class="amount" id="scanqty"><?php echo $scan; ?></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="row col-md-3" style="margin-top: 10px; margin-left: 5px; cursor: pointer;" onclick="filterData(2);">
                            <section class="panel">
                                <div class="panel-body bg-info">
                                    <div class="widget-summary">
                                        <div class="widget-summary-col">
                                            <div class="summary">
                                                <h4 class="title" style="text-align: center;">OUTSTANDING</h4>
                                                <div class="info" style="text-align: center;">
                                                    <strong class="amount" id="outstandingqty"><?php echo $scan - $onhand; ?></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Brand</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Brand', 'class' => 'k-input k-textbox', 'value' => (${T_TransactionStockMovementHeader_Brand} != ' ') ? ${T_TransactionStockMovementHeader_Brand} : "All", 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <label class="col-md-3 form-label">Model</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Model', 'class' => 'k-input k-textbox', 'value' => (${T_TransactionStockMovementHeader_Model} != ' ') ? ${T_TransactionStockMovementHeader_Model} : "All", 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <label class="col-md-3 form-label">Color</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Color', 'class' => 'k-input k-textbox', 'value' => (${T_TransactionStockMovementHeader_Color} != ' ') ? ${T_TransactionStockMovementHeader_Color} : "All", 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <label class="col-md-3 form-label">Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Status', 'class' => 'k-input k-textbox', 'value' => (${T_TransactionStockMovementHeader_Status} != ' ') ? ${T_TransactionStockMovementHeader_Status} : "All", 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-textbox" id="Remark" style="width: 300px; margin-top: 0px; margin-bottom: 0px;height:80px;" readonly="readonly"><?php echo (isset(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6" style="display:none;">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocStatus', 'value' => isset(${T_TransactionStockMovementHeader_DocStatus}) ? ${T_TransactionStockMovementHeader_DocStatus} : '0', 'readonly' => true, 'style' => 'margin-top:-9px;' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="form-title" style="text-align: center;padding: 10px; color: white; background-color: #a5201e; width: 150px; margin-left: 1px; border-radius: 6px 6px 0px 0px;">ON-HAND</div>
                <div id="gridSearch"></div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
function filterData(id){
    if(id==0){
        $("#form-title").html("ON-HAND");
    }else if(id==1){
        $("#form-title").html("SCAN");
    }else{
        $("#form-title").html("OUTSTANDING");
    }
    $("#gridSearch").data("kendoGrid").dataSource.page(1);
    $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.data = {type: id};
    $("#gridSearch").data("kendoGrid").dataSource.transport.options.read.url = site_url('Stockmovement/Stocktake/GetListDetail')+"?PRI=<?php echo ${T_TransactionStockMovementHeader_RecordID}; ?>";
    $("#gridSearch").data("kendoGrid").dataSource.read();
}
$(document).ready(function() {
    $(".KendoDatePicker").kendoDatePicker({format:DateFormat});

    $("#gridSearch").kendoGrid({
            toolbar: ["excel"],
            excel: {
                fileName: "stocktake.xlsx",
                filterable: true
            },            
            height: "500px",
            width: "100%",
            columns: [
            {
            "title":"Item ID",
            "width":"100px",
            "field":"<?php echo T_TransactionStockMovementDetail_ItemID; ?>",
            },
            {
            "title":"Brand",
            "width":"150px",
            "field":"<?php echo T_TransactionStockMovementDetail_Brand; ?>",
            },
            {
            "title":"Model",
            "width":"150px",
            "field":"<?php echo T_TransactionStockMovementDetail_Model; ?>",
            },
            {
            "title":"Color",
            "width":"100px",
            "field":"<?php echo T_TransactionStockMovementDetail_Color; ?>",
            },
            {
            "title":"Condition",
            "width":"100px",
            "field":"<?php echo T_TransactionStockMovementDetail_Status; ?>",
            },
            {
            "title":"IMEI",
            "width":"200px",
            "field":"<?php echo T_TransactionStockMovementDetail_IMEI; ?>",
            },
            {
            "title":"RFID",
            "width":"200px",
            "field":"<?php echo T_TransactionStockMovementDetail_EPC; ?>",
            },
            {
            "title":"Status",
            "width":"200px",
            "template": "#if(<?php echo T_TransactionStockMovementDetail_Quantity2; ?>==0){#  Missing #}else{# Scanned #}#"
            } 
            ],                     
            dataSource: {
                transport: {
                    read: {
                        type:"GET",
                        data: { table: ''},
                        url: function() {
                            return site_url('Stockmovement/Stocktake/GetListDetail')+"?PRI=<?php echo ${T_TransactionStockMovementHeader_RecordID}; ?>";
                        },
                        dataType: "json"
                    }
                },
                sync: function(e) {
                    $('#gridSearch').data('kendoGrid').dataSource.read();
                    $('#gridSearch').data('kendoGrid').refresh();
                },
                schema: {
                    data: function(data){
                        return data.data;
                    },
                    total: function(data){
                        return data.count;
                    },
                    model: {
                        id: "t1011r001",
                    }
                },
                pageSize: 30,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            autoBind:true,     
            sortable: true,
            resizable: true,
            selectable: true,
            scrollable: true,
            reorderable:true,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
        });
});
</script>
<style type="text/css">.mrg{margin-bottom:5px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title" id="form-title">Stock In</h2>
            </header>
            <div id="form-panel" class="panel-body" data-loading-overlay>
                <input type="hidden" id="RecordID" value="">
                <input type="hidden" id="TimeStamp" value="">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Type</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Type', 'class' => 'k-input', 'value' => '', "style" => "width: 100px;" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Status', 'class' => 'k-input', 'value' => '', "style" => "width: 100px;", "disabled" => "disabled" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">IMEI</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'IMEI', 'class' => 'k-input k-textbox', 'value' => '', "style" => "width: 250px;", "onblur" => "LookupData(this.value);" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Item ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'ItemID', 'class' => 'k-input k-textbox', 'value' => '', "style" => "width: 250px;", "disabled" => "disabled", "readonly" => "readonly" ); echo form_input($items); ?>
                                <div class="k-button" id="LookupEventItemID">
                                    <span class="k-icon k-i-search"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="row">
                            <label class="col-md-3 form-label">RFID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'RFID', 'class' => 'k-input k-textbox', 'value' => '', "style" => "width: 250px;", "onblur" => "fLookupByRFID(this.value);" ); echo form_input($items); ?>
                            <div class="k-button" id="scanTagButton" onclick="scanTag();">
                                <span class="fa fa-rss fa-lg"></span>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Brand</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Brand', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;", "disabled" => "disabled" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Cost Price</label>
                            <div class="col-md-9">
                                <input type="hidden" id="CostReal" />
                                <?php $items=array( 'id'=> 'CostPrice', 'class' => 'k-input k-textbox', 'value' => '', "style" => "width: 250px;" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Model</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Model', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;", "disabled" => "disabled" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Selling Price</label>
                            <div class="col-md-9">
                                <input type="hidden" id="SellingReal" />
                                <?php $items=array( 'id'=> 'SellingPrice', 'class' => 'k-input k-textbox', 'value' => '', "style" => "width: 250px;", 'readonly' => 'true' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Color</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Color', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;", "disabled" => "disabled" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">GST</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'GST', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Vendor</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'Vendor', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Payment</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'payment', 'class' => 'k-input', 'value' => '', "style" => "width: 250px;" ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-textbox" id="Remarks" style="width: 300px; margin-top: 0px; margin-bottom: 0px;height:70px !important;"><?php echo (isset(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            <footer class="panel-footer" style="margin-top:10px;">
                <button id="submitButtondetail" class="btn btn-primary" type="submit" onclick="InsertData(); return false;">Save</button>
                <button class="btn btn-default" type="button" onclick="ClearPage(); return false;">Clear</button>
            </footer>
        </section>
    </div>
</div>
<div id="gridStockAll" style="margin-top:10px;"></div>
<div id="gridStockIn" style="margin-top:10px;" style="display:none;"></div>
<div id="gridStockOut" style="margin-top:10px;" style="display:none;"></div>
<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailItemForm">
    <div class="col-md-5">

        <div class="form-group mrg" style="display:none">
            <div class="col-md-6">
                <!--  -->
            </div>
            <div class="col-md-6">
                <div class="row" >
                    <label class="col-md-3 form-label">Auto ID Type</label>
                    <div class="col-md-4">
                        <?php $items=array( 
                            'id'=> 'AutoIDType2', 
                            'class' => 'k-input', 
                            'value' => isset(${T_MasterDataItem_AutoIDType}) ? ${T_MasterDataItem_AutoIDType} : "",  
                        ); 
                        echo form_input($items); ?>
                    </div>
                </div>
            </div>
        </div>

        <label class="k-edit-label">Item ID</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'ItemID2', 
                'class' => 'k-input k-textbox', 
                'style' => 'background-color:#eee; text-transform: uppercase;',
                'value' => isset(${T_MasterDataItem_ItemID}) ? ${T_MasterDataItem_ItemID} : substr(AutoItemID('ITEM'),5),
                'readonly' => TRUE
            ); 
            //if(!isset(${T_MasterDataItem_ItemID})){ unset($items['readonly']); }
            echo form_input($items); ?>
        </div>
        
        <label class="k-edit-label">Item Group</label>
        <div class="k-edit-field">
            <input id="ItemGroupID2" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataItem_GroupID}) ? ${T_MasterDataItem_GroupID} : ''; ?>" />
            <?php $items=array(
                'id'=> 'ItemGroupName2', 
                'class' => '', 
                'value' => isset(${T_MasterDataItemGroup_ID}) ? ${T_MasterDataItemGroup_ID} : "",
                'style' => 'background-color:#eee;', 
                'style' => ''
            ); 
            echo form_input($items); echo"&nbsp;"; ?>
        </div>

        <label class="k-edit-label">Brand</label>
        <div class="k-edit-field">
            <?php $items=array(
                'id'=> 'Brand2',
                'class' => '',
                'style' => '',
                'value' => isset(${T_MasterDataItem_Brand}) ? ${T_MasterDataItem_Brand} : "",
            ); 
            echo form_input($items); ?>
        </div>

        <label class="k-edit-label">Model</label>
        <div class="k-edit-field">
            <?php $items=array(
                'id'=> 'Model2',
                'class' => '',
                'style' => '',
                'value' => isset(${T_MasterDataItem_Model}) ? ${T_MasterDataItem_Model} : "",
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Color</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'Color2', 
                'class' => 'k-input', 
                'value' => isset(${T_MasterDataItem_Color}) ? ${T_MasterDataItem_Color} : "",
                'style' => '',
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Status</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'Status2', 
                'class'=>'k-input', 
                'value' => isset(${T_MasterDataItem_Status}) ? ${T_MasterDataItem_Status} : "",
                'style' => '',
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">UOM ID</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'UOMID2', 
                'class'=>'k-input', 
                'value' => isset(${T_MasterDataItem_UOMID}) ? ${T_MasterDataItem_UOMID} : "",
                'style' => ''
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Cost Price</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id' => 'UnitPrice2', 
                'class' => 'k-input',
                'value' => isset(${T_MasterDataItem_UnitPrice}) ? ${T_MasterDataItem_UnitPrice} : "",
                'style' => ''
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
    </div>

    <div class="col-md-6">
        <label class="k-edit-label">Picture</label>
        <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataItem_Picture}) ? $meta{T_MasterDataItem_Picture} : ''; ?>">
        <?php
            if(isset(${T_MasterDataItem_Picture})){
                $voImg = (${T_MasterDataItem_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataItem_Picture};
            }else{
                $voImg = "assets/upload/photo/no-photo.png";
            }
        ?>
        <div class="col-md-6">
            <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                    <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                </div>
            </a>
            <form id="form-photo" action="<?php echo site_url('en/System/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                <input class="" style="width:170px;margin-top:3px;" id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
                <input name="ItemIDS" id="ItemIDS" value="" type="hidden"/> 
                <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
            </form>
            <div id="err"></div>
            <br><br><br>
        </div>

        <div class="row mrg" style="margin-top:15px;">
            <label class="k-edit-label">Remarks</label>
            <div class="k-edit-field">
                <textarea class="k-textbox" id="Remarks2" style="width: 300px;"><?php echo (isset(${T_MasterDataItem_Remarks}))? ${T_MasterDataItem_Remarks} : "" ?></textarea>
            </div>
        </div>
    </div>
    
    <div class="k-edit-buttons k-state-default">
        <button id="submitButtondetail2" class="btn btn-primary close-button" onclick="insertMasterItem();"><i class="el-icon-file-new"></i> Save</button>
        <button class="btn btn-default close-button" onclick="CloseModal('detailItemForm');"><i class="el-icon-remove"></i> Cancel</button>
    </div>
</div>
<!--  End Modal Form Detail -->
<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>
<script type="text/javascript">
var LSTable = 0 ;
function InsertData(){ 
    var voData = {
        RecordID: $("#RecordID").val(),
        TimeStamp: $("#TimeStamp").val(),
        Type: $('#Type').val(),
        DocDate: $('#DocDate').val(),
        ItemID: $('#ItemID').val(),
        Brand: $('#Brand').val(),
        Model: $('#Model').val(),
        Color: $('#Color').val(),
        Status: $('#Status').val(),
        IMEI: $('#IMEI').val(),
        RFID: $('#RFID').val(),
        CostPrice: $('#CostPrice').val(),
        SellingPrice: $('#SellingPrice').val(),
        payment: $('#payment').val(),
        Vendor: $('#Vendor').val(),
        GST: $('#GST').val(),
        Remarks: $('#Remarks').val()
    };
    //console.log(voData);
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('Stockmovement/Movement/insert'),
            beforeSend: function(){
                $('#submitButtondetail').attr('disabled', 'disabled');
                $('#submitButtondetail').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
            },
            success: function (result) {
            if (result.errorcode != 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
            } else {
                new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                if(voData.Type == "IN"){
                    $("#RecordID").val("");
                    $("#TimeStamp").val("");
                    $("#IMEI").val("");
                    $("#RFID").val("");
                    //$("#CostPrice").val("");
                    //$("#Remarks").val("");
                }else{
                    window.open(site_url('Pos/print_item/'+result.PRI));
                    ClearPage();
                }
                $('#gridStockAll').data('kendoGrid').dataSource.read();
                $('#gridStockAll').data('kendoGrid').refresh();
                $('#gridStockIn').data('kendoGrid').dataSource.read();
                $('#gridStockIn').data('kendoGrid').refresh();
                $('#gridStockOut').data('kendoGrid').dataSource.read();
                $('#gridStockOut').data('kendoGrid').refresh();
            }
            $('#submitButtondetail').prop("disabled", false);
            $('#submitButtondetail').html('Save');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var vomsg = jQuery.parseJSON(jqXHR.responseText);
                new PNotify({ title: "Failed", text: vomsg.msg[1], type: 'error', shadow: true });
                $('#submitButtondetail').prop("disabled", false);
                $('#submitButtondetail').html('Save');
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ItemID == "") { valid = 0; msg += "Item ID is required" + "\r\n"; }
    if (voData.Brand == "") { valid = 0; msg += "Brand is required" + "\r\n"; }
    if (voData.Model == "") { valid = 0; msg += "Model is required" + "\r\n"; }
    if (voData.Status == "") { valid = 0; msg += "Status is required" + "\r\n"; }
    if (voData.IMEI == "") { valid = 0; msg += "IMEI is required" + "\r\n"; }
    if (voData.RFID == "") { valid = 0; msg += "RFID is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
function ClearPage()
{
    $('#ItemID').val("");
    $('#Brand').data("kendoDropDownList").value("");
    $('#Model').data("kendoDropDownList").value("");
    $('#Status').data("kendoDropDownList").value("");
    $('#Color').data("kendoDropDownList").value("");
    $('#Vendor').data("kendoDropDownList").value("");
    $('#GST').data("kendoDropDownList").value("");
    $('#payment').data("kendoDropDownList").value("");
    $('#SellingPrice').val("");
    $('#IMEI').val("");
    $('#RFID').val("");
    $('#CostPrice').val("");
    $('#Remarks').val("");
    $('#submitButtondetail').html('Save');
}

function LookupData(IMEI)
{
    var Type = $('#Type').val();
    if(Type == "Out"){
        fillForm(IMEI,"IMEI");
    }
}
function fLookupByRFID(psRFID)
{
    var Type = $('#Type').val();
    if(Type == "Out"){
        if (psRFID != "") {
            var nLen = psRFID.length;
            if(nLen < 24){
                var sTemplate = '000000000000000000000000';
                var sPrefix = sTemplate.substr(nLen);
                psRFID = sPrefix.concat(psRFID);
                $("#RFID").val(psRFID);
            }
            fillForm(psRFID,"RFID");
        }
    }
}

function scanTag()
{
    var msg = {
        Title : "Web Triger",
        Text : "Start Scan"
    }
    var typeData = { broadType : "ReadTag", data : msg};
    conn.sendMsg(typeData);
}
function setToForm(epc)
{
    // if(epc == "Start Scan"){
    //     $("#EPC2").val("");
    // }else{
    //     $("#EPC2").val(epc);
    // }
}
function getdataScanSocket(epc)
{
    var Type = $('#Type').val();
    if(epc == "Start Scan"){
        $("#EPC2").val("");
        new PNotify({
            title: "Reader",
            text: "RFID Tag not found!",
            addclass: 'notification-primary',
            icon: 'fa fa-warning'
        });
    }else{
        new PNotify({
            title: "Reader",
            text: "Found :" + epc,
            addclass: 'notification-primary',
            icon: 'fa fa-info'
        });
        if(Type == "Out"){
            fillForm(epc,"RFID");
        }else{
            $("#RFID").val(epc);
        }
        
    }
}
function fillForm(epc,Type)
{
    if(epc){
        var voData = {
            epc: epc,
            type : Type
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Movement/GetDataIn'),
            beforeSend: function(){
                $("#form-panel").trigger('loading-overlay:show');
            },
            success: function (result) {
            if (result.errorcode != 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                
                $('#ItemID').val("");
                $('#Brand').data("kendoDropDownList").value("");
                $('#Model').data("kendoDropDownList").value("");
                $('#Status').data("kendoDropDownList").value("");
                $('#Color').data("kendoDropDownList").value("");
                $("#form-panel").trigger('loading-overlay:hide');
            } else {
                new PNotify({ title: "Success", text: "Item Found", type: 'success', shadow: true });
                
                $('#ItemID').val(result.data.ItemID);
                $('#Brand').data("kendoDropDownList").value(result.data.Brand);
                $('#Model').data("kendoDropDownList").value(result.data.Model);
                $('#Status').data("kendoDropDownList").value(result.data.Status);
                $('#Color').data("kendoDropDownList").value(result.data.Color);
                $('#IMEI').val(result.data.IMEI);
                $('#RFID').val(result.data.RFID);
                $('#CostPrice').val(result.data.CostPrice);
                //$('#payment').data("kendoDropDownList").value(result.data.Payment);
                $('#GST').data("kendoDropDownList").value(result.data.GST);
                $('#Vendor').data("kendoDropDownList").value(result.data.Vendor);
                $("#Vendor").data("kendoDropDownList").enable(false);
                $('#CostPrice').attr('readonly', true);
                $("#form-panel").trigger('loading-overlay:hide');
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
                $("#form-panel").trigger('loading-overlay:hide');
            }
        });
    }
}
function fillForm2(epc,Type)
{
    if(epc){
        var voData = {
            epc: epc,
            type : Type
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Movement/GetDataIn'),
            beforeSend: function(){
                //$("#form-panel").trigger('loading-overlay:show');
            },
            success: function (result) {
            if (result.errorcode != 0) {
                //new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                
                $('#ItemID').val("");
                $('#Brand').data("kendoDropDownList").value("");
                $('#Model').data("kendoDropDownList").value("");
                $('#Status').data("kendoDropDownList").value("");
                $('#Color').data("kendoDropDownList").value("");
                //$("#form-panel").trigger('loading-overlay:hide');
            } else {
                // new PNotify({ title: "Success", text: "Item Found", type: 'success', shadow: true });
                // $('#ItemID').val(result.data.ItemID);
                // $('#Brand').data("kendoDropDownList").value(result.data.Brand);
                // $('#Model').data("kendoDropDownList").value(result.data.Model);
                // $('#Status').data("kendoDropDownList").value(result.data.Status);
                // $('#Color').data("kendoDropDownList").value(result.data.Color);
                // $('#IMEI').val(result.data.IMEI);
                // $('#RFID').val(result.data.RFID);
                // $('#CostPrice').val(result.data.CostPrice);
                // //$('#Remarks').val(result.data.Remarks);
                //$("#form-panel").trigger('loading-overlay:hide');
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //alert(jQuery.parseJSON(jqXHR.responseText));
                //$("#form-panel").trigger('loading-overlay:hide');
            }
        });
    }
}


function onChange()
{
    var Type = $('#Type').val();
    if(Type == "Out")
    {
        document.getElementById('ItemID').readOnly = true;
        $("#Brand").data("kendoDropDownList").enable(false);
        $("#Model").data("kendoDropDownList").enable(false);
        $("#Color").data("kendoDropDownList").enable(false);
        $("#Status").data("kendoDropDownList").enable(false);
        document.getElementById('IMEI').readOnly = false;
        document.getElementById('RFID').readOnly = false;   //true
        $('#CostPrice').attr('readonly', true);
        $('#SellingPrice').attr('readonly', false);
        $("#Vendor").data("kendoDropDownList").enable(false);
        $("#LookupEventItemID").hide();
        $("#form-title").html("Stock Out");
    }else if(Type == "IN"){
        document.getElementById('ItemID').readOnly = true;
        $("#Brand").data("kendoDropDownList").enable(false);
        $("#Model").data("kendoDropDownList").enable(false);
        $("#Color").data("kendoDropDownList").enable(false);
        $("#Status").data("kendoDropDownList").enable(false);
        document.getElementById('IMEI').readOnly = false;
        document.getElementById('RFID').readOnly = false;
        $('#CostPrice').attr('readonly', false);
        $('#SellingPrice').attr('readonly', true);
        $("#Vendor").data("kendoDropDownList").enable(true);
        $("#LookupEventItemID").show();
        $("#form-title").html("Stock In");
    }
    ClearPage();
}
function onChangeType()
{
    var vomonth = $("#monthpickerAll").data("kendoDatePicker").value();
    var voyear = $("#yearspickerAll").data("kendoDatePicker").value();
    $("#monthpickerIn").data("kendoDatePicker").value(vomonth);
    $("#yearspickerIn").data("kendoDatePicker").value(voyear);
    $("#monthpickerOut").data("kendoDatePicker").value(vomonth);
    $("#yearspickerOut").data("kendoDatePicker").value(voyear);
    
    var Type = $('#TypeDoc').val();
    if(Type == "Out")
    {
        $("#gridStockAll").hide();
        $("#gridStockIn").hide();
        $("#gridStockOut").show();
        onChangeDateOut();
    }else if(Type == "IN"){
        $("#gridStockAll").hide();
        $("#gridStockIn").show();
        $("#gridStockOut").hide();
        onChangeDateIn();
    }else{
        $("#gridStockAll").show();
        $("#gridStockIn").hide();
        $("#gridStockOut").hide();
        onChangeDate();
        
    }
    $("#TypeDoc").data("kendoDropDownList").select(0);
    $("#TypeDocIn").data("kendoDropDownList").select(1);
    $("#TypeDocOut").data("kendoDropDownList").select(2);
}
function onChangeTypeIn()
{
    var vomonth = $("#monthpickerIn").data("kendoDatePicker").value();
    var voyear = $("#yearspickerIn").data("kendoDatePicker").value();
    $("#monthpickerAll").data("kendoDatePicker").value(vomonth);
    $("#yearspickerAll").data("kendoDatePicker").value(voyear);
    $("#monthpickerOut").data("kendoDatePicker").value(vomonth);
    $("#yearspickerOut").data("kendoDatePicker").value(voyear);

    var Type = $('#TypeDocIn').val();
    if(Type == "Out")
    {
        $("#gridStockAll").hide();
        $("#gridStockIn").hide();
        $("#gridStockOut").show();
        onChangeDateOut();
    }else if(Type == "IN"){
        $("#gridStockAll").hide();
        $("#gridStockIn").show();
        $("#gridStockOut").hide();
        onChangeDateIn();
    }else{
        $("#gridStockAll").show();
        $("#gridStockIn").hide();
        $("#gridStockOut").hide();
        onChangeDate();
    }
    $("#TypeDoc").data("kendoDropDownList").select(0);
    $("#TypeDocIn").data("kendoDropDownList").select(1);
    $("#TypeDocOut").data("kendoDropDownList").select(2);
}
function onChangeTypeOut()
{
    var vomonth = $("#monthpickerOut").data("kendoDatePicker").value();
    var voyear = $("#yearspickerOut").data("kendoDatePicker").value();
    $("#monthpickerAll").data("kendoDatePicker").value(vomonth);
    $("#yearspickerAll").data("kendoDatePicker").value(voyear);
    $("#monthpickerIn").data("kendoDatePicker").value(vomonth);
    $("#yearspickerIn").data("kendoDatePicker").value(voyear);

    var Type = $('#TypeDocOut').val();
    if(Type == "Out")
    {
        $("#gridStockAll").hide();
        $("#gridStockIn").hide();
        $("#gridStockOut").show();
        onChangeDateOut();
    }else if(Type == "IN"){
        $("#gridStockAll").hide();
        $("#gridStockIn").show();
        $("#gridStockOut").hide();
        onChangeDateIn();
    }else{
        $("#gridStockAll").show();
        $("#gridStockIn").hide();
        $("#gridStockOut").hide();
        onChangeDate();
    }
    $("#TypeDoc").data("kendoDropDownList").select(0);
    $("#TypeDocIn").data("kendoDropDownList").select(1);
    $("#TypeDocOut").data("kendoDropDownList").select(2);

}
function onChangeDate() {
    var vomonth = $("#monthpickerAll").data("kendoDatePicker").value();
    var voyear = $("#yearspickerAll").data("kendoDatePicker").value();
    var month = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'MM' );
    var defYear = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'yyyy' );
    var year = kendo.toString(kendo.parseDate(voyear,'yyyy-MM-dd'), 'yyyy' );
    var TransDate = (month == null) ? year : year+'-'+month;
    TransDate = (year == null) ? defYear+'-'+month : TransDate;
    TransDate = (year == null && month == null) ? null : TransDate;
    $("#gridStockAll").data("kendoGrid").dataSource.page(1);
    $("#gridStockAll").data("kendoGrid").dataSource.transport.options.read.data = {TransDate: TransDate};
    $("#gridStockAll").data("kendoGrid").dataSource.transport.options.read.url = site_url("Stockmovement/Movement/GetList");
    $("#gridStockAll").data("kendoGrid").dataSource.read();
}
function onChangeDateIn() {
    var vomonth = $("#monthpickerIn").data("kendoDatePicker").value();
    var voyear = $("#yearspickerIn").data("kendoDatePicker").value();
    var month = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'MM' );
    var defYear = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'yyyy' );
    var year = kendo.toString(kendo.parseDate(voyear,'yyyy-MM-dd'), 'yyyy' );
    var TransDate = (month == null) ? year : year+'-'+month;
    TransDate = (year == null) ? defYear+'-'+month : TransDate;
    TransDate = (year == null && month == null) ? null : TransDate;
    $("#gridStockIn").data("kendoGrid").dataSource.page(1);
    $("#gridStockIn").data("kendoGrid").dataSource.transport.options.read.data = {TransDate: TransDate};
    $("#gridStockIn").data("kendoGrid").dataSource.transport.options.read.url = site_url("Stockmovement/Movement/GetList")+"?customfilter[t6010f001]=IN";
    $("#gridStockIn").data("kendoGrid").dataSource.read();
}
function onChangeDateOut() {
    var vomonth = $("#monthpickerOut").data("kendoDatePicker").value();
    var voyear = $("#yearspickerOut").data("kendoDatePicker").value();
    var month = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'MM' );
    var defYear = kendo.toString(kendo.parseDate(vomonth,'yyyy-MM-dd'), 'yyyy' );
    var year = kendo.toString(kendo.parseDate(voyear,'yyyy-MM-dd'), 'yyyy' );
    var TransDate = (month == null) ? year : year+'-'+month;
    TransDate = (year == null) ? defYear+'-'+month : TransDate;
    TransDate = (year == null && month == null) ? null : TransDate;
    $("#gridStockOut").data("kendoGrid").dataSource.page(1);
    $("#gridStockOut").data("kendoGrid").dataSource.transport.options.read.data = {TransDate: TransDate};
    $("#gridStockOut").data("kendoGrid").dataSource.transport.options.read.url = site_url("Stockmovement/Movement/GetList")+"?customfilter[t6010f001]=Out";
    $("#gridStockOut").data("kendoGrid").dataSource.read();
}
function EditData(id)
{
    if(id){
        var voData = {
            RecordID: id
        };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Movement/GetDataEdit'),
            beforeSend: function(){
            },
            success: function (result) {
            if (result.errorcode != 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
            } else {
                new PNotify({ title: "Information", text: "Edit Item", type: 'info', shadow: true });
                $('#RecordID').val(result.data.RecordID);
                $('#TimeStamp').val(result.data.RecordTimeStamp);
                $('#Type').data("kendoDropDownList").value(result.data.Type);
                $('#ItemID').val(result.data.ItemID);
                $('#Brand').data("kendoDropDownList").value(result.data.Brand);
                $('#Model').data("kendoDropDownList").value(result.data.Model);
                $('#Status').data("kendoDropDownList").value(result.data.Status);
                $('#Color').data("kendoDropDownList").value(result.data.Color);
                $('#IMEI').val(result.data.IMEI);
                $('#RFID').val(result.data.RFID);
                $('#CostPrice').val(result.data.CostPrice);
                $('#Remarks').val(result.data.Remarks);
                $('#submitButtondetail').html('Update');
                if(result.data.Type == "Out")
                {
                    document.getElementById('ItemID').readOnly = true;
                    $("#Brand").data("kendoDropDownList").enable(false);
                    $("#Model").data("kendoDropDownList").enable(false);
                    $("#Color").data("kendoDropDownList").enable(false);
                    $("#Status").data("kendoDropDownList").enable(false);
                    document.getElementById('IMEI').readOnly = true;
                    document.getElementById('RFID').readOnly = true;
                
                    $("#LookupEventItemID").hide();
                    $("#form-title").html("Stock Out");
                }else{
                    document.getElementById('ItemID').readOnly = true;
                    $("#Brand").data("kendoDropDownList").enable(false);
                    $("#Model").data("kendoDropDownList").enable(false);
                    $("#Color").data("kendoDropDownList").enable(false);
                    $("#Status").data("kendoDropDownList").enable(false);
                    document.getElementById('IMEI').readOnly = false;
                    document.getElementById('RFID').readOnly = false;
                    $("#LookupEventItemID").show();
                    $("#form-title").html("Stock In");
                }

            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
}
function RemoveData(id)
{
    var result = confirm("Delete this record?");
    if (result) {
        if(id){
            var voData = {
                RecordID: id
            };
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Stockmovement/Movement/RemoveItem'),
                beforeSend: function(){
                },
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Remove Item", type: 'success', shadow: true });
                    $('#gridStockAll').data('kendoGrid').dataSource.read();
                    $('#gridStockAll').data('kendoGrid').refresh();
                    $('#gridStockIn').data('kendoGrid').dataSource.read();
                    $('#gridStockIn').data('kendoGrid').refresh();
                    $('#gridStockOut').data('kendoGrid').dataSource.read();
                    $('#gridStockOut').data('kendoGrid').refresh();
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }
    }
}

function insertMasterItem()
    {        
        var voData = {
            ItemID: $('#ItemID2').val(),
            ItemName: $('#ItemName2').val(),
            UOMID: $('#UOMID2').val(),
            EPC: $('#EPC2').val(),
            Barcode: $('#Barcode2').val(),
            UnitPrice: $('#UnitPrice2').val(),
            Picture: $('#photo').val(),
            GroupID: $('#ItemGroupName2').val(),
            Color: $('#Color2').val(),
            IMEI: $('#IMEI2').val(),
            Brand: $('#Brand2').val(),
            Status: $('#Status2').val(),
            SellingPrice: $('#SellingPrice2').val(),
            Remarks: $('#Remarks2').val(),
            Model: $('#Model2').val(),

        };
        var valid = checkForm2(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Item/insert'),
                beforeSend: function(){
                    $('#submitButtondetail2').attr('disabled', 'disabled');
                    $('#submitButtondetail2').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
                },
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    $('#submitButtondetail2').html('Success');
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    $("#detailItemForm").data("kendoWindow").center().close();
                    //window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }
    function checkForm2(voData) {
        var valid = 1;
        var msg = "";

        if (voData.ItemID == "") { valid = 0; msg += "Item ID is required" + "\r\n"; }
        if (voData.GroupID == "") { valid = 0; msg += "Item Group is required" + "\r\n"; }
        if (voData.Brand == "") { valid = 0; msg += "Brand is required" + "\r\n"; }
        if (voData.Model == "") { valid = 0; msg += "Model is required" + "\r\n"; }
        if (voData.Color == "") { valid = 0; msg += "Color is required" + "\r\n"; }
        if (voData.Status == "") { valid = 0; msg += "Status is required" + "\r\n"; }
        if (voData.UnitPrice == "") { valid = 0; msg += "Cost Price is required" + "\r\n"; }

        var voRes = {
            valid: valid,
            msg: msg
        }
        return voRes;
    }

$(document).ready(function() {
    kendoModal("detailItemForm","Add New Item","850px");
    $("#UnitPrice2").kendoNumericTextBox();
    $("#UOMID2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select UOM",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 1},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#AutoIDType2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_RecordID; ?>",
        optionLabel: "Select AutoID Type",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 2},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#ItemGroupName2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataItemGroup_Name; ?>",
        dataValueField: "<?php echo T_MasterDataItemGroup_ID; ?>",
        optionLabel: "Select Group Item",
        close: customTriger,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataItemGroup; ?>'},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Color2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Color",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 5},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Brand2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Brand",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 9},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Model2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Model",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 6},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Status2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Status",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 8},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    
    var data = [
        { text: "IN", value: "IN" },
        { text: "Out", value: "Out" }
    ];

    // create DropDownList from input HTML element
    $("#Type").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChange
    });
    var gridAll = $("#gridStockAll").kendoGrid({
        //toolbar: ["excel"],
        toolbar: kendo.template($("#template").html()),
        excel: {
            fileName: kendo.toString(new Date, "dd/MM/yyyy HH:mm") +"_stockinout.xlsx",
            filterable: true
        },
        height: "580px",
        width: "100%",
        columns: [
        { 
            "width": "30px",
            "template":
            ''
            +'<a class="k-button" href="javascript:void(0)" onclick="RemoveData(#=t6010r001#)"><span class="fa fa-trash"></span></a>'
        },
        {
            "title":"Type",
            "width":"80px",
            "field":"<?php echo T_TRANSINOUT_Type; ?>",
        },
        {
            "title":"Date",
            "width":"140px",
            "field":"<?php echo T_TRANSINOUT_Date; ?>",
        },
        {
            "title":"Item ID",
            "width":"120px",
            "field":"<?php echo T_TRANSINOUT_ItemID; ?>",
        },
        {
            "title":"Brand",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Brand; ?>",
        },
        {
            "title":"Model",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Model; ?>",
        },
        {
            "title":"Color",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Color; ?>",
        },
        {
            "title":"Status",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Status; ?>",
        },
        {
            "title":"IMEI",
            "width":"180px",
            "field":"<?php echo T_TRANSINOUT_IMEI; ?>",
        },
        {
            "title":"RFID",
            "width":"220px",
            "field":"<?php echo T_TRANSINOUT_RFID; ?>",
        },
        {
            "title":"Cost Price",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_CostPrice; ?>",
        },
        {
            "title":"Vendor",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Vendor; ?>",
        },
        {
            "title":"Pay Mode (vendor)",
            "width":"135px",
            "field":"<?php echo T_TRANSINOUT_Payment; ?>",
        },
        {
            "title":"GST (vendor)",
            "width":"110px",
            "field":"<?php echo T_TRANSINOUT_GST; ?>",
        },
        {
            "title":"Selling Price",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_SellingPrice; ?>",
        },
        {
            "title":"Pay Mode (Cust)",
            "width":"125px",
            "field":"<?php echo T_TRANSINOUT_PaymentOut; ?>",
        },
        {
            "title":"GST (Cust)",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_GSTOut; ?>",
        },
        {
            "title":"Invoice No",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_InvoiceNO; ?>",
        },
        {
            "title":"Remarks",
            "width":"200px",
            "field":"<?php echo T_TRANSINOUT_Remarks; ?>",
        }
        ],
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    url: site_url('Stockmovement/Movement/GetList'),
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#gridStockAll').data('kendoGrid').dataSource.read();
                $('#gridStockAll').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.count;
                },
                model: {
                    id: "<?php echo T_TRANSINOUT_RecordID; ?>",
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });

    var gridIn = $("#gridStockIn").kendoGrid({
        //toolbar: ["excel"],
        toolbar: kendo.template($("#templateIn").html()),
        excel: {
            fileName: kendo.toString(new Date, "dd/MM/yyyy HH:mm") +"_stockin.xlsx",
            filterable: true
        },
        height: "580px",
        width: "100%",
        columns: [
        { 
            "width": "30px",
            "template":
            ''
            +'<a class="k-button" href="javascript:void(0)" onclick="RemoveData(#=t6010r001#)"><span class="fa fa-trash"></span></a>'
        },
        {
            "title":"Type",
            "width":"80px",
            "field":"<?php echo T_TRANSINOUT_Type; ?>",
        },
        {
            "title":"Date",
            "width":"140px",
            "field":"<?php echo T_TRANSINOUT_Date; ?>",
        },
        {
            "title":"Item ID",
            "width":"120px",
            "field":"<?php echo T_TRANSINOUT_ItemID; ?>",
        },
        {
            "title":"Brand",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Brand; ?>",
        },
        {
            "title":"Model",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Model; ?>",
        },
        {
            "title":"Color",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Color; ?>",
        },
        {
            "title":"Status",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Status; ?>",
        },
        {
            "title":"IMEI",
            "width":"180px",
            "field":"<?php echo T_TRANSINOUT_IMEI; ?>",
        },
        {
            "title":"RFID",
            "width":"220px",
            "field":"<?php echo T_TRANSINOUT_RFID; ?>",
        },
        {
            "title":"Cost Price",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_CostPrice; ?>",
        },
        {
            "title":"Vendor",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Vendor; ?>",
        },
        {
            "title":"Pay Mode (vendor)",
            "width":"135px",
            "field":"<?php echo T_TRANSINOUT_Payment; ?>",
        },
        {
            "title":"GST (vendor)",
            "width":"110px",
            "field":"<?php echo T_TRANSINOUT_GST; ?>",
        },
        {
            "title":"Remarks",
            "width":"200px",
            "field":"<?php echo T_TRANSINOUT_Remarks; ?>",
        }
        ],
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    url: site_url('Stockmovement/Movement/GetList')+"?customfilter[t6010f001]=IN",
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#gridStockIn').data('kendoGrid').dataSource.read();
                $('#gridStockIn').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.count;
                },
                model: {
                    id: "<?php echo T_TRANSINOUT_RecordID; ?>",
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });

    var gridOut = $("#gridStockOut").kendoGrid({
        //toolbar: ["excel"],
        toolbar: kendo.template($("#templateOut").html()),
        excel: {
            fileName: kendo.toString(new Date, "dd/MM/yyyy HH:mm") +"_stockout.xlsx",
            filterable: true
        },
        height: "580px",
        width: "100%",
        columns: [
        { 
            "width": "30px",
            "template":
            ''
            +'<a class="k-button" href="javascript:void(0)" onclick="RemoveData(#=t6010r001#)"><span class="fa fa-trash"></span></a>'
        },
        {
            "title":"Type",
            "width":"80px",
            "field":"<?php echo T_TRANSINOUT_Type; ?>",
        },
        {
            "title":"Date",
            "width":"140px",
            "field":"<?php echo T_TRANSINOUT_Date; ?>",
        },
        {
            "title":"Item ID",
            "width":"120px",
            "field":"<?php echo T_TRANSINOUT_ItemID; ?>",
        },
        {
            "title":"Brand",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Brand; ?>",
        },
        {
            "title":"Model",
            "width":"150px",
            "field":"<?php echo T_TRANSINOUT_Model; ?>",
        },
        {
            "title":"Color",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Color; ?>",
        },
        {
            "title":"Status",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Status; ?>",
        },
        {
            "title":"IMEI",
            "width":"180px",
            "field":"<?php echo T_TRANSINOUT_IMEI; ?>",
        },
        {
            "title":"RFID",
            "width":"220px",
            "field":"<?php echo T_TRANSINOUT_RFID; ?>",
        },
        {
            "title":"Selling Price",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_SellingPrice; ?>",
        },
        {
            "title":"Vendor",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_Vendor; ?>",
        },
        {
            "title":"Pay Mode (Cust)",
            "width":"125px",
            "field":"<?php echo T_TRANSINOUT_PaymentOut; ?>",
        },
        {
            "title":"GST (Cust)",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_GSTOut; ?>",
        },
        {
            "title":"Invoice No",
            "width":"100px",
            "field":"<?php echo T_TRANSINOUT_InvoiceNO; ?>",
        },
        {
            "title":"Remarks",
            "width":"200px",
            "field":"<?php echo T_TRANSINOUT_Remarks; ?>",
        }
        ],
        dataSource: {
            transport: {            
                read: {
                    type:"GET",
                    url: site_url('Stockmovement/Movement/GetList')+"?customfilter[t6010f001]=Out",
                    dataType: "json"
                }
            },
            sync: function(e) {
                $('#gridStockOut').data('kendoGrid').dataSource.read();
                $('#gridStockOut').data('kendoGrid').refresh();
            },

            schema: {
                data: function(data){
                    return data.data;
                },
                total: function(data){
                    return data.count;
                },
                model: {
                    id: "<?php echo T_TRANSINOUT_RecordID; ?>",
                }
            },
            pageSize: 30,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        groupable: true,
        resizable: true,
        selectable: true,
        scrollable: true,
        reorderable:true,
        filterable: {
            mode: "row",
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
    });

    var data2 = [
        { text: "All", value: "All" },
        { text: "IN", value: "IN" },
        { text: "Out", value: "Out" }
    ];
    gridAll.find("#TypeDoc").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data2,
        index: 0,
        change: onChangeType
    });
    gridIn.find("#TypeDocIn").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data2,
        index: 0,
        change: onChangeTypeIn
    });

    gridOut.find("#TypeDocOut").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data2,
        index: 0,
        change: onChangeTypeOut
    });

    gridAll.find("#monthpickerAll").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM",
        dateInput: false,
        change: onChangeDate,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;
            if (calendar.view().name === "year") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "year") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    gridAll.find("#yearspickerAll").kendoDatePicker({
        start: "decade",
        depth: "decade",
        format: "yyyy",
        dateInput: false,
        change: onChangeDate,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;

            if (calendar.view().name === "decade") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "decade") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    gridIn.find("#monthpickerIn").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM",
        dateInput: false,
        change: onChangeDateIn,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;
            if (calendar.view().name === "year") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "year") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    gridIn.find("#yearspickerIn").kendoDatePicker({
        start: "decade",
        depth: "decade",
        format: "yyyy",
        dateInput: false,
        change: onChangeDateIn,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;

            if (calendar.view().name === "decade") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "decade") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    gridOut.find("#monthpickerOut").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM",
        dateInput: false,
        change: onChangeDateOut,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;
            if (calendar.view().name === "year") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "year") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });

    gridOut.find("#yearspickerOut").kendoDatePicker({
        start: "decade",
        depth: "decade",
        format: "yyyy",
        dateInput: false,
        change: onChangeDateOut,
        open: function(e) {
            var dp = e.sender;
            var calendar = dp.dateView.calendar;

            if (calendar.view().name === "decade") {
                calendar.element.find(".k-header").css("display", "none");
                calendar.element.find(".k-footer").css("display", "none");
            };

            calendar.bind("navigate", function(e) {
                var cal = e.sender;
                var view = cal.view();

                if (view.name === "decade") {
                    cal.element.find(".k-header").css("display", "none");
                    cal.element.find(".k-footer").css("display", "none");
                }

            });
        }
    });
    
    $("#Color").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Color",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 5},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Brand").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Brand",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 9},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Model").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Model",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 6},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Vendor").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Vendor",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 11},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#GST").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select GST",
        change: onChangeGST,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 12},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#payment").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Payment Type",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 10},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Status").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Status",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 8},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    onChangeType();
    });

    function onChangeGST() {
        var doctype = $('#Type').data("kendoDropDownList").value();
        var type = $("#GST").val();
        if(doctype == "IN"){
            if(type == "TAX INCLUSIVE"){
                var CostPrice = $("#CostPrice").val();
                $("#CostReal").val(CostPrice);
                var GST = parseFloat(CostPrice) * 0.07;
                var res = parseFloat(CostPrice) + parseFloat(GST);
                $("#CostPrice").val(res);
            }else{
                if($("#CostReal").val() != ""){
                    var CostPrice = $("#CostReal").val();
                }else{
                    var CostPrice = $("#CostPrice").val();
                }
                $("#CostReal").val("");
                $("#CostPrice").val(CostPrice);
            }
        }else{
            if(type == "TAX INCLUSIVE"){
                var SellingPrice = $("#SellingPrice").val();
                $("#SellingReal").val(SellingPrice);
                var GST = parseFloat(SellingPrice) * 0.07;
                var res = parseFloat(SellingPrice) + parseFloat(GST);
                $("#SellingPrice").val(res);
            }else{
                if($("#SellingReal").val() != ""){
                    var SellingPrice = $("#SellingReal").val();
                }else{
                    var SellingPrice = $("#SellingPrice").val();
                }
                $("#SellingReal").val("");
                $("#SellingPrice").val(SellingPrice);
            }
        }
    };
</script>

<script type="text/x-kendo-template" id="template">
    <div style="display: inline-block;">
        <a class="k-button k-button-icontext k-grid-excel" href="\\#"><span class="k-icon k-i-excel"></span>Export to Excel</a>
    </div>
    <div style="float: right;">
        <label class="category-label" for="category">Show by :</label>
        <input id="TypeDoc" style="width: 150px"/>
        <input id="monthpickerAll" style="width: 150px"/>
        <input id="yearspickerAll" style="width: 150px"/>
    </div>
</script>

<script type="text/x-kendo-template" id="templateIn">
    <div style="display: inline-block;">
        <a class="k-button k-button-icontext k-grid-excel" href="\\#"><span class="k-icon k-i-excel"></span>Export to Excel</a>
    </div>
    <div style="float: right;">
        <label class="category-label" for="category">Show by :</label>
        <input id="TypeDocIn" style="width: 150px"/>
        <input id="monthpickerIn" style="width: 150px"/>
        <input id="yearspickerIn" style="width: 150px"/>
    </div>
</script>

<script type="text/x-kendo-template" id="templateOut">
    <div style="display: inline-block;">
        <a class="k-button k-button-icontext k-grid-excel" href="\\#"><span class="k-icon k-i-excel"></span>Export to Excel</a>
    </div>
    <div style="float: right;">
        <label class="category-label" for="category">Show by :</label>
        <input id="TypeDocOut" style="width: 150px"/>
        <input id="monthpickerOut" style="width: 150px"/>
        <input id="yearspickerOut" style="width: 150px"/>
    </div>
</script>
<?php
    $dataItem = array(
        array('field' => T_MasterDataItem_ItemID, 'title' => 'Item ID', 'width' => '100px'),
        //array('field' => T_MasterDataItem_ItemName, 'title' => 'Item Name', 'width' => '100px'),
        //array('field' => T_MasterDataItem_IMEI, 'title' => 'IMEI', 'width' => '80px'),
        array('field' => T_MasterDataItem_Brand, 'title' => 'Brand', 'width' => '80px'),
        array('field' => T_MasterDataItem_Model, 'title' => 'Model', 'width' => '80px'),
        array('field' => T_MasterDataItem_Color, 'title' => 'Color', 'width' => '80px'),
        array('field' => T_MasterDataItem_Status, 'title' => 'Status', 'width' => '80px'),
        array('field' => T_MasterDataItemGroup_Name, 'title' => 'Group Item', 'width' => '80px'),
        //array('field' => T_MasterDataItem_EPC, 'title' => 'RFID', 'width' => '80px'),
    );
    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'RecordIDDetail2', 'column' => T_MasterDataItem_RecordID),
        array('id' => 'ItemID', 'column' =>T_MasterDataItem_ItemID),
        //array('id' => 'ItemName', 'column' =>T_MasterDataItem_ItemName),
        //array('id' => 'IMEI', 'column' =>T_MasterDataItem_IMEI),
        array('id' => 'Brand', 'column' =>T_MasterDataItem_Brand),
        array('id' => 'Model', 'column' =>T_MasterDataItem_Model),
        array('id' => 'Color', 'column' =>T_MasterDataItem_Color),
        array('id' => 'Status', 'column' =>T_MasterDataItem_Status),
        array('id' => 'ItemGroup', 'column' =>T_MasterDataItem_GroupID),
        //array('id' => 'EPC', 'column' =>T_MasterDataItem_EPC)
    );
    $filter = array();//T_MasterDataItem_FlagStockIn=>'0');

    //id, title, size, URL, data field in database, Throw Data To form when click
    echo kendoModalLookupAddMasterItem("ItemID", "Data Item", "800px", "Webservice/Read/Getlist", $dataItem, $columnItem,T_MasterDataItem,'',$filter);

?>
<script>
function customTriger()
{
    var status = $('#Status').data("kendoDropDownList").value();
    if(status == "NEW"){
        $('#GST').data("kendoDropDownList").value("TAX INCLUSIVE");
    }else{
        $('#GST').data("kendoDropDownList").value("NO TAX");
    }
}
</script>
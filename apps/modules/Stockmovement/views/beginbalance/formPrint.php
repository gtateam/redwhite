<style>
    hr {
        background: grey none repeat scroll 0 0;
        border: 0 none;
        height: 2px;
        margin: 10px 0;
    }
</style>
        <div class="invoice">
            <header class="clearfix">
                <div class="row">
                    <div class="col-sm-12 text-center mt-md mb-md">
                        <h3>Red White Mobile</h3>
                        <p>420 North Bridge Road, #01-20 North Bridge Centre, Singapura 188727</p>
                        <p>Phone : +65 6735 4811</p>
                    </div>
                </div>
            </header>
            <div class="bill-info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Date:</span>
                                <span class="value"><?php echo date(FORMATDATE,strtotime(${T_TransactionStockMovementHeader_DocDate})); ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-right">
                            <p class="mb-none">
                                <span class="text-dark">Doc No:</span>
                                <span class="value"><?php echo ${T_TransactionStockMovementHeader_DocNo}; ?></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="row">
                    <div class="col-sm-12 text-center mt-md mb-md">
                        <h4>STOCK BALANCE</h4>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table invoice-items table-condensed">
                    <thead id="head-detail">
                        <tr>
                            <th width="30px" data-col="RowIndex">#</th>
                            <th width="120px" data-col="ItemID">Item ID</th>
                            <th width="100px" data-col="Brand">Brand</th>
                            <th width="200px" data-col="ItemName">Model</th>
                            <th width="80px" data-col="Color">Color</th>
                            <th width="80px" data-col="Color">Status</th>
                            <th width="80px" data-col="ItemType">Item Type</th>
                            <th width="70px" data-col="Qty">Qty</th>
                            <th width="100px" data-col="LocationName">Loc Name</th>
                            <th width="200px" data-col="EPC">EPC</th>
                            <th width="180px" data-col="IMEI">IMEI</th>
                            <th data-col="RecordIDDetail" style="display:none;"></th>
                            <th data-col="RecordFlag" style="display:none;"></th>
                        </tr>
                    </thead>
                    <tbody id="list-detail">
                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                        foreach($Detail as $item): echo '<tr id="detail-'.$i. '">
                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                        <td id="detailBrandv-'.$i. '" data-val="'.$item[T_MasterDataItem_Brand]. '">'.$item[T_MasterDataItem_Brand]. '</td>
                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataItem_ItemName]. '">'.$item[T_MasterDataItem_ItemName]. '</td>
                        <td id="detailColorv-'.$i. '" data-val="'.$item[T_MasterDataItem_Color]. '">'.$item[T_MasterDataItem_Color]. '</td>
                        <td id="detailStatusv-'.$i. '" data-val="'.$item[T_MasterDataItem_Status]. '">'.$item[T_MasterDataItem_Status]. '</td>
                        <td id="detailItemTypev-'.$i. '" data-val="'.$item[T_MasterDataGeneralTableValue_Key]. '">'.$item[T_MasterDataGeneralTableValue_Key]. '</td>
                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>
                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                        <td id="detailBarcodev-'.$i. '" data-val="'.$item[T_MasterDataItem_IMEI]. '">'.$item[T_MasterDataItem_IMEI]. '</td>
                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordID]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordID]. '</td>
                        <td id="detailRecordFlagv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordFlag]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordFlag]. '</td>
                    </tr>'; $i++; endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="bill-data text-Left">
                    <p class="mb-none">
                        <span class="text-dark">Remarks:</span>
                        <span class="value"><?php echo (empty(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "-" ?></span>
                    </p>
                </div>
            </div>
           
        </div>

        <script>
            //window.print();
        </script>
   
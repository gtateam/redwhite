<style type="text/css">.mrg{margin-bottom:5px;}
[data-tip] {
	position:relative;

}
[data-tip]:before {
	content:'';
	/* hides the tooltip when not hovered */
	display:none;
	content:'';
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-bottom: 5px solid #1a1a1a;	
	position:absolute;
	top:30px;
	left:35px;
	z-index:8;
	font-size:0;
	line-height:0;
	width:0;
	height:0;
}
[data-tip]:after {
	display:none;
	content:attr(data-tip);
	position:absolute;
	top:35px;
	left:0px;
	padding:5px 8px;
	background:#1a1a1a;
	color:#fff;
	z-index:15;
	font-size: 0.75em;
	height:28px;
    width:65px;
	line-height:18px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	white-space:nowrap;
	word-wrap:normal;
}
[data-tip]:hover:before,
[data-tip]:hover:after {
	display:block;
}
</style>
<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_TransactionStockMovementHeader_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="editable" value="">
                <input type="hidden" id="stockinout" value="1">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordID}) ? ${T_TransactionStockMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordTimestamp}) ? ${T_TransactionStockMovementHeader_RecordTimestamp} : ''; ?>">
                <div class="form-group" style="display:none;">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc Type ID</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocType', 'class' => 'k-input k-textbox', 'value' => isset(${T_TransactionStockMovementHeader_DocTypeID}) ? ${T_TransactionStockMovementHeader_DocTypeID} : 'IVSO', 'style' => 'background-color:#eee', 'readonly' => 'TRUE', ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row" >
                            <label class="col-md-3 form-label">Doc Type Name</label>
                            <div class="col-md-9">
                                <?php  
                                if(isset(${T_TransactionStockMovementHeader_DocTypeID})){
                                $value = (${T_TransactionStockMovementHeader_DocTypeID}=="IVSI") ? 'Inventory - In' : 'Inventory - Out';}else{$value="Inventory - In";}
                                $items=array( 'id'=> 'DocName', 'class' => 'k-input k-textbox', 'style' => 'width:250px;', 'value' => $value, 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Doc No</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocNo', 'class' => 'k-input k-textbox', 'value' => isset(${T_TransactionStockMovementHeader_DocNo}) ? ${T_TransactionStockMovementHeader_DocNo} : substr(DocNo('IVSO'), 2), 'style' => 'background-color:#eee;', 'readonly' => 'TRUE' ); echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row" style="margin-top:5px;">
                            <label class="col-md-3 form-label">Doc Date</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocDate', 'class' => 'KendoDatePicker', 'value' => isset(${T_TransactionStockMovementHeader_DocDate}) ? ${T_TransactionStockMovementHeader_DocDate} : date(FORMATDATE) ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Remarks</label>
                            <div class="col-md-9">
                                <textarea class="k-textbox" id="Remark" style="width: 300px; margin-top: 0px; margin-bottom: 0px;height:80px;"><?php echo (isset(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "" ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row" style="display:none;">
                            <label class="col-md-3 form-label">Doc Status</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocStatus', 'value' => isset(${T_TransactionStockMovementHeader_DocStatus}) ? ${T_TransactionStockMovementHeader_DocStatus} : '0', 'readonly' => true, 'style' => 'margin-top:-9px;' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!--  -->
                    </div>
                </div>  
                 <div class="form-group">
                    <div class="col-md-6">
                        <div class="row" style="display:none;">
                            <label class="col-md-3 form-label">Type</label>
                            <div class="col-md-9">
                                <?php $items=array( 'id'=> 'DocTypex', 'onChange' => 'selectType(this.value);', 'value' => isset(${T_TransactionStockMovementHeader_DocTypeID}) ? ${T_TransactionStockMovementHeader_DocTypeID} : 'IVSI', 'style' => 'margin-top:-9px;' ); echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!--  -->
                    </div>
                </div>

                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <a id="DetailModal" class="mb-xs mt-xs mr-xs btn btn-xs btn-success"><i class="fa fa-plus"></i> &nbsp;Add New</a>
                            <a id="removeAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detail');"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
                            <a id="scanTagStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="scanTag(1);"><i class="fa fa-barcode"></i> &nbsp;Start Scan</a>
                                <a id="scanTagStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="scanTag(0);" style="display:none;"><i class="fa fa-barcode"></i> &nbsp;Stop Scan</a>
                            <span data-tip="SCAN IMEI"><input type="text" id="imeiscan" class="k-input k-textbox"/></span>
                            <div style="overflow:auto;">
                            <input id="DoRemoveID" type="hidden" />
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="80px"><input type="checkbox" id="detailCheckAll" onclick="CheckAll('detail');"> Action</th>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Item ID</th>
                                            <!-- <th data-col="ItemName">Item Name</th> -->
                                            <!-- <th data-col="IMEI">IMEI</th> -->
                                            <th data-col="Brand">Brand</th>
                                            <th data-col="Model">Model</th>
                                            <th data-col="Color">Color</th>
                                            <th data-col="Status">Status</th>
                                            <th data-col="ItemGroup">Group</th>
                                            <th data-col="EPC">RFID</th>
                                            <th data-col="Qty">Qty</th>
                                            <th data-col="LocationID">Location ID</th>
                                            <th data-col="LocationName">Location Name</th>
                                            <th data-col="RecordIDDetail" style="display:none;"></th>
                                            <th data-col="RecordFlag" style="display:none;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"><input type="checkbox" class="detailCheck" value="'.$i.'"> <a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><a onclick="removedetail('.$target. ','.$i. ');" href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a></td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                                        <td id="detailBrandv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Brand]. '">'.$item[T_TransactionStockMovementDetail_Brand]. '</td>
                                        <td id="detailModelv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Model]. '">'.$item[T_TransactionStockMovementDetail_Model]. '</td>
                                        <td id="detailColorv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Color]. '">'.$item[T_TransactionStockMovementDetail_Color]. '</td>
                                        <td id="detailStatusv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Status]. '">'.$item[T_TransactionStockMovementDetail_Status]. '</td>
                                        <td id="detailItemGroupv-'.$i. '" data-val="'.$item[T_MasterDataItemGroup_Name]. '">'.$item[T_MasterDataItemGroup_Name]. '</td>
                                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item['Loc1']. '">'.$item['Loc1']. '</td>
                                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordID]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordID]. '</td>
                                        <td id="detailRecordFlagv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordFlag]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordFlag]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="col-md-5">
        <input id="RecordFlag" type="hidden" />
        <input id="RecordIDDetail" type="hidden" />
        <input id="RecordIDDetail2" type="hidden" />
        <div class="k-edit-label">#</div>
        <div class="k-edit-field">
            <input type="text" id="RowIndex" primary="1" readonly/>
        </div>
        <div class="k-edit-label">Item ID</div>
        <div class="k-edit-field">
            <input type="text" id="ItemID" class="k-input k-textbox" style="text-transform: uppercase;" datarequired="1"  readonly/>
            <button class="k-button" id="LookupEventItemID">
                <div class="k-icon k-i-search">
            </button>
        </div>
        <div class="k-edit-label" style="display:none;">ItemName</div>
        <div class="k-edit-field" style="display:none;">
            <input type="text" id="ItemName" class="k-input k-textbox"  datarequired="0"  readonly/>
        </div>
        <div class="k-edit-label">Brand</div>
        <div class="k-edit-field">
            <input type="text" id="Brand" class="k-input k-textbox" datarequired="1"  readonly/>
        </div>
        <div class="k-edit-label">Model</div>
        <div class="k-edit-field">
            <input type="text" id="Model" class="k-input k-textbox" datarequired="1"  readonly/>
        </div>
        <div class="k-edit-label">Color</div>
        <div class="k-edit-field">
            <input type="text" id="Color" class="k-input k-textbox" datarequired="1"  readonly/>
        </div>

    </div>
    <div class="col-md-6">
        <div class="k-edit-label">Qty</div>
        <div class="k-edit-field">
            <input type="text" datarequired="1" id="Qty" />
        </div>
        <div id="AccessoriesEPC" style="display:none;">
            <div class="k-edit-label">RFID</div>
            <div class="k-edit-field">
                <input type="text" id="EPC" class="k-input k-textbox" datarequired="0" />
                <button class="k-button" id="" onclick="">
                    <div class="fa fa-rss fa-lg">
                </button>
            </div>
        </div>                            
        <div class="k-edit-label">Status</div>
        <div class="k-edit-field">
            <input type="text" id="Status" class="k-input k-textbox" datarequired="1"  readonly/>
        </div>
        <div class="k-edit-label">Item Group</div>
        <div class="k-edit-field">
            <input type="text" id="ItemGroup" class="k-input k-textbox" datarequired="1"  readonly/>
        </div>
        <div class="k-edit-label">Location ID</div>
        <div class="k-edit-field">    
            <input type="text" id="LocationID" class="k-input k-textbox"  style="text-transform: uppercase;" datarequired="1" readonly />
            <!-- <button class="k-button" id="LookupEventToLocation">
                <div class="k-icon k-i-search">
            </button> -->
        </div>
        <div class="k-edit-label">Location Name</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" datarequired="0" readonly id="LocationName"/>
        </div>
        
    </div>
    <div id="tableDetailModalSub" style="overflow: visible;display:none;">
        <div class="col-md-8">
            <a id="DetailModalSub" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" style="display:none;" onclick="openDetailModal();"><i class="fa fa-plus"></i> &nbsp;Add New</a>
            <a id="detailSubRemoveAll" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="RemoveAll('detailSub');" style="display:none;"><i class="fa fa-trash-o"></i> &nbsp;Remove</a>
        </div>
        <table id="table-detailSub" class="table table-responsive">
            <thead id="head-detailSub">
                <tr>
                    <th width="80px"><input type="checkbox" id="detailSubCheckAll" onclick="CheckAll('detailSub');"> Action</th>
                    <th data-col="IMEI">IMEI</th>
                    <th data-col="EPC2">RFID</th>
                </tr>
            </thead>
            <tbody id="list-detailSub">
            </tbody>
        </table>
    </div>
    <div class="k-edit-buttons k-state-default">
        <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');"><i class="el-icon-file-new"></i> Save</button>
        <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="el-icon-remove"></i> Cancel</button>
    </div>
</div>

<div style="display:none;" class="k-edit-form-container" id="detailSubForm">
    <div id="forIn" class="row col-md-12" style="display:block;">
        <!-- <div class="k-edit-label">IMEI</div>
        <div class="k-edit-field">
            <input onblur="customTriger3()" type="text" class="k-input k-textbox" id="IMEI" style="text-transform: uppercase" datarequired="1" />
        </div> -->

        <div class="k-edit-label">RFID</div>
        <div class="k-edit-field">
            <input onblur="customTriger3()" type="text" class="k-input k-textbox" id="EPC2" style="text-transform: uppercase" datarequired="1" />
            <button class="k-button" id="LookupEventItemID">
                <div class="fa fa-rss fa-lg">
            </button>
        </div>
        
        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetailSub" class="btn btn-primary close-button" onclick="adddetail('detailSub');"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailSubForm');"><i class="fa fa-cancel"></i> &nbsp;Cancel</button>
        </div>
    </div>
    <div id="forOut" class="row col-md-12" style="display:none;">
        <div id="kgSerialNo"></div>
    </div>
</div>
<!--  End Modal Form Detail -->

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailItemForm">
    <div class="col-md-5">

        <div class="form-group mrg" style="display:none">
            <div class="col-md-6">
                <!--  -->
            </div>
            <div class="col-md-6">
                <div class="row" >
                    <label class="col-md-3 form-label">Auto ID Type</label>
                    <div class="col-md-4">
                        <?php $items=array( 
                            'id'=> 'AutoIDType2', 
                            'class' => 'k-input', 
                            'value' => isset(${T_MasterDataItem_AutoIDType}) ? ${T_MasterDataItem_AutoIDType} : "",  
                        ); 
                        echo form_input($items); ?>
                    </div>
                </div>
            </div>
        </div>

        <label class="k-edit-label">Item ID</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'ItemID2', 
                'class' => 'k-input k-textbox', 
                'style' => 'background-color:#eee; text-transform: uppercase;',
                'value' => isset(${T_MasterDataItem_ItemID}) ? ${T_MasterDataItem_ItemID} : substr(AutoItemID('ITEM'),5),
                'readonly' => TRUE
            ); 
            //if(!isset(${T_MasterDataItem_ItemID})){ unset($items['readonly']); }
            echo form_input($items); ?>
        </div>
        
        <label class="k-edit-label">Item Group</label>
        <div class="k-edit-field">
            <input id="ItemGroupID2" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataItem_GroupID}) ? ${T_MasterDataItem_GroupID} : ''; ?>" />
            <?php $items=array(
                'id'=> 'ItemGroupName2', 
                'class' => '', 
                'value' => isset(${T_MasterDataItemGroup_ID}) ? ${T_MasterDataItemGroup_ID} : "",
                'style' => 'background-color:#eee;', 
                'style' => ''
            ); 
            echo form_input($items); echo"&nbsp;"; ?>
        </div>

        <label class="k-edit-label">Brand</label>
        <div class="k-edit-field">
            <?php $items=array(
                'id'=> 'Brand2',
                'class' => '',
                'style' => '',
                'value' => isset(${T_MasterDataItem_Brand}) ? ${T_MasterDataItem_Brand} : "",
            ); 
            echo form_input($items); ?>
        </div>

        <label class="k-edit-label">Model</label>
        <div class="k-edit-field">
            <?php $items=array(
                'id'=> 'Model2',
                'class' => '',
                'style' => '',
                'value' => isset(${T_MasterDataItem_Model}) ? ${T_MasterDataItem_Model} : "",
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Color</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'Color2', 
                'class' => 'k-input', 
                'value' => isset(${T_MasterDataItem_Color}) ? ${T_MasterDataItem_Color} : "",
                'style' => '',
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Status</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'Status2', 
                'class'=>'k-input', 
                'value' => isset(${T_MasterDataItem_Status}) ? ${T_MasterDataItem_Status} : "",
                'style' => '',
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">UOM ID</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id'=> 'UOMID2', 
                'class'=>'k-input', 
                'value' => isset(${T_MasterDataItem_UOMID}) ? ${T_MasterDataItem_UOMID} : "",
                'style' => ''
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
        
        
        <label class="k-edit-label">Cost Price</label>
        <div class="k-edit-field">
            <?php $items=array( 
                'id' => 'UnitPrice2', 
                'class' => 'k-input',
                'value' => isset(${T_MasterDataItem_UnitPrice}) ? ${T_MasterDataItem_UnitPrice} : "",
                'style' => ''
                //'readonly' => true  
            ); 
            echo form_input($items); ?>
        </div>
    </div>

    <div class="col-md-6">
        <label class="k-edit-label">Picture</label>
        <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataItem_Picture}) ? $meta{T_MasterDataItem_Picture} : ''; ?>">
        <?php
            if(isset(${T_MasterDataItem_Picture})){
                $voImg = (${T_MasterDataItem_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataItem_Picture};
            }else{
                $voImg = "assets/upload/photo/no-photo.png";
            }
        ?>
        <div class="col-md-6">
            <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                    <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                </div>
            </a>
            <form id="form-photo" action="<?php echo site_url('en/System/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                <input class="" style="width:170px;margin-top:3px;" id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
                <input name="ItemIDS" id="ItemIDS" value="" type="hidden"/> 
                <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
            </form>
            <div id="err"></div>
            <br><br><br>
        </div>

        <div class="row mrg" style="margin-top:15px;">
            <label class="k-edit-label">Remarks</label>
            <div class="k-edit-field">
                <textarea class="k-textbox" id="Remarks2" style="width: 300px;"><?php echo (isset(${T_MasterDataItem_Remarks}))? ${T_MasterDataItem_Remarks} : "" ?></textarea>
            </div>
        </div>
    </div>
    
    <div class="k-edit-buttons k-state-default">
        <button id="submitButtondetail2" class="btn btn-primary close-button" onclick="insertMasterItem();"><i class="el-icon-file-new"></i> Save</button>
        <button class="btn btn-default close-button" onclick="CloseModal('detailItemForm');"><i class="el-icon-remove"></i> Cancel</button>
    </div>
</div>
<!--  End Modal Form Detail -->

<div style="display:none;" id="Post" data-loading-overlay>
    <div class="k-edit-form-container" align="center">
        <img width="30px" src="assets/img/loading.gif" />
    </div>
</div>

<?php
    //Lookup Item
    $dataItem = array(
        array('field' => T_TransactionStockBalanceHeader_ItemID, 'title' => 'Item ID', 'width' => '50px'),
        array('field' => T_MasterDataItem_Brand, 'title' => 'Brand', 'width' => '80px'),
        array('field' => T_MasterDataItem_Model, 'title' => 'Model', 'width' => '80px'),
        array('field' => T_MasterDataItem_Color, 'title' => 'Color', 'width' => '50px'),
        array('field' => T_MasterDataItem_Status, 'title' => 'Status', 'width' => '50px'),
        array('field' => T_TransactionStockBalanceHeader_Quantity, 'title' => 'Qty', 'width' => '80px')
    );
    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'RecordIDDetail2', 'column' => T_TransactionStockBalanceHeader_RecordID),        
        array('id' => 'ItemID', 'column' => T_TransactionStockBalanceHeader_ItemID),
        array('id' => 'Brand', 'column' => T_MasterDataItem_Brand),
        array('id' => 'Model', 'column' => T_MasterDataItem_Model),
        array('id' => 'Color', 'column' => T_MasterDataItem_Color),
        array('id' => 'Status', 'column' => T_MasterDataItem_Status),
        array('id' => 'ItemGroup', 'column' => T_MasterDataItem_GroupID),
        array('id' => 'LocationID', 'column' => T_TransactionStockBalanceHeader_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );
    $filter = array('');

    //id, title, size, URL, data field in database, Throw Data To form when click
    echo kendoModalLookupAddMasterItem("ItemID", "Data Item", "800px", "Stockmovement/Stocktransfer/GetStockList", $dataItem, $columnItem,T_MasterDataItem,'',$filter);

    //Location Lookup To
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("ToLocation", "Data Location", "500px", "Masterdata/Location/GetLocation", $dataLocation, $columnLocation,T_MasterDataLocation);


?>
<script type="text/javascript" src="assets/js/apps.js"></script>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>

<?php 
if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>"; 
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {
    $('#imeiscan').on('keydown', function(e) {
        if (e.which == 13) {
             getdataScan();
            e.preventDefault();
        }
    });
    //Numeric
    $("#RowIndex").kendoNumericTextBox(); 
    $("#RowIndex2").kendoNumericTextBox(); 
    $("#UnitPrice2").kendoNumericTextBox().val();
    $("#Qty").kendoNumericTextBox();
    $('#removecount').hide();
    $('#ClearForm').hide();
    kendoModal("detailForm","Add Detail","850px");
    kendoModal("detailSubForm","Add Detail","500px");
    kendoModal("detailItemForm","Add New Item","850px");
    $("#forIn").hide();
    $("#forOut").show();     
    $("#Post").kendoWindow({
        width: "400px",
        title: "Posting ..",
        visible: false,
        modal: true,
        actions: [
        "Close"
        ],
    });
    customTriger();
     $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        cleardetail("detailSub", 0);
    });

    $("#kgSerialNo").delegate("tbody>tr", "dblclick", dblclickSerialNo);

    var data = [
        {text: "In", value:"IVSI"},
        {text: "Out", value:"IVSO"}
    ];
    $("#DocTypex").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        optionLabel: "Select"
    });

    if(ID){
        $("#DocTypex").data("kendoDropDownList").readonly();
    }

    $("#UOMID2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select UOM",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 1},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#AutoIDType2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_RecordID; ?>",
        optionLabel: "Select AutoID Type",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 2},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#ItemGroupName2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataItemGroup_Name; ?>",
        dataValueField: "<?php echo T_MasterDataItemGroup_ID; ?>",
        optionLabel: "Select Group Item",
        close: customTriger,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataItemGroup; ?>'},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Color2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Color",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 5},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Brand2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Brand",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 9},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Model2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Model",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 6},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Status2").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Status",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 8},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });

    var init = current_url()+"ReaderStatus";
    if(localStorage[init]==1){
        $("#scanTagStart").hide();
        $("#scanTagStop").show();
    }else{
        $("#scanTagStart").show();
        $("#scanTagStop").hide();
    }

});


$(document).on('click', 'button[id=removecount]', function() {
    var qty = document.getElementById("list-detailSub").rows.length;
    $('#Qty').data('kendoNumericTextBox').value(qty);
});

function selectType(type)
{
    var DocName = (type=="IVSI") ? "Inventory - In" : "Inventory - Out";
    $("#DocType").val(type);
    $("#DocName").val(DocName);
    if(type=="IVSO"){
        $("#LookupEventToLocation").hide();
        $("#forIn").hide();
        $("#forOut").show();
    }else{
        $("#LookupEventToLocation").show(); 
        $("#forIn").show();
        $("#forOut").hide();       
    }
    var voData = {
        type: type,
    };
    $.ajax({
        type: 'GET',
        data: voData,
        url:  site_url('Welcomes/getDocNO'),
        success: function (result) {
            $("#DocNo").val(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
}

//Insert
function insert()
{
    var detail = getDetailSubItem('detail');
    var voData = {
        DocType: $('#DocType').val(),
        DocDate: $('#DocDate').val(),
        DocStatus: $('#DocStatus').val(),
        Remark: $('#Remark').val(),
        detail: detail
    };
    var valid = checkForm(voData);
    console.log(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('Stockmovement/Stockout/insert'),
            beforeSend: function(){
                $('.btn-primary').attr('disabled', 'disabled');
                $('.btn-primary').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
            },
            success: function (result) {
            if (result.errorcode > 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
            } else {
                lsClear();
                var id = {
                        RecorID : result.id,
                };
                $.ajax({
                    type: 'POST',
                    data: id,
                    url: site_url('Stockmovement/Stockout/post/'+result.id),
                    beforeSend: function(){
                        $('#Post').trigger('loading-overlay:show');
                        $("#Post").data("kendoWindow").center().open();
                    },
                    success: function(resultpost){
                        if (result.errorcode > 0) {
                            new PNotify({ title: "Failed", text: resultpost.msg, type: 'error', shadow: true });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
                $('.btn-primary').html('Success');
                setTimeout(function(){
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    $("#Post").data("kendoWindow").center().close();
                    window.location.replace(current_url());
                }, 1000);
            }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

//Update
function update()
{
    var detail = getDetailSubItem('detail');
    var voData = {
        RecordID: ID,
        TimeStamp: $('#TimeStamp').val(),
        DocNo: $('#DocNo').val(),
        DocType: $('#DocType').val(),
        DocDate: $('#DocDate').val(),
        DocStatus: $('#DocStatus').val(),
        Remark: $('#Remark').val(),
        DoRemoveID : $("#DoRemoveID").val(),
        detail: detail,
    };
    var valid = checkForm(voData);
        if(valid.valid)
        {
        $.ajax({
            type: 'POST',
            data: voData,
            url: "<?php echo site_url('Stockmovement/Stockout/update'); ?>",
            beforeSend: function(){
                $('.btn-primary').attr('disabled', 'disabled');
                $('.btn-primary').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
            },
            success: function (result) {
            if (result.errorcode > 0) {
                new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
            } else {
                lsClear();

                var id = {
                        RecorID : result.id,
                };
                $.ajax({
                    type: 'POST',
                    data: id,
                    url: site_url('Stockmovement/Stockout/post/'+result.id),
                    beforeSend: function(){
                        $('#Post').trigger('loading-overlay:show');
                        $("#Post").data("kendoWindow").center().open();
                    },
                    success: function(resultpost){
                        if (result.errorcode > 0) {
                            new PNotify({ title: "Failed", text: resultpost.msg, type: 'error', shadow: true });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
                $('.btn-primary').html('Success');
                setTimeout(function(){
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    $("#Post").data("kendoWindow").center().close();
                    window.location.replace(current_url());
                }, 1000);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.DocDate == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.detail == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }


    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}

function checkForm2(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ItemID == "") { valid = 0; msg += "Item ID is required" + "\r\n"; }
    if (voData.GroupID == "") { valid = 0; msg += "Item Group is required" + "\r\n"; }
    if (voData.Brand == "") { valid = 0; msg += "Brand is required" + "\r\n"; }
    if (voData.Model == "") { valid = 0; msg += "Model is required" + "\r\n"; }
    if (voData.Color == "") { valid = 0; msg += "Color is required" + "\r\n"; }
    if (voData.Status == "") { valid = 0; msg += "Status is required" + "\r\n"; }
    if (voData.UnitPrice == "") { valid = 0; msg += "Cost Price is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

    function checkField(target){
        var msg = '';
        var field = getDetailField(target);
        var val   = getDetailItem(target);
         for (v = 0; v < val.length; v++) {
            if($("#"+field[i]).attr("primary") == "1"){
                if($("#"+field[i]).val() == val[v].RowIndex)
                {
                    msg+="Row Index Sudah Ada"+"\r\n";
                }            
            }
        }
        return msg;
    }

    // function customTriger(i){
    //     var typeItem = $('#ItemType').val();
    //     if(typeItem === 'SS'){
    //         $('#DetailModalSub').show()
    //         $('#tableDetailModalSub').show();
    //         $('#Qty').data('kendoNumericTextBox').readonly(true);
    //         $('#detailSubRemoveAll').show();
    //         $('#removecount').show();
    //         var qty = document.getElementById("list-detailSub").rows.length;
    //         $('#Qty').data('kendoNumericTextBox').value(qty);
    //     }else{
    //         $('#DetailModalSub').hide()
    //         $('#removecount').hide();
    //         $('#tableDetailModalSub').hide();
    //         $('#detailSubRemoveAll').hide();
    //         if(typeItem === 'NS'){
    //             $('#Qty').data('kendoNumericTextBox').readonly(false);
    //             if(!i){
    //                 $('#Qty').data('kendoNumericTextBox').value(0);
    //             }
    //             $('#removecount').hide();
    //         }else if(typeItem === 'S1'){
    //             $('#Qty').data('kendoNumericTextBox').readonly(true);
    //             $('#Qty').data('kendoNumericTextBox').value(1);
    //             $('#removecount').hide();
    //         }else if(typeItem === 'SN')
    //         {
    //             $('#Qty').data('kendoNumericTextBox').readonly(false);
    //             $('#removecount').hide();
    //         }
    //     }
    // }

function customTriger(i){
    var typeItem = $('#ItemGroup').val();
    if((typeItem === 'IG-00001') ||  (typeItem === 'Mobile Phone')){
        // logic here
        $('#Qty').data("kendoNumericTextBox").readonly();
        $("#Qty").data("kendoNumericTextBox").value("1");

        // Showing IMEI
        $('#DetailModalSub').show();
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);
        $("#AccessoriesEPC").hide();
    }else{
        if(typeItem === ''){
            // logic here
            $('#DetailModalSub').hide();
            $('#tableDetailModalSub').hide();
            $('#detailSubRemoveAll').hide();
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            $('#Qty').data('kendoNumericTextBox').value("");
            $("#AccessoriesEPC").hide();
        }else{
            $('#Qty').data("kendoNumericTextBox").enable(true);
            $("#Qty").data("kendoNumericTextBox").value(0);

            //hide IMEI
            $('#DetailModalSub').hide();
            $('#tableDetailModalSub').hide();
            $('#detailSubRemoveAll').hide();
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            $('#Qty').data('kendoNumericTextBox').value("");
            $("#AccessoriesEPC").show();
        }
    }
    var typeItem2 = $('#ItemGroupName2').val();
    var itemid = $("#ItemID2").val();
    if(typeItem2 === 'IG-00001'){
        // logic here
        $('#AutoIDType2').val(<?php echo SSTYPE; ?>);
         $('#IMEI2').removeAttr("readonly");
         $('#IMEI2').val('<?php if (isset($t8010f011)) { $IMEI = $t8010f011;}else{$IMEI = "";}echo $IMEI; ?>');
    }else{
        if(typeItem2 === ''){
            // logic here
            
        }else{
            $('#IMEI2').val(itemid);
            $('#IMEI2').attr("readonly","1");
            $('#AutoIDType2').val(<?php echo SNTYPE; ?>);
        }
    }
}

function customTriger2(){
    // var length = getDetailItem('detail').length;
    // for (i = 0; i < length; i++) { 
    //     var item = getDetailItem('detail');
    //     var itemExist = item[i].ItemID;
    //     var itemGroupExist = item[i].ItemGroup;
    //     var itemForm = $("#ItemID").val();
    //     if(itemGroupExist == "IG-00001" )
    //     {
    //         if(itemForm == itemExist )
    //         {
    //             new PNotify({ title: "Info", text: itemForm+' is avaiable on detail table', type: 'warning', shadow: true });
    //             $('#submitButtondetail').attr('disabled', 'disabled');
    //         }else{
    //             $("#submitButtondetail").removeAttr("disabled");
    //         }
    //     }else{
    //         $("#submitButtondetail").removeAttr("disabled");
    //     }
    // }
}

function customTriger3(){
    var length = getDetailItem('detailSub').length;
    var IMEIForm = $("#IMEI").val();
    var EPCForm = $("#EPC").val();
    var item = getDetailItem('detailSub');
    var editable =  $("#editable").val();
    for (i = 0; i < length; i++) { 
        var IMEIExist = item[i].IMEI;
        var EPCExist = item[i].EPC;
        if(editable == "yes"){
            
        }else{
            if((IMEIForm != IMEIExist) || (EPCForm != EPCExist)){
            document.getElementById("submitButtondetailSub").disabled = false;
            }
            if(IMEIForm == IMEIExist)
            {
                new PNotify({ title: "Info", text: "IMEI with value "+IMEIForm+' is avaiable on detail table', type: 'warning', shadow: true });
                document.getElementById("submitButtondetailSub").disabled = true;
                $("#IMEI").focus();
            }else{
                document.getElementById("submitButtondetailSub").disabled = false;
            }
            if(EPCForm == EPCExist)
            {
                new PNotify({ title: "Info", text: "EPC with value "+EPCForm+' is avaiable on detail table', type: 'warning', shadow: true });
                document.getElementById("submitButtondetailSub").disabled = true;
                $("#EPC").focus();
            }
        }
    }
}

function clearForm(){
    lsClear();
    $("#submitButtondetail").show();
    $("#ClearForm").attr("style","display:none;");
}

    function GetDataSub(id)
    {
        var voData = {
            RecordID: id
        }; 
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stocktransfer/GetSubItem')+"?editable=1",
            success: function (result) {
                $('#list-detailSub').append(result.html);
                var qty = document.getElementById("list-detailSub").rows.length;
                $('#Qty').data('kendoNumericTextBox').value(qty);
                var target = "detailSub";
                var RowIndex = $("#RowIndex").data("kendoNumericTextBox").value();
                var htmlUpdate = $('#list-'+target).html();
                var ID = current_url()+target+RowIndex;
                localStorage[ID] = htmlUpdate;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#detailSubForm").kendoWindow({
            width: "500px",
            title: "Serial NO",
            visible: false,
            modal: true,
            actions: [
            "Close"
            ],
        });

        $("#kgSerialNo").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                    },
                },
                sync: function(e) {
                    // $("#kgLogStocking").data("kendoGrid").dataSource.read();
                    // $("#kgLogStocking").data("kendoGrid").refresh();
                },
                schema: {
                    data: function(datas){
                        return datas.data;
                    },
                    total: function(datas){
                        return datas.count;
                    },
                    model: {
                        id: "RecordID",
                    }
                },                        
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
                autoBind: false,
                sortable: true,
                pageable: true,
                groupable: true,
                resizable: true,
                selectable: true,
                scrollable: true,
                reorderable:true,
                filterable: {
                    mode: "row",
                },
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                    height: "500px",
                    width: "100%",
                    columns: [
                        {"field":"<?php echo T_TransactionStockBalanceDetail_SerialNo; ?>","title":"Serial NO","width":"50px",},
                        {"field":"<?php echo T_TransactionStockBalanceDetail_IMEI; ?>","title":"IMEI","width":"50px",}
                    ],
            });

    });
    function openDetailModal()
    {
        var id = $("#RecordIDDetail2").val();
        $.ajax(
        {
            type: 'GET',
            url: site_url("Webservice/Read/Getlist"),
            dataType: 'json',
            data: { table: "t1992", customfilter: {t1992f001:id,t1992f003:1}  },
            success: function (result) {
                $("#kgSerialNo").data("kendoGrid").dataSource.data(result.data);
            }
        });
    }

    function dblclickSerialNo(e)
    {
        var grid = $("#kgSerialNo").data("kendoGrid");
        var voRow = grid.dataItem(grid.select());
        var SNexist = getTotal('detailSub','string',2);
        if(SNexist){
            if(SNexist.indexOf(voRow.t1992f002) == -1){
                GetDataSub(voRow.t1992r001);
            }
        }else{
            GetDataSub(voRow.t1992r001);
        }
    }

    function insertMasterItem()
    {
        var voData = {
            ItemID: $('#ItemID2').val(),
            ItemName: $('#ItemName2').val(),
            UOMID: $('#UOMID2').val(),
            AutoIDType: $('#AutoIDType2').val(),
            EPC: $('#EPC2').val(),
            Barcode: $('#Barcode2').val(),
            UnitPrice: $('#UnitPrice2').val(),
            Picture: $('#photo').val(),
            GroupID: $('#ItemGroupName2').val(),
            Color: $('#Color2').val(),
            IMEI: $('#IMEI2').val(),
            Brand: $('#Brand2').val(),
            Status: $('#Status2').val(),
            SellingPrice: $('#SellingPrice2').val(),
            Remarks: $('#Remarks2').val(),
            Model: $('#Model2').val(),

        };
        var valid = checkForm2(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Item/insert'),
                beforeSend: function(){
                    $('#submitButtondetail2').attr('disabled', 'disabled');
                    $('#submitButtondetail2').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
                },
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    $('#submitButtondetail2').html('Success');
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    $("#detailItemForm").data("kendoWindow").center().close();
                    //window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

    function autoLocation(){
        var sampledata = 1;
        var location = $("#LocationID").val();
        $.ajax({
            type: "POST",
            data: sampledata,
            url:  site_url("Stockmovement/Stockout/autoLocation"),
            success: function (result) {
                if(location == ""){
                    // $("#LocationID").val(result.data.t8030f001);
                    // $("#LocationName").val(result.data.t8030f002);
                }else{

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                new PNotify({ title: "Warning", text: jQuery.parseJSON(jqXHR.responseText), type: "Warning", shadow: true });
            }
        });
    }

function scanTag(id)
{
    var msg ="";
    var voData = {
        id: id,
    };
    var init = current_url()+"ReaderStatus";
    localStorage[init] = id;
    if(id){
        var msg = {
            Title : "Web Triger",
            Text : "Start Scan"
        }
        var typeData = { broadType : "Gate Start", data : msg};
        conn.sendMsg(typeData);
        $("#scanTagStart").hide();
        $("#scanTagStop").show();
    }else{
        var msg = {
            Title : "Web Triger",
            Text : "Stop Scan"
        }
        var typeData = { broadType : "Gate Stop", data : msg};
        conn.sendMsg(typeData);
        $("#scanTagStart").show();
        $("#scanTagStop").hide();
    }
}
function getdataScanSocket(epc)
{
    var msg ="";
    var rowCount = document.getElementById("list-detail").rows.length + 1;
    $('#list-detailSub').html("");
    var dataTable = localStorage[current_url()+"detailSub"+rowCount];
    if(dataTable){
        $('#list-detailSub').html(dataTable);
    }
    var subID = document.getElementById("list-detailSub").rows.length + 1;
    var voData = {
        id: rowCount,
        subID: subID,
        RFID: epc
    };
    if(epc != ""){
            $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stockout/GetItem'),
            success: function (result) {
                PNotify.removeAll();
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    var ITEMexist = getItemExist('detail','string',2,result.Data.t8010f001);
                    $("#RecordIDDetail2").val(result.Data.t1990r001);
                    if(ITEMexist.data){
                        if(ITEMexist.data.indexOf(result.Data.t8010f001) == -1){
                            $("#list-detail").append(result.html);
                            // save local storage detail
                            toPagination("detail");
                            var htmlUpdate = $('#list-detail').html();
                            var ID = current_url()+"detail";
                            localStorage[ID] = htmlUpdate;

                            $("#list-detailSub").append(result.htmlsub);

                            // save local storage detail sub
                            var rowIndex = parseInt(ITEMexist.id) + 1;
                            var htmlUpdate = $('#list-detailSub').html();
                            var ID = current_url()+"detailSub"+rowIndex;
                            localStorage[ID] = htmlUpdate;
                        }else{
                            var curQty = parseInt($("#detailQtyv-"+ITEMexist.id).attr("data-val")) +1;
                            $("#detailQtyv-"+ITEMexist.id).attr("data-val",curQty);
                            $("#detailQtyv-"+ITEMexist.id).attr("data-def",curQty);
                            $("#detailQtyv-"+ITEMexist.id).html(curQty);
                            blinkDIVOn("detail-"+ITEMexist.id);
                            
                            var dataTable = localStorage[current_url()+"detailSub"+ITEMexist.id];
                            if(dataTable){
                                $('#list-detailSub').html(dataTable);
                            }
                            $("#list-detailSub").append(result.htmlsub);

                            // save local storage detail sub
                            var htmlUpdate = $('#list-detailSub').html();
                            var ID = current_url()+"detailSub"+ITEMexist.id;
                            localStorage[ID] = htmlUpdate;
                        }
                    }else{
                        $("#list-detail").append(result.html);
                        // save local storage detail
                        toPagination("detail");
                        var htmlUpdate = $('#list-detail').html();
                        var ID = current_url()+"detail";
                        localStorage[ID] = htmlUpdate;

                        $("#list-detailSub").append(result.htmlsub);

                        // save local storage detail sub
                        var htmlUpdate = $('#list-detailSub').html();
                        var ID = current_url()+"detailSub1";
                        localStorage[ID] = htmlUpdate;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });          
    }
}
function getItemExist(target,type,ts,voItem) {
    var result = 0;
    var subTotal = 0;
    var res = [];
    var rowCount = document.getElementById("list-" + target).rows.length - 1;
    var id = rowCount+1;
    for (var i = 0; i <= rowCount; i++) {
        var itemcel = $('#list-' + target + ' tr').eq(i);
        res.push(itemcel.find('td').eq(ts).attr("data-val"));
        if(itemcel.find('td').eq(ts).attr("data-val") == voItem){
            id = i+1;
        }
        var result = {
            data : res,
            id : id
        }
    }
    return result;
}
function blinkDIVOn(target) {
    var timer = "";
    if (timer) clearTimeout(timer);
 
    document.getElementById(target).style.background = "gold";
    timer = setTimeout("blinkDIVOff('"+target+"')", 500);
}
 
function blinkDIVOff(target) {
    var timer = "";
    if (timer) clearTimeout(timer);
 
    document.getElementById(target).style.background = "beige";
    //timer = setTimeout("blinkDIVOn('"+target+"')", 500);
}
function getdataScan()
{
    var msg ="";
    var rowCount = document.getElementById("list-detail").rows.length + 1;
    $('#list-detailSub').html("");
    var dataTable = localStorage[current_url()+"detailSub"+rowCount];
    if(dataTable){
        $('#list-detailSub').html(dataTable);
    }
    var subID = document.getElementById("list-detailSub").rows.length + 1;
    var IMEI = $("#imeiscan").val();
    var voData = {
        id: rowCount,
        subID: subID,
        IMEI: IMEI
    };
    if(IMEI != ""){
            $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Stockmovement/Stockout/GetItem'),
            success: function (result) {
                PNotify.removeAll();
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    var ITEMexist = getItemExist('detail','string',2,result.Data.t8010f001);
                    $("#RecordIDDetail2").val(result.Data.t1990r001);
                    if(ITEMexist.data){
                        if(ITEMexist.data.indexOf(result.Data.t8010f001) == -1){
                            $("#list-detail").append(result.html);
                            // save local storage detail
                            toPagination("detail");
                            var htmlUpdate = $('#list-detail').html();
                            var ID = current_url()+"detail";
                            localStorage[ID] = htmlUpdate;

                            $("#list-detailSub").append(result.htmlsub);

                            // save local storage detail sub
                            var rowIndex = parseInt(ITEMexist.id) + 1;
                            var htmlUpdate = $('#list-detailSub').html();
                            var ID = current_url()+"detailSub"+rowIndex;
                            localStorage[ID] = htmlUpdate;
                        }else{
                            var curQty = parseInt($("#detailQtyv-"+ITEMexist.id).attr("data-val")) +1;
                            $("#detailQtyv-"+ITEMexist.id).attr("data-val",curQty);
                            $("#detailQtyv-"+ITEMexist.id).attr("data-def",curQty);
                            $("#detailQtyv-"+ITEMexist.id).html(curQty);
                            blinkDIVOn("detail-"+ITEMexist.id);
                            
                            var dataTable = localStorage[current_url()+"detailSub"+ITEMexist.id];
                            if(dataTable){
                                $('#list-detailSub').html(dataTable);
                            }
                            $("#list-detailSub").append(result.htmlsub);

                            // save local storage detail sub
                            var htmlUpdate = $('#list-detailSub').html();
                            var ID = current_url()+"detailSub"+ITEMexist.id;
                            localStorage[ID] = htmlUpdate;
                        }
                    }else{
                        $("#list-detail").append(result.html);
                        // save local storage detail
                        toPagination("detail");
                        var htmlUpdate = $('#list-detail').html();
                        var ID = current_url()+"detail";
                        localStorage[ID] = htmlUpdate;

                        $("#list-detailSub").append(result.htmlsub);

                        // save local storage detail sub
                        var htmlUpdate = $('#list-detailSub').html();
                        var ID = current_url()+"detailSub1";
                        localStorage[ID] = htmlUpdate;
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });          
    }
}
</script>
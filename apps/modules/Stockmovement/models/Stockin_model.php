<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stockin_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter, $TransDate) {
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        if($Customfilter != ''){
            $last = '';
            foreach ($Customfilter as $col => $val) {
                $unix = explode('_', $col);
                if(is_array($unix))
                {
                    $this->db->where_in($unix[0],$val);
                }else{
                    $this->db->where($col,$val);
                }
            }
        }
        if($TransDate != ''){
            $this->db->like(T_TRANSINOUT_Date,$TransDate,'after');
        }
        
        return $this->db->get($table);
    }

    public function getListCount($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter, $TransDate){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            //$this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                }
            }
        }
        if($Customfilter != ''){
            $last = '';
            foreach ($Customfilter as $col => $val) {
                $unix = explode('_', $col);
                if(is_array($unix))
                {
                    $this->db->where_in($unix[0],$val);
                }else{
                    $this->db->where($col,$val);
                }
            }
        }
        if($TransDate != ''){
            $this->db->like(T_TRANSINOUT_Date,$TransDate,'after');
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function parseFilters( $filters , $count = 0 ) { 
        $where = "";   
        $intcount = 0;
        $noend= false;
        $nobegin = false;

        if ( isset( $filters['filters'] ) ) {       
            $itemcount = count( $filters['filters'] );
            if ( $itemcount == 0 ) {
                $noend= true;
                $nobegin = true;
            } elseif ( $itemcount == 1 ) {
                $noend= true;
                $nobegin = true;          
            } elseif ( $itemcount > 1 ) {
                $noend= false;
                $nobegin = false;          
            }
            foreach ( $filters['filters'] as $key => $filter ) {
                if ( isset($filter['field'])) {
                    switch ( $filter['operator'] ) {
                        case 'startswith':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "%' ";
                            break;
                        case 'contains':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = " '%" . $filter['value'] . "%' ";
                            break;
                        case 'doesnotcontain':
                            $compare = " NOT LIKE ";
                            $field = $filter['field'];
                            $value = " '%" . $filter['value'] . "%' ";
                            break;
                        case 'endswith':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = "'%" . $filter['value'] . "' ";
                            break;
                        case 'eq':
                            $compare = " = ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'gt':
                            $compare = " > ";
                            $field = $filter['field'];
                            $value = $filter['value'];
                            break;
                        case 'lt':
                            $compare = " < ";
                            $field = $filter['field'];
                            $value = $filter['value'];
                            break;
                        case 'gte':
                            $compare = " >= ";
                            $field = $filter['field'];
                            $value = $filter['value'];
                            break;
                        case 'lte':
                            $compare = " <= ";
                            $field = $filter['field'];
                            $value = $filter['value'];
                            break;
                        case 'neq':
                            $compare = " <> ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                    }               
                    if ( $count == 0 && $intcount == 0 ) {
                        $before = "";
                        $end = " " . $filters['logic'] . " ";
                    } elseif ( $count > 0 && $intcount == 0 ) {
                        $before = "";
                        $end = " " . $filters['logic'] . " ";
                    } else {
                        $before = " " . $filters['logic'] . " ";
                        $end = "";
                    }       
                    $where .= ( $nobegin ? "" : $before ) . $field . $compare . $value . ( $noend ? "" : $end );
                    $count ++;
                    $intcount ++;
                } else {
                    $where .= " ( " . parseFilters( $filter , $count ) . " )" ;       
                }     
                $where = str_replace( " or  or " , " or " , $where );
                $where = str_replace( " and  and " , " and " , $where );
            }
        } else {
                $where = " 1 = 1 ";
        }
        return $where;
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arif
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Api Modules
 *
 * Item Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 05.11.2017	
 * @author	    Muhammad Arif
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Item extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Item_model');
	}

	public function getDetail($id='')
    {
    	$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
        $data = $this->Item_model->getDetail($id);
        if(count($data) > 0){
			$info->data = $data;
		}else{
			$info->data = null;
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function getList()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataItem;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Item_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Item_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->data = null;
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function Insert(){
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_MasterDataItem_ItemID   => $input_data["ItemID"],
					T_MasterDataItem_ItemName   => $input_data["ItemName"],
					T_MasterDataItem_UOMID   => $input_data["UOMID"],
					T_MasterDataItem_AutoIDType   => $input_data["AutoIDType"],
					T_MasterDataItem_EPC   => $input_data["EPC"],
					T_MasterDataItem_Barcode   => $input_data["Barcode"],
					T_MasterDataItem_UnitPrice   => $input_data["UnitPrice"],
					T_MasterDataItem_Picture   => $input_data["Picture"],
					T_MasterDataItem_GroupID   => $input_data["GroupID"],
					T_MasterDataItem_Color   => $input_data["Color"],
					T_MasterDataItem_IMEI   => $input_data["IMEI"],
					T_MasterDataItem_Brand   => $input_data["Brand"],
					T_MasterDataItem_Status   => $input_data["Status"],
					T_MasterDataItem_SellingPrice   => $input_data["SellingPrice"],
					T_MasterDataItem_Remarks   => $input_data["Remarks"],
				);
				$this->Item_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		try{
			if(check_column_json(T_MasterDataItem_RecordTimestamp, $input_data["TimeStamp"]) == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_MasterDataItem_RecordID   => $input_data["RecordID"],
					T_MasterDataItem_RecordTimestamp   => $input_data["TimeStamp"],
                    T_MasterDataItem_RecordStatus => 0,
					T_MasterDataItem_ItemID   => $input_data["ItemID"],
					T_MasterDataItem_ItemName   => $input_data["ItemName"],
					T_MasterDataItem_UOMID   => $input_data["UOMID"],
					T_MasterDataItem_AutoIDType   => $input_data["AutoIDType"],
					T_MasterDataItem_EPC   => $input_data["EPC"],
					T_MasterDataItem_Barcode   => $input_data["Barcode"],
					T_MasterDataItem_UnitPrice   => $input_data["UnitPrice"],
					T_MasterDataItem_GroupID   => $input_data["GroupID"],
					T_MasterDataItem_Color   => $input_data["Color"],
					T_MasterDataItem_IMEI   => $input_data["IMEI"],
					T_MasterDataItem_Brand   => $input_data["Brand"],
					T_MasterDataItem_Status   => $input_data["Status"],
					T_MasterDataItem_SellingPrice   => $input_data["SellingPrice"],
					T_MasterDataItem_Remarks   => $input_data["Remarks"],
				);

				$this->Item_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $input_data["RecordID"]
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		try{
			$this->Item_model->Delete($input_data["RecordID"]);
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $input_data["RecordID"]
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
	
}

/* End of file Item.php */
/* Location: ./app/modules/Api/controllers/Item.php */

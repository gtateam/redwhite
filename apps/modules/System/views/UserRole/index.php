<div class="row">
    <div class="col-md-12">
    <section class="panel panel-primary">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            </div>
            <a class="btn btn-primary" href="<?php echo site_url('System/Userrole/form'); ?>">+ Add New</a>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                <tr>
                    <th width="15px">Action</th>
                    <th>Role Name</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if(!empty($data)){
                    foreach ($data as $key => $item) {
                        $tool = '<a href="'.site_url("System/Userrole/form/".$item->{T_SystemUserRole_RecordID}).'"><i class="fa fa-pencil"></i></a><a onclick="RemoveData('.$item->{T_SystemUserRole_RecordID}.');" href="javascript:void(1);" class="delete-row"><i class="fa fa-trash-o"></i></a>';
                        echo "<tr><td class='actions'>".$tool."</td>
                        <td><b>".$item->{T_SystemUserRole_role_name}."</b></td>
                        </tr>";
                    } 
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
    </div>
</div>

<script>
function RemoveData(id)
    {
        var result = confirm("Delete this record?");
        if (result) {
            if(id){
                var voData = {
                    RecordID: id
                };
                $.ajax({
                    type: 'POST',
                    data: voData,
                    url:  site_url('System/Userrole/Delete'),
                    beforeSend: function(){
                    },
                    success: function (result) {
                    if (result.errorcode != 0) {
                        new PNotify({ title: "Failed", text: result.msg, type: 'warning', shadow: true });
                    } else {
                        new PNotify({ title: "Success", text: "Remove data", type: 'success', shadow: true });
                        location.reload();
                    }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jQuery.parseJSON(jqXHR.responseText));
                    }
                });
            }
        }
    }
</script>
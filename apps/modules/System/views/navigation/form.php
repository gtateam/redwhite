<div class="row">
    <div class="col-md-12">
        <?php echo form_open('',array('id'=>'form-navigation', 'class'=>'form-horizontal')); ?>
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
                        <a data-panel-dismiss="" class="panel-action panel-action-dismiss" href="#"></a>
                    </div>
                    <h2 class="panel-title">Form Navigation</h2>
                    <p class="panel-subtitle">
                        Manage menu navigation in application.
                    </p>
                </header>
                <div class="panel-body">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Alias</label>
                                    <div class="col-md-6">
                                        <?php
                                        $items = array(
                                        'id' => 'alias',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemNavigation_alias}) ? ${T_SystemNavigation_alias} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                        <input type="hidden" id="RecordID" value="<?php echo isset(${T_SystemNavigation_RecordID}) ? ${T_SystemNavigation_RecordID} : ''; ?>">
                                        <input type="hidden" id="RecordTimestamp" value="<?php echo isset(${T_SystemNavigation_RecordTimestamp}) ? ${T_SystemNavigation_RecordTimestamp} : ''; ?>">
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                  <label class="col-md-3 form-label">Title</label>
                                   <div class="col-md-9">
                                     <div class="input-group">
                                       <span class="input-group-addon primary"> EN </span>
                                       <?php
                                       $title = isset(${T_SystemNavigation_title}) ? unserialize(${T_SystemNavigation_title}) : "";
                                        $items = array(
                                        'id' => 'title-en',
                                        'class' => 'form-control',
                                        'value' => isset($title['en']) ? $title['en'] : ''
                                        );
                                        echo form_input($items);
                                        ?>  
                                     </div>                        
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <label class="col-md-3 form-label">&nbsp;</label>
                                  <div class="col-md-9">
                                      <div class="input-group">
                                       <span class="input-group-addon primary"> ID&nbsp; </span>
                                       <?php
                                        $items = array(
                                        'id' => 'title-id',
                                        'class' => 'form-control',
                                        'value' => isset($title['id']) ? $title['id'] : ''
                                        );
                                        echo form_input($items);
                                        ?>  
                                     </div>                            
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Ordering</label>
                                    <div class="col-md-3">
                                        <?php
                                        $items = array(
                                        'id' => 'ordering',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemNavigation_ordering}) ? ${T_SystemNavigation_ordering} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Publish</label>
                                    <div class="col-md-5">
                                        <?php echo form_dropdown('publish', array( '' => 'Pilih',1 => 'Active', 0 => 'Non Active'), (isset(${T_SystemNavigation_publish}) ? ${T_SystemNavigation_publish} : '') ,'class="form-control select2" id="publish"' ); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-3 form-label">Description</label>
                                    <div class="col-md-9">
                                        <?php
                                        $items = array(
                                        'id' => 'description',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemNavigation_description}) ? ${T_SystemNavigation_description} : ''
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Icon</label>
                                    <div class="col-md-5">
                                        <?php
                                        $items = array(
                                        'id' => 'icon',
                                        'class' => 'form-control',
                                        'value' => isset(${T_SystemNavigation_icon}) ? ${T_SystemNavigation_icon} : '' 
                                        );
                                        echo form_input($items);
                                        ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="col-md-3 form-label">Category</label>
                                    <div class="col-md-5">
                                        <?php if(isset(${T_SystemNavigation_icon}) && ${T_SystemNavigation_depth} == '0'){
                                        	echo form_dropdown('category_id', array( 0 => 'Pilih',1 => 'Parent', 2 => 'Sub Parent', 3 => 'Sub'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="category_id" onchange="set_cat(this.value);"' );
                                        }else{
                                        	echo form_dropdown('category_id', array( 0 => 'Pilih',1 => 'Parent', 2 => 'Sub Parent', 3 => 'Sub'), (isset($category_id) ? $category_id : '') ,'class="form-control select2" id="category_id" onchange="set_cat(this.value);"' );
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div id="sub"></div>
                                <div id="subParent"></div>
                            </div>
                        </div>
                </div>
                <footer class="panel-footer">
                    <button class="btn btn-primary" type="submit">Submit </button>
                    <button class="btn btn-default" type="button" onclick="goBack();">Cancel</button>
                </footer>
            </section>
        <?php echo form_close(); ?>
    </div>
</div>
<script>
    $('#form-navigation').submit(function(e){
        e.preventDefault();
        var title = {
            id : $('#title-id').val(),
            en : $('#title-en').val()
        };
        var json_data = {
            RecordID : $('#RecordID').val(),
            RecordTimestamp : $('#RecordTimestamp').val(),
            alias : $('#alias').val(),
            title : title,
            publish: $("#publish").val(),
            description : $('#description').val(),
            icon : $('#icon').val(),
            depth : $('#depth').val(),
            subParent : $('#subParentf').val(),
            ordering : $('#ordering').val(),
            type : 2
        };
        if($('#RecordID').val()){
        	var url = site_url('System/Navigation/Update');
        }else{
        	var url = site_url('System/Navigation/Insert');
        }
        $.ajax({
            type: 'POST',
            data:json_data,
            url: url,
            dataType: 'JSON',
            beforeSend: function(){
                $('.btn-primary').attr('disabled', 'disabled');
                $('.btn-primary').html('<img width="15px" src="assets/img/loading.gif" /> Loading...');
            },
            success: function(data){
                if(data.errorcode < 1){
                    $(".btn-success").prop("disabled", false);
                    setTimeout(function(){
                        $('.btn-primary').html('Success');
                        new PNotify({ title: "Success", text: data.msg, type: 'success', shadow: true });
                        window.location.replace(site_url('System/Navigation'));
                    }, 3000);
                }else{
                    new PNotify({ title: "Error", text: data.msg, type: 'error', shadow: true });
                }
            } 
        });
    });

    function set_cat(id)
    {
        var json_data = {
            id : id
        };
        $.ajax({
            type: 'POST',
            data:json_data,
            url: "<?php echo site_url('System/Navigation/Set_cat'); ?>",
            dataType: 'JSON',
            beforeSend: function(){

            },
            success: function(data){
                if(data.errorcode < 1){
                    $("#sub").html(data.form);
                }else{
                    alert(data.msg);
                }
            } 
        });
    }

    function setsubParent(id)
    {
        var json_data = {
            type : $("#category_id").val(),
            id : id
        };
        $.ajax({
            type: 'POST',
            data:json_data,
            url: "<?php echo site_url('System/Navigation/Set_sub'); ?>",
            dataType: 'JSON',
            beforeSend: function(){

            },
            success: function(data){
                if(data.errorcode < 1){
                    $("#subParent").html(data.form);
                }else{
                    alert(data.msg);
                }
            } 
        });
    }
    $( document ).ready(function() {
        <?php if(isset(${T_SystemNavigation_depth})):
        if(${T_SystemNavigation_depth} != 0): ?>
            set_cat(2);
            setTimeout(function(){
                $("#depth").val(<?php echo ${T_SystemNavigation_depth}; ?>);
                $("#category_id").val(2);
            }, 2000);
        <?php else: ?>
            $("#category_id").val(1);
        <?php
        endif;
        endif;
        ?>
    });
</script>
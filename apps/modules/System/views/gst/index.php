<div class="row">
    <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
                        <a data-panel-dismiss="" class="panel-action panel-action-dismiss" href="#"></a>
                    </div>
                    <h2 class="panel-title">System GST</h2>
                    <p class="panel-subtitle">
                        Manage GST in application.
                    </p>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-3 form-label">GST</label>
                                <div class="col-md-6">
                                    <?php
                                    $items = array(
                                    'id' => 'GST',
                                    'class' => '',
                                    'value' => isset($GST) ? $GST : ''
                                    );
                                    echo form_input($items);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="panel-footer">
                    <button class="btn btn-primary" type="button" onclick="update();">Save </button>
                    <button class="btn btn-default" type="button" onclick="goBack();">Cancel</button>
                </footer>
            </section>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $("#GST").kendoNumericTextBox({
        format: "p0",
        min: 0,
        max: 1,
        step: 0.01
    });
});

function update()
{
    var voData = {
        GST: $('#GST').val()
    };
    var valid = checkForm(voData);
    if(valid.valid)
    {
        $.ajax({
            type: 'POST',
            data: voData,
            url:  site_url('System/Gst/update'), 
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    var ID = current_url();
                    localStorage[ID] = "";
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }else{
        new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
    }
}

function checkForm(voData) {
    var valid = 1;
    var msg = "";
    if (voData.GST == "") { valid = 0; msg += "GST is required" + "\r\n"; }
    var voRes = {valid: valid, msg: msg};
    return voRes;
}
</script>
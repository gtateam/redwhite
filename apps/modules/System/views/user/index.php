<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_SystemUser_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_SystemUser_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_SystemUser_RecordStatus  => array(0,0,'50px','RS',1,'string',1),
    T_SystemUserMeta_photo  => array(1,1,'80px','Photo',0,'picture',1),
    T_SystemUserMeta_first_name  => array(1,1,'100px','First Name',0,'string',1),
    T_SystemUserMeta_last_name  => array(1,1,'100px','Last Name',0,'string',1),
    T_SystemUserMeta_phone  => array(1,1,'100px','Phone',0,'string',1),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'actBTN' => "80px",
    'postBTN' => "0",
    'table' => T_SystemUser,
    'tools' => array(T_SystemUser_RecordID,T_SystemUser_RecordTimestamp,T_SystemUser_RecordStatus,T_SystemUserMeta_photo,T_SystemUserMeta_first_name,T_SystemUserMeta_last_name,T_SystemUserMeta_phone),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'System/User/insert',
        'read' => 'Webservice/Read/getlist',
        'update' => 'System/User/update',
        'destroy' => 'System/User/delete',
        'form' => 'System/User/form',
        'post' => 'System/User/post',
        'unpost' => 'System/User/unPost'
    )
);
// generate gridView
echo onlyGridView($attr);
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Web Services Module
 *
 * Documentperiod Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 07.04.2017	
 * @author	    Muhammad Arif
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Documentperiod extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Documentperiod_model');
	}

    public function index()
    {
        $info = new stdClass();
		$info->msg = "Webservice API v1.0";
		$info->errorcode = 0;
        $this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

    public function getList()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_SystemDocumentPeriod;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Documentperiod_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Documentperiod_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function Insert(){
		$this->db->trans_begin();
		try{
            $PRI = $this->input->post("PRI");
			$this->db->where(T_SystemDocumentType_RecordID,$PRI);
            $sql = $this->db->get(T_SystemDocumentType);
            $parentData = $sql->num_rows();
			if ($parentData == 0)
			{
				$output = array('errorcode' => 200, 'msg' => 'undefined data parent for PRI ('.$PRI.').');
			}else{
				$data = array(
					T_SystemDocumentPeriod_PRI   => $PRI,
					T_SystemDocumentPeriod_Period   => $this->input->post("Period"),
					T_SystemDocumentPeriod_LastNo   => $this->input->post("LastNo"),
					T_SystemDocumentPeriod_IsCurrent   => $this->input->post("IsCurrent")
				);
				$this->Documentperiod_model->Insert($data);

				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		$this->db->trans_begin();
		try{
			if(check_column(T_SystemDocumentPeriod_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemDocumentPeriod_RecordID   => $this->input->post("RecordID"),
					T_SystemDocumentPeriod_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemDocumentPeriod_PRI   => $this->input->post("PRI"),
					T_SystemDocumentPeriod_Period   => $this->input->post("Period"),
					T_SystemDocumentPeriod_LastNo   => $this->input->post("LastNo"),
					T_SystemDocumentPeriod_IsCurrent   => $this->input->post("IsCurrent")
				);
				$this->Documentperiod_model->Update($data);
                
				
				$this->db->trans_commit();
				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$this->db->trans_rollback();
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Documentperiod_model->Delete($this->input->post("RecordID"));

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Documentperiod.php */
/* Documentperiod: ./app/modules/master/controllers/Documentperiod.php */

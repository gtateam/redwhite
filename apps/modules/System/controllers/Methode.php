<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * Methode Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Methode extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Methode_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Methode/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Methode_model->getDetail($id);
		$this->modules->render('/Methode/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Methode_model->getDetail($id);
		$this->modules->render('/Methode/formDetail', $data);
	}

	public function Insert(){
		try{
			
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemMethode_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_SystemMethode_id   => $this->input->post("id"),
					T_SystemMethode_controller_id   => $this->input->post("controller_id"),
					T_SystemMethode_alias   => $this->input->post("alias"),
					T_SystemMethode_title   => $this->input->post("title"),
					T_SystemMethode_description   => $this->input->post("description"),
					T_SystemMethode_created_status   => $this->input->post("created_status"),
					T_SystemMethode_status   => $this->input->post("status"),

					'detail'			=> $this->input->post("detail"),
				);
				$this->Methode_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemMethode_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemMethode_RecordID   => $this->input->post("RecordID"),
					T_SystemMethode_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemMethode_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemMethode_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemMethode_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemMethode_id   => $this->input->post("id"),
					T_SystemMethode_controller_id   => $this->input->post("controller_id"),
					T_SystemMethode_alias   => $this->input->post("alias"),
					T_SystemMethode_title   => $this->input->post("title"),
					T_SystemMethode_description   => $this->input->post("description"),
					T_SystemMethode_created_status   => $this->input->post("created_status"),
					T_SystemMethode_status   => $this->input->post("status"),

					'detail' => $this->input->post("detail"), 
					);

				$this->Methode_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Methode_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Methode.php */
/* Location: ./app/modules/System/controllers/Methode.php */

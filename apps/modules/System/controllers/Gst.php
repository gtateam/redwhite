<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arif
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * Gst Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Muhammad Arif
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Gst extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$sql = $this->db->get(T_SystemGST);
		$data['GST'] = $sql->first_row()->{T_SystemGST_Value};
		$this->modules->render('/gst/index',$data);
	}

	public function update()
	{
		try{
			$text = floatval($_POST['GST']) * 100;
			$data = array(T_SystemGST_Value => $_POST['GST'],T_SystemGST_Text => $text."%");
			$this->db->update(T_SystemGST,$data);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Navigation.php */
/* Location: ./app/modules/System/controllers/Navigation.php */

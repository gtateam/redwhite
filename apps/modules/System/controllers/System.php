<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * System Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class System extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('System_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/System/index',$data);
	}

	public function form($id='')
	{
		$data = $this->System_model->getDetail($id);
		$this->modules->render('/System/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->System_model->getDetail($id);
		$this->modules->render('/System/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemSystem_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_SystemSystem_id   => $this->input->post("id"),
					T_SystemSystem_app_name   => $this->input->post("app_name"),
					T_SystemSystem_company   => $this->input->post("company"),
					T_SystemSystem_serial_number   => $this->input->post("serial_number"),
					T_SystemSystem_registered   => $this->input->post("registered"),

					'detail'			=> $this->input->post("detail"),
				);
				$this->System_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemSystem_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemSystem_RecordID   => $this->input->post("RecordID"),
					T_SystemSystem_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemSystem_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemSystem_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemSystem_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemSystem_id   => $this->input->post("id"),
					T_SystemSystem_app_name   => $this->input->post("app_name"),
					T_SystemSystem_company   => $this->input->post("company"),
					T_SystemSystem_serial_number   => $this->input->post("serial_number"),
					T_SystemSystem_registered   => $this->input->post("registered"),

					'detail' => $this->input->post("detail"), 
					);

				$this->System_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->System_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file System.php */
/* Location: ./app/modules/System/controllers/System.php */

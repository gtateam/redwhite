<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * DeliveryOrder Model
 *
 * @package     App
 * @subpackage  Modules
 * @category    Module Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Deliveryorder_model extends CI_Model
{
    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataLocation_".$col);
                $this->db->where($col,$val);
            }
        }
        $this->db->select(T_MasterDataLocation_RecordID." AS 'RecordID',".T_MasterDataLocation_RecordTimestamp." AS 'RecordTimestamp',".T_MasterDataLocation_RecordStatus." AS 'RecordStatus',".T_MasterDataLocation_LocationID." AS 'LocationID.',".T_MasterDataLocation_LocationName." AS 'LocationName'");
        return $this->db->get($table);
    }

    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataLocation_".$col);
                $this->db->where($col,$val);
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function getDetail($id){
        $this->db->where(T_TransactionDO_RecordID,$id);
        $query = $this->db->get(T_TransactionDO);
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($id);
        }
        return $data;
    }

    public function getDetailItem($id)
    {
        $this->db->where(T_TransactionDODetail_PRI,$id);
        $this->db->order_by(T_TransactionDODetail_RecordID);
        $query = $this->db->get(T_TransactionDODetail);
        $data = $query->result("array");
        return $data;
    }

    public function Insert($data){
        $this->db->trans_begin();
        $detail = $data['detail'];
        unset($data['detail']);
        $this->db->insert(T_TransactionDO, $data);
        $parentID = $this->db->insert_id();
        foreach($detail as $item){
            $val = array(
				T_TransactionDODetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_TransactionDODetail_PRI => $parentID,
				T_TransactionDODetail_EPC => $item["EPC"]

            );
            $this->db->insert(T_TransactionDODetail,$val);
        }
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            //UpdateDocNo($data[T_SalesPaymentHeader_DocTypeID]);
            $this->db->trans_commit();
        }
    }

    public function Update($data){
        $this->db->trans_begin();
        $detail = $data["detail"];
        unset($data["detail"]);
        $this->db->where(T_TransactionDO_RecordID, $data[T_TransactionDO_RecordID]);
        $this->db->update(T_TransactionDO,$data);
        $this->db->delete(T_TransactionDODetail, array(T_TransactionDODetail_PRI => $data[T_TransactionDO_RecordID]));
        foreach($detail as $item){
            $val = array(
                T_TransactionDODetail_RecordTimestamp => date("Y-m-d g:i:s",now()),
				T_TransactionDODetail_PRI => $data[T_TransactionDO_RecordID],
				T_TransactionDODetail_EPC => $item["EPC"]

            );
            $this->db->insert(T_TransactionDODetail,$val);
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_TransactionDO, array(T_TransactionDO_RecordID => $id));
        $this->db->delete(T_TransactionDODetail, array(T_TransactionDODetail_PRI => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Location Model
 *
 * @package     App
 * @subpackage  Modules
 * @category    Module Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Location_model extends CI_Model
{
    public function getDetail($id)
    {
        $this->db->where(T_MasterDataLocation_RecordID,$id);
        $query = $this->db->get(T_MasterDataLocation);
        //echo $this->db->last_query();die;
        $data = $query->first_row("array");
        return $data;
        //print_r($data);die;
    } 

    public function getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataLocation_".$col);
                $this->db->where($col,$val);
            }
        }
        $this->db->select(T_MasterDataLocation_RecordID." AS 'RecordID',".T_MasterDataLocation_RecordTimestamp." AS 'RecordTimestamp',".T_MasterDataLocation_RecordStatus." AS 'RecordStatus',".T_MasterDataLocation_LocationID." AS 'LocationID.',".T_MasterDataLocation_LocationName." AS 'LocationName'");
        return $this->db->get($table);
    }

    public function getLocation($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $customfilter){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
        }else{
            if ($searchignoreCase == TRUE) {
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->order_by($sortfield, $sortdir);
                    
                }
            }
        }
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataLocation_".$col);
                $this->db->where($col,$val);
            }
        }
        return $this->db->get($table);
    }

    public function getListCount($table,$customfilter=null){
        if($customfilter != ''){
            foreach ($customfilter as $col => $val) {
                $col = constant("T_MasterDataLocation_".$col);
                $this->db->where($col,$val);
            }
        }
        $query = $this->db->get($table);
        $rowcount = $query->num_rows();
        return $rowcount;
    }  

    public function Insert($data){
    $this->db->trans_begin();
        unset($data[0]);
        $this->db->insert(T_MasterDataLocation, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            UpdateAutoItemID("LC");
            $this->db->trans_commit();
        }
    }

    public function Update($data){
    $this->db->trans_begin();
        $this->db->where(T_MasterDataLocation_RecordID,$data[T_MasterDataLocation_RecordID]);
        $this->db->update(T_MasterDataLocation, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($data){
    $this->db->trans_begin();
        $this->db->where(T_MasterDataLocation_RecordID,$data);
        $this->db->delete(T_MasterDataLocation);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

}

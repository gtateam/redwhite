<style>
hr {
    background: grey none repeat scroll 0 0;
    border: 0 none;
    height: 2px;
    margin: 10px 0;
}
</style>
    <div class="invoice">
        <header class="clearfix">
            <div class="row">
                <div class="col-sm-12 text-center mt-md mb-md">
                    <h3>Red White Mobile</h3>
                    <p>420 North Bridge Road, #01-20 North Bridge Centre, Singapura 188727</p>
                    <p>Phone : +65 6735 4811</p>
                </div>
            </div>
        </header>
        <div class="bill-info">
            <div class="row">
                <div class="col-md-6">
                    <div class="bill-data text-left">
                        <p class="mb-none">
                            <span class="text-dark">Date:</span>
                            <span class="value"><?php echo date("Y-m-d H:i:s"); ?></span>
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="bill-data text-right">
                        <p class="mb-none">
                            <!-- <span class="text-dark">Doc No:</span>
                            <span class="value"><?php //echo ${T_MasterData_DocNo}; ?></span> -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="row">
                <div class="col-sm-12 text-center mt-md mb-md">
                    <h4>MASTER ITEM</h4>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table invoice-items table-condensed">
                <thead id="head-detail">
                    <tr>
                        <th width="30px" data-col="RowIndex">#</th>
                        <th width="120px" data-col="ItemID">Item ID</th>
                        <th width="100px" data-col="Brand">Brand</th>
                        <th width="200px" data-col="ItemName">Model</th>
                        <th width="80px" data-col="Color">Color</th>
                        <th width="80px" data-col="Color">Status</th>
                        <th width="80px" data-col="ItemType">Item Type</th>
                        <th width="100px" data-col="CostPrice">Cost Price</th>
                        <th width="200px" data-col="SellingPrice">Selling Price</th>
                        <th width="180px" data-col="IMEI">IMEI</th>
                        <th data-col="RecordIDDetail" style="display:none;"></th>
                        <th data-col="RecordFlag" style="display:none;"></th>
                    </tr>
                </thead>
                <tbody id="list-detail">
                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div class="bill-data text-Left">
                <p class="mb-none">
                    <span class="text-dark">Remarks:</span>
                </p>
            </div>
        </div>
       
    </div>

    <script>
        //window.print();
    </script>

<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataLocation_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataLocation_RecordID}) ? ${T_MasterDataLocation_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataLocation_RecordTimestamp}) ? ${T_MasterDataLocation_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Location ID</label>
                            <div class="col-md-6">
                                <?php $items=array( 
                                    'id'=> 'LocationID', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'value' => isset(${T_MasterDataLocation_LocationID}) ? ${T_MasterDataLocation_LocationID} : AutoItemID('LC'),
                                    'readonly' => "true"
                                ); 
                                //if(!isset(${T_MasterDataLocation_ID})){ unset($items['readonly']); }
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                    </div>
                    <div class="col-md-6" style="margin-bottom:30px;">
                        <div class="row">
                            <label class="col-md-3 form-label">Location Name</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'LocationName',
                                    'class' => 'k-input k-textbox',
                                    'value' => isset(${T_MasterDataLocation_LocationName}) ? ${T_MasterDataLocation_LocationName} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_MasterDataLocation_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>

<?php
if (isset($t8030r001)) {
    $ID = $t8030r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataLocation_RecordID}) ? ${T_MasterDataLocation_RecordID} : 0; ?>;
$(document).ready(function() {
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            ID: $('#LocationID').val(),
            Name: $('#LocationName').val()
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Location/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        TimeStamp: $('#TimeStamp').val(),
        ID: $('#LocationID').val(),
        Name: $('#LocationName').val()
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Masterdata/Location/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Masterdata/Location/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ID == "") { valid = 0; msg += "Location ID ID is required" + "\r\n"; }
    //if (voData.Name == "") { valid = 0; msg += "Item Group Name is required" + "\r\n"; }
    //if (voData.Pictur == "") { valid = 0; msg += "UOMID is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>
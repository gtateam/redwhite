<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataGeneralTable_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataGeneralTable_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataGeneralTable_RecordStatus  => array(0,0,'50px','RS',1,'string',1),
    T_MasterDataGeneralTable_ID  => array(1,1,'100px','General Table ID',1,'string',1),
    T_MasterDataGeneralTable_Name  => array(1,1,'100px','General Table Name',0,'string',1),
);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'actBTN' => "50px",
    'postBTN' => "0",
    'table' => T_MasterDataGeneralTable,
    'tools' => array(T_MasterDataGeneralTable_RecordID,T_MasterDataGeneralTable_RecordTimestamp,T_MasterDataGeneralTable_RecordStatus,T_MasterDataGeneralTable_ID,T_MasterDataGeneralTable_Name),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Generaltable/insert',
        'read' => 'Webservice/Read/getlist',
        'update' => 'Masterdata/Generaltable/update',
        'destroy' => 'Masterdata/Generaltable/delete',
        'form' => 'Masterdata/Generaltable/form',
        'post' => 'Pos/post',
        'unpost' => 'Pos/unPost'
    )
);
// generate gridView
echo onlyGridView($attr);
?>
<script type="text/javascript">
$(document).ready(function() {
    $("#addnewclass").hide();
});
</script>

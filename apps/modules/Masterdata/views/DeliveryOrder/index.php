<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_TransactionDO_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_TransactionDO_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_TransactionDO_DocNo  => array(1,1,'50px','DO Number',1,'string',1)
);
// Column DropdownList => |Text|URL|KEY|
// $dropdownlist = array(
//     T_TransactionDO_NameCode => array('GroupCode','master/DeliveryOrder/GetItemType/1',T_TransactionDO_Name)
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'actBTN' => "50px",
    'postBTN' => "0",
    'table' => T_TransactionDO,
    'tools' => array(T_TransactionDO_RecordID,T_TransactionDO_RecordTimestamp,T_TransactionDO_DocNo),
    'column' => $column,
    //'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/DeliveryOrder/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/DeliveryOrder/Update',
        'destroy' => 'Masterdata/DeliveryOrder/Delete',
        'form' => 'Masterdata/DeliveryOrder/form',
        'post' => 'Pos/post',
        'unpost' => 'Pos/unPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>
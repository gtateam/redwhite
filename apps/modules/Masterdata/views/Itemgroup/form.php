<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataItemGroup_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataItemGroup_RecordID}) ? ${T_MasterDataItemGroup_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataItemGroup_RecordTimestamp}) ? ${T_MasterDataItemGroup_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Item Group ID</label>
                            <div class="col-md-6">
                                <?php $items=array( 
                                    'id'=> 'ItemGroupID', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'value' => isset(${T_MasterDataItemGroup_ID}) ? ${T_MasterDataItemGroup_ID} : AutoItemID('IG'),
                                    'readonly' => "true"
                                ); 
                                //if(!isset(${T_MasterDataItemGroup_ID})){ unset($items['readonly']); }
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Item Group Name</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'ItemGroupName',
                                    'class' => 'k-input k-textbox',
                                    'value' => isset(${T_MasterDataItemGroup_Name}) ? ${T_MasterDataItemGroup_Name} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Unique</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'Unique',
                                    'class' => '',
                                    'value' => isset(${T_MasterDataItemGroup_Unique}) ? ${T_MasterDataItemGroup_Unique} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom:30px;">
                        <div class="row">
                            <label class="col-md-3 form-label">Picture</label>
                            <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataItemGroup_Picture}) ? $meta{T_MasterDataItemGroup_Picture} : ''; ?>">
                            <?php
                                if(isset(${T_MasterDataItemGroup_Picture})){
                                    $voImg = (${T_MasterDataItemGroup_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataItemGroup_Picture};
                                }else{
                                    $voImg = "assets/upload/photo/no-photo.png";
                                }
                            ?>
                            <div class="col-md-6">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                                        <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                                    </div>
                                </a>
                                <form id="form-photo" action="<?php echo site_url('en/Systems/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                    <input style="width:170px" id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataItemGroup_Picture}) ? ${T_MasterDataItemGroup_Picture} : ''; ?>" />
                                    <input name="ItemIDS" id="ItemIDS" value="" type="hidden"/> 
                                    <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                    <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataItemGroup_Picture}) ? ${T_MasterDataItemGroup_Picture} : ''; ?>" />
                                </form>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_MasterDataItemGroup_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>

<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataItemGroup_RecordID}) ? ${T_MasterDataItemGroup_RecordID} : 0; ?>;
$(document).ready(function() {
    var data = [
        { text: "TRUE", value: "TRUE" },
        { text: "FALSE", value: "FALSE" }
    ];

    // create DropDownList from input HTML element
    $("#Unique").kendoDropDownList({
        optionLabel: "Select",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data
    });
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            ID: $('#ItemGroupID').val(),
            Name: $('#ItemGroupName').val(),
            Picture: $('#photo').val()
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Itemgroup/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        TimeStamp: $('#TimeStamp').val(),
        ID: $('#ItemGroupID').val(),
        Name: $('#ItemGroupName').val(),
        Picture: $('#photo').val()
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Masterdata/Itemgroup/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Masterdata/Itemgroup/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ID == "") { valid = 0; msg += "Item Group ID is required" + "\r\n"; }
    //if (voData.Name == "") { valid = 0; msg += "Item Group Name is required" + "\r\n"; }
    //if (voData.Pictur == "") { valid = 0; msg += "UOMID is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>
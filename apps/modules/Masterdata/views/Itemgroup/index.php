
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataItemGroup_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataItemGroup_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataItemGroup_Picture => array(1,1,'35px','Picture',0,'picture',0),
    T_MasterDataItemGroup_ID  => array(1,1,'50px','ID',1,'string',1),
    T_MasterDataItemGroup_Name  => array(1,1,'100px','Name',0,'string',0)
);
// variable attribute for gridview

$attr = array(
    'id'=>'grid',
    'actBTN' => "75px",
    'postBTN' => "0",
    'table' => T_MasterDataItemGroup,
    'tools' => array(
        T_MasterDataItemGroup_RecordID,
        T_MasterDataItemGroup_RecordTimestamp,
        T_MasterDataItemGroup_ID,
        T_MasterDataItemGroup_Name,
        T_MasterDataItemGroup_Picture
    ),
    'column' => $column,
    'url' => array(
        'create' => 'Masterdata/Itemgroup/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Itemgroup/Update',
        'destroy' => 'Masterdata/Itemgroup/Delete',
        'form' => 'Masterdata/Itemgroup/Form',
        'post' => '',
        'unpost' => ''
    )
);
// generate gridView
echo onlygridview($attr); 
?>
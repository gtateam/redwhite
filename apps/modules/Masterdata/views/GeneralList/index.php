<!------------- Begin Code master GridView ------------->
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataGeneralList_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataGeneralList_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataGeneralList_NameCode  => array(1,1,'50px','Name Code',1,'string',1),
    T_MasterDataGeneralList_Name  => array(1,1,'100px','Name',0,'string',0),
    T_MasterDataGeneralList_GroupName  => array(1,1,'100px','Group Name',0,'string',0),
    T_MasterDataGeneralList_GroupCode  => array(1,1,'100px','Group Code',0,'int',0),
    T_MasterDataGeneralList_Publish  => array(0,0,'100px','Status',0,'string',0)
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    T_MasterDataGeneralList_NameCode => array('GroupCode','master/generallist/GetItemType/1',T_MasterDataGeneralList_Name)
);
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_MasterDataGeneralList,
    'tools' => array(T_MasterDataGeneralList_RecordID,T_MasterDataGeneralList_RecordTimestamp,T_MasterDataGeneralList_NameCode,T_MasterDataGeneralList_Name,T_MasterDataGeneralList_GroupName,T_MasterDataGeneralList_GroupCode,T_MasterDataGeneralList_Publish),
    'column' => $column,
    //'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/GeneralList/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/GeneralList/Update',
        'destroy' => 'Masterdata/GeneralList/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>
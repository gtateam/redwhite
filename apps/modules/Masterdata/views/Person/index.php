
<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-size: 40px 38px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
<!------------- Begin Code master GridView ------------->
<?php
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataperson_RecordID  => array(0,0,'100px','Record ID',1,'string',1),
    T_MasterDataperson_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataperson_Picture  => array(0,1,'35px','Photo',1,'picture',1),
    T_MasterDataperson_PersonName  => array(1,1,'100px','Person Name',1,'string',1),
    //T_MasterDataperson_PersonTypeID  => array(0,1,'100px','Type ID',1,'string',1),
    //T_MasterDataperson_IdentityTypeID  => array(1,1,'100px','Identity Type',1,'string',1),
    //T_MasterDataperson_IdentityNo  => array(0,1,'100px','Identity No',1,'string',1),
    T_MasterDataperson_Address  => array(1,1,'100px','Address',1,'string',1),
    T_MasterDataCountry_CountryName  => array(0,1,'100px','Country',1,'string',1),
    //T_MasterDataperson_EPC  => array(1,1,'100px','EPC',1,'string',1),
    //T_MasterDataperson_Barcode  => array(1,1,'100px','Barcode',1,'string',1),
    T_MasterDataperson_Telephone  => array(1,1,'100px','Telephone',1,'string',1),
);
// Column DropdownList => |Text|URL|
    $dropdownlist = array(
    T_MasterDataCountry_RecordID => array('Country','Masterdata/Person/getCountry/',T_MasterDataCountry_CountryName)
);
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'actBTN' => "75px",
    'postBTN' => "0",
    'table' => T_MasterDataperson,
    'tools' => array(
        T_MasterDataperson_RecordID,
        T_MasterDataperson_RecordTimestamp,
        T_MasterDataperson_Picture,
        T_MasterDataperson_PersonName,
        //T_MasterDataperson_PersonTypeID,
        //T_MasterDataperson_IdentityTypeID,
        //T_MasterDataperson_IdentityTypeID,
        //T_MasterDataperson_IdentityNo,
        T_MasterDataperson_Address,
        T_MasterDataperson_Country,
        //T_MasterDataperson_EPC,
        //T_MasterDataperson_Barcode,
        T_MasterDataperson_Telephone,
        ),
    'column' => $column,
    'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Person/insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Person/update',
        'destroy' => 'Masterdata/Person/delete',
        'form' => 'Masterdata/Person/form',
        'post' => 'Masterdata/Person/post',
        'unpost' => 'Masterdata/Person/unPost'
    )
);
// generate gridView
echo onlyGridView($attr)
?>
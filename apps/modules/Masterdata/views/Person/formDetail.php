<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Detail</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataperson_RecordID}) ? ${T_MasterDataperson_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataperson_RecordTimestamp}) ? ${T_MasterDataperson_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Person Name</label>
                            <div class="col-md-6">
                                <?php $items=array( 
                                    'id'=> 'PersonName', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonName}) ? ${T_MasterDataperson_PersonName} : "",
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Address</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'Address',
                                    'class' => 'form-control',
                                    'value' => isset(${T_MasterDataperson_Address}) ? ${T_MasterDataperson_Address} : "",
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Telephone</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Telephone', 
                                    'class'=>'form-control', 
                                    'value' => isset(${T_MasterDataperson_Telephone}) ? ${T_MasterDataperson_Telephone} : "",
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                                </div>
                        </div><br>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Person ID</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'PersonID', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonID}) ? ${T_MasterDataperson_PersonID} : "",  
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Person Type ID</label>
                            <div class="col-md-9">
                                <input id="PersonTypeID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataperson_PersonTypeID}) ? ${T_MasterDataperson_PersonTypeID} : ''; ?>" />
                                <?php $items=array(
                                    'id'=> 'PersonTypeName', 
                                    'class' => 'k-textbox form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonTypeName}) ? ${T_MasterDataperson_PersonTypeName} : "",
                                    //'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Photo</label>
                            <?php
                                if(isset(${T_MasterDataperson_Picture})){
                                    $voImg = (${T_MasterDataperson_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataperson_Picture};
                                }else{
                                    $voImg = "assets/upload/photo/no-photo.png";
                                }
                            ?>
                            <div class="col-md-6">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                                        <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                                    </div>
                                </a>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity Type ID</label>
                            <div class="col-md-9">
                                <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataperson_Picture}) ? $meta{T_MasterDataperson_Picture} : ''; ?>">
                                <input id="IdentityTypeID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataperson_IdentityTypeID}) ? ${T_MasterDataperson_IdentityTypeID} : ''; ?>" />
                                <?php $items=array(
                                    'id'=> 'IdentityTypeName', 
                                    'class' => 'k-textbox form-control', 
                                    'value' => isset(${T_MasterDataperson_IdentityTypeName}) ? ${T_MasterDataperson_IdentityTypeName} : "",
                                    //'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Country</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Country',
                                    'class' => 'form-control',   
                                    //'style' => 'width:150px;', 
                                    'value' => isset(${T_MasterDataperson_Country}) ? ${T_MasterDataperson_Country} : "",  
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity No</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'IdentityNo', 
                                    'class' => 'form-control',
                                    'value' => isset(${T_MasterDataperson_IdentityNo}) ? ${T_MasterDataperson_IdentityNo} : "",
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">State</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'State', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_State}) ? ${T_MasterDataperson_State} : "",
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Barcode</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Barcode', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_Barcode}) ? ${T_MasterDataperson_Barcode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">City</label>
                            <div class="col-md-4">
                                <?php $items=array(
                                    'id'=> 'City', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_City}) ? ${T_MasterDataperson_City} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">EPC</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'EPC', 
                                    'class'=>'form-control', 
                                    'value' => isset(${T_MasterDataperson_EPC}) ? ${T_MasterDataperson_EPC} : "",
                                    'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Postal Code</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'PostalCode', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PostalCode}) ? ${T_MasterDataperson_PostalCode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
            </footer>
        </section>
    </div>
</div>
<?php
$Country = isset(${T_MasterDataperson_Country}) ? ${T_MasterDataperson_Country} : 0;
$State = isset(${T_MasterDataperson_State}) ? ${T_MasterDataperson_State} : 0;
?>
<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataperson_RecordID}) ? ${T_MasterDataperson_RecordID} : 0; ?>;
var Country = "<?php echo $Country; ?>";
var State = "<?php echo $State; ?>";
var Validasi = "<?php echo $validasi; ?>";
$(document).ready(function() {
    if(ID){
        $("#password").hide();
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: Country},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: State},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
    }else{
        $("#ChangePass").hide();
        $("#State").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });

        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    }

    $("#Country").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataCountry_CountryName; ?>",
        dataValueField: "<?php echo T_MasterDataCountry_RecordID; ?>",
        optionLabel: "Select",
        change: setState,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataCountry; ?>'},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
    });

    function setState(e) {
        var dataItem = this.dataItem(e.item);
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: dataItem.<?php echo T_MasterDataCountry_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    };

    function setCity(e) {
        var dataItem = this.dataItem(e.item);
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: dataItem.<?php echo T_MasterDataState_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
    }
});
</script>
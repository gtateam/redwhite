<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataperson_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataperson_RecordID}) ? ${T_MasterDataperson_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataperson_RecordTimestamp}) ? ${T_MasterDataperson_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Person Name</label>
                            <div class="col-md-6">
                                <?php $items=array( 
                                    'id'=> 'PersonName', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonName}) ? ${T_MasterDataperson_PersonName} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Address</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'Address',
                                    'class' => 'form-control',
                                    'value' => isset(${T_MasterDataperson_Address}) ? ${T_MasterDataperson_Address} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Telephone</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Telephone', 
                                    'class'=>'form-control', 
                                    'value' => isset(${T_MasterDataperson_Telephone}) ? ${T_MasterDataperson_Telephone} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                                </div>
                        </div><br>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Person ID</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'PersonID', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonID}) ? ${T_MasterDataperson_PersonID} : "",  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Person Type ID</label>
                            <div class="col-md-9">
                                <input id="PersonTypeID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataperson_PersonTypeID}) ? ${T_MasterDataperson_PersonTypeID} : ''; ?>" />
                                <?php $items=array(
                                    'id'=> 'PersonTypeName', 
                                    'class' => 'k-textbox form-control', 
                                    'value' => isset(${T_MasterDataperson_PersonTypeName}) ? ${T_MasterDataperson_PersonTypeName} : "",
                                    'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventPersonTypeID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Photo</label>
                            <?php
                                if(isset(${T_MasterDataperson_Picture})){
                                    $voImg = (${T_MasterDataperson_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataperson_Picture};
                                }else{
                                    $voImg = "assets/upload/photo/no-photo.png";
                                }
                            ?>
                            <div class="col-md-6">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                                        <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                                    </div>
                                </a>
                                <form id="form-photo" action="<?php echo site_url('en/Systems/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                    <input id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataperson_Picture}) ? ${T_MasterDataperson_Picture} : ''; ?>" />
                                    <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                    <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataperson_Picture}) ? ${T_MasterDataperson_Picture} : ''; ?>" />
                                </form>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity Type ID</label>
                            <div class="col-md-9">
                                <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataperson_Picture}) ? $meta{T_MasterDataperson_Picture} : ''; ?>">
                                <input id="IdentityTypeID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataperson_IdentityTypeID}) ? ${T_MasterDataperson_IdentityTypeID} : ''; ?>" />
                                <?php $items=array(
                                    'id'=> 'IdentityTypeName', 
                                    'class' => 'k-textbox form-control', 
                                    'value' => isset(${T_MasterDataperson_IdentityTypeName}) ? ${T_MasterDataperson_IdentityTypeName} : "",
                                    'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventIdentityID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Country</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Country',
                                    'class' => 'form-control',   
                                    //'style' => 'width:150px;', 
                                    'value' => isset(${T_MasterDataperson_Country}) ? ${T_MasterDataperson_Country} : "",  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Identity No</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'IdentityNo', 
                                    'class' => 'form-control',
                                    'value' => isset(${T_MasterDataperson_IdentityNo}) ? ${T_MasterDataperson_IdentityNo} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">State</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'State', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_State}) ? ${T_MasterDataperson_State} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Barcode</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'Barcode', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_Barcode}) ? ${T_MasterDataperson_Barcode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">City</label>
                            <div class="col-md-4">
                                <?php $items=array(
                                    'id'=> 'City', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_City}) ? ${T_MasterDataperson_City} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">EPC</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'EPC', 
                                    'class'=>'form-control', 
                                    'value' => isset(${T_MasterDataperson_EPC}) ? ${T_MasterDataperson_EPC} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Postal Code</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'PostalCode', 
                                    'class' => 'form-control', 
                                    'value' => isset(${T_MasterDataperson_PostalCode}) ? ${T_MasterDataperson_PostalCode} : "",
                                    //'style' => 'background-color:#eee; ', 
                                    //'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_MasterDataperson_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>
<?php
//Person Type Lookup
    //field in database data to load
    $dataPersonType = array(
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Key', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTableValue_Description, 'title' => 'Description', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnPersonType = array(
        array('id' => 'PersonTypeID', 'column' => T_MasterDataGeneralTableValue_RecordID),
        array('id' => 'PersonTypeName', 'column' => T_MasterDataGeneralTableValue_Description),
    );

    $customfilter = array(
        T_MasterDataGeneralTableValue_PRI => "7",
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("PersonTypeID", "Person Type", "500px", "Webservice/Read/Getlist", $dataPersonType, $columnPersonType,T_MasterDataGeneralTableValue,"",$customfilter);
    
//Person Type Lookup
    //field in database data to load
    $dataIdentity = array(
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Key', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTableValue_Description, 'title' => 'Description', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnIdentity = array(
        array('id' => 'IdentityTypeID', 'column' => T_MasterDataGeneralTableValue_RecordID),
        array('id' => 'IdentityTypeName', 'column' => T_MasterDataGeneralTableValue_Description),
    );

    $customfilter2 = array(
        T_MasterDataGeneralTableValue_PRI => "6",
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("IdentityID", "Identity", "500px", "Webservice/Read/Getlist", $dataIdentity, $columnIdentity,T_MasterDataGeneralTableValue,"",$customfilter2);

?>
<?php
$Country = isset(${T_MasterDataperson_Country}) ? ${T_MasterDataperson_Country} : 0;
$State = isset(${T_MasterDataperson_State}) ? ${T_MasterDataperson_State} : 0;
?>
<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataperson_RecordID}) ? ${T_MasterDataperson_RecordID} : 0; ?>;
var Country = "<?php echo $Country; ?>";
var State = "<?php echo $State; ?>";
var Validasi = "<?php echo $validasi; ?>";
$(document).ready(function() {
    if(ID){
        $("#password").hide();
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: Country},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: State},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
    }else{
        $("#ChangePass").hide();
        $("#State").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });

        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    }

    $("#Country").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataCountry_CountryName; ?>",
        dataValueField: "<?php echo T_MasterDataCountry_RecordID; ?>",
        optionLabel: "Select",
        change: setState,
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataCountry; ?>'},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
    });

    function setState(e) {
        var dataItem = this.dataItem(e.item);
        $("#State").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataState_StateName; ?>",
            dataValueField: "<?php echo T_MasterDataState_RecordID; ?>",
            optionLabel: "Select",
            change: setCity,
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataState; ?>', filter: '<?php echo T_MasterDataState_CountryID; ?>', filval: dataItem.<?php echo T_MasterDataCountry_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
        $("#City").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: [],
            optionLabel: "Select"
        });
    };

    function setCity(e) {
        var dataItem = this.dataItem(e.item);
        $("#City").kendoDropDownList({
            filter: "contains",
            dataTextField: "<?php echo T_MasterDataCity_CityName; ?>",
            dataValueField: "<?php echo T_MasterDataCity_RecordID; ?>",
            optionLabel: "Select",
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: site_url("Webservice/Generallist/getCountry"),
                        data: {table: '<?php echo T_MasterDataCity; ?>', filter: '<?php echo T_MasterDataCity_StateID; ?>', filval: dataItem.<?php echo T_MasterDataState_RecordID; ?>},
                        type: "POST"
                    }
                },
                schema: {
                    data: function(data){
                        return data.data;
                    }
                }
            },
            animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
                }
            }
        });
    }
});

function changePass()
{
    $("#password").show();
    $("#ChangePass").hide();
}

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            PersonName: $('#PersonName').val(),
            PersonID: $('#PersonID').val(),
            PersonTypeID: $('#PersonTypeID').val(),
            PersonTypeName: $('#PersonTypeName').val(),
            IdentityTypeID: $('#IdentityTypeID').val(),
            IdentityTypeName: $('#IdentityTypeName').val(),
            IdentityNo: $('#IdentityNo').val(),
            Picture: $('#photo').val(),
            Address: $('#Address').val(),
            Country: $('#Country').val(),
            State: $('#State').val(),
            City: $('#City').val(),
            PostalCode: $('#PostalCode').val(),
            Telephone: $('#Telephone').val(),
            EPC: $('#EPC').val(),
            Barcode: $('#Barcode').val()
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Person/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        PersonName: $('#PersonName').val(),
        PersonTypeID: $('#PersonTypeID').val(),
        PersonTypeName: $('#PersonTypeName').val(),
        IdentityTypeID: $('#IdentityTypeID').val(),
        IdentityTypeName: $('#IdentityTypeName').val(),
        IdentityNo: $('#IdentityNo').val(),
        Picture: $('#photo').val(),
        PictureExist: $('#PictureExist').val(),
        Address: $('#Address').val(),
        Country: $('#Country').val(),
        State: $('#State').val(),
        City: $('#City').val(),
        PostalCode: $('#PostalCode').val(),
        Telephone: $('#Telephone').val(),
        EPC: $('#EPC').val(),
        Barcode: $('#Barcode').val()
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Masterdata/Person/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Masterdata/Person/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.PersonName == "") { valid = 0; msg += "Person Name is required" + "\r\n"; }
    if (voData.PersonID == "") { valid = 0; msg += "Person ID is required" + "\r\n"; }
    if (voData.PersonTypeID == "") { valid = 0; msg += "Address is required" + "\r\n"; }
    if (voData.IdentityTypeID == "") { valid = 0; msg += "Identity TypeID is required" + "\r\n"; }
    if (voData.IdentityNo == "") { valid = 0; msg += "Identit yNo is required" + "\r\n"; }
    if (voData.Address == "") { valid = 0; msg += "Address is required" + "\r\n"; }
    if (voData.Country == "") { valid = 0; msg += "Country is required" + "\r\n"; }
    if (voData.State == "") { valid = 0; msg += "State is required" + "\r\n"; }
    if (voData.City == "") { valid = 0; msg += "City is required" + "\r\n"; }
    if (voData.PostalCode == "") { valid = 0; msg += "Postal Code is required" + "\r\n"; }
    if (voData.Telephone == "") { valid = 0; msg += "Telephone is required" + "\r\n"; }
    if (voData.EPC == "") { valid = 0; msg += "EPC is required" + "\r\n"; }
    if (voData.Barcode == "") { valid = 0; msg += "Barcode is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>
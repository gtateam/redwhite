
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataAccount_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataAccount_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataAccount_AccountID  => array(1,1,'150px','Account ID',1,'string',1),
    T_MasterDataAccount_AccountName  => array(1,1,'350px','Account Name',0,'string',0),
    T_MasterDataAccount_AccountType  => array(1,1,'200px','Account Type',0,'string',0),
    T_MasterDataAccount_CashBankAccount  => array(1,1,'200px','Cash Bank',0,'string',0)
);
// Column DropdownList => |Text|URL|a
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'Account',
    'table' => T_MasterDataAccount,
    'actBTN' => "130px",
    'postBTN' => "0",
    'tools' => array(T_MasterDataAccount_RecordID,T_MasterDataAccount_RecordTimestamp,T_MasterDataAccount_AccountID,T_MasterDataAccount_AccountName,T_MasterDataAccount_AccountType,T_MasterDataAccount_CashBankAccount),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Account/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Account/Update',
        'destroy' => 'Masterdata/Account/Delete',
        'form' => 'Masterdata/Account/Form',
        'post' => 'Masterdata/Account/Post',
        'unpost' => 'Masterdata/Account/UnPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>

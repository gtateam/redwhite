
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataUom_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataUom_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataUom_ID  => array(1,1,'50px','UOM ID',1,'string',1),
    T_MasterDataUom_Name  => array(1,1,'100px','UOM Name',0,'string',0),
    T_MasterDataUom_Description => array(1,1,'100px','UOM Description',0,'string',0)
);
// Column DropdownList => |Text|URL|a
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'UOM',
    'table' => T_MasterDataUom,
    'tools' => array(T_MasterDataUom_RecordID,T_MasterDataUom_RecordTimestamp,T_MasterDataUom_ID,T_MasterDataUom_Name),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Uom/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Uom/Update',
        'destroy' => 'Masterdata/Uom/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>
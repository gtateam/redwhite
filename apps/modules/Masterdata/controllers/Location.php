<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Location Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Location extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Location_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Location/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Location_model->getDetail($id);
		$this->modules->render('/Location/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Location_model->getDetail($id);
		$this->modules->render('/Location/formDetail', $data);
	}

	public function getList()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataLocation;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'f001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Location_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Location_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
	
	public function getLocation()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataLocation;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'f001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Location_model->getLocation($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Location_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_MasterDataLocation_LocationID   => $this->input->post("ID"),
					T_MasterDataLocation_LocationName   => $this->input->post("Name")
				);
				$this->Location_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_MasterDataLocation_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_MasterDataLocation_RecordID   => $this->input->post("RecordID"),
					T_MasterDataLocation_LocationID   => $this->input->post("ID"),
					T_MasterDataLocation_LocationName   => $this->input->post("Name")
				);

				$this->Location_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Location_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Location.php */
/* Location: ./app/modules/MasterData/controllers/Location.php */

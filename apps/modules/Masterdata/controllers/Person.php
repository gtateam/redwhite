<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Item Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Person extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Person_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Person/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Person_model->getDetail($id);
		$this->modules->render('/Person/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Person_model->getDetail($id);
		$this->modules->render('/Person/formDetail', $data);
	}

	public function getList()
    {
       $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataperson;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Person_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Person_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->data = null;
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

		public function upload()
	{
		$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
		$path = 'assets/upload/photo/'; // upload directory

		if(isset($_FILES['image']))
		{
			$img = $_FILES['image']['name'];
			$tmp = $_FILES['image']['tmp_name'];
				
			// get uploaded file's extension
			$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
			
			// can upload same image using rand function
			$final_image = rand(1000,1000000).".".$ext;
			
			// check's valid format
			if(in_array($ext, $valid_extensions)) 
			{					
				$path = $path.strtolower($final_image);	
					
				if(move_uploaded_file($tmp,$path)) 
				{
					$output['res'] = "<img class='img-responsive' src='$path' width='145px'/>";
					$output['img'] = $path; 
				}
			} 
			else 
			{
				$output['res'] = 'invalid';
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				if($this->input->post('Picture') != NULL){
				$data = array(
						T_MasterDataperson_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_MasterDataperson_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_MasterDataperson_UpdatedAt  => $this->input->ip_address(),
						T_MasterDataperson_RecordStatus => '0',
						T_MasterDataperson_RecordFlag => '0',
						T_MasterDataperson_PersonID => $this->input->post("PersonID"),
						T_MasterDataperson_PersonName => $this->input->post("PersonName"),
						T_MasterDataperson_PersonTypeID => $this->input->post("PersonTypeID"),
						T_MasterDataperson_IdentityTypeID => $this->input->post("IdentityTypeID"),
						T_MasterDataperson_IdentityNo => $this->input->post("IdentityNo"),
						T_MasterDataperson_Picture => $this->input->post("Picture"),
						T_MasterDataperson_Address => $this->input->post("Address"),
						T_MasterDataperson_Country => $this->input->post("Country"),
						T_MasterDataperson_State => $this->input->post("State"),
						T_MasterDataperson_City => $this->input->post("City"),
						T_MasterDataperson_PostalCode => $this->input->post("PostalCode"),
						T_MasterDataperson_Telephone => $this->input->post("Telephone"),
						T_MasterDataperson_EPC => $this->input->post("EPC"),
						T_MasterDataperson_Barcode => $this->input->post("Barcode"),
						T_MasterDataperson_PersonTypeName => $this->input->post("PersonTypeName"),
						T_MasterDataperson_IdentityTypeName => $this->input->post("IdentityTypeName"),
				);
					}else{
						$data = array(
						T_MasterDataperson_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_MasterDataperson_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_MasterDataperson_UpdatedAt  => $this->input->ip_address(),
						T_MasterDataperson_RecordStatus => '0',
						T_MasterDataperson_RecordFlag => '0',
						T_MasterDataperson_PersonID => $this->input->post("PersonID"),
						T_MasterDataperson_PersonName => $this->input->post("PersonName"),
						T_MasterDataperson_PersonTypeID => $this->input->post("PersonTypeID"),
						T_MasterDataperson_IdentityTypeID => $this->input->post("IdentityTypeID"),
						T_MasterDataperson_IdentityNo => $this->input->post("IdentityNo"),
						T_MasterDataperson_Picture => 'assets/upload/photo/no-photo.png',
						T_MasterDataperson_Address => $this->input->post("Address"),
						T_MasterDataperson_Country => $this->input->post("Country"),
						T_MasterDataperson_State => $this->input->post("State"),
						T_MasterDataperson_City => $this->input->post("City"),
						T_MasterDataperson_PostalCode => $this->input->post("PostalCode"),
						T_MasterDataperson_Telephone => $this->input->post("Telephone"),
						T_MasterDataperson_EPC => $this->input->post("EPC"),
						T_MasterDataperson_Barcode => $this->input->post("Barcode"),
						T_MasterDataperson_PersonTypeName => $this->input->post("PersonTypeName"),
						T_MasterDataperson_IdentityTypeName => $this->input->post("IdentityTypeName"),
				);
					}
				$this->Person_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
					);

				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_MasterDataperson_RecordTimestamp, 'RecordTimeStamp') == TRUE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				//$validImage = $this->input->post('Picture');
				if($this->input->post('Picture') != NULL){
					$data = array(
						T_MasterDataperson_RecordID => $this->input->post("RecordID"),
						T_MasterDataperson_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_MasterDataperson_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_MasterDataperson_UpdatedAt  => $this->input->ip_address(),
						T_MasterDataperson_RecordStatus => '0',
						T_MasterDataperson_RecordFlag => '0',
						//T_MasterDataperson_PersonID => $this->input->post("PersonID"),
						T_MasterDataperson_PersonName => $this->input->post("PersonName"),
						T_MasterDataperson_PersonTypeID => $this->input->post("PersonTypeID"),
						T_MasterDataperson_IdentityTypeID => $this->input->post("IdentityTypeID"),
						T_MasterDataperson_IdentityNo => $this->input->post("IdentityNo"),
						T_MasterDataperson_Picture => $this->input->post("Picture"),
						T_MasterDataperson_Address => $this->input->post("Address"),
						T_MasterDataperson_Country => $this->input->post("Country"),
						T_MasterDataperson_State => $this->input->post("State"),	
						T_MasterDataperson_City => $this->input->post("City"),
						T_MasterDataperson_PostalCode => $this->input->post("PostalCode"),
						T_MasterDataperson_Telephone => $this->input->post("Telephone"),
						T_MasterDataperson_EPC => $this->input->post("EPC"),
						T_MasterDataperson_Barcode => $this->input->post("Barcode"),
						T_MasterDataperson_PersonTypeName => $this->input->post("PersonTypeName"),
						T_MasterDataperson_IdentityTypeName => $this->input->post("IdentityTypeName"),
						
					);
				}else{
						$data = array(
						T_MasterDataperson_RecordID => $this->input->post("RecordID"),
						T_MasterDataperson_RecordTimestamp => date("Y-m-d g:i:s",now()),
						T_MasterDataperson_UpdatedBy  => $this->ezrbac->getCurrentUserID(),
						T_MasterDataperson_UpdatedAt  => $this->input->ip_address(),
						T_MasterDataperson_RecordStatus => '0',
						T_MasterDataperson_RecordFlag => '0',
						//T_MasterDataperson_PersonID => $this->input->post("PersonID"),
						T_MasterDataperson_PersonName => $this->input->post("PersonName"),
						T_MasterDataperson_PersonTypeID => $this->input->post("PersonTypeID"),
						T_MasterDataperson_IdentityTypeID => $this->input->post("IdentityTypeID"),
						T_MasterDataperson_IdentityNo => $this->input->post("IdentityNo"),
						T_MasterDataperson_Picture => $this->input->post("PictureExist"),
						T_MasterDataperson_Address => $this->input->post("Address"),
						T_MasterDataperson_Country => $this->input->post("Country"),
						T_MasterDataperson_State => $this->input->post("State"),	
						T_MasterDataperson_City => $this->input->post("City"),
						T_MasterDataperson_PostalCode => $this->input->post("PostalCode"),
						T_MasterDataperson_Telephone => $this->input->post("Telephone"),
						T_MasterDataperson_EPC => $this->input->post("EPC"),
						T_MasterDataperson_Barcode => $this->input->post("Barcode"),
						T_MasterDataperson_PersonTypeName => $this->input->post("PersonTypeName"),
						T_MasterDataperson_IdentityTypeName => $this->input->post("IdentityTypeName"),
					);
				}

				$this->Person_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Person_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetCountry($id="")
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$GroupCode = !empty($id) ? $id : $this->input->post('GroupCode');
		$getList = $this->Person_model->CountryDropdownList($GroupCode);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	
}

/* End of file Item.php */
/* Location: ./app/modules/MasterData/controllers/Item.php */

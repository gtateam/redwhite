
$(document).ready(function (e) {
	
	$("#form-photo").on('submit',(function(e) {
		e.preventDefault();
		var val =$('#ItemID').val();
		if(val==null){
			val = $('#ItemGroupID').val();
		}
		$("#ItemIDS").val(val);
		var voData = new FormData(this);
		console.log(voData);
		$.ajax({
        	url: "en/System/User/upload",
			type: "POST",
			data:  voData,
			contentType: false,
    	    cache: false,
			processData:false,
			beforeSend : function()
			{
				//$("#preview").fadeOut();
				$("#err").fadeOut();
			},
			success: function(data)
		    {
				if (data.res == "invalid"){
					new PNotify({ title: "Failed", text: "No Selected File", type: 'error', shadow: true });
				}
				if(data == 'invalid')
				{
					// invalid file format.
					$("#err").html("Invalid File !").fadeIn();
				}
				else
				{
					// view uploaded file.
					$("#preview").html(data.res).fadeIn();
					document.getElementById("thumbnail-photo").href=data.img; 
					$("#photo").val(data.img);
					$("#form-photo")[0].reset();	
					if(data.res != "invalid"){
						new PNotify({ title: "Success", text: "Upload Success", type: 'success', shadow: true });
					}
					
				}
		    },
		  	error: function(e) 
	    	{
				$("#err").html(e).fadeIn();
	    	} 	        
	   });
	}));
});


var Connection2 = (function () {

    function Connection2(url) {

        this.open = false;

        this.socket = new WebSocket("ws://" + url);
        this.setupConnectionEvents();
    }

    Connection2.prototype = {
        setupConnectionEvents: function () {
            var self = this;

            self.socket.onopen = function (evt) {
                self.connectionOpen(evt);
            };
            self.socket.onerror = function (evt) {
                self.connectionError(evt);
            };
            self.socket.onmessage = function (evt) {
                self.connectionMessage(evt);
            };
            self.socket.onclose = function (evt) {
                self.connectionClose(evt);
            };
        },

        connectionOpen: function (evt) {
            this.open = true;
            this.addSystemMessage("Connected");
        },

        connectionError: function (evt) {
            //console.log(evt.target.url);
            new PNotify({
                title: 'SYSTEM',
                text: 'WebSocket not started on ' + evt.target.url,
                addclass: 'notification-danger',
                icon: 'fa fa-info',
                hide: false,
                buttons: {
                    sticker: false
                }
            });
        },
        connectionMessage: function (evt) {
            //console.log(evt);
            var data = JSON.parse(evt.data);

            this.addChatMessage(data.msg);
        },
        connectionClose: function (evt) {
            this.open = false;
            this.addSystemMessage("Disconnected");
        },

        sendMsg: function (message) {

            this.socket.send(JSON.stringify({
                msg: message
            }));
        },

        addChatMessage: function (data) {
            console.log(data.broadType);
            switch (data.broadType) {
                case Broadcast.POST:
                    this.addNewPost(data);
                    break;
                case "Notif":
                    this.pushNotification(data);
                    break;
                case "dataPOS":
                    this.ItemPOS(data);
                    break;
                case "ReadTag":
                    this.ReadTag(data);
                    break;
                default:
                    console.log("nothing to do");
            }
        },

        addNewPost: function (data) {

            var newPost = data.data;

            newHtml = "<div>" +
                "<span> " + newPost.postText + "</span>" +
                "</div>";

            $("#messages").prepend(newHtml);
        },

        pushNotification: function (data) {
            if (current_controller() == "Stockout") {
                if (data.data.UserLogin != UserLogin) {
                    new PNotify({
                        title: data.data.Title,
                        text: data.data.Text,
                        addclass: 'notification-primary',
                        icon: 'fa fa-info'
                    });
                    // PNotify.desktop.permission();
                    // (new PNotify({
                    //     title: 'Notifikasi',
                    //     text: data.data.Type + ' oleh \n' + data.data.UserLogin + " ( " + data.data.Branch + " ) \n" + data.data.Object,
                    //     hide: true,
                    //     desktop: {
                    //         desktop: true,
                    //         icon: 'assets/upload/photo/460989.png'
                    //     }
                    // })).get().click(function () {
                    //     alert('Hey! You clicked the desktop notification!');
                    // });
                    //updatenotif();
                }
            }

        },
        ItemPOS: function(data){
            console.log(data);
            if (current_controller() == "Movement" || current_controller() == "") {
                new PNotify({
                    title: data.data.Title,
                    text: data.data.Text,
                    addclass: 'notification-primary',
                    icon: 'fa fa-info'
                });
                getdataScanSocket(data.data.Text);
            }
        },
        ReadTag: function (data) {
            setToForm(data.data.Text);
        },
        addSystemMessage: function (msg) {
            // this.chatwindow.innerHTML += "<p>" + msg + "</p>";
        }
    };

    return Connection2;

})();

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CI_Phpmailer{
 
    public function phpmailer()
    {
        $CI = & get_instance();
        log_message('Debug', 'phpmailer class is loaded.');
    }
 
    public function load()
    {
        include_once APPPATH.'third_party\PHPMailer\src\Exception.php';
        include_once APPPATH.'third_party\PHPMailer\src\PHPMailer.php';
        include_once APPPATH.'third_party\PHPMailer\src\SMTP.php';
        print_out($this);
        //$mail = new PHPMailer(true); 
        return  APPPATH.'third_party\PHPMailer\src\PHPMailer.php';
    }
}